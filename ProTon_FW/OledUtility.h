#ifndef OLED_UTILITY
#define OLED_UTILITY

#include <Arduino.h>
#include "Model.h"

//public:
void oledInit();
void oledU8g2Test(void);
void oledPrintList(listItem* list,int itemCount, int selectedItemIndex);
void oledPrintNormalMode(double dBVoiceVolum,String* usrOpt);
void oledBleConnectNotifier(bool state);
void oledBLegreetingUser(String UsrName);
void oledBleChangeVoiceVolume(double Vol,int algo,String Intinsity,bool mute,bool Bass,bool Comp);
void oledBleHearingTestNotifier(int Dir ,int freqInx,double volValue);
void oledStartUp(void);
void setContrast(ContrastValues newContrastValue);
bool printSplachScreen(void);
void setWritingFontSize(FontSize newSize);
void printEditModeMenu(String* editMenuItems, String* currentUsrOpt);
void showSelectedItem(String header,String item);
void showSelectedLevel(String header,String budy);
void oledShowFreq(String dir,float freq, float hearingTestvolumedB);
void myPerfectAudio(String SS);
void OledPrintLogo();
void oledTestBuff1(void);
void oledTestBuff2(void);
//private:
void updateNotificationBar(String *currentUsrOpt);
void _oledPrintListSmlFnt(listItem* list,int itemCount, int selectedItemIndex);
void _oledPrintListMidFnt(listItem* list,int itemCount, int selectedItemIndex);
void _oledPrintListLrgFnt(listItem* list,int itemCount, int selectedItemIndex);
void _writeClice(void);
char* string2char(String command);
void StringPtrToStringArray(String* src, String des[]);
#endif // OLED_UTILITY