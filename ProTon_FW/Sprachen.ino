/*
 * Sprachen.ino
 * Version 0.3.0.2
 */
#include <Arduino.h>

char En_menuItem_1  []= "user profile";
char En_menuItem_1_1  []= "load profile";
char En_menuItem_1_1_1  []= "memory 1";
char En_menuItem_1_1_2  []= "memory 2";
char En_menuItem_1_1_3  []= "memory 3";
char En_menuItem_1_2  []= "new profile";

char En_menuItem_1_3  []= "delete profile";
char En_menuItem_1_3_1  []= "memory 1";
char En_menuItem_1_3_2  []= "memory 2";
char En_menuItem_1_3_3  []= "memory 3";

char En_menuItem_2  []= "operating mode";
char En_menuItem_2_1  []= "linear";
char En_menuItem_2_2  []= "personalized";
char En_menuItem_2_2_1  []= "hearing profile 1";
char En_menuItem_2_2_2  []= "hearing profile 2";
char En_menuItem_2_2_3  []= "hearing profile 3";

char En_menuItem_3  []= "sound settings";
char En_menuItem_3_1  []= "audio presets";
char En_menuItem_3_1_1  []= "EQ";
char En_menuItem_3_1_1_1  []= "Jazz";
char En_menuItem_3_1_1_2  []= "Rock";
char En_menuItem_3_1_1_3  []= "Flat";
char En_menuItem_3_1_2  []= "dynamic bass";
char En_menuItem_3_1_2_1  []= "on";
char En_menuItem_3_1_2_2  []= "off";
char En_menuItem_3_2  []= "compressor";

char En_menuItem_4  []= "hearing test";
char En_menuItem_4_1  []= "start hearing test";
char En_menuItem_4_2  []= "noise generator";
char En_menuItem_4_2_1  []= "on";
char En_menuItem_4_2_2  []= "off";
char En_menuItem_4_2_3  []= "automatic";
char En_menuItem_4_3  []= "start Octav test";
char En_menuItem_4_4  []= "start Terz test";

char En_menuItem_5  []= "settings";
char En_menuItem_5_1  []= "general";
char En_menuItem_5_1_1  []= "hearing test";
char En_menuItem_5_1_1_1  []= "fast";
char En_menuItem_5_1_1_2  []= "according to standard";
char En_menuItem_5_1_1_3  []= "exactly";
char En_menuItem_5_1_2  []= "max. volume";
char En_menuItem_5_1_2_1  []= "volume max. in dB";
char En_menuItem_5_1_3  []= "initial volume";
char En_menuItem_5_1_3_1  []= "minimal";
char En_menuItem_5_1_3_2  []= "default";
char En_menuItem_5_1_3_3  []= "since last start";
char En_menuItem_5_2  []= "expert mode";
char En_menuItem_5_2_1  []= "step size";
char En_menuItem_5_2_1_1  []= "step in dB";
char En_menuItem_5_2_2  []= "dynamic bass";
char En_menuItem_5_2_2_1  []= "on";
char En_menuItem_5_2_2_2  []= "off";
char En_menuItem_5_2_3  []= "noise generator";
char En_menuItem_5_2_3_1  []= "on";
char En_menuItem_5_2_3_2  []= "off";
char En_menuItem_5_2_4  []= "language";
char En_menuItem_5_2_4_1  []= "german";
char En_menuItem_5_2_4_2  []= "english";
char En_menuItem_5_2_5  []= "information";
char En_menuItem_5_2_5_1  []= "version";
char En_menuItem_5_2_5_2  []= "serial number";
char En_menuItem_5_2_5_3  []= "support";
char En_menuItem_6 []= "HearingTest start";
char En_menuItem_7 [] = "MultiUser Input";
char En_menuItem_8 [] = "SingleUser Input";
char En_menuItem_9 [] = "Intensity";
char En_menuItem_9_1[] = "20%";
char En_menuItem_9_2[] = "40%";
char En_menuItem_9_3[] = "60%";
char En_menuItem_9_4[] = "80%";
char En_menuItem_9_5[] = "100%";
char En_menuItem_10 []= "Soundselect";
char En_menuItem_10_1 [] = "Sound 1";
char En_menuItem_10_2 [] = "Sound 2";
char En_menuItem_10_3 [] = "Sound 3";
char En_menuItem_10_4 [] = "Sound 4";

///////////////////////////////////////////////////////
char De_menuItem_1 []=  "Benutzerprofil";
char De_menuItem_1_1 []=  "Profil laden";
char De_menuItem_1_1_1 []=  "Speicher 1";
char De_menuItem_1_1_2 []=  "Speicher 2";
char De_menuItem_1_1_3 []=  "Speicher 3";
char De_menuItem_1_2 []=  "Neues Profil";

char De_menuItem_1_3 []=  "Profil loeschen";
char De_menuItem_1_3_1 []=  "Speicher 1";
char De_menuItem_1_3_2 []=  "Speicher 2";
char De_menuItem_1_3_3 []=  "Speicher 3";

char De_menuItem_2 []=  "Betriebsmodi";
char De_menuItem_2_1 []=  "direkt";
char De_menuItem_2_2 []=  "Personalisiert";
char De_menuItem_2_2_1 []=  "Profil 1";
char De_menuItem_2_2_2 []=  "Profil 2";
char De_menuItem_2_2_3 []=  "Profil 3";

char De_menuItem_3 []=  "Audio";
char De_menuItem_3_1 []=  "Audio Presets";
char De_menuItem_3_1_1 []=  "EQ";
char De_menuItem_3_1_1_1 []=  "Jazz";
char De_menuItem_3_1_1_2 []=  "Rock";
char De_menuItem_3_1_1_3 []=  "Flat";
char De_menuItem_3_1_2 []=  "Dyn. Bass Boost";
char De_menuItem_3_1_2_1 []=  "An";
char De_menuItem_3_1_2_2 []=  "Aus";
char De_menuItem_3_2 []=  "Kompressor";

char De_menuItem_4 []=  "Hoertest";
char De_menuItem_4_1 []=  "Auto Messung st";
char De_menuItem_4_2 []=  "Rauschgenerator";
char De_menuItem_4_2_1 []=  "An";
char De_menuItem_4_2_2 []=  "Aus";
char De_menuItem_4_2_3 []=  "Automatik";
char De_menuItem_4_3 []=  "Manu Octav st";
char De_menuItem_4_4 []=  "Manu Terz st";

char De_menuItem_5 []=  "Einstellungen";
char De_menuItem_5_1 []=  "Generell";
char De_menuItem_5_1_1 []=  "Hoertest";
char De_menuItem_5_1_1_1 []=  "schnell";
char De_menuItem_5_1_1_2 []=  "nach DIN Norm";
char De_menuItem_5_1_1_3 []=  "genau";
char De_menuItem_5_1_2 []=  "max. Volume";
char De_menuItem_5_1_2_1 []=  "Volume max. in dB";
char De_menuItem_5_1_3 []=  "Init Volume";
char De_menuItem_5_1_3_1 []=  "minimal";
char De_menuItem_5_1_3_2 []=  "Standard";
char De_menuItem_5_1_3_3 []=  "nach letztem Start";
char De_menuItem_5_2 []=  "Expertenmodus";
char De_menuItem_5_2_1 []=  "Schrittweite";
char De_menuItem_5_2_1_1 []=  "Schrittweite in dB";
char De_menuItem_5_2_2 []=  "Dyn. Bass Boost";
char De_menuItem_5_2_2_1 []=  "An";
char De_menuItem_5_2_2_2 []=  "Aus";
char De_menuItem_5_2_3 []=  "Rauschgenerator";
char De_menuItem_5_2_3_1 []=  "An";
char De_menuItem_5_2_3_2 []=  "Aus";
char De_menuItem_5_2_4 []=  "Sprache";
char De_menuItem_5_2_4_1 []=  "Deutsch";
char De_menuItem_5_2_4_2 []=  "Englisch";
char De_menuItem_5_2_5 []=  "Information";
char De_menuItem_5_2_5_1 []=  "Versionsnummer";
char De_menuItem_5_2_5_2 []=  "Seriennummer";
char De_menuItem_5_2_5_3 []=  "Support";
char De_menuItem_6 [] = "Messung starten";
char De_menuItem_7 [] = "MultiUser Input";
char De_menuItem_8 [] = "SingleUser Input";
char De_menuItem_9 [] = "Intensitaet";
char De_menuItem_9_1[] = "20%";
char De_menuItem_9_2[] = "40%";
char De_menuItem_9_3[] = "60%";
char De_menuItem_9_4[] = "80%";
char De_menuItem_9_5[] = "100%";
char De_menuItem_10 []= "Soundwahl";
char De_menuItem_10_1 [] = "Sound 1";
char De_menuItem_10_2 [] = "Sound 2";
char De_menuItem_10_3 [] = "Sound 3";
char De_menuItem_10_4 [] = "Sound 4";


char menuItem_1[] = "Benutzer";
char menuItem_1_1 [] = "Profil laden";
char menuItem_1_1_1 [] = "Speicher 1";
char menuItem_1_1_2 [] = "Speicher 2";
char menuItem_1_1_3 [] = "Speicher 3";
char menuItem_1_2 [] = "Neues Profil";

char menuItem_1_3 [] = "Profil loeschen";
char menuItem_1_3_1 [] = "Speicher 1";
char menuItem_1_3_2 [] = "Speicher 2";
char menuItem_1_3_3 [] = "Speicher 3";

char menuItem_2 [] = "Sound";
char menuItem_2_1 [] = "direkt";
char menuItem_2_2 [] = "Personalisiert";
char menuItem_2_2_1 [] = "Profil 1";
char menuItem_2_2_2 [] = "Profil 2";
char menuItem_2_2_3 [] = "Profil 3";

char menuItem_3 [] = "Audio";
char menuItem_3_1 [] = "Audio Presets";
char menuItem_3_1_1 [] = "EQ";
char menuItem_3_1_1_1 [] = "Jazz";
char menuItem_3_1_1_2 [] = "Rock";
char menuItem_3_1_1_3 [] = "Flat";
char menuItem_3_1_2 [] = "Dyn. Bass Boost";
char menuItem_3_1_2_1 [] = "An";
char menuItem_3_1_2_2 [] = "Aus";
char menuItem_3_2 [] = "Kompressor";
char menuItem_3_2_1 [] = "An";
char menuItem_3_2_2 [] = "Aus";

char menuItem_4 [] = "Hoertest";
char menuItem_4_1 [] = "Auto Mess st";
char menuItem_4_2 [] = "Rauschgen.";
char menuItem_4_2_1 [] = "An";
char menuItem_4_2_2 [] = "Aus";
char menuItem_4_2_3 [] = "Automatik";
char menuItem_4_3 [] = "Manu Octav st";
char menuItem_4_4 [] = "Manu Terz st";

char menuItem_5 [] = "Einstellungen";
char menuItem_5_1 [] = "Generell";
char menuItem_5_1_1 [] = "Hoertest";
char menuItem_5_1_1_1 [] = "schnell";
char menuItem_5_1_1_2 [] = "nach DIN Norm";
char menuItem_5_1_1_3 [] = "genau";
char menuItem_5_1_2 [] = "max. Vol.";
char menuItem_5_1_2_1 [] = "Vol. max. dB";
char menuItem_5_1_3 [] = "Init Vol.";
char menuItem_5_1_3_1 [] = "minimal";
char menuItem_5_1_3_2 [] = "Standard";
char menuItem_5_1_3_3 [] = "NLS";  // nach letztem Start
char menuItem_5_2 [] = "Expertenmodus";
char menuItem_5_2_1 [] = "Step Messung";
char menuItem_5_2_1_1 [] = "Step in dB";
char menuItem_5_2_2 [] = "Dyn. Bass Boost";
char menuItem_5_2_2_1 [] = "An";
char menuItem_5_2_2_2 [] = "Aus";
char menuItem_5_2_3 [] = "Rauschgen.";
char menuItem_5_2_3_1 [] = "An";
char menuItem_5_2_3_2 [] = "Aus";
char menuItem_5_2_4 [] = "Sprache";
char menuItem_5_2_4_1 [] = "Deutsch";
char menuItem_5_2_4_2 [] = "English";
char menuItem_5_2_5 [] = "Information";
char menuItem_5_2_5_1 [] = "Versionsnummer";
char menuItem_5_2_5_2 [] = "Seriennummer";
char menuItem_5_2_5_3 [] = "Support";
char menuItem_6 [] = "Hoertest start";
char menuItem_7 [] = "MultiUser Input";
char menuItem_8 [] = "SingleUser Input";
char menuItem_9 [] = "Intensitaet";
char menuItem_9_1[] = "20%";
char menuItem_9_2[] = "40%";
char menuItem_9_3[] = "60%";
char menuItem_9_4[] = "80%";
char menuItem_9_5[] = "100%";
char menuItem_10 []= "SoundSelect";
char menuItem_10_1 [] = "Sound 1";
char menuItem_10_2 [] = "Sound 2";
char menuItem_10_3 [] = "Sound 3";
char menuItem_10_4 [] = "Sound 4";




void LanguageSelect()
{
    switch (SprachAuswahl)
    {
        case 0: 
        default:
            peakDeutsch();
        break;
        case 1:
            peakEnglish();
        break;
    }
}

void peakDeutsch()
{
    Serial.println("peakDeutsch start..");
    //TODO: 
    strcpy(menuItem_1 , De_menuItem_1) ;
    strcpy(menuItem_1_1 , De_menuItem_1_1 );
    strcpy(menuItem_1_1_1 , De_menuItem_1_1_1 );
    strcpy(menuItem_1_1_2 , De_menuItem_1_1_2 );
    strcpy(menuItem_1_1_3 , De_menuItem_1_1_3 );
    strcpy(menuItem_1_2 , De_menuItem_1_2 );

    strcpy(menuItem_1_3 , De_menuItem_1_3 );
    strcpy(menuItem_1_3_1 , De_menuItem_1_3_1 );
    strcpy(menuItem_1_3_2 , De_menuItem_1_3_2 );
    strcpy(menuItem_1_3_3 , De_menuItem_1_3_3 );

    strcpy(menuItem_2 , De_menuItem_2 );
    strcpy(menuItem_2_1 , De_menuItem_2_1 );
    strcpy(menuItem_2_2 , De_menuItem_2_2 );
    strcpy(menuItem_2_2_1 , De_menuItem_2_2_1 );
    strcpy(menuItem_2_2_2 , De_menuItem_2_2_2 );
    strcpy(menuItem_2_2_3 , De_menuItem_2_2_3 );

    strcpy(menuItem_3 , De_menuItem_3 );
    strcpy(menuItem_3_1 , De_menuItem_3_1 );
    strcpy(menuItem_3_1_1 , De_menuItem_3_1_1 );
    strcpy(menuItem_3_1_1_1 , De_menuItem_3_1_1_1 );
    strcpy(menuItem_3_1_1_2 , De_menuItem_3_1_1_2 );
    strcpy(menuItem_3_1_1_3 , De_menuItem_3_1_1_3 );
    strcpy(menuItem_3_1_2 , De_menuItem_3_1_2 );
    strcpy(menuItem_3_1_2_1 , De_menuItem_3_1_2_1 );
    strcpy(menuItem_3_1_2_2 , De_menuItem_3_1_2_2 );
    strcpy(menuItem_3_2 , De_menuItem_3_2 );
    strcpy(menuItem_3_2_1 , " " );
    strcpy(menuItem_3_2_2 , " " );

    strcpy(menuItem_4 , De_menuItem_4 );
    strcpy(menuItem_4_1 , De_menuItem_4_1 );
    strcpy(menuItem_4_2 , De_menuItem_4_2 );
    strcpy(menuItem_4_2_1 , De_menuItem_4_2_1 );
    strcpy(menuItem_4_2_2 , De_menuItem_4_2_2 );
    strcpy(menuItem_4_2_3 , De_menuItem_4_2_3 );
    strcpy(menuItem_4_3 , De_menuItem_4_3 );
    strcpy(menuItem_4_4 , De_menuItem_4_4 );
    
    strcpy(menuItem_5 , De_menuItem_5 );
    strcpy(menuItem_5_1 , De_menuItem_5_1 );
    strcpy(menuItem_5_1_1 , De_menuItem_5_1_1 );
    strcpy(menuItem_5_1_1_1 , De_menuItem_5_1_1_1 );
    strcpy(menuItem_5_1_1_2 , De_menuItem_5_1_1_2 );
    strcpy(menuItem_5_1_1_3 , De_menuItem_5_1_1_3 );
    strcpy(menuItem_5_1_2 , De_menuItem_5_1_2 );
    strcpy(menuItem_5_1_2_1 , De_menuItem_5_1_2_1 );
    strcpy(menuItem_5_1_3 , De_menuItem_5_1_3 );
    strcpy(menuItem_5_1_3_1 , De_menuItem_5_1_3_1 );
    strcpy(menuItem_5_1_3_2 , De_menuItem_5_1_3_2 );
    strcpy(menuItem_5_1_3_3 , De_menuItem_5_1_3_3 );
    strcpy(menuItem_5_2 , De_menuItem_5_2 );
    strcpy(menuItem_5_2_1 , De_menuItem_5_2_1 );
    strcpy(menuItem_5_2_1_1 , De_menuItem_5_2_2_1);
    strcpy(menuItem_5_2_2 , De_menuItem_5_2_2 );
    strcpy(menuItem_5_2_2_1 , De_menuItem_5_2_2_1 );
    strcpy(menuItem_5_2_2_2 , De_menuItem_5_2_2_2 );
    strcpy(menuItem_5_2_3 , De_menuItem_5_2_3 );
    strcpy(menuItem_5_2_3_1 , De_menuItem_5_2_3_1 );
    strcpy(menuItem_5_2_3_2 , De_menuItem_5_2_3_2 );
    strcpy(menuItem_5_2_4 , De_menuItem_5_2_4 );
    strcpy(menuItem_5_2_4_1 , De_menuItem_5_2_4_1 );
    strcpy(menuItem_5_2_4_2 , De_menuItem_5_2_4_2 );
    strcpy(menuItem_5_2_5 , De_menuItem_5_2_5_1 );
    strcpy(menuItem_5_2_5_1 , De_menuItem_5_2_5_1 );
    strcpy(menuItem_5_2_5_2 , De_menuItem_5_2_5_2 );
    strcpy(menuItem_5_2_5_3 , De_menuItem_5_2_5_3 );
    strcpy(menuItem_6 , De_menuItem_6);
    strcpy(menuItem_7 , De_menuItem_7);
    strcpy(menuItem_8 , De_menuItem_8);
    strcpy(menuItem_9 , De_menuItem_9);
    strcpy(menuItem_9_1 , De_menuItem_9_1);
    strcpy(menuItem_9_2 , De_menuItem_9_2);
    strcpy(menuItem_9_3 , De_menuItem_9_3);
    strcpy(menuItem_9_4 , De_menuItem_9_4);
    strcpy(menuItem_9_5 , De_menuItem_9_5);
    strcpy(menuItem_10 , De_menuItem_10);
    strcpy(menuItem_10_1 , De_menuItem_10_1);
    strcpy(menuItem_10_2 , De_menuItem_10_2);
    strcpy(menuItem_10_3 , De_menuItem_10_3);
    strcpy(menuItem_10_4 , De_menuItem_10_4);

    
    Serial.println("peakDeutsch end..");
}


void peakEnglish()
{
    Serial.println("peakEnglish start..");
    //TODO:

strcpy(menuItem_1 , En_menuItem_1) ;
strcpy(menuItem_1_1 , En_menuItem_1_1 );
strcpy(menuItem_1_1_1 , En_menuItem_1_1_1 );
strcpy(menuItem_1_1_2 , En_menuItem_1_1_2 );
strcpy(menuItem_1_1_3 , En_menuItem_1_1_3 );
strcpy(menuItem_1_2 , En_menuItem_1_2 );

strcpy(menuItem_1_3 , En_menuItem_1_3 );
strcpy(menuItem_1_3_1 , En_menuItem_1_3_1 );
strcpy(menuItem_1_3_2 , En_menuItem_1_3_2 );
strcpy(menuItem_1_3_3 , En_menuItem_1_3_3 );

strcpy(menuItem_2 , En_menuItem_2 );
strcpy(menuItem_2_1 , En_menuItem_2_1 );
strcpy(menuItem_2_2 , En_menuItem_2_2 );
strcpy(menuItem_2_2_1 , En_menuItem_2_2_1 );
strcpy(menuItem_2_2_2 , En_menuItem_2_2_2 );
strcpy(menuItem_2_2_3 , En_menuItem_2_2_3 );

strcpy(menuItem_3 , En_menuItem_3 );
strcpy(menuItem_3_1 , En_menuItem_3_1 );
strcpy(menuItem_3_1_1 , En_menuItem_3_1_1 );
strcpy(menuItem_3_1_1_1 , En_menuItem_3_1_1_1 );
strcpy(menuItem_3_1_1_2 , En_menuItem_3_1_1_2 );
strcpy(menuItem_3_1_1_3 , En_menuItem_3_1_1_3 );
strcpy(menuItem_3_1_2 , En_menuItem_3_1_2 );
strcpy(menuItem_3_1_2_1 , En_menuItem_3_1_2_1 );
strcpy(menuItem_3_1_2_2 , En_menuItem_3_1_2_2 );
strcpy(menuItem_3_2 , En_menuItem_3_2 );
strcpy(menuItem_3_2_1 , " " );
strcpy(menuItem_3_2_2 , " " );

strcpy(menuItem_4 , En_menuItem_4 );
strcpy(menuItem_4_1 , En_menuItem_4_1 );
strcpy(menuItem_4_2 , En_menuItem_4_2 );
strcpy(menuItem_4_2_1 , En_menuItem_4_2_1 );
strcpy(menuItem_4_2_2 , En_menuItem_4_2_2 );
strcpy(menuItem_4_2_3 , En_menuItem_4_2_3 );
strcpy(menuItem_4_3 , En_menuItem_4_3 );
strcpy(menuItem_4_4 , En_menuItem_4_4 );

strcpy(menuItem_5 , En_menuItem_5 );
strcpy(menuItem_5_1 , En_menuItem_5_1 );
strcpy(menuItem_5_1_1 , En_menuItem_5_1_1 );
strcpy(menuItem_5_1_1_1 , En_menuItem_5_1_1_1 );
strcpy(menuItem_5_1_1_2 , En_menuItem_5_1_1_2 );
strcpy(menuItem_5_1_1_3 , En_menuItem_5_1_1_3 );
strcpy(menuItem_5_1_2 , En_menuItem_5_1_2 );
strcpy(menuItem_5_1_2_1 , En_menuItem_5_1_2_1 );
strcpy(menuItem_5_1_3 , En_menuItem_5_1_3 );
strcpy(menuItem_5_1_3_1 , En_menuItem_5_1_3_1 );
strcpy(menuItem_5_1_3_2 , En_menuItem_5_1_3_2 );
strcpy(menuItem_5_1_3_3 , En_menuItem_5_1_3_3 );
strcpy(menuItem_5_2 , En_menuItem_5_2 );
strcpy(menuItem_5_2_1 , En_menuItem_5_2_1 );
strcpy(menuItem_5_2_1_1 , En_menuItem_5_2_2_1);
strcpy(menuItem_5_2_2 , En_menuItem_5_2_2 );
strcpy(menuItem_5_2_2_1 , En_menuItem_5_2_2_1 );
strcpy(menuItem_5_2_2_2 , En_menuItem_5_2_2_2 );
strcpy(menuItem_5_2_3 , En_menuItem_5_2_3 );
strcpy(menuItem_5_2_3_1 , En_menuItem_5_2_3_1 );
strcpy(menuItem_5_2_3_2 , En_menuItem_5_2_3_2 );
strcpy(menuItem_5_2_4 , En_menuItem_5_2_4 );
strcpy(menuItem_5_2_4_1 , En_menuItem_5_2_4_1 );
strcpy(menuItem_5_2_4_2 , En_menuItem_5_2_4_2 );
strcpy(menuItem_5_2_5 , En_menuItem_5_2_5_1 );
strcpy(menuItem_5_2_5_1 , En_menuItem_5_2_5_1 );
strcpy(menuItem_5_2_5_2 , En_menuItem_5_2_5_2 );
strcpy(menuItem_5_2_5_3 , En_menuItem_5_2_5_3 );
strcpy(menuItem_6 , En_menuItem_6);
strcpy(menuItem_7 , En_menuItem_7);
strcpy(menuItem_8 , En_menuItem_8);
 strcpy(menuItem_9 , De_menuItem_9);
    strcpy(menuItem_9_1 , En_menuItem_9_1);
    strcpy(menuItem_9_2 , En_menuItem_9_2);
    strcpy(menuItem_9_3 , En_menuItem_9_3);
    strcpy(menuItem_9_4 , En_menuItem_9_4);
    strcpy(menuItem_9_5 , En_menuItem_9_5);
        strcpy(menuItem_10 , En_menuItem_10);
    strcpy(menuItem_10_1 , En_menuItem_10_1);
    strcpy(menuItem_10_2 , En_menuItem_10_2);
    strcpy(menuItem_10_3 , En_menuItem_10_3);
    strcpy(menuItem_10_4 , En_menuItem_10_4);

Serial.println("peakEnglish end..");
}
