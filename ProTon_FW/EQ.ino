#include <Arduino.h>

#include "AidaDSP.h"
#include "Define_adrress.h"

#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array


//equalizer_t EQsleft[31] ; 
equalizer_t EQleft20, EQleft25, EQleft31, EQleft40, EQleft50, EQleft63, EQleft80, EQleft100, EQleft125, EQleft160, 
            EQleft200, EQleft250, EQleft315, EQleft400, EQleft500, EQleft630, EQleft800, EQleft1000, EQleft1250, 
            EQleft1600, EQleft2000, EQleft2500, EQleft3150, EQleft4000, EQleft5000, EQleft6300, EQleft8000, EQleft10000, 
            EQleft12500, EQleft16000, EQleft20000;

//equalizer_t EQsright[31] ;
equalizer_t EQright20, EQright25, EQright31, EQright40, EQright50, EQright63, EQright80, EQright100, EQright125, EQright160, 
            EQright200, EQright250, EQright315, EQright400, EQright500, EQright630, EQright800, EQright1000, EQright1250, 
            EQright1600, EQright2000, EQright2500, EQright3150, EQright4000, EQright5000, EQright6300, EQright8000, EQright10000, 
            EQright12500, EQright16000, EQright20000;


extern float hearingTestSWAP[3][31];
extern float saveIteration1[3][31];
extern float saveIteration2[3][31];
extern float f_count[31];
extern bool fineState;
//extern float saveIteration3[3][9];

void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void MainDynamicBassBypassON(void);

void EqConfigurations()            //it's name was  ProfileAndi()
{
    if(!fineState )    //9 Filter
    {       
      setEqParametersOktave();
    }
    else              //31 Filter
    {
      setEqParametersTerZ();  
    }
    
    switch (preset)               //  preset to switch between the Algorithms..
    {
        case 1:                    //Algorithms 1
        Serial.println("erste Iteration!");
        
        saveIteration1InEqBoost();

        sendEqDataToBoard();

        printIteration(3,31,saveIteration1[0] );
        
        break;

    case 2:                     //Algorithms 2
        Serial.println("zweite Iteration!");
        //////////////////////////////thies Funktions has not yet been created///////////////////////////
        saveIteration2InEqBoost();

        sendEqDataToBoard();

        printIteration(3,31,saveIteration2[0] );
    }
}

    void printIteration(int m,int n ,float *Iteration )
    {
        for (int i = 0; i <= m; i++) //Zeilenweise
        {
            for (int j = 0; j <= n; j++) //Spaltenweise
            {
                Serial.print(Iteration[i * n + j]);
                Serial.print("\t");
            }
            Serial.println(" ");
        }
    }

    void setEqParametersOktave()
    {
          //in each line we configure one frequence..
          
        //Filters for the left side
        float Guete = 1.41;
        EQleft20.Q = Guete;     EQleft20.boost = 0.00;       EQleft20.f0 = f_count[0];       EQleft20.type = Parametric;       EQleft20.phase = true;       EQleft20.onoff = false;
        EQleft25.Q = Guete;     EQleft25.boost = 0.00;       EQleft25.f0 = f_count[1];       EQleft25.type = Parametric;       EQleft25.phase = true;       EQleft25.onoff = false;
        EQleft31.Q = Guete;     EQleft31.boost = 0.00;       EQleft31.f0 = f_count[2];       EQleft31.type = Parametric;       EQleft31.phase = true;       EQleft31.onoff = false;
        EQleft40.Q = Guete;     EQleft40.boost = 0.00;       EQleft40.f0 = f_count[3];       EQleft40.type = Parametric;       EQleft40.phase = true;       EQleft40.onoff = false;
        EQleft50.Q = Guete;     EQleft50.boost = 0.00;       EQleft50.f0 = f_count[4];       EQleft50.type = Parametric;       EQleft50.phase = true;       EQleft50.onoff = false;
        EQleft63.Q = Guete;     EQleft63.boost = 0.00;       EQleft63.f0 = f_count[5];       EQleft63.type = Parametric;       EQleft63.phase = true;       EQleft63.onoff = true;
        EQleft80.Q = Guete;     EQleft80.boost = 0.00;       EQleft80.f0 = f_count[6];       EQleft80.type = Parametric;       EQleft80.phase = true;       EQleft80.onoff = false;
        EQleft100.Q = Guete;    EQleft100.boost = 0.00;      EQleft100.f0 = f_count[7];      EQleft100.type = Parametric;      EQleft100.phase = true;      EQleft100.onoff = false;
        EQleft125.Q = Guete;    EQleft125.boost = 0.00;      EQleft125.f0 = f_count[8];      EQleft125.type = Parametric;      EQleft125.phase = true;      EQleft125.onoff = true;
        EQleft160.Q = Guete;    EQleft160.boost = 0.00;      EQleft160.f0 = f_count[9];      EQleft160.type = Parametric;      EQleft160.phase = true;      EQleft160.onoff = false;
        EQleft200.Q = Guete;    EQleft200.boost = 0.00;      EQleft200.f0 = f_count[10];     EQleft200.type = Parametric;      EQleft200.phase = true;      EQleft200.onoff = false;
        EQleft250.Q = Guete;    EQleft250.boost = 0.00;      EQleft250.f0 = f_count[11];     EQleft250.type = Parametric;      EQleft250.phase = true;      EQleft250.onoff = true;
        EQleft315.Q = Guete;    EQleft315.boost = 0.00;      EQleft315.f0 = f_count[12];     EQleft315.type = Parametric;      EQleft315.phase = true;      EQleft315.onoff = false;
        EQleft400.Q = Guete;    EQleft400.boost = 0.00;      EQleft400.f0 = f_count[13];     EQleft400.type = Parametric;      EQleft400.phase = true;      EQleft400.onoff = false;
        EQleft500.Q = Guete;    EQleft500.boost = 0.00;      EQleft500.f0 = f_count[14];     EQleft500.type = Parametric;      EQleft500.phase = true;      EQleft500.onoff = true;
        EQleft630.Q = Guete;    EQleft630.boost = 0.00;      EQleft630.f0 = f_count[15];     EQleft630.type = Parametric;      EQleft630.phase = true;      EQleft630.onoff = false;
        EQleft800.Q = Guete;    EQleft800.boost = 0.00;      EQleft800.f0 = f_count[16];     EQleft800.type = Parametric;      EQleft800.phase = true;      EQleft800.onoff = false;
        EQleft1000.Q = Guete;   EQleft1000.boost = 0.00;     EQleft1000.f0 = f_count[17];    EQleft1000.type = Parametric;     EQleft1000.phase = true;     EQleft1000.onoff = true;
        EQleft1250.Q = Guete;   EQleft1250.boost = 0.00;     EQleft1250.f0 = f_count[18];    EQleft1250.type = Parametric;     EQleft1250.phase = true;     EQleft1250.onoff = false;
        EQleft1600.Q = Guete;   EQleft1600.boost = 0.00;     EQleft1600.f0 = f_count[19];    EQleft1600.type = Parametric;     EQleft1600.phase = true;     EQleft1600.onoff = false;
        EQleft2000.Q = Guete;   EQleft2000.boost = 0.00;     EQleft2000.f0 = f_count[20];    EQleft2000.type = Parametric;     EQleft2000.phase = true;     EQleft2000.onoff = true;
        EQleft2500.Q = Guete;   EQleft2500.boost = 0.00;     EQleft2500.f0 = f_count[21];    EQleft2500.type = Parametric;     EQleft2500.phase = true;     EQleft2500.onoff = false;
        EQleft3150.Q = Guete;   EQleft3150.boost = 0.00;     EQleft3150.f0 = f_count[22];    EQleft3150.type = Parametric;     EQleft3150.phase = true;     EQleft3150.onoff = false;
        EQleft4000.Q = Guete;   EQleft4000.boost = 0.00;     EQleft4000.f0 = f_count[23];    EQleft4000.type = Parametric;     EQleft4000.phase = true;     EQleft4000.onoff = true;
        EQleft5000.Q = Guete;   EQleft5000.boost = 0.00;     EQleft5000.f0 = f_count[24];    EQleft5000.type = Parametric;     EQleft5000.phase = true;     EQleft5000.onoff = false;
        EQleft6300.Q = Guete;   EQleft6300.boost = 0.00;     EQleft6300.f0 = f_count[25];    EQleft6300.type = Parametric;     EQleft6300.phase = true;     EQleft6300.onoff = false;
        EQleft8000.Q = Guete;   EQleft8000.boost = 0.00;     EQleft8000.f0 = f_count[26];    EQleft8000.type = Parametric;     EQleft8000.phase = true;     EQleft8000.onoff = true;
        EQleft10000.Q = Guete;  EQleft10000.boost = 0.00;    EQleft10000.f0 = f_count[27];   EQleft10000.type = Parametric;    EQleft10000.phase = true;    EQleft10000.onoff = false;
        EQleft12500.Q = Guete;  EQleft12500.boost = 0.00;    EQleft12500.f0 = f_count[28];   EQleft12500.type = Parametric;    EQleft12500.phase = true;    EQleft12500.onoff = false;
        EQleft16000.Q = Guete;  EQleft16000.boost = 0.00;    EQleft16000.f0 = f_count[29];   EQleft16000.type = Parametric;    EQleft16000.phase = true;    EQleft16000.onoff = true;
        EQleft20000.Q = Guete;  EQleft20000.boost = 0.00;    EQleft20000.f0 = f_count[30];   EQleft20000.type = Parametric;    EQleft20000.phase = true;    EQleft20000.onoff = false;

        //Filters for the right side
        EQright20.Q = Guete;     EQright20.boost = 0.00;       EQright20.f0 = f_count[0];       EQright20.type = Parametric;       EQright20.phase = true;       EQright20.onoff = false;
        EQright25.Q = Guete;     EQright25.boost = 0.00;       EQright25.f0 = f_count[1];       EQright25.type = Parametric;       EQright25.phase = true;       EQright25.onoff = false;
        EQright31.Q = Guete;     EQright31.boost = 0.00;       EQright31.f0 = f_count[2];       EQright31.type = Parametric;       EQright31.phase = true;       EQright31.onoff = false;
        EQright40.Q = Guete;     EQright40.boost = 0.00;       EQright40.f0 = f_count[3];       EQright40.type = Parametric;       EQright40.phase = true;       EQright40.onoff = false;
        EQright50.Q = Guete;     EQright50.boost = 0.00;       EQright50.f0 = f_count[4];       EQright50.type = Parametric;       EQright50.phase = true;       EQright50.onoff = false;
        EQright63.Q = Guete;     EQright63.boost = 0.00;       EQright63.f0 = f_count[5];       EQright63.type = Parametric;       EQright63.phase = true;       EQright63.onoff = true;
        EQright80.Q = Guete;     EQright80.boost = 0.00;       EQright80.f0 = f_count[6];       EQright80.type = Parametric;       EQright80.phase = true;       EQright80.onoff = false;
        EQright100.Q = Guete;    EQright100.boost = 0.00;      EQright100.f0 = f_count[7];      EQright100.type = Parametric;      EQright100.phase = true;      EQright100.onoff = false;
        EQright125.Q = Guete;    EQright125.boost = 0.00;      EQright125.f0 = f_count[8];      EQright125.type = Parametric;      EQright125.phase = true;      EQright125.onoff = true;
        EQright160.Q = Guete;    EQright160.boost = 0.00;      EQright160.f0 = f_count[9];      EQright160.type = Parametric;      EQright160.phase = true;      EQright160.onoff = false;
        EQright200.Q = Guete;    EQright200.boost = 0.00;      EQright200.f0 = f_count[10];     EQright200.type = Parametric;      EQright200.phase = true;      EQright200.onoff = false;
        EQright250.Q = Guete;    EQright250.boost = 0.00;      EQright250.f0 = f_count[11];     EQright250.type = Parametric;      EQright250.phase = true;      EQright250.onoff = true;
        EQright315.Q = Guete;    EQright315.boost = 0.00;      EQright315.f0 = f_count[12];     EQright315.type = Parametric;      EQright315.phase = true;      EQright315.onoff = false;
        EQright400.Q = Guete;    EQright400.boost = 0.00;      EQright400.f0 = f_count[13];     EQright400.type = Parametric;      EQright400.phase = true;      EQright400.onoff = false;
        EQright500.Q = Guete;    EQright500.boost = 0.00;      EQright500.f0 = f_count[14];     EQright500.type = Parametric;      EQright500.phase = true;      EQright500.onoff = true;
        EQright630.Q = Guete;    EQright630.boost = 0.00;      EQright630.f0 = f_count[15];     EQright630.type = Parametric;      EQright630.phase = true;      EQright630.onoff = false;
        EQright800.Q = Guete;    EQright800.boost = 0.00;      EQright800.f0 = f_count[16];     EQright800.type = Parametric;      EQright800.phase = true;      EQright800.onoff = false;
        EQright1000.Q = Guete;   EQright1000.boost = 0.00;     EQright1000.f0 = f_count[17];    EQright1000.type = Parametric;     EQright1000.phase = true;     EQright1000.onoff = true;
        EQright1250.Q = Guete;   EQright1250.boost = 0.00;     EQright1250.f0 = f_count[18];    EQright1250.type = Parametric;     EQright1250.phase = true;     EQright1250.onoff = false;
        EQright1600.Q = Guete;   EQright1600.boost = 0.00;     EQright1600.f0 = f_count[19];    EQright1600.type = Parametric;     EQright1600.phase = true;     EQright1600.onoff = false;
        EQright2000.Q = Guete;   EQright2000.boost = 0.00;     EQright2000.f0 = f_count[20];    EQright2000.type = Parametric;     EQright2000.phase = true;     EQright2000.onoff = true;
        EQright2500.Q = Guete;   EQright2500.boost = 0.00;     EQright2500.f0 = f_count[21];    EQright2500.type = Parametric;     EQright2500.phase = true;     EQright2500.onoff = false;
        EQright3150.Q = Guete;   EQright3150.boost = 0.00;     EQright3150.f0 = f_count[22];    EQright3150.type = Parametric;     EQright3150.phase = true;     EQright3150.onoff = false;
        EQright4000.Q = Guete;   EQright4000.boost = 0.00;     EQright4000.f0 = f_count[23];    EQright4000.type = Parametric;     EQright4000.phase = true;     EQright4000.onoff = true;
        EQright5000.Q = Guete;   EQright5000.boost = 0.00;     EQright5000.f0 = f_count[24];    EQright5000.type = Parametric;     EQright5000.phase = true;     EQright5000.onoff = false;
        EQright6300.Q = Guete;   EQright6300.boost = 0.00;     EQright6300.f0 = f_count[25];    EQright6300.type = Parametric;     EQright6300.phase = true;     EQright6300.onoff = false;
        EQright8000.Q = Guete;   EQright8000.boost = 0.00;     EQright8000.f0 = f_count[26];    EQright8000.type = Parametric;     EQright8000.phase = true;     EQright8000.onoff = true;
        EQright10000.Q = Guete;  EQright10000.boost = 0.00;    EQright10000.f0 = f_count[27];   EQright10000.type = Parametric;    EQright10000.phase = true;    EQright10000.onoff = false;
        EQright12500.Q = Guete;  EQright12500.boost = 0.00;    EQright12500.f0 = f_count[28];   EQright12500.type = Parametric;    EQright12500.phase = true;    EQright12500.onoff = false;
        EQright16000.Q = Guete;  EQright16000.boost = 0.00;    EQright16000.f0 = f_count[29];   EQright16000.type = Parametric;    EQright16000.phase = true;    EQright16000.onoff = true;
        EQright20000.Q = Guete;  EQright20000.boost = 0.00;    EQright20000.f0 = f_count[30];   EQright20000.type = Parametric;    EQright20000.phase = true;    EQright20000.onoff = false;

    }

    void setEqParametersTerZ()
    {
        //in each line we configure one frequence..
        
        //Filters for the left side
        float Guete = 4.38;
        EQleft20.Q = Guete;     EQleft20.boost = 0.00;       EQleft20.f0 = f_count[0];       EQleft20.type = Parametric;       EQleft20.phase = true;       EQleft20.onoff = true;
        EQleft25.Q = Guete;     EQleft25.boost = 0.00;       EQleft25.f0 = f_count[1];       EQleft25.type = Parametric;       EQleft25.phase = true;       EQleft25.onoff = true;
        EQleft31.Q = Guete;     EQleft31.boost = 0.00;       EQleft31.f0 = f_count[2];       EQleft31.type = Parametric;       EQleft31.phase = true;       EQleft31.onoff = true;
        EQleft40.Q = Guete;     EQleft40.boost = 0.00;       EQleft40.f0 = f_count[3];       EQleft40.type = Parametric;       EQleft40.phase = true;       EQleft40.onoff = true;
        EQleft50.Q = Guete;     EQleft50.boost = 0.00;       EQleft50.f0 = f_count[4];       EQleft50.type = Parametric;       EQleft50.phase = true;       EQleft50.onoff = true;
        EQleft63.Q = Guete;     EQleft63.boost = 0.00;       EQleft63.f0 = f_count[5];       EQleft63.type = Parametric;       EQleft63.phase = true;       EQleft63.onoff = true;
        EQleft80.Q = Guete;     EQleft80.boost = 0.00;       EQleft80.f0 = f_count[6];       EQleft80.type = Parametric;       EQleft80.phase = true;       EQleft80.onoff = true;
        EQleft100.Q = Guete;    EQleft100.boost = 0.00;      EQleft100.f0 = f_count[7];      EQleft100.type = Parametric;      EQleft100.phase = true;      EQleft100.onoff = true;
        EQleft125.Q = Guete;    EQleft125.boost = 0.00;      EQleft125.f0 = f_count[8];      EQleft125.type = Parametric;      EQleft125.phase = true;      EQleft125.onoff = true;
        EQleft160.Q = Guete;    EQleft160.boost = 0.00;      EQleft160.f0 = f_count[9];      EQleft160.type = Parametric;      EQleft160.phase = true;      EQleft160.onoff = true;
        EQleft200.Q = Guete;    EQleft200.boost = 0.00;      EQleft200.f0 = f_count[10];     EQleft200.type = Parametric;      EQleft200.phase = true;      EQleft200.onoff = true;
        EQleft250.Q = Guete;    EQleft250.boost = 0.00;      EQleft250.f0 = f_count[11];     EQleft250.type = Parametric;      EQleft250.phase = true;      EQleft250.onoff = true;
        EQleft315.Q = Guete;    EQleft315.boost = 0.00;      EQleft315.f0 = f_count[12];     EQleft315.type = Parametric;      EQleft315.phase = true;      EQleft315.onoff = true;
        EQleft400.Q = Guete;    EQleft400.boost = 0.00;      EQleft400.f0 = f_count[13];     EQleft400.type = Parametric;      EQleft400.phase = true;      EQleft400.onoff = true;
        EQleft500.Q = Guete;    EQleft500.boost = 0.00;      EQleft500.f0 = f_count[14];     EQleft500.type = Parametric;      EQleft500.phase = true;      EQleft500.onoff = true;
        EQleft630.Q = Guete;    EQleft630.boost = 0.00;      EQleft630.f0 = f_count[15];     EQleft630.type = Parametric;      EQleft630.phase = true;      EQleft630.onoff = true;
        EQleft800.Q = Guete;    EQleft800.boost = 0.00;      EQleft800.f0 = f_count[16];     EQleft800.type = Parametric;      EQleft800.phase = true;      EQleft800.onoff = true;
        EQleft1000.Q = Guete;   EQleft1000.boost = 0.00;     EQleft1000.f0 = f_count[17];    EQleft1000.type = Parametric;     EQleft1000.phase = true;     EQleft1000.onoff = true;
        EQleft1250.Q = Guete;   EQleft1250.boost = 0.00;     EQleft1250.f0 = f_count[18];    EQleft1250.type = Parametric;     EQleft1250.phase = true;     EQleft1250.onoff = true;
        EQleft1600.Q = Guete;   EQleft1600.boost = 0.00;     EQleft1600.f0 = f_count[19];    EQleft1600.type = Parametric;     EQleft1600.phase = true;     EQleft1600.onoff = true;
        EQleft2000.Q = Guete;   EQleft2000.boost = 0.00;     EQleft2000.f0 = f_count[20];    EQleft2000.type = Parametric;     EQleft2000.phase = true;     EQleft2000.onoff = true;
        EQleft2500.Q = Guete;   EQleft2500.boost = 0.00;     EQleft2500.f0 = f_count[21];    EQleft2500.type = Parametric;     EQleft2500.phase = true;     EQleft2500.onoff = true;
        EQleft3150.Q = Guete;   EQleft3150.boost = 0.00;     EQleft3150.f0 = f_count[22];    EQleft3150.type = Parametric;     EQleft3150.phase = true;     EQleft3150.onoff = true;
        EQleft4000.Q = Guete;   EQleft4000.boost = 0.00;     EQleft4000.f0 = f_count[23];    EQleft4000.type = Parametric;     EQleft4000.phase = true;     EQleft4000.onoff = true;
        EQleft5000.Q = Guete;   EQleft5000.boost = 0.00;     EQleft5000.f0 = f_count[24];    EQleft5000.type = Parametric;     EQleft5000.phase = true;     EQleft5000.onoff = true;
        EQleft6300.Q = Guete;   EQleft6300.boost = 0.00;     EQleft6300.f0 = f_count[25];    EQleft6300.type = Parametric;     EQleft6300.phase = true;     EQleft6300.onoff = true;
        EQleft8000.Q = Guete;   EQleft8000.boost = 0.00;     EQleft8000.f0 = f_count[26];    EQleft8000.type = Parametric;     EQleft8000.phase = true;     EQleft8000.onoff = true;
        EQleft10000.Q = Guete;  EQleft10000.boost = 0.00;    EQleft10000.f0 = f_count[27];   EQleft10000.type = Parametric;    EQleft10000.phase = true;    EQleft10000.onoff = true;
        EQleft12500.Q = Guete;  EQleft12500.boost = 0.00;    EQleft12500.f0 = f_count[28];   EQleft12500.type = Parametric;    EQleft12500.phase = true;    EQleft12500.onoff = true;
        EQleft16000.Q = Guete;  EQleft16000.boost = 0.00;    EQleft16000.f0 = f_count[29];   EQleft16000.type = Parametric;    EQleft16000.phase = true;    EQleft16000.onoff = true;
        EQleft20000.Q = Guete;  EQleft20000.boost = 0.00;    EQleft20000.f0 = f_count[30];   EQleft20000.type = Parametric;    EQleft20000.phase = true;    EQleft20000.onoff = true;

        //Filters for the right side
        EQright20.Q = Guete;     EQright20.boost = 0.00;       EQright20.f0 = f_count[0];       EQright20.type = Parametric;       EQright20.phase = true;       EQright20.onoff = true;
        EQright25.Q = Guete;     EQright25.boost = 0.00;       EQright25.f0 = f_count[1];       EQright25.type = Parametric;       EQright25.phase = true;       EQright25.onoff = true;
        EQright31.Q = Guete;     EQright31.boost = 0.00;       EQright31.f0 = f_count[2];       EQright31.type = Parametric;       EQright31.phase = true;       EQright31.onoff = true;
        EQright40.Q = Guete;     EQright40.boost = 0.00;       EQright40.f0 = f_count[3];       EQright40.type = Parametric;       EQright40.phase = true;       EQright40.onoff = true;
        EQright50.Q = Guete;     EQright50.boost = 0.00;       EQright50.f0 = f_count[4];       EQright50.type = Parametric;       EQright50.phase = true;       EQright50.onoff = true;
        EQright63.Q = Guete;     EQright63.boost = 0.00;       EQright63.f0 = f_count[5];       EQright63.type = Parametric;       EQright63.phase = true;       EQright63.onoff = true;
        EQright80.Q = Guete;     EQright80.boost = 0.00;       EQright80.f0 = f_count[6];       EQright80.type = Parametric;       EQright80.phase = true;       EQright80.onoff = true;
        EQright100.Q = Guete;    EQright100.boost = 0.00;      EQright100.f0 = f_count[7];      EQright100.type = Parametric;      EQright100.phase = true;      EQright100.onoff = true;
        EQright125.Q = Guete;    EQright125.boost = 0.00;      EQright125.f0 = f_count[8];      EQright125.type = Parametric;      EQright125.phase = true;      EQright125.onoff = true;
        EQright160.Q = Guete;    EQright160.boost = 0.00;      EQright160.f0 = f_count[9];      EQright160.type = Parametric;      EQright160.phase = true;      EQright160.onoff = true;
        EQright200.Q = Guete;    EQright200.boost = 0.00;      EQright200.f0 = f_count[10];     EQright200.type = Parametric;      EQright200.phase = true;      EQright200.onoff = true;
        EQright250.Q = Guete;    EQright250.boost = 0.00;      EQright250.f0 = f_count[11];     EQright250.type = Parametric;      EQright250.phase = true;      EQright250.onoff = true;
        EQright315.Q = Guete;    EQright315.boost = 0.00;      EQright315.f0 = f_count[12];     EQright315.type = Parametric;      EQright315.phase = true;      EQright315.onoff = true;
        EQright400.Q = Guete;    EQright400.boost = 0.00;      EQright400.f0 = f_count[13];     EQright400.type = Parametric;      EQright400.phase = true;      EQright400.onoff = true;
        EQright500.Q = Guete;    EQright500.boost = 0.00;      EQright500.f0 = f_count[14];     EQright500.type = Parametric;      EQright500.phase = true;      EQright500.onoff = true;
        EQright630.Q = Guete;    EQright630.boost = 0.00;      EQright630.f0 = f_count[15];     EQright630.type = Parametric;      EQright630.phase = true;      EQright630.onoff = true;
        EQright800.Q = Guete;    EQright800.boost = 0.00;      EQright800.f0 = f_count[16];     EQright800.type = Parametric;      EQright800.phase = true;      EQright800.onoff = true;
        EQright1000.Q = Guete;   EQright1000.boost = 0.00;     EQright1000.f0 = f_count[17];    EQright1000.type = Parametric;     EQright1000.phase = true;     EQright1000.onoff = true;
        EQright1250.Q = Guete;   EQright1250.boost = 0.00;     EQright1250.f0 = f_count[18];    EQright1250.type = Parametric;     EQright1250.phase = true;     EQright1250.onoff = true;
        EQright1600.Q = Guete;   EQright1600.boost = 0.00;     EQright1600.f0 = f_count[19];    EQright1600.type = Parametric;     EQright1600.phase = true;     EQright1600.onoff = true;
        EQright2000.Q = Guete;   EQright2000.boost = 0.00;     EQright2000.f0 = f_count[20];    EQright2000.type = Parametric;     EQright2000.phase = true;     EQright2000.onoff = true;
        EQright2500.Q = Guete;   EQright2500.boost = 0.00;     EQright2500.f0 = f_count[21];    EQright2500.type = Parametric;     EQright2500.phase = true;     EQright2500.onoff = true;
        EQright3150.Q = Guete;   EQright3150.boost = 0.00;     EQright3150.f0 = f_count[22];    EQright3150.type = Parametric;     EQright3150.phase = true;     EQright3150.onoff = true;
        EQright4000.Q = Guete;   EQright4000.boost = 0.00;     EQright4000.f0 = f_count[23];    EQright4000.type = Parametric;     EQright4000.phase = true;     EQright4000.onoff = true;
        EQright5000.Q = Guete;   EQright5000.boost = 0.00;     EQright5000.f0 = f_count[24];    EQright5000.type = Parametric;     EQright5000.phase = true;     EQright5000.onoff = true;
        EQright6300.Q = Guete;   EQright6300.boost = 0.00;     EQright6300.f0 = f_count[25];    EQright6300.type = Parametric;     EQright6300.phase = true;     EQright6300.onoff = true;
        EQright8000.Q = Guete;   EQright8000.boost = 0.00;     EQright8000.f0 = f_count[26];    EQright8000.type = Parametric;     EQright8000.phase = true;     EQright8000.onoff = true;
        EQright10000.Q = Guete;  EQright10000.boost = 0.00;    EQright10000.f0 = f_count[27];   EQright10000.type = Parametric;    EQright10000.phase = true;    EQright10000.onoff = true;
        EQright12500.Q = Guete;  EQright12500.boost = 0.00;    EQright12500.f0 = f_count[28];   EQright12500.type = Parametric;    EQright12500.phase = true;    EQright12500.onoff = true;
        EQright16000.Q = Guete;  EQright16000.boost = 0.00;    EQright16000.f0 = f_count[29];   EQright16000.type = Parametric;    EQright16000.phase = true;    EQright16000.onoff = true;
        EQright20000.Q = Guete;  EQright20000.boost = 0.00;    EQright20000.f0 = f_count[30];   EQright20000.type = Parametric;    EQright20000.phase = true;    EQright20000.onoff = true;

    }
    void saveIteration1InEqBoost()
    {
        EQleft20.boost = saveIteration1[LINKS][0];
        EQleft25.boost = saveIteration1[LINKS][1];
        EQleft31.boost = saveIteration1[LINKS][2];
        EQleft40.boost = saveIteration1[LINKS][3];
        EQleft50.boost = saveIteration1[LINKS][4];
        EQleft63.boost = saveIteration1[LINKS][5];
        EQleft80.boost = saveIteration1[LINKS][6];
        EQleft100.boost = saveIteration1[LINKS][7];
        EQleft125.boost = saveIteration1[LINKS][8];
        EQleft160.boost = saveIteration1[LINKS][9];
        EQleft200.boost = saveIteration1[LINKS][10];
        EQleft250.boost = saveIteration1[LINKS][11];
        EQleft315.boost = saveIteration1[LINKS][12];
        EQleft400.boost = saveIteration1[LINKS][13];
        EQleft500.boost = saveIteration1[LINKS][14];
        EQleft630.boost = saveIteration1[LINKS][15];
        EQleft800.boost = saveIteration1[LINKS][16];
        EQleft1000.boost = saveIteration1[LINKS][17];
        EQleft1250.boost = saveIteration1[LINKS][18];
        EQleft1600.boost = saveIteration1[LINKS][19];
        EQleft2000.boost = saveIteration1[LINKS][20];
        EQleft2500.boost = saveIteration1[LINKS][21];
        EQleft3150.boost = saveIteration1[LINKS][22];
        EQleft4000.boost = saveIteration1[LINKS][23];
        EQleft5000.boost = saveIteration1[LINKS][24];
        EQleft6300.boost = saveIteration1[LINKS][25];
        EQleft8000.boost = saveIteration1[LINKS][26];
        EQleft10000.boost = saveIteration1[LINKS][27];
        EQleft12500.boost = saveIteration1[LINKS][28];
        EQleft16000.boost = saveIteration1[LINKS][29];
        EQleft20000.boost = saveIteration1[LINKS][30];

        EQright20.boost = saveIteration1[RECHTS][0];
        EQright25.boost = saveIteration1[RECHTS][1];
        EQright31.boost = saveIteration1[RECHTS][2];
        EQright40.boost = saveIteration1[RECHTS][3];
        EQright50.boost = saveIteration1[RECHTS][4];
        EQright63.boost = saveIteration1[RECHTS][5];
        EQright80.boost = saveIteration1[RECHTS][6];
        EQright100.boost = saveIteration1[RECHTS][7];
        EQright125.boost = saveIteration1[RECHTS][8];
        EQright160.boost = saveIteration1[RECHTS][9];
        EQright200.boost = saveIteration1[RECHTS][10];
        EQright250.boost = saveIteration1[RECHTS][11];
        EQright315.boost = saveIteration1[RECHTS][12];
        EQright400.boost = saveIteration1[RECHTS][13];
        EQright500.boost = saveIteration1[RECHTS][14];
        EQright630.boost = saveIteration1[RECHTS][15];
        EQright800.boost = saveIteration1[RECHTS][16];
        EQright1000.boost = saveIteration1[RECHTS][17];
        EQright1250.boost = saveIteration1[RECHTS][18];
        EQright1600.boost = saveIteration1[RECHTS][19];
        EQright2000.boost = saveIteration1[RECHTS][20];
        EQright2500.boost = saveIteration1[RECHTS][21];
        EQright3150.boost = saveIteration1[RECHTS][22];
        EQright4000.boost = saveIteration1[RECHTS][23];
        EQright5000.boost = saveIteration1[RECHTS][24];
        EQright6300.boost = saveIteration1[RECHTS][25];
        EQright8000.boost = saveIteration1[RECHTS][26];
        EQright10000.boost = saveIteration1[RECHTS][27];
        EQright12500.boost = saveIteration1[RECHTS][28];
        EQright16000.boost = saveIteration1[RECHTS][29];
        EQright20000.boost = saveIteration1[RECHTS][30];

    }

    void saveIteration2InEqBoost()
    {
        EQleft20.boost = saveIteration2[LINKS][0] + HD660[LINKS][0];
        EQleft25.boost = saveIteration2[LINKS][1] + HD660[LINKS][1];
        EQleft31.boost = saveIteration2[LINKS][2] + HD660[LINKS][2];
        EQleft40.boost = saveIteration2[LINKS][3] + HD660[LINKS][3];
        EQleft50.boost = saveIteration2[LINKS][4] + HD660[LINKS][4];
        EQleft63.boost = saveIteration2[LINKS][5] + HD660[LINKS][5];
        EQleft80.boost = saveIteration2[LINKS][6] + HD660[LINKS][6];
        EQleft100.boost = saveIteration2[LINKS][7] + HD660[LINKS][7];
        EQleft125.boost = saveIteration2[LINKS][8] + HD660[LINKS][8];
        EQleft160.boost = saveIteration2[LINKS][9] + HD660[LINKS][9];
        EQleft200.boost = saveIteration2[LINKS][10] + HD660[LINKS][10];
        EQleft250.boost = saveIteration2[LINKS][11] + HD660[LINKS][11];
        EQleft315.boost = saveIteration2[LINKS][12] + HD660[LINKS][12];
        EQleft400.boost = saveIteration2[LINKS][13] + HD660[LINKS][13];
        EQleft500.boost = saveIteration2[LINKS][14] + HD660[LINKS][14];
        EQleft630.boost = saveIteration2[LINKS][15] + HD660[LINKS][15];
        EQleft800.boost = saveIteration2[LINKS][16] + HD660[LINKS][16];
        EQleft1000.boost = saveIteration2[LINKS][17] + HD660[LINKS][17];
        EQleft1250.boost = saveIteration2[LINKS][18] + HD660[LINKS][18];
        EQleft1600.boost = saveIteration2[LINKS][19] + HD660[LINKS][19];
        EQleft2000.boost = saveIteration2[LINKS][20] + HD660[LINKS][20];
        EQleft2500.boost = saveIteration2[LINKS][21] + HD660[LINKS][21];
        EQleft3150.boost = saveIteration2[LINKS][22] + HD660[LINKS][22];
        EQleft4000.boost = saveIteration2[LINKS][23] + HD660[LINKS][23];
        EQleft5000.boost = saveIteration2[LINKS][24] + HD660[LINKS][24];
        EQleft6300.boost = saveIteration2[LINKS][25] + HD660[LINKS][25];
        EQleft8000.boost = saveIteration2[LINKS][26] + HD660[LINKS][26];
        EQleft10000.boost = saveIteration2[LINKS][27] + HD660[LINKS][27];
        EQleft12500.boost = saveIteration2[LINKS][28] + HD660[LINKS][28];
        EQleft16000.boost = saveIteration2[LINKS][29] + HD660[LINKS][29];
        EQleft20000.boost = saveIteration2[LINKS][30] + HD660[LINKS][30];

        EQright20.boost = saveIteration2[RECHTS][0] + HD660[RECHTS][0];
        EQright25.boost = saveIteration2[RECHTS][1] + HD660[RECHTS][1];
        EQright31.boost = saveIteration2[RECHTS][2] + HD660[RECHTS][2];
        EQright40.boost = saveIteration2[RECHTS][3] + HD660[RECHTS][3];
        EQright50.boost = saveIteration2[RECHTS][4] + HD660[RECHTS][4];
        EQright63.boost = saveIteration2[RECHTS][5] + HD660[RECHTS][5];
        EQright80.boost = saveIteration2[RECHTS][6] + HD660[RECHTS][6];
        EQright100.boost = saveIteration2[RECHTS][7] + HD660[RECHTS][7];
        EQright125.boost = saveIteration2[RECHTS][8] + HD660[RECHTS][8];
        EQright160.boost = saveIteration2[RECHTS][9] + HD660[RECHTS][9];
        EQright200.boost = saveIteration2[RECHTS][10] + HD660[RECHTS][10];
        EQright250.boost = saveIteration2[RECHTS][11] + HD660[RECHTS][11];
        EQright315.boost = saveIteration2[RECHTS][12] + HD660[RECHTS][12];
        EQright400.boost = saveIteration2[RECHTS][13] + HD660[RECHTS][13];
        EQright500.boost = saveIteration2[RECHTS][14] + HD660[RECHTS][14];
        EQright630.boost = saveIteration2[RECHTS][15] + HD660[RECHTS][15];
        EQright800.boost = saveIteration2[RECHTS][16] + HD660[RECHTS][16];
        EQright1000.boost = saveIteration2[RECHTS][17] + HD660[RECHTS][17];
        EQright1250.boost = saveIteration2[RECHTS][18] + HD660[RECHTS][18];
        EQright1600.boost = saveIteration2[RECHTS][19] + HD660[RECHTS][19];
        EQright2000.boost = saveIteration2[RECHTS][20] + HD660[RECHTS][20];
        EQright2500.boost = saveIteration2[RECHTS][21] + HD660[RECHTS][21];
        EQright3150.boost = saveIteration2[RECHTS][22] + HD660[RECHTS][22];
        EQright4000.boost = saveIteration2[RECHTS][23] + HD660[RECHTS][23];
        EQright5000.boost = saveIteration2[RECHTS][24] + HD660[RECHTS][24];
        EQright6300.boost = saveIteration2[RECHTS][25] + HD660[RECHTS][25];
        EQright8000.boost = saveIteration2[RECHTS][26] + HD660[RECHTS][26];
        EQright10000.boost = saveIteration2[RECHTS][27] + HD660[RECHTS][27];
        EQright12500.boost = saveIteration2[RECHTS][28] + HD660[RECHTS][28];
        EQright16000.boost = saveIteration2[RECHTS][29] + HD660[RECHTS][29];
        EQright20000.boost = saveIteration2[RECHTS][30] + HD660[RECHTS][30];

    }

    void sendEqDataToBoard()
    {  
       int EQtmpAddressleft = personalizedMidEQ1Addr;
       int EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
       int EQtmpAddressleft2 = personalizedMidEQ1_5Addr;

       int EQtmpAddressright = personalizedMidEQ1_2Addr;
       int EQtmpAddressright1 = personalizedMidEQ1_4Addr;
       int EQtmpAddressright2 = personalizedMidEQ1_6Addr;
        
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft20);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft25);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft31);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft40);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft50);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft63);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft80);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft100);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft125);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft160);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft200);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft250);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft315);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft400);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft500);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft630);          EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft800);          EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1250);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1600);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft2000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft2500);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft3150);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft4000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft5000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft6300);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft8000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft10000);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft12500);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft16000);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft2, &EQleft20000);        EQtmpAddressleft2 += 5;

       
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright20);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright25);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright31);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright40);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright50);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright63);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright80);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright100);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright125);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright160);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright200);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright250);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright315);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright400);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright500);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright630);          EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright800);          EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1250);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1600);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright2000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright2500);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright3150);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright4000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright5000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright6300);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright8000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright10000);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright12500);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright16000);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright2, &EQright20000);        EQtmpAddressright2 += 5;

    }

        // void sendFilters()
    // {
    //     if (fineState)
    //     {
    //         send31FiltersToDSP();
    //     }
    //     else
    //     {
    //         send9FiltersToDSP();
    //     }    
    // }




    // void send9FiltersToDSP()
    // {
    //     EQtmpAddressleft = personalizedMidEQ1Addr;
    //     EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
        
    //     EQtmpAddressright = personalizedMidEQ1_2Addr;
    //     EQtmpAddressright1 = personalizedMidEQ1_4Addr;
    //     int EQtmpAddress = 0;
    //     for (size_t i = 0; i < 9; i++)
    //     {
    //         if (fineAndCourseFreqMapper[i] < 15)    EQtmpAddress = EQtmpAddressleft;
    //         else                                    EQtmpAddress = EQtmpAddressleft1;     
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsleft[fineAndCourseFreqMapper[i]]);          EQtmpAddress += 5;   
             
    //     }
    //     for (size_t i = 0; i < 9; i++)
    //     {
    //         if (fineAndCourseFreqMapper[i] < 15)    EQtmpAddress = EQtmpAddressright;
    //         else                                    EQtmpAddress = EQtmpAddressright1;     
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsright[fineAndCourseFreqMapper[i]]);          EQtmpAddress += 5;   
             
    //     }
        
    // }
    // void send31FiltersToDSP()
    // {
    //     EQtmpAddressleft = personalizedMidEQ1Addr;
    //     EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
    //     EQtmpAddressleft2 = personalizedMidEQ1_5Addr;

    //     EQtmpAddressright = personalizedMidEQ1_2Addr;
    //     EQtmpAddressright1 = personalizedMidEQ1_4Addr;
    //     EQtmpAddressright2 = personalizedMidEQ1_6Addr;
    //     int EQtmpAddress = 0;
    //     for (size_t i = 0; i < 31; i++)
    //     {
    //         if ( i < 15 )                          EQtmpAddress = EQtmpAddressleft;
    //         else if( i > 15 && i < 30)             EQtmpAddress = EQtmpAddressleft1;
    //         else                                   EQtmpAddress = EQtmpAddressleft2;
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsleft[i]);          EQtmpAddress += 5;    
    //     }
    //     for (size_t i = 0; i < 31; i++)
    //     {
    //         if ( i < 15 )                          EQtmpAddress = EQtmpAddressright;
    //         else if( i > 15 && i < 30)             EQtmpAddress = EQtmpAddressright1;
    //         else                                   EQtmpAddress = EQtmpAddressright2;
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsright[i]);          EQtmpAddress += 5;    
    //     }
    // }