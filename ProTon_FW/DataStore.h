#ifndef DATA_STORE
#define DATA_STORE

#include <Arduino.h>
#include "Model.h"


#define USERS_COUNT 3
#define WrPin 32
#define EEPROM_ADDRESS 0x54
#define HEARING_TEST_COLOMS 9
#define HEARING_TEST_ROWS 2
#define HEARING_TEST_ELEMENTS_COUNT HEARING_TEST_COLOMS*HEARING_TEST_ROWS

#define USER_STRUCT_SIZE sizeof(User)
#define HEARING_TEST_SIZE sizeof(float) * HEARING_TEST_ELEMENTS_COUNT

#define User1FlagBIT 0
#define User2FlagBIT 1
#define User3FlagBIT 2

#define firstRunFlagsAddress  500

#define currentUserAddress  501


void printSize(void);
void eepromInit(void);
// read/write byte
void writeByteToEepromAddress(uint16_t stroingAddress, byte byteData);
byte readByteFromEepromAddress(uint16_t stroingAddress);
//read/write Array
void writeTowDimArray(uint16_t eeaddress, float dataArray[HEARING_TEST_ROWS][HEARING_TEST_COLOMS]);
float *readTowDimArray(uint16_t eeaddress);

void _writeEEPROM(uint16_t eeaddress, byte *data, int dataSize);

void changeCurrentUser(uint8_t newUserID);
uint8_t getCurrentUserIdx();
void getUserFromEEPROM();
void getAllUserFromEEPROM();
bool isItFirstRun();
void applyFirstRun();
//User staruct functions:
//1-Name:
void setUserName(String usrName);
String getUserName(void);
//2-Options:
void setUserOptions(UserOptions opt);
void setLinerUserOptions(bool lin);
void setP1UserOptions(bool p1Option);
void setP2UserOptions(bool p2Option);
void setCompUserOptions(bool compOption);
void setBassUserOptions(bool bassOption);

UserOptions getUserOptions(void);
//3-Hear test flag:
void setUserHearingTestFlag(byte hearTestFlag);
byte getUserHearingTestFlag(void);
//4-Hear test result:
void setUserHearingResult(float hearingRes[HEARING_TEST_ROWS][HEARING_TEST_COLOMS]);
void getUserHearingResult(float res[HEARING_TEST_ROWS][HEARING_TEST_COLOMS]);

// utils function
void _cpyHearingTest(float src[2][9], float dst[2][9]);
uint16_t getBassUserAddress();
uint16_t getUserNameAddress();
uint16_t getOptionAddress();
uint16_t getDoneTestAddress();
uint16_t getHearingResultAddress();

// printing function for debug
void printCurrentUserData(void);
void _printHearingResult(float res[HEARING_TEST_ROWS][HEARING_TEST_COLOMS]);


////////////////////////////////////////////////////////////////////////////////////
//////////////////////             for review:
////////////////////////////////////////////////////////////////////////////////////

void writeHearingTestToEEprom(int userNummber, float hearingTestToWrite[HEARING_TEST_ROWS][HEARING_TEST_COLOMS]);

void getHearingTestFromEEprom(int userNummber);
void writeUserStrToEeprom(uint16_t stroingAddress, User userdata);
User readUserStrFromEeprom(uint16_t stroingAddress);

void upateUserStrInEeprom(uint16_t stroingAddress, User newData);

void updateStoredUser(int userNum);

int _getUserHearingTestAddress(int userNum);
//TODO: utils function just to first run algo

bool isUserDoneHearingTest(uint8_t usrId);
void setUserHearingTestStatus(uint8_t usrId, bool status );
void setUserTest(bool bitVal);
void setFirstRunFlag(bool FRFlag);

#endif //DATA_STORE