/*
 *    hearingtest.ino
 */
#include <Arduino.h>
#include "AidaDSP.h"
extern bool fineState;
extern float headphoneGain;
uint8_t countHearingTest = 0; // Hilfsvariable für Fallunterscheidung beim Drücken des Encoders beim Hörtest
uint8_t countNoise = 0;       //Zählvariable für Fallunterscheidung beim Rauschgenerator

float sendMinVol = pow(10, hearingTestVolMINdB / 20.0);

int hearingTestfrequencySwitch;

int TestDelay = 1500; // Delay für Testfunktion
int SeiteTEST;        //
int counter = 0;
uint8_t hearingTest_state = false;

//HearingTestFunktions :
void hearingTest();
void hearingTestINIT();
void Sinus_Generator();
void hearingSinusMod();
void hearingTestPRINT();
void FrequenzAusgabe();
void hearingBubbleSort();
void HearingButton(void);
void Sinus_GeneratorMod(float, float);
void Algo1(void);

void hearingTest()
{
  fineState = true;
  Serial.println(F("hearing test 1..."));
  VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);

  Serial.println(F("Linke Seite!"));
  testLINKS = true;
  testRECHTS = false;
  hearingSinusMod();

  Serial.println(F("Rechte Seite!"));
  testLINKS = false;
  testRECHTS = true;
  hearingSinusMod();

  f_counter = 0;
  hearingBubbleSort();
  hearingTestPRINT();
}

/*******************************************************************
   void hearingTestINIT()

   Initialisierung des Hörtests - Alle Werte auf Startwerte setzen
 *******************************************************************/
void hearingTestINIT() // Hörtest initialisieren
{
  f_counter = 0;                             //Zählvariable für Frequenzwahl auf Startwert
  hearingTestvolumedB = hearingTestVolMINdB; // Startlautstärke ist Minimum
  sine_count = 0;                            // Sinuszähler auf Minimum
  volume_state = false;                      // Es wird keine Lautstärkeänderung über Encoder zugelassen
}

/*******************************************************************
   void Sinus_GeneratorMod(float dBVolumeVal, float freq)

   generate sin wave with spacific frequency at spacific volume value
 *******************************************************************/
void Sinus_GeneratorMod(float dBVolumeVal, float freq)
{
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, dBVolumeVal);         //Schreibe aktuelle Volume in DSP
}

/*******************************************************************
   void Sinus_Generator()

   Sinusgenerator ein- und nach Pause wieder ausschalten
 *******************************************************************/
void Sinus_Generator()
{
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, f_count[f_counter]); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, hearingTestvolumedB);         //Schreibe aktuelle Volume in DSP
}

/*******************************************************************
   void hearingSinus()

   der eigentliche Hörtest
 *******************************************************************/

void hearingSinusMod()
{
  SeiteTEST = SeitenwahlOHNEnoise(); // Variable für Seitenwahl (1 -> links, 2 -> rechts)
  hearingTestINIT();                     // Initialisierung
  while ((f_counter <= f_countMAX))      // solange Lautstärke oder Frequenz nicht MAX. erreicht hat
  {
    FrequenzAusgabe();                 // Zuweisung des Laufindizes der richtigen Frequenz (aus Array)
    Serial.print(hearingTestvolumedB); // formatierte Ausgabe der Frequenz
    Serial.println(F(" dB"));

    String hearSide = (SeiteTEST == 1) ? "Links:" : "Rechts:";
    oledShowFreq(hearSide, f_count[f_counter], hearingTestvolumedB);

    Sinus_Generator();                                               // mache Ton an und nach einer Pause wieder aus
    while ((digitalRead(encoderSwitchPin) == HIGH && counter < 600)) // = 400ms
    {
      delay(1);
      counter++;
      //Serial.print(".");
    }
    Serial.print("\n");
    counter = 0;
    hearingTestvolumedB = hearingTestvolumedB + hearingTestStepdB;                            // inkrementiere Lautstärke um Schrittweite (Default: 5dB)
    if ((digitalRead(encoderSwitchPin) == LOW) || hearingTestvolumedB == hearingTestVolMAXdB) // Wenn mit Button bestätigt oder max. Lautstärke erreicht wird, dann speichern
    {
      // HearingButton();
      hearingTestSPL[SeiteTEST][f_counter] = VolumedBFS + hearingTestvolumedB - hearingTestStepdB;
      hearingTestHL[SeiteTEST][f_counter] = hearingTestSPL[SeiteTEST][f_counter] - hearingTestSAVE[SeiteTEST + 2][f_counter];
      hearingTestSAVE[SeiteTEST][f_counter] = hearingTestHL[SeiteTEST][f_counter];

      //hearingTestSAVE[SeiteTEST][f_counter] = (hearingTestSAVE[SeiteTEST + 2][f_counter] + hearingTestvolumedB - hearingTestStepdB); // Speichern der aktuellen Lautstärke in Array; 100 addiert, um dB[HL] zu erhalten
      Serial.println(F("gespeichert"));
      delay(50);
      f_counter++;                               // nächste Frequenz
      hearingTestvolumedB = hearingTestVolMINdB; // fange mit Minimallautstärke an
      //hearingTestvolumedB = (hearingTestSAVE[SeiteTEST + 2][f_counter] * (-1));
    }
    delay(350);
  }
  Serial.println(F("Ende!")); // Wenn Lautstärke oder Frequenz max ist, oder mit Button bestätigt wurde
}


/*
//alt hearingSinusMod()
void hearingSinusMod()
{
  SeiteTEST = SeitenwahlOHNEnoise(); // Variable für Seitenwahl (1 -> links, 2 -> rechts)
  hearingTestINIT();                     // Initialisierung
  while ((f_counter <= f_countMAX))      // solange Lautstärke oder Frequenz nicht MAX. erreicht hat
  {
    FrequenzAusgabe();                 // Zuweisung des Laufindizes der richtigen Frequenz (aus Array)
    Serial.print(hearingTestvolumedB); // formatierte Ausgabe der Frequenz
    Serial.println(F(" dB"));

    String hearSide = (SeiteTEST == 1) ? "Links:" : "Rechts:";
    oledShowFreq(hearSide, f_count[f_counter], hearingTestvolumedB);

    Sinus_Generator();                                               // mache Ton an und nach einer Pause wieder aus
    while ((digitalRead(encoderSwitchPin) == HIGH && counter < 800)) // = 400ms
    {
      delay(2);
      counter++;
      //Serial.print(".");
    }
    Serial.print("\n");
    counter = 0;
    hearingTestvolumedB = hearingTestvolumedB + hearingTestStepdB;                            // inkrementiere Lautstärke um Schrittweite (Default: 5dB)
    if ((digitalRead(encoderSwitchPin) == LOW) || hearingTestvolumedB == hearingTestVolMAXdB) // Wenn mit Button bestätigt oder max. Lautstärke erreicht wird, dann speichern
    {
     // HearingButton();
       hearingTestSAVE[SeiteTEST][f_counter] = (hearingTestSAVE[SeiteTEST + 2][f_counter] + hearingTestvolumedB - hearingTestStepdB); // Speichern der aktuellen Lautstärke in Array; 100 addiert, um dB[HL] zu erhalten
      Serial.println(F("gespeichert"));
      delay(50);
      f_counter++;                               // nächste Frequenz
      hearingTestvolumedB = hearingTestVolMINdB; // fange mit Minimallautstärke an
      //hearingTestvolumedB = (hearingTestSAVE[SeiteTEST + 2][f_counter] * (-1));
    }
    delay(350);
  }
  Serial.println(F("Ende!")); // Wenn Lautstärke oder Frequenz max ist, oder mit Button bestätigt wurde
}
*/

void hearingTestPRINT() // Ausgabe des gespeicherten Arrays
{
  if (SerialTermON == true)
  {
    int ii, jj;
    for (ii = 0; ii <= 2; ii++) //Zeilenweise
    {
      for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
      {
        Serial.print(hearingTestSAVE[ii][jj]);
        Serial.print("\t");
      }
      Serial.println(" ");
    }
  }
}

/*******************************************************************
   void FrequenzAusgabe()

   for printing on lcdDevice
 *******************************************************************/
void FrequenzAusgabe()
{

  //TODO: conver switch to indexed printing using arrau it self
  if (SerialTermON == true)
  {
    int x = f_counter;
    
    switch (x)
    {
    case 0:
      Serial.print(F("f:    63Hz\t"));
      break;
    case 1:
      Serial.print(F("f:    125Hz\t"));
      break;
    case 2:
      Serial.print(F("f:    250Hz\t"));
      break;
    case 3:
      Serial.print(F("f:    500Hz\t"));
      break;
    case 4:
      Serial.print(F("f:    1kHz\t"));
      break;
    case 5:
      Serial.print(F("f:    2kHz\t"));
      break;
    case 6:
      Serial.print(F("f:    4kHz\t"));
      break;
    case 7:
      Serial.print(F("f:    8kHz\t"));
      break;
    case 8:
      Serial.print(F("f:   16kHz\t"));
      break;
    }
  }
}

void hearingBubbleSort()
{
  if ((SerialTermON == true) && ((debugON == true) || (PINdebugON == true)))
  {
    Serial.println(F("Vorher!"));
    hearingTestPRINT();
  }
  for (int valueLEFT = 0; valueLEFT <= (f_countMAX - 1); valueLEFT++)
  {
    for (int valueRIGHT = 0; valueRIGHT <= (f_countMAX - (valueLEFT + 1)); valueRIGHT++)
    {
      if (hearingTestSAVE[FREQ][valueRIGHT] > hearingTestSAVE[FREQ][valueRIGHT + 1])
      {
        for (int ii = 0; ii <= 4; ii++)
        {
          float SWAP1 = hearingTestSAVE[ii][valueRIGHT];
          hearingTestSAVE[ii][valueRIGHT] = hearingTestSAVE[ii][valueRIGHT + 1];
          hearingTestSAVE[ii][valueRIGHT + 1] = SWAP1;
        }
      }
    }
  }
  if ((SerialTermON == true) && ((debugON == true) || (PINdebugON == true)))
  {
    Serial.print("Sorted Array: ");
    for (int i = 0; i < 19; i++)
    {
      Serial.print(hearingTestSAVE[FREQ][i]);
      Serial.print(",");
    }
    Serial.println("");
    Serial.print("Max Number: ");
    Serial.print(hearingTestSAVE[FREQ][8]);
    Serial.println("");
    Serial.print("Min Number: ");
    Serial.print(hearingTestSAVE[FREQ][0]);
    Serial.println("");
    Serial.println(F("Nachher!"));
    hearingTestPRINT();
    delay(1000); //Make sure we have enough time to see the output before starting the demo again.
  }
}


bool Octav = false;
bool Terz = false;
void HeaingTestManuellOctav()
{
  ManualHearingTestFlag = true;
  Octav = true;
  Terz = false;
  selectedMode(0);
}

void HeaingTestManuellTerz()
{
  ManualHearingTestFlag = true;
  Terz = true;
  Octav = false;
  selectedMode(1);
}

void selectedMode(int S)
{
  if(!S)
  {
    Serial.println("Octav Mode..");
    float volumeVal = -100.00 ;
    VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);  //set the voice to the maximum
    SeiteTEST = switchDirection(1);                      //set Left
    String hearSide = (SeiteTEST == 1 ) ? "Links" : "Rechts" ;
    oledShowFreq(hearSide, f_countOctav[f_counter], -120); //shows in the oled
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    //turn on frequency 63 at -100 db
    Sinus_GeneratorMod(volumeVal,f_countOctav[0]);           //start generating the sine wave
    
    activatUserInputMode(0,101,applyOctav,showOctav,18);     //start the input mode
  }
  else
  {
    Serial.println("Terz Mode..");
    float volumeVal = -100.00 ;
    VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);  //set the voice to the maximum
    SeiteTEST = switchDirection(1);                      //set Left
    String hearSide = (SeiteTEST == 1 ) ? "Links" : "Rechts" ;
    
    oledShowFreq(hearSide, f_countTerz[f_counter],  -120);
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    Sinus_GeneratorMod(volumeVal,f_countTerz[0]);
    activatUserInputMode(0,101,applyTerz,showTerz,62);
  }
}

void applyOctav(int16_t *sel,uint8_t length)
{ 
   //mapper from 9 to 31 
  int mapper[]={5, 8, 11, 14, 17, 20, 23, 26, 29 };

  /*TODO:
  1-split long array into 2 small arrays 
  2-convert each on to propreate dB values
  3-Store each array in it's location */
  float temp[2][31];
  int k = 0 ;
  for (int j = 1; j < 3; j++)
  {
    for (int i = 0; i < 9; i++)
    {
      TODO: //Store data in hearingTestSAVE according to Index 
      //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
      temp[j][i] = (float) (*(sel + k) - 120);
      if( k < 9)
        Serial.printf("%i-freq : %.1f , templinks vol: %.1f  \n",mapper[i],f_countOctav[i], temp[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , temprechts vol: %.1f \n",mapper[i],f_countOctav[i], temp[j][i]) ;
      hearingTestSPL[j][mapper[i]] = (float) (*(sel + k) ) ;
      hearingTestHL[j][mapper[i]] = hearingTestSPL[j][mapper[i]] - hearingTestSAVE[j + 2][mapper[i]];
      hearingTestSAVE[j][mapper[i]] = hearingTestHL[j][mapper[i]];
      if( k < 9)
        Serial.printf("%i-freq : %.1f , links vol: %.1f  \n",mapper[i],f_countOctav[i], hearingTestSAVE[j][mapper[i]]) ; 
      else
        Serial.printf("%i-freq : %.1f , rechts vol: %.1f \n",mapper[i],f_countOctav[i], hearingTestSAVE[j][mapper[i]]) ;
      Serial.println("---------------------------------------------------");
      k++;
    }
  }
  k = 0;
  //nothings to hear
  MainDirectMode( );
  //or MainPersonalizedMode();
}

int seite = 1; // Strat Left
void showOctav(int16_t sel)
{
  Serial.printf("showOctav saved value: %i at index: %i \n",sel,selectedValIdx);
  if(selectedValIdx == 9)  //check right 
    seite = switchDirection(2);
  //1- scale selected value index to volume in dB
  float volumeVal = (float) sel - 120 ; 
  //2_Print on the OLED
  String hearSide = (seite == 1) ? "Links" : "Rechts" ;
  oledShowFreq(hearSide, f_countOctav[selectedValIdx], volumeVal);
  //3_send dB Value to Hardware
  
  Sinus_GeneratorMod(volumeVal,f_countOctav[selectedValIdx]);  
  
}


void applyTerz(int16_t *sel,uint8_t length)
{ 
  
  /*TODO:
  1-split long array into 2 small arrays 
  2-convert each on to propreate dB values
  3-Store each array in it's location */
  float temp[2][31];
  int k = 0 ;
  for (int j = 1; j < 3; j++)
  {
    for (int i = 0; i < 31; i++)
    {
      TODO: //Store data in hearingTestSAVE according to Index 
      //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
      temp[j][i] = (float) (*(sel + k) - 120);
      if( k < 31 )
        Serial.printf("%i-freq : %.1f , templinks vol: %.1f  \n",i,f_countOctav[i], temp[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , temprechts vol: %.1f \n",i,f_countOctav[i], temp[j][i]) ;
      hearingTestSPL[j][i] = (float) (*(sel + k) ) ;
      hearingTestHL[j][i] = hearingTestSPL[j][i] - hearingTestSAVE[j + 2][i];
      hearingTestSAVE[j][i] = hearingTestHL[j][i];
      if( k < 31 )
        Serial.printf("%i-freq : %.1f , links vol: %.1f  \n",i,f_countOctav[i], hearingTestSAVE[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , rechts vol: %.1f \n",i,f_countOctav[i], hearingTestSAVE[j][i]) ;
      Serial.println("---------------------------------------------------");
      k++;
    }
  }
  k = 0;
  MainDirectMode( );

  // for (int i = 0; i < 31; i++)
  // {
  //   TODO: //Store data in hearingTestSAVE according to Index 
  //   //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
  //   hearingTestSAVE[1][i] = (float) (*(sel + i) -100.0);
  //   hearingTestSAVE[2][i] = (float) (*(sel + i + 30) - 100.0);
  //   Serial.printf("%i-freq : %.1f , links vol: %.1f , rechts vol: %.1f \n",i,hearingTestSAVE[0][i], hearingTestSAVE[1][i], hearingTestSAVE[2][i]) ; 
     
  // }
}

void showTerz(int16_t sel)
{
  Serial.printf("showTerz saved value: %i at index: %i \n",sel,selectedValIdx);
  if(selectedValIdx == 31)  //check right 
      seite = switchDirection(2);
  //1- scale selected value index to volume in dB
  float volumeVal = (float) sel - 120 ; 
  //2_Print on the OLED
  String hearSide = (seite == 1) ? "Links" : "Rechts" ;
  oledShowFreq(hearSide, f_countTerz[selectedValIdx], volumeVal);
  //3_send dB Value to Hardware
  
  Sinus_GeneratorMod(volumeVal,f_countTerz[selectedValIdx]); 
  
}

int switchDirection(int D)
{
  if (D == 1)
  {
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    testLINKS = true;
    testRECHTS = false;
    return 1;
  }
  else if(D == 2)
  {
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS, 2);
    testLINKS = false;
    testRECHTS = true;
    return 2;
  }
}
