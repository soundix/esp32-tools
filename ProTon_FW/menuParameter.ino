/*
 *    menuParameter.ino
 * Version 0.3.0.2
 */

#include "menuLib.h"
#include <Arduino.h>

#define LIST_SIZE 7

uint8_t user1FLAG = true;
uint8_t user2FLAG = false;
uint8_t user3FLAG = false;
uint8_t linFLAG = true;
uint8_t per1FLAG = false;
uint8_t per2FLAG = false;
uint8_t per3FLAG = false;
uint8_t compFLAG = false;
uint8_t bassFLAG = false;
bool int20FLAG = false;
bool int40FLAG = false;
bool int60FLAG = true;
bool int80FLAG = false;
bool int100FLAG = false;

extern char menuItem_1[];
extern char menuItem_1_1[];
extern char menuItem_1_1_1[];
extern char menuItem_1_1_2[];
extern char menuItem_1_1_3[];
extern char menuItem_1_2[];
extern char menuItem_1_3[];
extern char menuItem_1_3_1[];
extern char menuItem_1_3_2[];
extern char menuItem_1_3_3[];

extern char menuItem_2[];
extern char menuItem_2_1[];
extern char menuItem_2_2[];
extern char menuItem_2_2_1[];
extern char menuItem_2_2_2[];
extern char menuItem_2_2_3[];
extern char menuItem_2_2_4[];
extern char menuItem_2_2_5[];

extern char menuItem_3[];
extern char menuItem_3_1[];
extern char menuItem_3_1_1[];
extern char menuItem_3_1_1_1[];
extern char menuItem_3_1_1_2[];
extern char menuItem_3_1_1_3[];
extern char menuItem_3_1_2[];
extern char menuItem_3_1_2_1[];
extern char menuItem_3_1_2_2[];
extern char menuItem_3_2[];
extern char menuItem_3_2_1[];
extern char menuItem_3_2_2[];

extern char menuItem_4[];
extern char menuItem_4_1[];
extern char menuItem_4_2[];
extern char menuItem_4_2_1[];
extern char menuItem_4_2_2[];
extern char menuItem_4_2_3[];
extern char menuItem_4_3[];
extern char menuItem_4_4[];

extern char menuItem_5[];
extern char menuItem_5_1[];
extern char menuItem_5_1_[];
extern char menuItem_5_1_1[];
extern char menuItem_5_1_1_1[];
extern char menuItem_5_1_1_2[];
extern char menuItem_5_1_1_3[];
extern char menuItem_5_1_2[];
extern char menuItem_5_1_2_1[];
extern char menuItem_5_1_3[];
extern char menuItem_5_1_3_1[];
extern char menuItem_5_1_3_2[];
extern char menuItem_5_1_3_3[];
extern char menuItem_5_2[];
extern char menuItem_5_2_1[];
extern char menuItem_5_2_1_1[];
extern char menuItem_5_2_2[];
extern char menuItem_5_2_2_1[];
extern char menuItem_5_2_2_2[];
extern char menuItem_5_2_3[];
extern char menuItem_5_2_3_1[];
extern char menuItem_5_2_3_2[];
extern char menuItem_5_2_4[];
extern char menuItem_5_2_4_1[];
extern char menuItem_5_2_4_2[];
extern char menuItem_5_2_5[];
extern char menuItem_5_2_5_1[];
extern char menuItem_5_2_5_2[];
extern char menuItem_5_2_5_3[];
extern char menuItem_6[];
extern char menuItem_7[];
extern char menuItem_7_1[];
extern char menuItem_7_2[];
extern char menuItem_7_3[];
extern char menuItem_7_4[];
extern char menuItem_7_5[];

extern uint8_t SprachAuswahl;
extern double dBVoiceVolum;
extern uint8_t IntensityChoice;
extern uint8_t headphone;

extern uint8_t SoundSelect;
//extern uint8_t restoreflagHearingTest;

//items handler singtures:
void storeProfileMemory_1(void);  //menuItem_1_1_1
void storeProfileMemory_2(void);  //menuItem_1_1_2
void storeProfileMemory_3(void);  //menuItem_1_1_3
void newProfile(void);            //menuItem_1_2
void deleteProfileMemory_1(void); //menuItem_1_3_1
void deleteProfileMemory_2(void); //menuItem_1_3_2
void deleteProfileMemory_3(void); //menuItem_1_3_3
void linear(void);                //menuItem_2_1
void activateCurve_1(void);       //menuItem_2_2_1
void activateCurve_2(void);       //menuItem_2_2_2
void activateCurve_3(void);       //menuItem_2_2_3
void Menu_3_1_1_1_Handler(void);
void Menu_3_1_1_2_Handler(void);
void Menu_3_1_1_3_Handler(void);
void Menu_3_1_2_1_Handler(void);
void Menu_3_1_2_2_Handler(void);
void compressorOn(void);  //menuItem_3_2_1
void compressorOff(void); //menuItem_3_2_2
void startHearingTest(void);  //hearingTest Automatik
void startHearingTestManulOctav(void);   //HeaingTest ManuellOctav
void startHearingTestManulTerz(void);    //HeaingTest ManuellTerz   
void Menu_4_2_1_Handler(void); //to be deleted
void Menu_4_2_2_Handler(void); //to be deleted
void Menu_4_2_3_Handler(void); //to be deleted
void Menu_5_1_2_Handler(void);
void changeLcdContrastMinimum(void); //menuItem_5_1_1     30%
void changeLcdContrastMidium(void);  //menuItem_5_1_2
void changeLcdContrastHigh(void);    //menuItem_5_1_3
void Menu_5_1_3_1_Handler(void);
void Menu_5_1_3_2_Handler(void);
void Menu_5_1_3_3_Handler(void);
void Menu_5_2_1_Handler(void);
void Menu_5_2_2_1_Handler(void);
void Menu_5_2_2_2_Handler(void);
void Menu_5_2_3_1_Handler(void);
void Menu_5_2_3_2_Handler(void);
void Menu_5_2_4_1_Handler(void);
void Menu_5_2_4_2_Handler(void);
void Menu_5_2_5_1_Handler(void);
void Menu_5_2_5_2_Handler(void);
void Menu_5_2_5_3_Handler(void);
void intensityLevel20(void);
void intensityLevel40(void);
void intensityLevel60(void);
void intensityLevel80(void);
void intensityLevel100(void);
void Soundwahl1(void);
void Soundwahl2(void);
void Soundwahl3(void);
void Soundwahl4(void);

void MultiUserInputHandler(void);
void SingleUserInputHandler(void);

/*****Laden*****/
listItem Menu_1_1[3] = {
    {menuItem_1_1_1, NULL, 0, &storeProfileMemory_1}, //Speicherplatz 1
    {menuItem_1_1_2, NULL, 0, &storeProfileMemory_2}, //Speicherplatz 2
    {menuItem_1_1_3, NULL, 0, &storeProfileMemory_3}, //Speicherplatz 3
};

/*****Löschen*****/
listItem Menu_1_3[3] = {
    {menuItem_1_3_1, NULL, 0, &deleteProfileMemory_1}, //Speicherplatz 1
    {menuItem_1_3_2, NULL, 0, &deleteProfileMemory_2}, //Speicherplatz 2
    {menuItem_1_3_3, NULL, 0, &deleteProfileMemory_3}, //Speicherplatz 3
};

/*****Benutzerprofil*****/
listItem Menu_1[3] = {
    {menuItem_1_1, Menu_1_1, 3, NULL},    //Laden
    {menuItem_1_2, NULL, 0, &newProfile}, //neues Profil
    {menuItem_1_3, Menu_1_3, 3, NULL},    //löschen
};

/*****personalisiert*****/
listItem Menu_2_2[3] = {
    {menuItem_2_2_1, NULL, 0, &activateCurve_1}, //Kurve 1
    {menuItem_2_2_2, NULL, 0, &activateCurve_2}, //Kurve 2
    {menuItem_2_2_3, NULL, 0, &activateCurve_3}, //Kurve 3
};

/****Betriebsmodi*****/
listItem Menu_2[2] = {
    {menuItem_2_1, NULL, 0, &linear},  //linear
    {menuItem_2_2, Menu_2_2, 3, NULL}, //personalisiert
};

/*****EQ******/
listItem Menu_3_1_1[3] = {
    {menuItem_3_1_1_1, NULL, 0, &Menu_3_1_1_1_Handler}, //Jazz
    {menuItem_3_1_1_2, NULL, 0, &Menu_3_1_1_2_Handler}, //Rock
    {menuItem_3_1_1_3, NULL, 0, &Menu_3_1_1_3_Handler}, //Flat
};

/*****Dynamic Bass*****/
listItem Menu_3_1_2[2] = {
    {menuItem_3_1_2_1, NULL, 0, &Menu_3_1_2_1_Handler}, //An
    {menuItem_3_1_2_2, NULL, 0, &Menu_3_1_2_2_Handler}, //Aus
};
/*****Audio Presets****/
listItem Menu_3_1[2] = {
    {menuItem_3_1_1, Menu_3_1_1, 3, NULL}, //EQ
    {menuItem_3_1_2, Menu_3_1_2, 2, NULL}, //Dynamic DynamicBassBypassAddr
};

/****Kompressor****/
listItem Menu_3_2[2] = {
    {menuItem_3_2_1, NULL, 0, &compressorOn},  //An
    {menuItem_3_2_2, NULL, 0, &compressorOff}, //Aus
};

/*****Soundeinstellungen*****/
listItem Menu_3[2] = {
    {menuItem_3_1, Menu_3_1, 2, NULL}, //Audio Presets
    {menuItem_3_2, Menu_3_2, 2, NULL}, //Kompressor
};

/****Rauschgenerator*****/
listItem Menu_4_2[3] = {
    {menuItem_4_2_1, NULL, 0, &Menu_4_2_1_Handler}, //An
    {menuItem_4_2_2, NULL, 0, &Menu_4_2_2_Handler}, //Aus
    {menuItem_4_2_3, NULL, 0, &Menu_4_2_3_Handler}, //Automatik
};
/****Hörtest*****/
listItem Menu_4[4] = {
    {menuItem_4_1, NULL, 0, &startHearingTest}, //starten
    {menuItem_4_3, NULL, 0, &startHearingTestManulOctav}, //Octav starten
    {menuItem_4_4, NULL, 0, &startHearingTestManulTerz}, //Terz starten
    {menuItem_4_2, Menu_4_2, 3, NULL},          //Rauschgenerator
};

/*****changeLcdContrast********/
listItem Menu_5_1_1[3] = {
    {menuItem_5_1_1_1, NULL, 0, &changeLcdContrastMinimum}, //schnell
    {menuItem_5_1_1_2, NULL, 0, &changeLcdContrastMidium},  //nach DirectHandler
    {menuItem_5_1_1_3, NULL, 0, &changeLcdContrastHigh},    //genau
};

/******Startlautstärke*****/
listItem Menu_5_1_3[3] = {
    {menuItem_5_1_3_1, NULL, 0, &Menu_5_1_3_1_Handler}, //Minimal
    {menuItem_5_1_3_2, NULL, 0, &Menu_5_1_3_2_Handler}, //Standard
    {menuItem_5_1_3_3, NULL, 0, &Menu_5_1_3_3_Handler}, //nach letztem Start
};

/****Dynamic Bass An AUS*****/
listItem Menu_5_2_2[2] = {
    {menuItem_5_2_2_1, NULL, 0, &Menu_5_2_2_1_Handler}, //Dynamic Bass An
    {menuItem_5_2_2_2, NULL, 0, &Menu_5_2_2_2_Handler}, //Dynamic Bass Aus
};

/*****Rauschgenerator AN AUS******/
listItem Menu_5_2_3[2] = {
    {menuItem_5_2_3_1, NULL, 0, &Menu_5_2_3_1_Handler}, //Rauschgenerator AN
    {menuItem_5_2_3_2, NULL, 0, &Menu_5_2_3_2_Handler}, //Rauschgenerator AUS
};

/*****Sprache******/
listItem Menu_5_2_4[2] = {
    {menuItem_5_2_4_1, NULL, 0, &Menu_5_2_4_1_Handler}, //Deutsch
    {menuItem_5_2_4_2, NULL, 0, &Menu_5_2_4_2_Handler}, //Englisch
};

/*****Information******/
listItem Menu_5_2_5[3] = {
    {menuItem_5_2_5_1, NULL, 0, &Menu_5_2_5_1_Handler}, //Versionsnummer
    {menuItem_5_2_5_2, NULL, 0, &Menu_5_2_5_2_Handler}, //Seriennummer
    {menuItem_5_2_5_3, NULL, 0, &Menu_5_2_5_3_Handler}, //Support
};
/****Generell******/
listItem Menu_5_1[3] = {
    {menuItem_5_1_1, Menu_5_1_1, 3, NULL},          //Hörtest
    {menuItem_5_1_2, NULL, 0, &Menu_5_1_2_Handler}, //max. Lautstärke
    {menuItem_5_1_3, Menu_5_1_3, 3, NULL},          //Startlautstärke
};
/******Expertenmodus*******/
listItem Menu_5_2[5] = {
    {menuItem_5_2_1, NULL, 0, &Menu_5_2_1_Handler}, //Schrittweite
    {menuItem_5_2_2, Menu_5_2_2, 2, NULL},          //Dynamic Bass
    {menuItem_5_2_3, Menu_5_2_3, 2, NULL},          //Rauschgenerator
    {menuItem_5_2_4, Menu_5_2_4, 2, NULL},          //Sprache
    {menuItem_5_2_5, Menu_5_2_5, 3, NULL},          //Information
};

/*****Einstellungen*******/
listItem Menu_5[2] = {
    {menuItem_5_1, Menu_5_1, 3, NULL}, //Generell
    {menuItem_5_2, Menu_5_2, 5, NULL}, //Expertenmodus
};

listItem Menu_9[5] = {
    {menuItem_9_1, NULL, 0, &intensityLevel20},  //Intensität 20%
    {menuItem_9_2, NULL, 0, &intensityLevel40},  //Intensität 40%
    {menuItem_9_3, NULL, 0, &intensityLevel60},  //Intensität 60%
    {menuItem_9_4, NULL, 0, &intensityLevel80},  //Intensität 80%
    {menuItem_9_5, NULL, 0, &intensityLevel100}, //Intensität 100%
};

listItem Menu_10[] = {
    {menuItem_10_1, NULL, 0, &Soundwahl1}, //Intensität 20%
    {menuItem_10_2, NULL, 0, &Soundwahl2}, //Intensität 40%
    {menuItem_10_3, NULL, 0, &Soundwahl3}, //Intensität 60%
    {menuItem_10_4, NULL, 0, &Soundwahl4}, //Intensität 80%
};

/*****Hauptmenue*****/
listItem mainList[LIST_SIZE] = {
    // {menuItem_8, NULL, NULL, &SingleUserInputHandler},    // Test Single UserInput
    // {menuItem_7, NULL, NULL, &MultiUserInputHandler},    // Test Multi UserInput
    //{menuItem_6, NULL, NULL, &startHearingTest}, // Hörtest starten
    {menuItem_3, Menu_3, 2, NULL},   //Soundeinstellungen
    {menuItem_9, Menu_9, 5, NULL},   // Intensität (20%, 40%, 60%, 80%, 100%)
    {menuItem_10, Menu_10, 4, NULL}, // Soundauswahl
    {menuItem_1, Menu_1, 3, NULL},   //Benutzerprofil
    {menuItem_2, Menu_2, 2, NULL},   //Betriebsmodi
    {menuItem_4, Menu_4, 4, NULL}, //Hörtest
    {menuItem_5, Menu_5, 2, NULL}, //Einstellungen
};

Menu mainMenu = Menu(mainList, LIST_SIZE, mxDeapth);

String englishLetters[26] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                             "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

void MultiUserInputHandler(void)
{
  activatUserInputMode(0, 25, selectName, showLetters, 5);
  showLetters(0);
}

void selectName(int16_t *sel, uint8_t length)
{
  char arr[length];
  Serial.println(" ============= ");
  for (uint16_t i = 0; i < length; i++)
  {
    Serial.print(englishLetters[sel[i]]);
    arr[i] = englishLetters[sel[i]].charAt(0);
  }
  Serial.println("");
  Serial.println(" ============= ");
  showSelectedItem("entered input:", String(arr));
  delay(1000);
}

void showLetters(int16_t sel)
{
  String showMsg = "";
  if (sel == 0)
  {
    showMsg = englishLetters[25] + "." + englishLetters[sel] + "." + englishLetters[sel + 1];
  }
  else if (sel == 25)
  {
    showMsg = englishLetters[sel - 1] + "." + englishLetters[sel] + "." + englishLetters[0];
  }
  else
  {
    showMsg = englishLetters[sel - 1] + "." + englishLetters[sel] + "." + englishLetters[sel + 1];
  }
  showSelectedItem("UserInput", showMsg);
  //return String(showMsg);
}

uint16_t level[5] = {20, 40, 60, 80, 100};

void SingleUserInputHandler(void)
{
  /*
   activatUserInputMode(0, 4, selectLevel, showLevel, 1);
  showLevel(0);
  */
}

void selectLevel(int16_t *sel, uint8_t length)
{
  /*
  uint16_t i = 0;
  uint16_t arr[length];
  Serial.println(" ============= ");
  for (i = 0; i < length; i++)
  {
    Serial.print(level[sel[i]]);
    arr[i] = level[sel[i]];
  }

if (i == 0)
{
   intensityLevel20();
}

else if (i == 1)
{
   intensityLevel40();
}

else if (i == 2)
{
   intensityLevel60();
}

else if (i == 3)
{
   intensityLevel80();
}

else if (i == 4)
{
   intensityLevel100();
}

  Serial.println("");
  Serial.println(" ============= ");
  showSelectedItem("entered input:", String(arr[0]));
  delay(1000);

  */
}

void showLevel(int16_t sel)
{
  /*
  String showMsg = "";
  if (sel == 0)
    showMsg = String(level[4]) + "." + String(level[sel]) + "." + String(level[sel + 1]);
  else if (sel == 4)
    showMsg = String(level[sel - 1]) + "." + String(level[sel]) + "." + String(level[0]);
  else
    showMsg = String(level[sel - 1]) + "." + String(level[sel]) + "." + String(level[sel+1]);
    
  showSelectedItem("UserInput", showMsg);
  //return String(showMsg);
  */
}

void newProfile(void) //menuItem_1_2
{
  // neues Profil
}

void storeProfileMemory_1(void) //menuItem_1_1_1
{
  //Profil laden Speicherplatz 1
  //user1FLAG = true;

  VersionsnummerAbfrage();
}

void storeProfileMemory_2(void) //menuItem_1_1_2
{
  //Profil laden Speicherplatz 2

  //user2FLAG = true;
}

void storeProfileMemory_3(void) //menuItem_1_1_3
{
  //Profil laden Speicherplatz 3
  //user3FLAG = true;
}

void deleteProfileMemory_1(void) //menuItem_1_3_1
{
  //Profil löschen Speicherplatz 1
}

void deleteProfileMemory_2(void) //menuItem_1_3_2
{
  //Profil löschen Speicherplatz 2
}

void deleteProfileMemory_3(void) //menuItem_1_3_3
{
  //Profil löschen Speicherplatz 3
}

void linear(void) //menuItem_2_1
{
  //linear
  linFLAG = true;
  per1FLAG = false;
  per2FLAG = false;
  per3FLAG = false;

  MainDirectMode();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainDynamicBassBypassOFF();
}

void activateCurve_1(void) //menuItem_2_2_1
{
  //personalisiert Kurve 1
  linFLAG = false;
  per1FLAG = true;
  per2FLAG = false;
  per3FLAG = false;

  preset = 1;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void activateCurve_2(void) //menuItem_2_2_2
{
  //personalisiert Kurve 2
  // EqConfigurations();
  linFLAG = false;
  per1FLAG = false;
  per2FLAG = true;
  per3FLAG = false;

  preset = 2;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void activateCurve_3(void) //menuItem_2_2_3
{
  //personalisiert Kurve 3
  linFLAG = false;
  per1FLAG = false;
  per2FLAG = false;
  per3FLAG = true;
  preset = 3;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void Menu_3_1_1_1_Handler(void)
{
  //EQ Jazz
}

void Menu_3_1_1_2_Handler(void)
{
  // EQ Rock
}

void Menu_3_1_1_3_Handler(void)
{
  //EQ Flat
}

void Menu_3_1_2_1_Handler(void)
{
  //Dynamic Bass An

  MainDynamicBassBypassON();
}

void Menu_3_1_2_2_Handler(void)
{
  //Dynamic Bass Aus

  MainDynamicBassBypassOFF();
}

void compressorOn(void)
{
  //Kompressor An

  PersonalizedMultibandCompressorBypassMultibandCompressor();
  //PersonalizedMultibandCompressorBypassRMSCompressor();
}

void compressorOff(void)
{
  // Kompressor Aus

  PersonalizedMultibandCompressorBypassCompOFF();
}

void startHearingTest(void)
{
  //Hörtest starten
  //hearingSinus();
  //hearingSinusMod();               //Modifiziert
  //restoreflagHearingTest = true;
  MainHearingTestMode();
  hearingTest();
}
void startHearingTestManulOctav(void)
{
  MainHearingTestMode();
  HeaingTestManuellOctav();
  
}
void startHearingTestManulTerz(void)
{
  MainHearingTestMode();
  HeaingTestManuellTerz();
}

void Menu_4_2_1_Handler(void)
{
  //Rauschgenerator An
}

void Menu_4_2_2_Handler(void)
{
  //Rauschgenerator Aus
}

void Menu_4_2_3_Handler(void)
{
  //Rauschgenerator Automatik
}

void Menu_5_1_2_Handler(void)
{
  //max. Lautstärke
}

void changeLcdContrastMinimum(void)
{
  //Hörtest schnell
}

void changeLcdContrastMidium(void)
{
  //Hörtest nach DIN Norm
}

void changeLcdContrastHigh(void)
{
  //Hörtest genau
}

void Menu_5_1_3_1_Handler(void)
{
  //Startlautstärke Minimal
}

void Menu_5_1_3_2_Handler(void)
{
  //Startlautstärke Standard
}

void Menu_5_1_3_3_Handler(void)
{
  //Startlautstärke nach letztem Start
}

void Menu_5_2_1_Handler(void)
{
  //Schrittweite
}

void Menu_5_2_2_1_Handler(void)
{
  //Dynamic Bass An
  MainDynamicBassBypassOFF();
}

void Menu_5_2_2_2_Handler(void)
{
  //Dynamic Bass Aus

  MainDynamicBassBypassON();
}

void Menu_5_2_3_1_Handler(void)
{
  headphone = true;
  //Rauschgenerator An
}

void Menu_5_2_3_2_Handler(void)
{
  headphone = false;
  //Rauschgenerator Aus
}

void Menu_5_2_4_1_Handler(void)
{
  //Deutsch
  SprachAuswahl = 0;
  LanguageSelect();
}

void Menu_5_2_4_2_Handler(void)
{
  //Englisch
  SprachAuswahl = 1;
  LanguageSelect();
}

void Menu_5_2_5_1_Handler(void)
{
  //Versionsnummer
  VersionsnummerAbfrage();
}

void Menu_5_2_5_2_Handler(void)
{
  //Seriennummer
}

void Menu_5_2_5_3_Handler(void)
{
  //Support
}

void intensityLevel20(void)
{
  int20FLAG = true;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 0;
  MainPersonalizedMode();
  Serial.println("Intensität 20%");
}

void intensityLevel40(void)
{
  int20FLAG = false;
  int40FLAG = true;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 1;
  MainPersonalizedMode();
  Serial.println("Intensität 40%");
}

void intensityLevel60(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = true;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 2;
  MainPersonalizedMode();
  Serial.println("Intensität 60%");
}

void intensityLevel80(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = true;
  int100FLAG = false;

  IntensityChoice = 3;
  MainPersonalizedMode();
  Serial.println("Intensität 80%");
}

void intensityLevel100(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = true;

  IntensityChoice = 4;
  MainPersonalizedMode();
  Serial.println("Intensität 80%");
}

void Soundwahl1(void)
{
  SoundSelect = 0;
  Serial.println("Sound1 gewählt");
}

void Soundwahl2(void)
{
  SoundSelect = 1;
  Serial.println("Sound2 gewählt");
}

void Soundwahl3(void)
{
  SoundSelect = 2;
  Serial.println("Sound3 gewählt");
}

void Soundwahl4(void)
{
  SoundSelect = 3;
  Serial.println("Sound4 gewählt");
}

/*
void DirectHandler(void)
{
  Serial.println(F("Direct"));
  muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 1, 4);
}

void ParaHandler(void)
{
  Serial.println(F("Para"));
  muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 2, 4);
}

void HearingtestHandler(void)
{
  Serial.print(" handler of mainMenu2");
  Serial.println(F("Hearing Test"));
  //muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 4, 4);
  restoreflagHearingTest = true;
  hearingTest();
}
void NoisegeratorHandler(void)
{
  //Serial.print(" handler of mainMenu3");
  Serial.println(F("Noise Test"));
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 4);
  restoreflagNoise = true;
  //NoiseGenerator();
}

void subMenuItem1(void)
{
  Serial.print(" handler of subMenuItem1");
}
void subMenuItem3(void)
{
  Serial.print(" handler of subMenuItem3");
  LanguageSelect();
  Serial.println(" ");
  Serial.print(menuItem_1);
}
void subMenuItem4(void)
{
  Serial.print(" handler of subMenuItem4");
}

void sub_subMenuItem1(void)
{
  Serial.print(" handler of sub-subMenuItem1");
}
*/
