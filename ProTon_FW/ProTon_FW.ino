/*
* ProTon_FW.ino
* Version 0.3.0.3
*/

//#define CS43131 //Hardware config.
//#define EEPROM
//#define Drachenfels
#include "AidaDSP.h"
#include "menuLib.h"
#include <Arduino.h>
#include <WiFi.h>
/*
* für ADAU1701 --> #include "AidaDSP.h"
* für ADAU1442 --> #include "AidaDSP2.h"
*/
#include "Define_adrress.h"
#include "parameter.h"
#include "OledUtility.h"
#include "DataStore.h"

//Config ADAU1701 over I2C
#ifndef EEPROM //define EEPROM to use EEPROM
#include "SoftI2C_mod.h"
#include "TxBuffer_IC_1_plus.h"
#define SDA_PIN 21
#define SCL_PIN 22
#endif

#ifdef CS43131
#include "CS43131_config.h"
#endif


// DEFINES USER INTERFACE
#define VOLMAX 0.00
#define VOLMIN -99.00
//#define VolumeINIT 80
//#define VOLMIN -120.00

#define max_number_of_pulses 96
#define encoderStep 0.5
#define EncoderPulsesPerClick 1

// DEFINES I/O

/*
* für ESP32:
ESP 32 WRROM - DEV Kit
#define encoderPin1 35
#define encoderPin2 32
#define encoderSwitchPin 33 //push button switch


* für MEGA2560:
#define encoderPin1 2
#define encoderPin2 3
#define encoderSwitchPin 4 //push button switch
*/

#define PIN_LED 13
#define PIN_LED1 7
#define PIN_DEBUG 14
#define PIN_AudioDetect 53
#define encoderPin1 35
#define encoderPin2 32
#define encoderSwitchPin 33 //push button switch
#define SELFBOOT 17
#define WP 18
#define ADAU_RST 5
#define LevelShift 12



//function prototyping:
void updateEncoder();
void initHardWare(void);
void shortPressHandler(void);
void longPressHandler(void);
void extremeLongPressHandler(void);
void muteVoice(bool muteVal);
void changeVoiceVolum(void);
double dbVlaueToEncoder(double dbVal);
double encoderToDBVoice(double minval, double maxval, double pulses);
bool ManualHearingTestFlag = false;
//mode activition functions:
void activitNormalMode(void);
void activitEditMode(void);
void activitMenuMode(void);

//extern void VolumeMonoConvertANDsend(int, int);
//extern void VolumeStereoConvertANDsend(int, int);

#ifndef EEPROM
bool i2c_write_block(byte ic_addr, word reg_addr, byte * data, int data_length);
void config_adau1701();
#endif

#ifdef CS43131
void config_cs43131(byte ic_addr, byte * data, int data_length);
#endif


void activatUserInputMode(int16_t minVal, int16_t maxVal, void (*InputCompleteHandler)(int16_t *, uint8_t), void (*InputScrolHandler)(int16_t), int8_t selectedValCount);
void saveLastMode(void);
void restoreLastMode(void);

//TODO: debug function remove it later;
void printCurrentMODE(void);
void LCDprintFLAG(void);
void printOnTerminal(String);

void initBluetooth(void);
void bluetoothREAD(void);

volatile bool vertuelSwitch = false;

//Encoder staff:
//1-Normal mode encoder vars:
volatile uint32_t nm_lastEncoded = 0b11;
volatile double nm_encoderValue = 0;
volatile double nm_lastencoderValue = 0;
double dBVoiceVolum;

//2-Edit mode encoder vars:
volatile uint32_t em_lastEncoded = 0b11;
volatile double em_encoderValue = 0;
volatile double em_lastencoderValue = 0;

//3-Menu mode encoder vars:
volatile uint32_t mm_lastEncoded = 0b11;
volatile double mm_encoderValue = 0;
volatile double mm_lastencoderValue = 0;

//4-User Input mode encoder vars:
volatile uint32_t ur_lastEncoded = 0b11;
volatile double ur_encoderValue = 0;
volatile double ur_lastencoderValue = 0;

//double dBVoiceVolum;

//Mode Flags:
volatile bool NORMAL_MODE_Flag = true; //normal mode: mute\unmute + change voice volume.
volatile bool EDIT_MODE_Flag = false;  //edit mode: simple menu to set basic function.
volatile bool MENU_MODE_Flag = false;  //menu mode: extanded menu to set all functions.
volatile bool USER_INPUT_Flag = false;
//volatile bool MUTE_Flag = false;      //moved to parameter.h
volatile bool updateContentFlag = false; //to update terminal & lcd contents.
volatile bool refreshEditMenuTrigger = false;
int lastMode = 0;
//lcd things
extern Menu mainMenu;
int editMenuSelectedIndex = 0;
extern String screenPlatz[5];
extern String currentUserOptions[5];
extern bool Octav;
//user input mode staff:
int16_t maxSelectedVal = 0;
int16_t minSelectedVal = 0;
int16_t tempSelectedVal = 0;
int16_t *selectedValPtr;
uint16_t selectedValIdx = 0;
uint16_t selectedValCnt = 0;
void (*userInputFunc)(int16_t *sel, uint8_t length);
void (*showInputFunc)(int16_t);

#ifndef EEPROM
SoftI2C * i2c;
#endif

void setup()
{
  
  //turn off bt & wifi
  WiFi.mode(WIFI_OFF);
  btStop();
  //btStart();

  Serial.begin(SerialBAUD);
  while (!Serial)
    ;

  
  initHardWare();

  oledInit();
  // eepromInit();
  #ifndef Drachenfels
  OledPrintLogo();
  delay(2000);
  myPerfectAudio("my.");
  delay(1000);
  myPerfectAudio("perfect.");
  delay(1000);
  myPerfectAudio("audio.");
  delay(1000);
  
  //oledU8g2Test();
  oledStartUp();
  #endif
  printOnTerminal("starting setup..");
  BLEInit();
  changeVoiceVolum();
  //activatUserInputMode(0, 25, userSelectHandler, showOption, 10);
  //activatUserInputMode(0, 2, userSelectHandler, showOption, 1);

  printCurrentMODE();
  
  //initBluetooth();
  digitalWrite(LevelShift, HIGH);   //to enable TSX0108 LevelShift IC
}


void loop()
{
  //bluetoothREAD();

  int held = 0;
  while (digitalRead(encoderSwitchPin) == LOW && held < 40) // war 20
  {
    delay(100);
    held++;
  }
  if (held < MenuPressDelayMenu && held > 0)
  {
    shortPressHandler();
  }
  else if (held >= MenuPressDelayMenu && held < MenuPressDelayHOME) // MenuPressDelayHOME war 20
  {
    longPressHandler();
  }
  else if (held >= MenuPressDelayHOME && held < 40)
  {
    extremeLongPressHandler();
  }
  if (updateContentFlag)
  {
    //when normal is active
    
    updatePlatz();
    if (NORMAL_MODE_Flag)
      if (nm_lastencoderValue != nm_encoderValue)
      {
        //updateNotificationBar(currentUserOptions);
        changeVoiceVolum();
        nm_lastencoderValue = nm_encoderValue; 
        return;
      }
    //when edit is active
    if (EDIT_MODE_Flag)
    {
      if (em_lastencoderValue != em_encoderValue)
      {
        int index = abs(em_encoderValue / EncoderPulsesPerClick);
        //editMenuSelectedIndex = ; // from 0 -> 6
        editMenuScrolTo(index % 7);
        printEditModeMenu(screenPlatz, currentUserOptions);
        refreshEditMenuTrigger = true;
        em_lastencoderValue = em_encoderValue;
        return;
      }
    }
    //when menu is active
    if (MENU_MODE_Flag)
    {
      if (mm_lastencoderValue != mm_encoderValue)
        if (mainMenu.getScrollAbility() != 0)
        {
          int index = abs(mm_encoderValue / EncoderPulsesPerClick);
          int selectedMenuItemIndex = index % mainMenu.getMenuItemCount();
          //printing staff:
          /*
              Serial.print("encoderValue : ");                       Serial.println(mm_encoderValue);
              Serial.print("lastencoderValue : ");                   Serial.println(mm_lastencoderValue);
              Serial.print("encoderValue/EncoderPulsesPerClick :");  Serial.println(mm_encoderValue/EncoderPulsesPerClick );
              Serial.print("index : ");                              Serial.println(index);
              Serial.print("currentMenuItemCount : ");               Serial.println(mainMenu.getMenuItemCount());
              Serial.print("index%menuItemCnt : ");                  Serial.println(selectedMenuItemIndex);
            */
          mainMenu.scrol(selectedMenuItemIndex);
          listItem *list = mainMenu.printStyledList(printListStyle);
          oledPrintList(list, mainMenu.getMenuItemCount(), selectedMenuItemIndex);
          //mainMenu.printLCDList();
          mm_lastencoderValue = mm_encoderValue;
          return;
        }
    }
    //when user input is active
    if (USER_INPUT_Flag)
    {
      if (ur_lastencoderValue != ur_encoderValue)
      {
        int index = abs(ur_encoderValue / EncoderPulsesPerClick);
        tempSelectedVal = (index % maxSelectedVal) + minSelectedVal;
        showInputFunc(tempSelectedVal);
        ur_lastencoderValue = ur_encoderValue;
        return;
      }
    }
    updateContentFlag = false; //make updateContentFlag false agine
  }
  if (EDIT_MODE_Flag && refreshEditMenuTrigger)
  {
    editMenuScrolTo(getEditMenuScroller());
    printEditModeMenu(screenPlatz, currentUserOptions);
    refreshEditMenuTrigger = false;
  }
}

void initHardWare()
{
  //TODO: add restart here with pins to enter self boot mode!
  pinMode(encoderPin1, INPUT_PULLUP);
  pinMode(encoderPin2, INPUT_PULLUP);
  pinMode(encoderSwitchPin, INPUT_PULLUP);
  //pinMode(LED_BUILTIN, OUTPUT);
  pinMode(SELFBOOT, OUTPUT);
  pinMode(ADAU_RST, OUTPUT);
  pinMode(WP, OUTPUT);
  pinMode(LevelShift, OUTPUT);

  digitalWrite(encoderPin1, HIGH);      //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH);      //turn pullup resistor on
  digitalWrite(encoderSwitchPin, HIGH); //turn pullup resistor on
  //digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(ADAU_RST, LOW); //ADAU1701 processor turend off

  delay(500);

  digitalWrite(ADAU_RST, HIGH); //ADAU1701 processor turend on
  digitalWrite(WP, HIGH);

  #ifdef EEPROM
  digitalWrite(SELFBOOT, HIGH);
  #else
  digitalWrite(SELFBOOT, LOW);
  #endif

  delay(1000);

  #ifdef CS43131 
  config_cs43131(cs43131_i2c_addr, cs43131_dat, sizeof(cs43131_dat));
  #endif

  #ifndef EEPROM
  config_adau1701();
  #endif

  attachInterrupt(digitalPinToInterrupt(encoderPin1), updateEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoderPin2), updateEncoder, CHANGE);
  Wire.begin(SDA, SCL, 400000UL); // join i2c bus (address optional for master)
  delay(10);
}

void IRAM_ATTR updateEncoder()
{
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit
  int encoded = (MSB << 1) | LSB;     //converting the 2 pin value to single number

  if (NORMAL_MODE_Flag && !MUTE_Flag ) //here we just change voice volume:
  {
    int sum = (nm_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      nm_encoderValue = nm_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      nm_encoderValue = nm_encoderValue - encoderStep;
    nm_lastEncoded = encoded; //store this value for next time
    if ( nm_encoderValue <= 3 )     //Encoder Minimum Greenzen
    {
      nm_encoderValue = 3;
    }
    else if(nm_encoderValue >= 100 ) //Encoder Maximum Greenzen
    { 
      nm_encoderValue = 100;  
    }
     
  }

  if (EDIT_MODE_Flag) //here we scrol through edit menu
  {
    int sum = (em_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      em_encoderValue = em_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      em_encoderValue = em_encoderValue - encoderStep;
    em_lastEncoded = encoded; //store this value for next time
  }

  if (MENU_MODE_Flag) //here we scrol through main menu
  {
    int sum = (mm_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      mm_encoderValue = mm_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      mm_encoderValue = mm_encoderValue - encoderStep;
    mm_lastEncoded = encoded; //store this value for next time
  }

  if (USER_INPUT_Flag) //here we let the user to choose item
  {
    int sum = (ur_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      ur_encoderValue = ur_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      ur_encoderValue = ur_encoderValue - encoderStep;
    ur_lastEncoded = encoded; //store this value for next time
  }
  updateContentFlag = true;
}

void shortPressHandler(void)
{
  printOnTerminal("short press");
  if (NORMAL_MODE_Flag)
  {
    bool key = MUTE_Flag;
    (key) ? muteVoice(false) : muteVoice(true);
    updatePlatz();
    oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
    //printCurrentMODE();
    return;
  }

  if (EDIT_MODE_Flag)
  {
    int excutionVal = MesseMenuExcute();

    if (excutionVal == 4) //back handler excuted!
    {
      activitNormalMode();
      printCurrentMODE();
      refreshEditMenuTrigger = false;
      changeVoiceVolum();
      updateContentFlag = true;
      return;
    }
    updatePlatz();
    refreshEditMenuTrigger = true;
    return;
  }

  if (MENU_MODE_Flag)
  {
    Serial.println("now you try to excute some item handler");
    mainMenu.execute();
    //this step just to ensure that loop will update screen
    mm_encoderValue = 0;
    mm_lastencoderValue = 0.1;
    updateContentFlag = true;
    return;
  }
  if (USER_INPUT_Flag)
  {
    Serial.println("now you try to enter some value");
    //this step just to ensure that loop will update screen
    selectedValPtr[selectedValIdx] = tempSelectedVal;
    selectedValIdx++;
    float freq = 0.0;
    if(ManualHearingTestFlag)
    {
      if(Octav)
        freq = f_countOctav[selectedValIdx];
      else
        freq = f_countTerz[selectedValIdx];
      sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq);
      VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, hearingTestVolMINdB);         //Schreibe aktuelle Volume in DSP

    }
    Serial.print("current Inputs: ");
    Serial.println(selectedValIdx);
    Serial.print("max inputs: ");
    Serial.println(selectedValCnt);
    //Serial.println(showInputFunc(selectedValPtr[selectedValIdx]));
    if (selectedValIdx == selectedValCnt)
    {
      Serial.println("u did reach max input so go back to the last activated  Mode ");
      userInputFunc(selectedValPtr, selectedValIdx);
      restoreLastMode();
      updateContentFlag = true;
    }
    else
    {
      Serial.println("u did not reach max input so you can enter ");
      ur_encoderValue = 0;
      ur_lastencoderValue = 0.1;
      updateContentFlag = true;
    }

    return;
  }
}

void longPressHandler(void)
{
  printOnTerminal("long press");
  if (NORMAL_MODE_Flag)
  {
    activitEditMode();
    printCurrentMODE();
    editMenuScrolTo(0);
    printEditModeMenu(screenPlatz, currentUserOptions);
    updateContentFlag = true;
    return;
  }

  if (EDIT_MODE_Flag)
  {
    activitMenuMode();
    refreshEditMenuTrigger = false;
    mainMenu.scrol(0);
    listItem *list = mainMenu.printStyledList(printListStyle);
    oledPrintList(list, mainMenu.getMenuItemCount(), 0);
    updateContentFlag = true;
    return;
  }

  if (MENU_MODE_Flag)
  {
    int state = mainMenu.pop();
    if (state)
    {
      //this step just to ensure that loop will update screen
      mm_encoderValue = 0;
      mm_lastencoderValue = 0.1;
      updateContentFlag = true;
    }
    else
    {
      activitNormalMode();
      LCDprintFLAG();
      changeVoiceVolum();
      //updateContentFlag = true;
    }
  }
  if (USER_INPUT_Flag)
  {
    userInputFunc(selectedValPtr, selectedValIdx);
    restoreLastMode();
    //activitMenuMode();
    printCurrentMODE();
    updateContentFlag = true;
    return;
  }
}

void extremeLongPressHandler(void)
{
  Serial.println("extreme long press");
  if (NORMAL_MODE_Flag)
  {
    printCurrentMODE();
    return;
  }

  if (EDIT_MODE_Flag || MENU_MODE_Flag)
  {
    activitNormalMode();
    printCurrentMODE();
    changeVoiceVolum();
    updateContentFlag = true; // <-go back to loop and print voice Volume
    return;
  }
}

void muteVoice(bool muteVal)
{
  Serial.print("muteVoice(");
  Serial.print(muteVal);
  Serial.print(")");
  if (muteVal == true)
  {
    MUTE_Flag = true;
    VolumeStereoConvertANDsend(MasterVolumeAddr, VOLMIN);
    Serial.println("Mute");
  }
  else
  {
    MUTE_Flag = false;
    VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    Serial.println("Unmute");
  }
}

void changeVoiceVolum()
{
  //Serial.printf("nm_encoderValue %f before run\n",nm_encoderValue);
  dBVoiceVolum = encoderToDBVoice(VOLMIN, VOLMAX, (nm_encoderValue - VolumeINIT)); // dB
  printOnTerminal(" Master Vol. : \n" + String(dBVoiceVolum) + "dB");
  oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
  //updateNotificationBar(currentUserOptions);
  VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
  //Serial.printf("nm_encoderValue %f after run\n", VolumeINIT - dbVlaueToEncoder(dBVoiceVolum) );
  
  return;
}


double dbVlaueToEncoder(double dbVal)
{
  if (dbVal < 0)
  {
    return  (dbVal * max_number_of_pulses)/( VOLMIN);
  }
  else //(dbVal >= 0)
  {
    
    return 200 - (dbVal * max_number_of_pulses)/( VOLMAX) ;
  }
}  
/**
   This function transform pulses from encoder in user defined range values
   @param minval
   @param maxval
   @param pulses - the actual pulses count see getPulses()
   @return double - return a value between minval and maxval when user turn encoder knob
*/
double encoderToDBVoice(double minval, double maxval, double pulses)
{
  double val = 0.00;

  if (minval < 0 && maxval <= 0)
  {
    if (pulses >= 0)
      return maxval;
    else
    {
      val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval;
      //Serial.print("val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval:");
      //Serial.println(val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval);
      
      if (val < minval)
        return minval;
      else
        return val;
    }
  }
  else if (minval >= 0 && maxval > 0)
  {
    if (pulses >= 0)
    {
      val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval;
      //Serial.print("val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval :");
      //Serial.println(val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval);
      
      if (val > maxval)
        return maxval;
      else
        return val;
    }
    else
      return minval;
  }
  else if (minval < 0 && maxval > 0)
  {
    if (pulses >= 0)
    {
      if (pulses == 0)
        return 0.00;
      else
      {
        val = pulses * (maxval / max_number_of_pulses);
        if (val > maxval)
          return maxval;
        else
          return val;
      }
    }
    else // pulses < 0
    {
      val = abs(pulses) * (minval / max_number_of_pulses);

      if (val < minval)
        return minval;
      else
        return val;
    }
  }
}

void AudioDetect(void)
{
  if (digitalRead(PIN_AudioDetect) == true)
  {
  }
  else
  {
    changeVoiceVolum();
  }
}

void printOnTerminal(String message)
{
  if (SerialTermON == true)
  {
    Serial.println(message);
  }
}

//TODO: Utilites
void printCurrentMODE()
{
  String msg = "";
  if (NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "NORMAL MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "EDIT MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "MENU MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & USER_INPUT_Flag)
    msg = " USER INPUT MODE IS ACTIVE NOW..";

  Serial.println("/= = = = = = = = = = = = = = = = /");
  Serial.println(msg);
  Serial.println("/= = = = = = = = = = = = = = = = /");
  return;
}

void activitNormalMode(void)
{
  NORMAL_MODE_Flag = true;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = false;
}

void activitEditMode(void)
{
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = true;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = false;
}

void activitMenuMode(void)
{
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = true;
  USER_INPUT_Flag = false;
}

void activatUserInputMode(int16_t minVal, int16_t maxVal, void (*singleInputHandler)(int16_t *, uint8_t), void (*showInputHandler)(int16_t), int8_t selectedValCount)
{
  //save the last Mode

  saveLastMode();
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = true;

  maxSelectedVal = maxVal;
  minSelectedVal = minVal;
  selectedValCnt = selectedValCount;
  selectedValPtr = new int16_t[selectedValCnt];
  selectedValIdx = 0;
  userInputFunc = singleInputHandler;
  showInputFunc = showInputHandler;
}

void saveLastMode()
{

  if (NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 0;
  else if (!NORMAL_MODE_Flag & EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 1;
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 2;
}

void restoreLastMode()
{
  switch (lastMode)
  {
  case 0:
    activitNormalMode();
    break;
  case 1:
    activitEditMode();
    break;
  case 2:
    activitMenuMode();
    break;
  }
}


#ifndef EEPROM
//This function writes a block of data to the I2C Bus
bool i2c_write_block(byte ic_addr, word reg_addr, byte * data, int data_length){

    /* This function needs  SoftI2C.h/I2CMaster.h included and a global SoftI2C object called i2c.*/

    i2c->startWrite(ic_addr);

    i2c->write(highByte(reg_addr));

    i2c->write(reg_addr);

    for( int i = 0; i < data_length; i++){

        i2c->write(data[i]);

    }

    return i2c->endWrite();

}
#endif

#ifndef EEPROM
void config_adau1701(){

  i2c = new SoftI2C(SDA_PIN, SCL_PIN);

      Serial.print("Init ADAU1701: ");
      Serial.println(i2c_write_block(DEVICE_ADDR_7bit, adau1701_core_reg, adau1701_core_dat1, sizeof(adau1701_core_dat1)));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_prog_reg, adau1701_prog_dat, sizeof(adau1701_prog_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_param_reg, adau1701_param_dat, sizeof(adau1701_param_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_hwconf_reg, adau1701_hwconf_dat, sizeof(adau1701_hwconf_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_core_reg, adau1701_core_dat2, sizeof(adau1701_core_dat2));

  delete i2c;

};
#endif

/* #ifdef CS43131
void config_cs43131(byte ic_addr, byte * data, int data_length){

    /*  This function needs the CS43131_config.h included. The format must be:
        
        byte cs43131_i2c_addr = 0x30; //chip address 0x60 shifted right

        byte cs43131_reg[N] = { 0xRR, 0xRR, 0xRR, 0xDD, 0xDD, //3 register bytes, 2 data bytes
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD,
                                ...
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD};
                                
        0xRR = register byte
        0xDD = data byte*/
    
    /*

    i2c = new SoftI2C(SDA_PIN, SCL_PIN);

    for(int i = 0; i < data_length; i = i + 5){

      i2c->startWrite(ic_addr);
      
      i2c->write(data[i]);

      i2c->write(data[i+1]);
      
      i2c->write(data[i+2]);
      
      i2c->write(data[i+3]);
      
      i2c->write(data[i+4]);

      i2c->endWrite();

    }

    delete i2c;

}
#endif */