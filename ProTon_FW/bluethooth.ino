/*
 *    bluetooth.ino
 */

int tx = 1;        // Pin für transmit serial
int rx = 0;        // Pin für receive serial
char inSerial[15]; // Charakterarray für Keywords

uint8_t TackBool = false;



void linear(void);                //menuItem_2_1
void activateCurve_1(void);       //menuItem_2_2_1
void activateCurve_2(void);       //menuItem_2_2_2
void activateCurve_3(void);       //menuItem_2_2_3
void Menu_3_1_2_1_Handler(void);      //BASS ON
void Menu_3_1_2_2_Handler(void);      //BASS OFF
void compressorOn(void);  //menuItem_3_2_1
void compressorOff(void); //menuItem_3_2_2
//void hearingTest();                // Hörtest starten
void startHearingTest(void);
//void Menu_4_1_Handler(void);

void initBluetooth(void)
{
    pinMode(tx, OUTPUT);
    pinMode(rx, OUTPUT); // eigentlich ist das ein INPUT, aber es wird zunächst nur unidirektional gesendet.
}

void bluetoothREAD(void)
{
    int i = 0;
    int m = 0;
    delay(500);
    if (Serial.available() > 0)
    {
        while (Serial.available() > 0)
        {
            inSerial[i] = Serial.read();
            i++;
        }
        inSerial[i] = '\0';
        Check_Protocol(inSerial);
    }
}

void Check_Protocol(char inStr[])
{
  int i = 0;
  int m = 0;
  Serial.println(inStr);

  if(!strcmp(inStr,"linear"))   //Linear Mode
  {
    //allpinslow();
    // ToDo
    linear();

    Serial.println("Linear Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }
  
  if(!strcmp(inStr,"P1"))   //Personalisiert 1 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_1();
    Serial.println("P1 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"P2"))   //Personalisiert 2 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_2();
    Serial.println("P2 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"P3"))   //Personalisiert 3 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_3();
    Serial.println("P3 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"BassON"))   //Bass Boost ON
  {
    //allpinslow();
    // ToDo
    Menu_3_1_2_1_Handler();
    Serial.println("Bass Boost ON");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"BassOFF"))   //Bass Boost OFF
  {
    //allpinslow();
    // ToDo
    Menu_3_1_2_2_Handler();
    Serial.println("Bass Boost OFF");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"CompON"))   //Compressor ON
  {
    //allpinslow();
    // ToDo
    compressorOn();
    Serial.println("Compressor ON");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

if(!strcmp(inStr,"CompOFF"))   //Compressor OFF
  {
    //allpinslow();
    // ToDo
    compressorOff();
    Serial.println("Compressor OFF");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"TestStart"))   //Hörtest starten
  {
    //allpinslow();
    // ToDo
    //hearingTest();
    startHearingTest();
    Serial.println("Hoertest start");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"Tack"))   //Test Acknowledge
  {
    TackBool = true;
    
    Serial.println("bestaetigt");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  
  
  else
  {
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
    TackBool = false;
  }
}