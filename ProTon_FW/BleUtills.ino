#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <ArduinoJson.h>
#include <Arduino.h>
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"

extern User deviceUsers[USERS_COUNT];        //bring the array of users   
extern void intensityLevel20();
extern void intensityLevel40();
extern void intensityLevel60();
extern void intensityLevel80();

extern void intensityLevel100();

extern bool fineState ;
float tempArray[2][31] ;    
int lastDirection = 0;
String intinsityAsString = "2" ;
// https://www.uuidgenerator.net/


#define BLE_READ    BLECharacteristic::PROPERTY_READ
#define BLE_WRITE   BLECharacteristic::PROPERTY_WRITE


//BLE UUID's:

/*
 *
 *   TheMainService_UUID contains six sendUsersProfilesChar, linearP1P2Char, MuteVoiceChar, 
 *    BassSelectChar, CompSelectChar, IntensityChar, VoiceUpDownChar 
 * 
 */
#define TheMainService_UUID               "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define sendUsersProfilesChar_UUID        "43fdcd92-fa15-41d6-9be8-3708d0b91f0e"
#define linearP1P2Char_UUID               "8a42b311-f1f3-4389-bb22-9f47bbae00d2"
#define MuteVoiceChar_UUID                "beb5483e-36e1-4688-b7f5-ea07361b26a8"         // Mute
#define BassSelectChar_UUID               "66ea3d83-08b6-4e3f-b5b8-79237962442a" // Bass                   B1-B0
#define CompSelectChar_UUID               "ffcf4483-b621-4be4-8522-0592699a5532" // Compressor             C1-C0
#define IntensityChar_UUID                "c1960a3c-33c0-11e9-b210-d663bd873d93" // Intensity 20%, 40%, 60%, 80%, 100%
#define VoiceUpDownChar_UUID              "e59fa828-b46a-4d63-8fbc-3eb361985c47"

//*******************************************************************************************//

/*
 *
 * ManuelHearingService contains four characteristics: TonGeneratoreChar, iHeardTheToneChar, 
 * applyResultChar, BackToNormalChar
 * 
 */

#define ManuelHearingSrvs_UUID            "9dc68d22-364f-4fd5-94e2-dec2f3e66d2e"
#define TonGeneratoreChar_UUID            "5ed10ee4-c0ed-4c49-8eec-9fbe4fca64c5"
#define iHeardTheToneChar_UUID            "8647b1f1-cc5d-4a0c-bba7-4fa4d7e2ef8a"
#define applyResultChar_UUID              "91bb85b4-c800-4988-aad5-6985b0b6866a"  
#define BackToNormalChar_UUID             "03bba7fa-b915-499b-9fc7-ee8be47bbde8"  


//*******************************************************************************************//

// #define ChangedBLevelChar_UUID            "10d5294c-99d5-49bd-ad21-347a52a2c64c"
// #define ChangedBFineUPChar_UUID           "8b692ea8-af99-481e-8eb1-c0195640b950"
// //#define ChangedBFineDownChar_UUID         "4f09cda6-6980-405b-b471-8cb0965c1afb"
// #define FineChar_UUID                     "c2bc865b-a57e-438d-82e6-c4817be97128"   // ON-Off
// #define LinksRechtsChar_UUID             "0bf1a846-3304-4ebf-9c70-bb47e6e2735b"  // L-R

// #define ChangeVoiceVolumeChar_UUID        "bf8ecca6-eab5-48d3-b600-ca7d151c5a7d"         // Volume


// #define UserMangementSrvs_UUID            "d2ce3196-c0b2-4e90-aef1-8fd9ac9272b8"
// #define NewUserChar_UUID                  "67cd46aa-82e5-476e-8bde-1374e183ca25"
//*******************************************************************************************//
                  // "996f758e-33c0-11e9-b210-d663bd873d93"
                  // "764dd03c-4fe8-486b-8340-6ade505bae0f"
                  // "3cae3efc-d2bb-483a-b9c4-7713ab1c068a"
                  //"6cf79dc9-1393-429a-83d2-96800ac8f905"
                  //"3134fdec-117d-4efc-bbf1-1a1e96a4b436"

//*******************************************************************************************//

               
void changePersonlize(int);
//void muteVoice(bool );
void ledControl(bool);
void changeVoiceVolum(void);
void changeIntensity(int);
void MainPersonalizedMode(void);
void startHearingTest(void);
//void HearingButton(void);
void activateCurve_1(void);
void sinGeneratore(int);

bool muteFlag = false;
bool LEDFlag = false;

//*******************************************************************************************//
class MyServerCallBack : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    Serial.println("*********");
    //Serial.print("there is: ");
    Serial.print(pServer->getConnectedCount());
    Serial.println(" you are connected now");
    Serial.println("*********");
    Ble_CONTROL_MODE_FLAG = true ;
    oledBleConnectNotifier(true);
  }
  void onDisconnect(BLEServer *pServer)
  {
    Serial.println("*********");
    //Serial.print("there is: ");
    Serial.print(pServer->getConnectedCount());
    Serial.println(" you are disconnected now");
    Serial.println("*********");
    Ble_CONTROL_MODE_FLAG = false ;
    oledBleConnectNotifier(false);
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////                       ///////////////////////////////////////////////// 
///////////////////////////////////////////     JSON READ and WRITE        /////////////////////////////////////////////
////////////////////////////////////////////////                       /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class sendUsersProfilesCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_ARRAY_SIZE(62) + JSON_OBJECT_SIZE(11) + 350;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str() ;
    
    Serial.print("strVal :") ; Serial.println( strVal ) ; 
    
    deserializeJson(doc, strVal);
    const char* name = doc["n"]; 
    bool linear = doc["l"]; 
    bool p1 = doc["p1"]; 
    bool p2 = doc["p2"]; 
    String temp1 = doc["lo"]; 
    temp1.replace(",", ".");
    float linearOffset = String(temp1).toFloat() ;
    String temp2 = doc["p1o"]; 
    temp2.replace(",", ".");
    float p1Offset = String(temp2).toFloat() ;
    bool mute = doc["m"]; 
    bool pass = doc["b"]; 
    bool comp = doc["c"]; 
    int volume = doc["v"]; 
    JsonArray result = doc["r"];
    
    // Serial.print("linear :") ; Serial.println( linear) ; 
    // Serial.print("p1 :") ; Serial.println( p1 ) ; 
    // Serial.print("p2 :") ; Serial.println( p2 ) ; 
    // Serial.print("linearOffset :") ; Serial.println(linearOffset ) ; 
    // Serial.print("p1Offset :") ; Serial.println( p1Offset ) ; 
    // Serial.print("mute :") ; Serial.println( mute ) ; 
    // Serial.print("pass :") ; Serial.println( pass ) ; 
    // Serial.print("comp :") ; Serial.println( comp ) ; 
    // Serial.print("volume :") ; Serial.println( volume ) ; 

     int k = 0 ;
    Serial.println("The saved array :") ; 
   // int resTest[31] = result[31].as<int>() ;
    //TODO: how to know fine state??
    /*fine = true;
    if (result[0].as<int>() == 0 && result[1].as<int>() == 0 ) 
    {
      fine = false;
    }*/

  int fine = doc["hd"];
   if(fine == 31)
    {
    
      fineState = true;
    }
    else
    {
    
      fineState = false;
    }
  
    if(fineState == true){

       for(int  i = 1; i < 3; i++)
        {
          for( int f_counter = 0; f_counter < 31; f_counter++)
          {
            hearingTestvolumedB = result[(i-1) * 31 + f_counter].as<int>() ;
            delay(2);
            Serial.print(String( hearingTestvolumedB )+ "  ") ;
      
            hearingTestSPL[i][f_counter] = VolumedBFS + hearingTestvolumedB;
            hearingTestHL[i][f_counter] = hearingTestSPL[i][f_counter] - hearingTestSAVE[i + 2][f_counter];
            hearingTestSAVE[i][f_counter] = hearingTestHL[i][f_counter];
            k++ ;
          }   
          Serial.println() ;
        }
      Serial.println() ;

    }else{

       for(int  i = 1; i < 3; i++)
        {
          for( int f_counter = 0; f_counter < 9; f_counter++)
          {
            hearingTestvolumedB = result[(i-1) * 9 + f_counter].as<int>() ;
            delay(2);
            Serial.print(String( hearingTestvolumedB )+ "  ") ;

            int temp = 5 + (3 * f_counter);
      
            hearingTestSPL[i][temp] = VolumedBFS + hearingTestvolumedB;
            hearingTestHL[i][temp] = hearingTestSPL[i][temp] - hearingTestSAVE[i + 2][temp];
            hearingTestSAVE[i][temp] = hearingTestHL[i][temp];
            k++ ;
          }   
          Serial.println() ;
        }
      Serial.println() ;      

    }


    if (mute)   muteVoice(true);
    if (linear) executeAlgorithms( 0 , linearOffset) ;
    if (p1)     executeAlgorithms( 1 , p1Offset) ;
    if (p2)     executeAlgorithms( 2 , 0) ;
    if (pass)   setMainDynamicBassBypass(1);
    if(comp)    setPersonalizedCompressorBypassMode(1);
    strcpy(deviceUsers[0].userName, name );
    deviceUsers[0].options.L = linear ;
    deviceUsers[0].options.P1 = p1 ;
    deviceUsers[0].options.P2 = p2 ;
    deviceUsers[0].options.Comp = comp ;
    deviceUsers[0].options.Pass = pass;

    String temp = doc["n"];
    oledBLegreetingUser(temp);
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};
//*******************************************************************************************//

class linearP1P2CallBack : public BLECharacteristicCallbacks
{
   void onWrite(BLECharacteristic *pCharacteristic)
   {
    std::string value = pCharacteristic->getValue();
   // const char* strVal = value.c_str();
     Serial.print("strVal :") ; Serial.println( value.c_str() ) ; 
    const size_t capacity = JSON_OBJECT_SIZE(2) + 30;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc, value );

    int Algo = doc["Algo"];
    preset = Algo ;   
    String temp = doc["offset"];    
    temp.replace(",", ".");
    float offset = String(temp).toFloat() ;
    Serial.print("Algo :") ; Serial.println( Algo ) ; 
    Serial.print("temp :") ; Serial.println( temp ) ; 
    Serial.print("offset :") ; Serial.println( offset ) ; 
    //if(Algo == 2) Algo = 1;//only P1
    executeAlgorithms( Algo , offset); 
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic) {  }
};

//*******************************************************************************************//

class MuteCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    if (i == 0)
      muteVoice(false);
    else
      muteVoice(true);  
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);  
  }

  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string muteStatus = "";
    muteStatus = (muteFlag) ? "voice muted" : "voice unmuted";

    /*if (muteFlag)
      muteStatus = "voice muteed";
    else
      muteStatus = "voice unmuteed";
    */
    pCharacteristic->setValue(muteStatus);
  }
};
//*******************************************************************************************//
class BassSelectCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    bassFLAG = (bool)i;
    setMainDynamicBassBypass(bassFLAG);
  oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string bassStatus = "Bass mode is disabled";
    if (bassFLAG)
      bassStatus = "Bass mode is enabled";
    pCharacteristic->setValue(bassStatus);
  }
};
//*******************************************************************************************//
class CompSelectCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    Serial.println( i );
    setPersonalizedCompressorBypassMode(i);
    compFLAG = (bool)i;
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string CompressorStatus = "compressor is OFF";

    if (currentCompressorMode == CompressorMode_MultiBand)
      CompressorStatus = "Compressor in multiBand Mode.";
    else if (currentCompressorMode == CompressorMode_RMS)
      CompressorStatus = "Compressor in RMS Mode";

    pCharacteristic->setValue(CompressorStatus);
  }
};
//*******************************************************************************************//
class IntensityCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    
    //the new Intincity Protocol works only in the master and in the new branches after 23.09.2019.
    /*Serial.print(strVal);
    strVal.replace("0", "20");
    strVal.replace("1", "40");
    strVal.replace("2", "60");
    strVal.replace("3", "80");
    strVal.replace("4", "100");*/
    intinsityAsString = strVal;
    
    int i = strVal.toInt();
    Serial.print(" I :") ; Serial.println( i ) ; 
    //  if(!MUTE_Flag)
    // {
    //     muteVoice(true);
    //   if (i = 0)        intensityLevel20();
    //   else if ( i = 1)  intensityLevel40();
    //   else if ( i = 2)  intensityLevel60();
    //   else if ( i = 3)  intensityLevel80();
    //   else if ( i = 4)  intensityLevel100();
    //   muteVoice(false);
    // }
     if(!MUTE_Flag)
    {
      muteVoice(true);
      IntensityChoice = i;
      if (per1FLAG || per2FLAG)
        MainPersonalizedMode();
      muteVoice(false);
    }
      oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string currentPersonaliz = "";
    pCharacteristic->setValue(currentPersonaliz);
  }
};
//*******************************************************************************************//
class VoiceUpDownCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String volumeVal_str = value.c_str();
    dBVoiceVolum = volumeVal_str.toInt();
    Serial.print("the new Voice volume is: ");  Serial.println(dBVoiceVolum);
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
    VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    nm_encoderValue =VolumeINIT -  dbVlaueToEncoder(dBVoiceVolum);
    //Serial.print("nm_encoderValue in Ble is: ");  Serial.println(nm_encoderValue);
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};

//*******************************************************************************************//
int headphonePosition =0;
class TonGeneratoreCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    const char* strVal = value.c_str();
    Serial.print("strVal :") ; Serial.println( value.c_str() ) ; //{"D":1,"F":5,"V":"-83,9","P":0}
    
    const size_t capacity = JSON_OBJECT_SIZE(4) + 20;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc, strVal );

    int Direction = doc["D"];   // 1 : links - 2 : Rechts
    int frequencyIndex = doc["F"];   // sendIndex 0
    headphonePosition = doc["P"];//    1:headphoneVorne  ,0:headphoneHinten
    String temp = doc["V"]; // between -80,0 
    temp.replace("," ,".");
    float dBValue = String(temp).toFloat() ;
    Serial.print("Direction :") ; Serial.println( Direction ) ; 
    Serial.print("frequencyIndex :") ; Serial.println( frequencyIndex ) ;
    Serial.print("headphonePosition :") ; Serial.println( headphonePosition );
    Serial.print("dBValue :") ; Serial.println( dBValue ) ; 
    Serial.println("**********TonGeneratoreCallBack*************");
    float freq = f_count[frequencyIndex] ;
    Serial.print("change freq :");   Serial.println( freq );
    oledBleHearingTestNotifier(Direction,frequencyIndex,dBValue);
    
    if(headphonePosition){
      
      if(Direction == 2){

         Direction = 1;

      } else{

         Direction = 2;

      }
    }

      generatingTonWithFreq( Direction ,freq ,dBValue );

  }
  void onRead(BLECharacteristic *pCharacteristic){  }
};

//*******************************************************************************************//

class iHeardTheToneCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_OBJECT_SIZE(4) + 30;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    Serial.print("strVal :") ; Serial.println( value.c_str() ) ; 

    deserializeJson(doc, value );
    int Direction = doc["D"];      // 1 : links - 2 : Recht
    int frequencyIndex = doc["F"]; 
    //bool fineState = doc["fine"];
    String temp = doc["V"]; // between -80,0 
    temp.replace("," ,".");
    float dBValue = String(temp).toFloat() ;
    headphonePosition = doc["P"];//    1:headphoneVorne  ,0:headphoneHinten
    if(headphonePosition){
      
      if(Direction == 2){

         Direction = 1;

      } else{

         Direction = 2;

      }
    }

    Serial.print("Direction :") ; Serial.println( Direction ) ; 
    Serial.print("frequencyIndex :") ; Serial.println( frequencyIndex ) ; 
    Serial.print("dBValue :") ; Serial.println( dBValue ) ; 
    Serial.println("*********iHeardTheToneCallBack**************");
    
    if(Direction <= 2 && frequencyIndex < 31)
    {
      tempArray[Direction - 1][frequencyIndex] = dBValue ;
    }
    for(int j =0 ; j < 2 ; j++)
    {
      for(int k = 0 ; k < 31 ; k++)
        Serial.print(String( tempArray[j][k])+ "  ") ;
      Serial.println() ;
    }
    if(frequencyIndex == 30)
    {
      frequencyIndex = 0;
      if (Direction == 1){ Direction = 2 ;}
      else if (Direction == 2) { Direction = 1 ;}
    }
    dBValue = -120 ;
    float freq = f_count[frequencyIndex] ;
    oledBleHearingTestNotifier(Direction,frequencyIndex,dBValue);
    generatingTonWithFreq(Direction ,freq ,dBValue );
  }

  void onRead(BLECharacteristic *pCharacteristic) { }
};

//*******************************************************************************************//
class applyResultCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_ARRAY_SIZE(62) + JSON_OBJECT_SIZE(2) + 320;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str() ;
    deserializeJson(doc, strVal );
    Serial.println("===================================================="); 
    Serial.print("strVal :") ; Serial.println( strVal ) ; 
    Serial.println("===================================================="); 
    
    JsonArray result = doc["result"];
    int fine = doc["hd"];

    int k = 0 ;
   if(fine == 31)
    {
    
      fineState = true;
    }
    else
    {
    
      fineState = false;
    }
    Serial.println("======================= hearingTestvolumedB ============================="); 
   

    if(fineState == true){

       for(int  i = 1; i < 3; i++)
        {
          for( int f_counter = 0; f_counter < 31; f_counter++)
          {
            hearingTestvolumedB = result[(i-1) * 31 + f_counter].as<int>() ;
            delay(2);
            Serial.print(String( hearingTestvolumedB )+ "  ") ;
      
            hearingTestSPL[i][f_counter] = VolumedBFS + hearingTestvolumedB;
            hearingTestHL[i][f_counter] = hearingTestSPL[i][f_counter] - hearingTestSAVE[i + 2][f_counter];
            hearingTestSAVE[i][f_counter] = hearingTestHL[i][f_counter];
            k++ ;
          }   
          Serial.println() ;
        }
      Serial.println() ;

    }else{

      for(int  i = 1; i < 3; i++)
        {
          for( int f_counter = 0; f_counter < 9; f_counter++)
          {
            hearingTestvolumedB = result[(i-1) * 9 + f_counter].as<int>() ;
            delay(2);
            Serial.print(String( hearingTestvolumedB )+ "  ") ;

            int temp = 5 + (3 * f_counter);
      
            hearingTestSPL[i][temp] = VolumedBFS + hearingTestvolumedB;
            hearingTestHL[i][temp] = hearingTestSPL[i][temp] - hearingTestSAVE[i + 2][temp];
            hearingTestSAVE[i][temp] = hearingTestHL[i][temp];
            k++ ;
          }   
          Serial.println() ;
        }
      Serial.println() ;       

    }


    Serial.println("======================hearingTestSAVEArray=============================="); 
    for(int  i = 0; i < 5; i++)
    {
      for( int j = 0; j < 31; j++)
      {
        Serial.print(String( hearingTestSAVE[i][j])+ "  ") ;
      }   
      Serial.println() ;
    }
    Serial.println() ;
    
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};


//*******************************************************************************************//
class BackToNormalCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    MainDirectMode();
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
    PersonalizedMultibandCompressorBypassCompOFF();   
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};
//*******************************************************************************************//

void BLEInit(void)
{
  //1-create BLE device:
  #ifdef Drachenfels
  BLEDevice::init("ProTon/Drachenfels");
  #else
  BLEDevice::init("ProTon PLUS");
  #endif
  //BLEDevice::setMTU(512);
  esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);
  //2-create server:
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallBack);
  //3-create services:
  BLEService *TheMainService = pServer->createService(TheMainService_UUID);
  BLEService *ManuelHearingService = pServer->createService(ManuelHearingSrvs_UUID);

  //4-create characteristics:
  BLECharacteristic *sendUsersProfilesCharacteristic = TheMainService->createCharacteristic(sendUsersProfilesChar_UUID, BLE_WRITE);
 
  BLECharacteristic *linearP1P2Characteristic = TheMainService->createCharacteristic(linearP1P2Char_UUID, BLE_WRITE);

  BLECharacteristic *MuteVoiceCharacteristic = TheMainService->createCharacteristic(MuteVoiceChar_UUID, BLE_WRITE);

  BLECharacteristic *BassSelectCharacteristic = TheMainService->createCharacteristic(BassSelectChar_UUID, BLE_WRITE);

  BLECharacteristic *CompSelectCharacteristic = TheMainService->createCharacteristic(CompSelectChar_UUID, BLE_WRITE);

  BLECharacteristic *IntensityCharacteristic = TheMainService->createCharacteristic(IntensityChar_UUID, BLE_WRITE);

  BLECharacteristic *VoiceUpDownCharacteristic = TheMainService->createCharacteristic(VoiceUpDownChar_UUID, BLE_WRITE);

  //*******************************************************************************************// 

  BLECharacteristic *TonGeneratoreCharacteristic = ManuelHearingService->createCharacteristic(TonGeneratoreChar_UUID, BLE_WRITE);

  BLECharacteristic *iHeardTheToneCharacteristic = ManuelHearingService->createCharacteristic(iHeardTheToneChar_UUID, BLE_WRITE);

  BLECharacteristic *applyResultCharacteristic = ManuelHearingService->createCharacteristic(applyResultChar_UUID, BLE_WRITE);
  
  BLECharacteristic *BackToNormalCharacteristic = ManuelHearingService->createCharacteristic(BackToNormalChar_UUID, BLE_WRITE); 
   
 //*******************************************************************************************//

  sendUsersProfilesCharacteristic->setCallbacks(new sendUsersProfilesCallBack);
  linearP1P2Characteristic->setCallbacks(new linearP1P2CallBack);
  MuteVoiceCharacteristic->setCallbacks(new MuteCallBack);
  BassSelectCharacteristic->setCallbacks(new BassSelectCallBack);
  CompSelectCharacteristic->setCallbacks(new CompSelectCallBack);
  IntensityCharacteristic->setCallbacks(new IntensityCallBack);
  VoiceUpDownCharacteristic->setCallbacks(new VoiceUpDownCallBack);
  TonGeneratoreCharacteristic->setCallbacks(new TonGeneratoreCallBack);
  iHeardTheToneCharacteristic->setCallbacks(new iHeardTheToneCallBack);
  applyResultCharacteristic->setCallbacks(new applyResultCallBack);
  BackToNormalCharacteristic->setCallbacks(new BackToNormalCallBack);
  
  //*******************************************************************************************//
  //6-start all services:
  TheMainService->start();
  ManuelHearingService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();
  printOnTerminal("Start The services ..");
}

// void ledControl(bool LEDVal)
// {
//   if (LEDVal)
//   {
//     LEDFlag = true;
//     dc_source(DEVICE_ADDR_7bit, DC1, 1.00);
//     Serial.println(F("LED ON .."));
//   }
//   else
//   {
//     LEDFlag = false;
//     dc_source(DEVICE_ADDR_7bit, DC1, 0.00);
//     Serial.println(F("LED OFF .."));
//   }
// }


void sinGeneratore(int freq)
{
  Serial.print("change freq :");   Serial.println( freq );

  VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
  mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq); 
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, 0);       //hearingTestvolumedB 

}
void executeAlgorithms(int A ,float OFF){
  switch (A)
    {
      case 0 :
      linFLAG = true;
      per1FLAG = false;
      per2FLAG = false;
      Serial.println(" linearCallBack");
      preset = 0;
      MainDirectMode();
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum + OFF) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainDynamicBassBypassOFF();
      Serial.println("linear..");
      break;
  
      case 1 :
      linFLAG = false;
      per1FLAG = true;
      per2FLAG = false;
      preset = 1;
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum + OFF) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainPersonalizedMode();
      Serial.println("P1CallBack( PLUS Agorithmus )..");
      break;
    
      case 2 :
      linFLAG = false;
      per1FLAG = false;
      per2FLAG = true;
      preset = 2;
      muteVoice(true);
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainPersonalizedMode();
      muteVoice(false);
      Serial.println("P2CallBack( LIFESTYLE Algorithmus )");
      break;
  }
}
void generatingTonWithFreq(int D ,float F ,float dB )
{
  VolumeStereoConvertANDsend(MasterVolumeAddr, VOLMAX );     //set the voice to the maximum value
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
  
  #ifndef Drachenfels 
  //temporal changes must be resored to the orginal functionality when the new Hardware arrive..
  if (D == 1)     
    {
      mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2, 2);
    } 
  else if (D == 2)   
   {
      mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1, 2);
    }
  #else
  if(lastDirection != D)
     mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, D, 2);
  
  #endif
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, F); 
  if (headphonePosition)
    {
      gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
      if (D == 1)
        mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2, 2);
      else
        mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1, 2);
       Serial.println("headphonePosition ist Vrone");
    }
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, dB );       //hearingTestvolumedB 
  //MasterVolumeMono(DEVICE_ADDR_7bit, hearingTestSineVolumeAddr, dBValue);
  lastDirection = D ;
}
 
