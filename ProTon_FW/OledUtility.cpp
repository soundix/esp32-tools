/*
* OledUtility.cpp
* Version 0.3.0.2
*/

#include "OledUtility.h"
#include <Wire.h>
#include <U8g2lib.h>
#include "Oled_Logo.h"


/*
    https://github.com/olikraus/u8g2/wiki/u8g2reference#reference

    https://github.com/olikraus/u8g2/wiki/fntlistall#25-pixel-height

*/

U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/U8X8_PIN_NONE, /* clock=*/SCL, /* data=*/SDA); // ESP32 Thing, HW I2C with pin remapping

uint8_t contrastValue = 0;
FontSize fSize = smallFont;

String bassam[4];
//TODO: adjust those matrices for better screen view.
uint16_t linesIndicesSmlFont[4] = {10, 27, 41, 56};
uint16_t linesIndicesMidFont[4] = {17, 34, 52, 63}; //{20,40,60,63}
uint16_t linesIndicesLrgFont[4] = {13, 33, 53, 73}; //73 is not applicable for this screen size just for modularity purpose
uint16_t *currentLinesIndices = linesIndicesSmlFont;
String editModeMenuItems[7];
String notificationBarItems[5];

void oledInit()
{
    u8g2.begin();
    u8g2.setContrast(contrastValue);
    u8g2.setFontDirection(0);
    //setWritingFontSize(fSize);
    u8g2.enableUTF8Print(); // enable UTF8 support
    u8g2.setPowerSave(0);
    u8g2.clearBuffer();
    setContrast(highContrast);
    setWritingFontSize(smallFont);
}

void oledU8g2Test(void)
{
    // updateNotificationBar(usrOpt);
    u8g2.drawStr(0, currentLinesIndices[1], "ProTon Technologies");
    u8g2.drawStr(0, currentLinesIndices[2], "User Interface");
    u8g2.drawStr(0, currentLinesIndices[3], "User Interface");

    u8g2.sendBuffer();
}

void oledPrintList(listItem *list, int itemCount, int selectedItemIndex)
{
    /*  1-clear oled
        2-re-write Notification bar
        3-drow the list starting by selected item.
    
    for(size_t i = 0; i < itemCount && i<3; i++)
    {
        Serial.println(list[i].itemName);
    }
    Serial.println("menu sended back..");
    Serial.print("menuItem count: ");           Serial.println(itemCount);
    Serial.print("selectedMenuItemIndex: ");    Serial.println(selectedItemIndex);
    Serial.println("menu sended back is end..");
    //u8g2.setCursor(0, 10);
    //u8g2.setCursor(0, 26);
    */

    u8g2.clearBuffer();
    switch (fSize)
    {
    case smallFont:
        _oledPrintListSmlFnt(list, itemCount, selectedItemIndex);
        break;
    case midFont:
        _oledPrintListMidFnt(list, itemCount, selectedItemIndex);
        break;
    case largFont:
        //_oledPrintListLrgFnt(list, itemCount, selectedItemIndex);
        break;
    }
    u8g2.sendBuffer();
}

void _oledPrintListSmlFnt(listItem *list, int itemCount, int selectedItemIndex)
{
    String *lineContanet;
    uint8_t idx = 0;

    for (size_t i = 0; i < itemCount; i++)
    {
        if (i == selectedItemIndex - 1)
            u8g2.drawStr(0, currentLinesIndices[0], string2char("   " + list[selectedItemIndex - 1].itemName));
        if (i == selectedItemIndex) //here we add pointer
            u8g2.drawStr(0, currentLinesIndices[1], string2char(">" + list[selectedItemIndex].itemName));
        if (i == selectedItemIndex + 1)
        {
            u8g2.drawStr(0, currentLinesIndices[2], string2char("   " + list[selectedItemIndex + 1].itemName));
            u8g2.drawStr(0, currentLinesIndices[3], string2char("--------------------"));
        }
    }
}

void _oledPrintListMidFnt(listItem *list, int itemCount, int selectedItemIndex)
{
    u8g2.drawStr(0, currentLinesIndices[1], string2char(list[selectedItemIndex].itemName));
}

void _oledPrintListLrgFnt(listItem *list, int itemCount, int selectedItemIndex)
{
    /*
    if (selectedItemIndex == 0 ||selectedItemIndex == (itemCount-1))
    {
        Serial.println("Edged printing here.");
    }
    else
    {
        Serial.println("normal printing here.");
    }
    */
    String *lineContanet;
    uint8_t idx = 0;
    for (size_t i = selectedItemIndex; i < itemCount && i <= (selectedItemIndex + 3); i++)
        if (i == selectedItemIndex) //here we add pointer
            u8g2.drawStr(0, currentLinesIndices[idx], string2char(">" + list[selectedItemIndex + (idx++)].itemName));
        else
            u8g2.drawStr(0, currentLinesIndices[idx], string2char("   " + list[selectedItemIndex + (idx++)].itemName));
}

void oledPrintNormalMode(double dBVoiceVolum,String *usrOpt)
{
    u8g2.clearBuffer();
    //u8g2.setFont(u8g2_font_7x14_mf);
    updateNotificationBar(usrOpt);
    //u8g2.drawStr(0 , 64 , string2char("voice volume:"));
    u8g2.setFont(u8g2_font_fub20_tf);
    u8g2.drawStr(0, 50, string2char("" + String(dBVoiceVolum)));
    u8g2.drawStr(85, 50, string2char("dB"));
    u8g2.sendBuffer();
}
void OledPrintLogo()
{
  u8g2.clearBuffer();
  u8g2.drawBitmap( 0, 0, 16, 64, ProTonLogo);  
  u8g2.sendBuffer(); 
}

///BLE 
void oledBleConnectNotifier(bool state)
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_crox4hb_tf);
  if (state)
    {
      u8g2.drawStr(30, 30, string2char("Mobile"));
      u8g2.drawStr(10, 50, string2char("connected"));
        //u8g2.drawBitmap( 0, 0, 16, 64, BleConictedLogo);
    } 
  else
    {
      u8g2.drawStr(30, 30, string2char("Mobile"));
      u8g2.drawStr(5, 50, string2char("disconnected"));
      //u8g2.drawBitmap( 0, 0, 16, 64, BleDisconictedLogo);
    } 
  u8g2.sendBuffer();   
}

void myPerfectAudio(String SS)
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_inb19_mf);
  u8g2.drawStr(0, 50, string2char(SS));
  u8g2.sendBuffer();
}

void oledBLegreetingUser(String UsrName)
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_t0_22b_tf);
  u8g2.drawStr(0, 50, string2char("Hallo " + UsrName));
  u8g2.sendBuffer();
}

void oledBleAlgorithmNotifier(int Algorithm)
{

}
  
void oledBleChangeVoiceVolume(double Vol,int algo,String Intinsity,bool mute,bool Bass,bool comp)
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_fub11_tf  );
  u8g2.drawStr(80, 15, string2char( Intinsity + ""));
  if (Bass)
    u8g2.drawStr(30, 15, string2char(" B "));
  else if(!Bass)
    u8g2.drawStr(30, 15, string2char("   "));
  if (comp)
    u8g2.drawStr(60, 15, string2char(" C "));
  else if(!comp)
    u8g2.drawStr(60, 15, string2char("   "));
  //u8g2.drawStr(110, 10, string2char());  
    switch (algo)
    {
    case 0 :
        u8g2.drawStr(0, 15, string2char("Lin"));
        break;
    case 1 :
        u8g2.drawStr(0, 15, string2char("P1"));
        break;
    case 2 :
        u8g2.drawStr(0, 10, string2char("P2"));
        break;
    }  
  u8g2.setFont(u8g2_font_fub20_tf);
  
   if(mute)
     u8g2.drawStr(0, 50, string2char("MUTE"));
   else
    {
        u8g2.drawStr(0, 50, string2char("" + String(Vol)));
        u8g2.drawStr(85, 50, string2char("dB"));
    }
  u8g2.sendBuffer(); 
}

void oledBleHearingTestNotifier(int Dir ,int freqInx,double volValue)
{
    int tempFreqs[31] = {
   20, 25, 31, 40, 50, 63, 80, 100,125, 160, 200, 250, 315, 400, 500,630, 800, 1000,
    1250, 1600,2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000
    };
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_t0_17b_tf);
  u8g2.drawStr(0, 25, string2char(" Hearing Test"));
  u8g2.setFont(u8g2_font_t0_22b_tf); 
  u8g2.drawStr(0, 50, string2char( String(tempFreqs[freqInx])+":"+ String(volValue)));
  u8g2.sendBuffer(); 
}

// void oledBleupdateNotificationBar(String *usrOpt)
// {
//     // u8g2.clearBuffer();
//     u8g2.setFont(u8g2_font_7x14_mf);
   
//     u8g2.drawStr(0,  12,string2char (usrOpt[0]));
//     u8g2.drawStr(12, 12,string2char (usrOpt[1]));
//     u8g2.drawStr(40, 12,string2char (usrOpt[2]));
//     u8g2.drawStr(80, 12,string2char (usrOpt[3]));
//     u8g2.drawStr(96, 12,string2char (usrOpt[4]));
// }

void oledStartUp(void)
{
    //TODO: add here progress bar for downloading program;
    u8g2.setFont(u8g2_font_7x14_mf);
    for (size_t i = 0; i < 13; i++)
    {
        u8g2.clearBuffer();
        u8g2.setCursor(0, 26);
        u8g2.drawFrame(0, 26, 120, 10);
        u8g2.drawBox(0, 26, i * 10, 10);
        u8g2.drawStr(0, 56, "download setting");
        u8g2.sendBuffer();
        delay(75);
    }
    delay(500);
    u8g2.clearBuffer();
    u8g2.drawFrame(0, 26, 120, 10);
    u8g2.drawBox(0, 26, 120, 10);
    u8g2.drawStr(0, 56, "  download Done");
    u8g2.sendBuffer();
}

void updateNotificationBar(String *usrOpt)
{
    // u8g2.clearBuffer();
    //u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.setFont(u8g2_font_fub11_tf  );
   /*
    u8g2.drawStr(0,  10,string2char (usrOpt[0]));
    u8g2.drawStr(32, 10,string2char (usrOpt[1]));
    u8g2.drawStr(64, 10,string2char (usrOpt[2]));
    u8g2.drawStr(96, 10,string2char (usrOpt[3]));
  */
    //u8g2.drawStr(0,  12,string2char ("U:" + usrOpt[0]));        // User Number Notifier
    u8g2.drawStr(0, 12,string2char  (usrOpt[1]));               // Algorithm Notifier
    u8g2.drawStr(25, 12,string2char (usrOpt[2]));               //BLE Notifier 
    u8g2.drawStr(60, 12,string2char (usrOpt[3]));               //Mute Notifier
    u8g2.drawStr(90, 12,string2char ( usrOpt[4] + ""));  //Intinsity Notifier
}

//this is the start/boot screen
bool printSplachScreen(void)
{
    //TODO:

    return true;
}

char *string2char(String command)
{
    if (command.length() != 0)
    {
        char *p = const_cast<char *>(command.c_str());
        return p;
    }
}

void StringPtrToStringArray(String *src, String des[])
{
    //TODO: how to ensure they are matched in length??!!
    /*
        for(size_t i = 0; i < des.length(); i++)
        {
            des[i] = *(src+i);
        }
    */
}

void _writeClice(void)
{
}

void setWritingFontSize(FontSize newSize)
{
    // fSize = newSize;
    switch (newSize)
    {
    case smallFont:
        currentLinesIndices = linesIndicesSmlFont;
        u8g2.setFont(u8g2_font_7x14_mf);
        break;

    case midFont:
        currentLinesIndices = linesIndicesMidFont;
        u8g2.setFont(u8g2_font_fub11_tf);
        break;
    case largFont:
        currentLinesIndices = linesIndicesLrgFont;
        u8g2.setFont(u8g2_font_fub20_tf);
        break;
    }
}

void printEditModeMenu(String *editMenuItems, String *currentUsrOpt)
{
    //TODO: add code to mange largFont
    //editModeMenuItems = editMenuItems;
    //notificationBarItems = currentUsrOpt;
    setWritingFontSize(midFont);
    StringPtrToStringArray(currentUsrOpt, notificationBarItems);

    Serial.print("currentUser:");
    Serial.println(notificationBarItems[0]);
    for (size_t i = 0; i < 5; i++)
    {
        Serial.println(editMenuItems[i]);
    }
    Serial.println("======");
    // String line1 = editMenuItems[0] + editMenuItems[1] + editMenuItems[2] ;
    // String line2 = editMenuItems[3] + editMenuItems[4];
    u8g2.clearBuffer();
    
    u8g2.drawStr(10, 25, string2char(editMenuItems[0]));
    u8g2.drawStr(40, 25, string2char(editMenuItems[1]));
    u8g2.drawStr(80, 25, string2char(editMenuItems[2]));
    //u8g2.drawStr(0, currentLinesIndices[1], string2char(line1));
    
    u8g2.drawStr(5, currentLinesIndices[2], string2char(editMenuItems[3]));
    u8g2.drawStr(64, currentLinesIndices[2], string2char(editMenuItems[4]));

    //u8g2.drawStr(0, currentLinesIndices[2], string2char(line2));
    u8g2.sendBuffer();
}

void showSelectedItem(String header,String budy)
{
    u8g2.clearBuffer();
    u8g2.setCursor(0, linesIndicesSmlFont[0]);
    //u8g2.drawFrame(0, 26, 120, 10);
    //u8g2.drawBox(0, 26, i * 10, 10);
    u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.drawStr(0, linesIndicesSmlFont[0], string2char(String(header)));
    u8g2.setFont(u8g2_font_fub20_tf);
    u8g2.drawStr(0, linesIndicesSmlFont[2], string2char(String(budy)));
    u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.sendBuffer();
    delay(10);
}

void showSelectedLevel(String header,String budy)
{
    u8g2.clearBuffer();
    u8g2.setCursor(0, linesIndicesSmlFont[0]);

    u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.drawStr(40, linesIndicesMidFont[0], string2char(String(header)));
   // u8g2.drawStr(25, linesIndicesMidFont[0], string2char(String("<")));
    u8g2.setFont(u8g2_font_courB18_tf );
    u8g2.drawStr(40, linesIndicesMidFont[2], string2char(String(budy)));
    u8g2.drawStr(85, linesIndicesMidFont[2], string2char(String("%")));
    u8g2.drawTriangle(30,40, 25,44, 30,48);
    u8g2.drawTriangle(105,40, 110,44, 105,48);
    //u8g2.drawStr(80, linesIndicesMidFont[2], string2char(String(">")));
    u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.sendBuffer();
    delay(10);
}

void oledShowFreq(String dir,float freq, float hearingTestvolumedB)
{
    //String str = String(hearingTestvolumedB);
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_t0_15b_tf );
    u8g2.drawStr(0, linesIndicesMidFont[0], string2char(String(dir)));
    u8g2.drawStr(0, linesIndicesMidFont[1], string2char("f: "+String(freq)+" Hz"));
    u8g2.drawStr(0, linesIndicesMidFont[2], string2char("Vol: "+String(hearingTestvolumedB)+" dB"));
    u8g2.sendBuffer();
    delay(10);

}


void oledTestBuff1(void)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.drawStr(10, linesIndicesMidFont[0], string2char(String("buffer cleared")));
    u8g2.sendBuffer();
    delay(10);
}
void oledTestBuff2(void)
{
    // u8g2.clearBuffer();
    //u8g2.setFont(u8g2_font_7x14_mf);
    u8g2.drawStr(0, linesIndicesMidFont[2], string2char(String("added without clearing")));
    u8g2.sendBuffer();
    delay(10);
}

void setContrast(ContrastValues newContrastValue)
{
    switch (newContrastValue)
    {
    case lowContrast:
        contrastValue = 0;
        break;
    case midContrast:
        contrastValue = 100;
        break;
    case highContrast:
        contrastValue = 255;
        break;
    }
}