#include <Arduino.h>

#ifndef CS43131_config_h
#define CS43131_config_h

#ifndef SoftI2C_h
#include "SoftI2C_mod.h"
#endif

#ifndef SDA_Pin
#define SDA_PIN 21
#endif
#ifndef SCL_PIN
#define SCL_PIN 22
#endif

extern byte cs43131_i2c_addr;

extern byte cs43131_dat[50];

void config_cs43131(byte ic_addr, byte * data, int data_length);

#endif