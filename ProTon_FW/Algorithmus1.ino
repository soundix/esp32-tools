/*
* Algorithmus1.ino
* Version 0.3.0.2
*/

#include "headphoneParameter.h"
#include <Arduino.h>
#define FREQ 0   // Speicheradresse für Array
#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array




extern uint8_t SoundSelect;
extern bool fineState;
int jj = 0;

float saveIteration1[3][31] = { // Profil 1
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

float saveIteration2[3][31] = { // Profil 2
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

/*
float saveIteration3[3][9] = { // Profil 3
    63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00};
*/
float hearingTestSWAP[3][31] = { // Zwischenspeicherung des Messeregnisses
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};
/*
float testAndi[3][9] = {
    {63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},
    {30.00, 15.00, 15.00, 5.00, 5.00, 25.00, 40.00, 45.00, 40.00},
    {30.00, 10.00, 15.00, 5.00, 15.00, 30.00, 50.00, 50.00, 45.00}};
*/
float hearingTestErgebnis[3][31]{// Gespeichertes Ergebnis des Hörtests
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

extern float headphoneMW[3][1];



/*
float saveIteration1[3][9];
float saveIteration2[3][9];
float saveIteration3[3][9];
*/
extern int f_countMAX;

float MWLINKS;
float MWRECHTS;
float MAXvalue;
float MINvalue;
float SchrittLinks = 0.00;
float SchrittRechts = 0.00;
uint8_t hearingLeftCASE = 0;
uint8_t hearingRightCASE = 0; // Fallunterscheidung
                              // 1. Hörkurve innerhalb Algo1NormalGrenze (25dB)
                              // 2. ein Teil der Hörkurve außerhalb der Algo1NormalGrenze
                              // 3. Hörkurve außerhalb der Algo1NormalGrenze
                              // 4. Hörkurve um Algo1NormalGrenze herum
float left20, left25, left31, left40, left50, left63, left80, left100, left125, left160, left200, left250, left315, left400, left500, left630, left800, left1000, left1250, left1600, left2000, left2500, left3150, left4000, left5000, left6300, left8000, left10000, left12500, left16000, left20000;
float right20, right25, right31, right40, right50, right63, right80, right100, right125, right160, right200, right250, right315, right400, right500, right630, right800, right1000, right1250, right1600, right2000, right2500, right3150, right4000, right5000, right6300, right8000, right10000, right12500, right16000, right20000;

void hearingTestSAVEinSWAP(void);
void MainDynamicBassBypassON(void);
void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void hearingTestIntensity(void);
//float MINcalculate(int Seite);
float MINcalculate(int Seite);
float MAXcalculate(int Seite);
void choose_algo();
int choose_algo_seite(int);
/*
 *  void Algo1(void); 
 *  einfacher Algorithmus zur Demonstration auf Messe
 *  es werden stupide in drei Iterationsschritten die Defizite angehoben und alle drei Profile zum Hören angeboten
 */

float MW[2], MIN[2], MAX[2];
int AnzahlkleinerGrenze[2], AnzahlgroesserGrenze[2], AnzahlgleichGrenze[2];

void Algo1(void)
{
    hearingTestSAVEinSWAP();                                                        // Zwischenspeichern des Hörtests

    /*
    MWLINKS = MittelwertBACK(LINKS);                                                 // Mittelwert linke Seite bilden
    MWRECHTS = MittelwertBACK(RECHTS);                                               // Mittelwert rechte Seite bilden
    int AnzahlkleinerGrenzeLINKS = KurvekleinerGrenze(LINKS, Algo1NormalGrenze);     // Anzahl der Messpunkte innerhalb der Grenze links
    int AnzahlkleinerGrenzeRECHTS = KurvekleinerGrenze(RECHTS, Algo1NormalGrenze);   // Anzahl der Messpunkte innerhalb der Grenze rechte
    int AnzahlgroesserGrenzeLINKS = KurvegroesserGrenze(LINKS, Algo1NormalGrenze);   // Anzahl der Messpunkte außerhalb der Grenze links
    int AnzahlgroesserGrenzeRECHTS = KurvegroesserGrenze(RECHTS, Algo1NormalGrenze); // Anzahl der Messpunkte außerhalb der Grenze rechts
    int AnzahlgleichGrenzeLINKS = KurvegleichGrenze(LINKS, Algo1NormalGrenze);       // Anzahl der Messpunkte auf der Grenze links (inkl. Toleranzband)
    int AnzahlgleichGrenzeRECHTS = KurvegleichGrenze(RECHTS, Algo1NormalGrenze);     // Anzahl der Messpunkte auf der Grenze rechts (inkl. Toleranzband)
    float MINlinks = MINcalculate(LINKS);
    float MINrechts = MINcalculate(RECHTS);
    float MAXlinks = MAXcalculate(LINKS);
    float MAXrechts = MAXcalculate(RECHTS);
    */


    float MWSWAP[2] = {0.00, 0.00};
    //float MWSWAPR = 0.00;
    //int AnzahlkleinerGrenze[2] = {0, 0};
    //int AnzahlkleinerGrenzeRECHTS = 0;
    //int AnzahlgroesserGrenze[2] = {0, 0};
    //int AnzahlgroesserGrenzeRECHTS = 0;
    //int AnzahlgleichGrenze[2] = {0, 0};
    //int AnzahlgleichGrenzeRECHTS = 0;

    if(fineState){

    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        MWSWAP[LINKS-1] = MWSWAP[LINKS-1] + hearingTestErgebnis[LINKS][kk];
        MWSWAP[RECHTS-1] = MWSWAP[RECHTS-1] + hearingTestErgebnis[RECHTS][kk];

        if (hearingTestErgebnis[LINKS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[RECHTS-1]++;
        }

    }

    MW[LINKS-1] = MWSWAP[LINKS-1] / (f_countMAX + 1);
    MW[RECHTS-1] = MWSWAP[RECHTS-1] / (f_countMAX + 1);
    
    MIN[LINKS-1] = hearingTestSAVE[LINKS][0];
    MIN[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[LINKS][kk] < MIN[LINKS-1])
        {
            MIN[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] < MIN[RECHTS-1])
        {
            MIN[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    MAX[LINKS-1] = hearingTestSAVE[LINKS][0];
    MAX[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[LINKS][kk] >= MAX[LINKS-1])
        {
            MAX[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] >= MAX[RECHTS-1])
        {
            MAX[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    } else {

    for (int kk = 5; kk <= f_countMAX; kk += 3)
    {
        MWSWAP[LINKS-1] = MWSWAP[LINKS-1] + hearingTestErgebnis[LINKS][kk];
        MWSWAP[RECHTS-1] = MWSWAP[RECHTS-1] + hearingTestErgebnis[RECHTS][kk];

        if (hearingTestErgebnis[LINKS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[RECHTS-1]++;
        }

    }

    MW[LINKS-1] = MWSWAP[LINKS-1] / (f_countMAX + 1);
    MW[RECHTS-1] = MWSWAP[RECHTS-1] / (f_countMAX + 1);
    
    MIN[LINKS-1] = hearingTestSAVE[LINKS][0];
    MIN[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 8; kk <= f_countMAX; kk += 3)
   {
        if (hearingTestSAVE[LINKS][kk] < MIN[LINKS-1])
        {
            MIN[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] < MIN[RECHTS-1])
        {
            MIN[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }
 
    MAX[LINKS-1] = hearingTestSAVE[LINKS][5];
    MAX[RECHTS-1] = hearingTestSAVE[RECHTS][5];

    for (int kk = 8; kk <= f_countMAX; kk += 3)
    {
        if (hearingTestSAVE[LINKS][kk] >= MAX[LINKS-1])
        {
            MAX[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] >= MAX[RECHTS-1])
        {
            MAX[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    }

    Serial.print(F("Anzahl kleiner Grenze Links: "));
    Serial.println(AnzahlkleinerGrenze[LINKS-1]);
    Serial.print(F("Anzahl kleiner Grenze Rechts: "));
    Serial.println(AnzahlkleinerGrenze[RECHTS-1]);
    Serial.print(F("Anzahl groesser Grenze Links: "));
    Serial.println(AnzahlgroesserGrenze[LINKS-1]);
    Serial.print(F("Anzahl groesser Grenze Rechts: "));
    Serial.println(AnzahlgroesserGrenze[RECHTS-1]);
    Serial.print(F("Anzahl gleich Grenze Links: "));
    Serial.println(AnzahlgleichGrenze[LINKS-1]);
    Serial.print(F("Anzahl gleich Grenze Rechts: "));
    Serial.println(AnzahlgleichGrenze[RECHTS-1]);
    Serial.print(F("Mittelwert links: "));
    Serial.println(MW[LINKS-1]);
    Serial.print(F("Mittelwert rechts: "));
    Serial.println(MW[RECHTS-1]);
    Serial.print(F("Minimum links: "));
    Serial.println(MIN[LINKS-1]);
    Serial.print(F("Minimum rechts: "));
    Serial.println(MIN[RECHTS-1]);
    Serial.print(F("Maximum links: "));
    Serial.println(MAX[LINKS-1]);
    Serial.print(F("Maximum rechts: "));
    Serial.println(MAX[RECHTS-1]);

    hearingTestIntensity();

    //choose_algo();

    Iteration1(LINKS);
    Iteration1(RECHTS);
    Iteration2(LINKS);
    Iteration2(RECHTS);

    
    if (headphone == false)
    {  
    
        
        Iteration1(LINKS);
        Iteration1(RECHTS);
        Iteration2(LINKS);
        Iteration2(RECHTS);
        // Iteration3(LINKS);
        //Iteration3(RECHTS);
    }

    if (headphone == true)
    {
        Iteration1_ohneHeadphone(LINKS);
        Iteration1_ohneHeadphone(RECHTS);
        //Iteration2_ohneHeadphone(LINKS);
        //Iteration2_ohneHeadphone(RECHTS);
        // Iteration3(LINKS);
        //Iteration3(RECHTS);
    }

    
}

// Algorithmus P1
void Iteration1(int Seite)
{

    for (jj = 0; jj <= f_countMAX; jj++)
    {
        saveIteration1[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
    }
}


void Iteration1_ohneHeadphone(int Seite)
{
    for (jj = 0; jj <= f_countMAX; jj++)
    {
        saveIteration1[Seite][jj] = (((HeadphoneFrequenzgang[Seite][jj] - headphoneMW[Seite][1]) + hearingTestSWAP[Seite][jj]) / 2);
    }
}


void Iteration2(int Seite)
{

    if(fineState){
        for (jj = 0; jj <= f_countMAX; jj++)
        {
            
            //switch (SoundSelect)
            switch (3)
            {
            case 0:
                Sound2(Seite);
                break;

            case 1:
                Sound3(Seite);
                break;

            case 2:
                Sound4(Seite);
                break;

            case 3:
                HPcomp(Seite);
                break;
            default:
                Sound5(Seite);
                break;
            }
            
        //Sound5(Seite);
        }
    } else {

        for (jj = 5; jj <= f_countMAX; jj += 3)
        {
            
            //switch (SoundSelect)
            switch (3)
            {
            case 0:
                Sound2(Seite);
                break;

            case 1:
                Sound3(Seite);
                break;

            case 2:
                Sound4(Seite);
                break;

            case 3:
                HPcomp(Seite);
                break;
            default:
                Sound5(Seite);
                break;
            }
            
        //Sound5(Seite);
        }

    }
    
}

//kopfhörer HD 660 kompensation
void HPcomp(int Seite){
    for (jj = 0; jj <= f_countMAX; jj++)
        {
            saveIteration1[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
        }
}

//Algorithmus P2
void Sound2(int Seite)
{

    //saveIteration2[Seite][jj] = saveIteration1[Seite][jj] + (saveIteration1[Seite][jj] / 8);
    if (hearingTestSAVE[Seite][jj] <= MW[Seite-1])
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] - MW[Seite-1]) + (MW[Seite-1] / 3);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    if (hearingTestSAVE[Seite][jj] >MW[Seite-1])
    {
        //saveIteration2[Seite][jj] = MittelwertBACK(Seite) - hearingTestSWAP[Seite][jj];
        saveIteration2[Seite][jj] = (MW[Seite-1] - hearingTestSWAP[Seite][jj]) + (MW[Seite-1] / 3);
    }
}

//Algo P3
void Sound3(int Seite)
{

    if ((hearingTestSAVE[Seite][jj] < MAX[Seite-1]) && (MAX[Seite-1] - hearingTestSAVE[Seite][jj] <= 20.00))
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (MAX[Seite-1]) - hearingTestSWAP[Seite][jj] + (hearingTestSWAP[Seite][jj]/3);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    if ((hearingTestSAVE[Seite][jj] < MAX[Seite-1]) && (MAX[Seite-1] - hearingTestSAVE[Seite][jj] > 20.00))
    {
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
    }
}

//Algorithmus P4
void Sound4(int Seite)
{

    if (hearingTestSAVE[Seite][jj] <= Algo1NormalGrenze)
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
}

/*
void Iteration2(int Seite)
{
    */

//Algorithmus P5
void Sound5(int Seite)
{
    float neueGrenze;
    //if (MAXcalculate(Seite) <= 0.00)
    //{
    neueGrenze = (MAX[Seite-1] + MW[Seite-1]) / 2;
    //}
    //if (MAXcalculate(Seite) > 0.00)
    //{
    //  neueGrenze = MittelwertBACK(Seite) - MAXcalculate(Seite);
    //}
    Serial.print(F("neue Grenze: "));
    Serial.println(neueGrenze);

    if (hearingTestSAVE[Seite][jj] <= neueGrenze)
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    /*       
        if (hearingTestSAVE[Seite][jj] > MittelwertBACK(Seite))
        {
           
            saveIteration2[Seite][jj] = (MittelwertBACK(Seite) - hearingTestSWAP[Seite][jj]) + (MittelwertBACK(Seite) / 3);
        }
        */
}

/************************************************************
 * float MittelwertBACK(int Seite)
 * int Seite: 1 = links; 2 = RECHTS
 * 
 * bildet arithmetischen Mittelwert aller Messpunkte
 * und gibt ihn als float zurück
 ************************************************************/
float MittelwertBACK(int Seite)
{

    float MWSWAP = 0.00;
    float Mittelwert = 0.00;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        MWSWAP = MWSWAP + hearingTestErgebnis[Seite][kk];
    }
    return Mittelwert = MWSWAP / (f_countMAX + 1);
}

/************************************************************
 * int KurvekleinerGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * innerhalb der Grenze liegen
 ************************************************************/
int KurvekleinerGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] <= GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

/************************************************************
 * int KurvegroesserGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * außerhalb der Grenze liegen
 ************************************************************/
int KurvegroesserGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] > GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

/************************************************************
 * int KurvegleichGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * auf der Grenze liegen (inkl. Toleranzband)
 ************************************************************/
int KurvegleichGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] == GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

float MINcalculate(int Seite)
{
    float min = hearingTestSAVE[Seite][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[Seite][kk] < min)
        {
            min = hearingTestSAVE[Seite][kk];
        }
    }
    return min;
}

float MAXcalculate(int Seite)
{
    float max = hearingTestSAVE[Seite][0];

    for (int kk = 1; kk <= f_countMAX;kk++)
    {
        if (hearingTestSAVE[Seite][kk] >= max)
        {
            max = hearingTestSAVE[Seite][kk];
        }
    }
    return max;
}

int choose_algo_seite(int Seite){ //0 = Hifi; 1 = Medical1; 2 = Medical2

    int klassifiziert[9] = {0}; //0: below 30db, 1: hysteresis, 2: above 25db
    int anzahlMesspunkte[3] = {0}; //0: below 30db, 1: hysteresis, 2: above 25db

    for( int i = 0; i < f_countMAX; i++){ //label the data

        if( hearingTestSAVE[Seite][i] > 40){ //below 40db

            return 2; //2 = Medical

        } else if( hearingTestSAVE[Seite][i] > 30){ //below 30db

            klassifiziert[i] = 0;
            anzahlMesspunkte[0]++; 

        } else if( hearingTestSAVE[Seite][i] > 25){ //between 30db and 25db

            klassifiziert[i] = 1;
            anzahlMesspunkte[1]++;

        } else { //above 25db

            klassifiziert[i] = 2;
            anzahlMesspunkte[2]++;

        }

    }
    
    //Hifi

    if( anzahlMesspunkte[2] == (f_countMAX || (f_countMAX - 1)) ) { //all above 25db || one below 35db, other above 25db

        return 0;

    } /* else

    if( (anzahlMesspunkte[2] == (f_countMAX - 2)) && (anzahlMesspunkte[1] == 2)){ //two in hysteresis, other above 25db

        bool tmp = false;

        for(int i = 0; i < (f_countMAX - 1) ; i++){

            if( (klassifiziert[i] == 1) && (klassifiziert[i+1] == 1) ){ //neighbors in hysteresis

                for(int j = 2; j <= (f_countMAX - i); j++){

                    if( klassifiziert[i + j] < 2){ //any other below 25db

                        tmp = true;
                        break;

                    }

                }

            }else if( (i == (f_countMAX - 1))){

                return 0; //0 = Hifi

            }else if(tmp)
            {
                break;
            }
            

        }

    } */
    else if( (anzahlMesspunkte[2] == (f_countMAX - 2)) && (anzahlMesspunkte[1] == 2)){ //two in hysteresis

        for (int i = 0; i < f_countMAX; i++)
        {
            if ( (klassifiziert[i] == 1) && (klassifiziert[i + 1] == 1) )
            {
                return 1;
            }
            
        }

        return 0;
        
    
    } else if((anzahlMesspunkte[2] = f_countMAX-1)){

                return 0; //0 = Hifi

            } else

    //Medical

    if (anzahlMesspunkte[1] >= 3) { // three or more in hysteresis
  
        return 1;

    } else

    if( (anzahlMesspunkte[1] >= 1) && (anzahlMesspunkte[0] = 1)){ // min. one in hyteresis and one below 35db

        return 1;

    } else { //everything else

        return 1;

    }
    

}


void choose_algo(){

    Serial.println("Algorithmus linke Seite: ");
    int temp = choose_algo_seite(LINKS);
        switch (temp)
        {
        case 0:
            Iteration2(LINKS);
            Serial.println("Hifi");
            break;
        
        case 1:
            Iteration1(LINKS);
            Serial.println("Medical1");
            break;

        case 2:
            Iteration1(LINKS);
            Serial.println("Medical2");
            break;

        default:
            Serial.println("something went wrong...");
            break;
        }
        
        Serial.println("Algorithmus rechte Seite: ");
        temp = choose_algo_seite(RECHTS);
        switch (temp)
        {
        case 0:
            Iteration2(RECHTS);
            Serial.println("Hifi");
            break;
        
        case 1:
            Iteration1(RECHTS);
            Serial.println("Medical1");
            break;

        case 2:
            Iteration1(RECHTS);
            Serial.println("Medical2");
            break;

        default:
            Serial.println("something went wrong...");
            break;
        }
}