/*
*   Eingabe von Steuerbefehlen zur Kontrolle des ProTon plus über seriellen Monitor
*
*/

#include "Wire.h"

//int incomingByte;
extern double dBVoiceVolum;

void ReadSerialInput(void)
{
  int i;
  int eingabe[7];
  while (Serial.available() > 0)
  {
    i++;
  }
  if (eingabe[0] == '*' && eingabe[3] == '+' && eingabe[6] == '#')
  {
    if (eingabe[0] == '0' && eingabe[1] == '1')
    {
      dBVoiceVolum = -(eingabe[4]);
      printOnTerminal(" Master Vol. : \n" + String(dBVoiceVolum) + "dB");
      oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
      //updateNotificationBar(currentUserOptions);
      VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    }
  }
}