/*
 * AudioFunktionen.ino
 *  * * Version 0.3.0.2
 */
#include"arduino.h"
#include "AidaDSP.h"

extern uint8_t user1FLAG;
extern uint8_t user2FLAG;
extern uint8_t user3FLAG;
extern uint8_t linFLAG;
extern uint8_t compFLAG;
extern uint8_t per1FLAG;
extern uint8_t per2FLAG;
extern uint8_t per3FLAG;
extern uint8_t bassFLAG;

extern void AIDA_WRITE_REGISTER(uint8_t dspAddress, uint16_t address, uint8_t length, uint8_t *data);

void setMainDynamicBassBypass(bool );
void setPersonalizedCompressorBypassMode(int mode);

enum compressorMode 
{
CompressorMode_OFF,          //no compresor at all
CompressorMode_MultiBand,    //multiBand compresor
CompressorMode_RMS           //RMS compresor
};

compressorMode currentCompressorMode = CompressorMode_OFF;


void MainDynamicBassBypassON(void);
void MainDynamicBassBypassOFF(void);
void MainDirectMode(void);
void MainPersonalizedMode(void );
void MainHearingTestMode(void);
void MainEQBypassON(void);
void MainEQBypassOFF(void);
void PersonalizedMultibandCompressorBypassCompOFF(void);
void PersonalizedMultibandCompressorBypassMultibandCompressor(void);
void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void hearingSineSwitchLeft(void);
void hearingSineSwitchRight(void);
void muteADAU(void);
void unmuteADAU(void);



void setMainDynamicBassBypass(bool enable)
{
    if (enable) 
    {
        muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
        Serial.println(F("Bass On.."));
    }
    else 
    {
        muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
        Serial.println(F("Bass off.."));
    }
    bassFLAG = enable;
}

void setPersonalizedCompressorBypassMode(int mode)
{
    switch (mode)
    {
        case 0:                         //off
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 1);
             Serial.println(F("Comp off.."));
            compFLAG = false;
        break;
    
        case 1:                         //MultibandCompressor
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 2);
             Serial.println(F("Comp On .."));
        break;
/*
        case 2:                         //RMSCompressor
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 3);
            compFLAG = true;
        break;
        */
    }

}
/******************************
 * void MainDynamicBassBypassON(void)
 * 
 * schaltet Dynamic Bass AN
 *****************************/
void MainDynamicBassBypassON(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
    bassFLAG = true;
}

/******************************
 * void MainDynamicBassBypassOFF(void)
 * 
 * schaltet Dynamic Bass AUS
 *****************************/
void MainDynamicBassBypassOFF(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
    bassFLAG = false;
}

/******************************
 * void MainDirectMode(void)
 * 
 * schaltet in direct mode
 *****************************/
void MainDirectMode(void)
{
    muteADAU();
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 1, 3);
    unmuteADAU();
    Serial.println(F("1 - Direkt"));
}

/******************************
 * void MainPersonalizedMode( int ,int )
 * 
 * schaltet in personalized mode
 *****************************/
void MainPersonalizedMode( void )
{
    Serial.println(F("2 - personalized"));
    //MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, hearingTestvolumedB / 20.0));
    muteADAU();
    Algo1( );
    EqConfigurations( );
    delay(200);
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 2, 3);
    unmuteADAU();
    //PersonalizedMultibandCompressorBypassRMSCompressor();
   //MainDynamicBassBypassON();
}

/******************************
 * void MainHearingTestMode(void)
 * 
 * schaltet Hörtest an
 *****************************/
void MainHearingTestMode(void)
{
    //mux(DEVICE_ADDR_7bit, EQBypassAddr, 1, 2);
    muteADAU();
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
    unmuteADAU();
    Serial.println(F("123 - Hörtest"));
    //hearingTest();
}

/******************************
 * void MainEQBypassON(void)
 * 
 * schaltet EQ Aus
 *****************************/
void MainEQBypassON(void)
{
    //muxnoiseless(DEVICE_ADDR_7bit, EQBypassAddr, 1);
}

/******************************
 * void MainEQBypassOFF(void)
 * 
 * schaltet EQ An
 *****************************/
void MainEQBypassOFF(void)
{
    //muxnoiseless(DEVICE_ADDR_7bit, EQBypassAddr, 2);
}

/******************************
 * void PersonalizedMultibandCompressorBypassCompOFF(void)
 * 
 * schaltet Kompressor aus
 *****************************/
void PersonalizedMultibandCompressorBypassCompOFF(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 1);
    compFLAG = false;
}

/******************************
 * void PersonalizedMultibandCompressorBypassMultibandCompressor(void)
 * 
 * schaltet MultibandKompressor an
 *****************************/
void PersonalizedMultibandCompressorBypassMultibandCompressor(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 2);
    compFLAG = true;
}

/******************************
 * void PersonalizedMultibandCompressorBypassRMSCompressor(void)
 * 
 * schaltet RMSKompressor an
 *****************************/
void PersonalizedMultibandCompressorBypassRMSCompressor(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 3);
    compFLAG = true;
}

/******************************
 * void hearingSineSwitchLeft(void)
 * 
 * schaltet Sinusgenerator auf linken Kanal
 *****************************/
void hearingSineSwitchLeft(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1);
}

/******************************
 * void hearingSineSwitchRight(void)
 * 
 * schaltet Sinusgenerator auf rechten Kanal
 *****************************/
void hearingSineSwitchRight(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2);
}

void muteADAU(){

    byte data[2] = {0x00, 0x18};
    AIDA_WRITE_REGISTER(DEVICE_ADDR_7bit, 0x081C, 2, data);

}

void unmuteADAU(){

    byte data[2] = {0x00, 0x1C};
    AIDA_WRITE_REGISTER(DEVICE_ADDR_7bit, 0x081C, 2, data);

}

/*
void VolumeUpSlewRate(double raisetime)
{
    for (int i = 0; i <= raisetime; i++)
    {
        VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    }
}

*/