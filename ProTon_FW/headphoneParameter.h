/*
* headphoneParameter.h
*  Kopfhörerparameter 
*/



//DT770pro250Ohm dB[SPL]
float HeadphoneFrequenzgang[3][9] = {
  { 63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},   // FREQ
  { 82.56,  85.78,  82.06,  85.05,   84.94,   84.28,   79.34,   93.61,    96.60},   // LINKS
  { 80.84,  82.18,  82.11,  85.89,   84.71,   84.32,   80.05,   94.89,    97.26}    // RECHTS
};



float headphoneMW[3][1] = {
  {0.00},                     // nix, nur ein Dummy. Ich war zu faul, um das umzurechnen.
  {86.36},                    // LINKS
  {86.74}                     // RECHTS
};
