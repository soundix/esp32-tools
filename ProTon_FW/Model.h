/*
* Model.h
* Version 0.3.0.2
*/

#ifndef MODEL
#define MODEL


#include <Arduino.h>


// this is the basic menu element:
struct listItem
{
  String itemName;
  listItem*  subMenu;
  int subMenuItemsCount;
  void (*itemHandler)();
} ;
struct UserOptions
{
  bool L : 1;
  bool P1 : 1;
  bool P2 : 1;
  bool P3 : 1;
  bool P4 : 1;
  bool P5 : 1;
  bool Comp : 1;
  bool Pass : 1;
};

struct User
{
  char userName[10];
  UserOptions options;                                     // hier werden alle Parameter (von der App in dieser Variable gespeichert) // Bassam dokumentiert noch die einzelnen Bits
  byte doneTestBefore;                              // true oder false, ob ein Hearingtest schon gemacht wurde oder nicht
  float userHearingResult[2][9];
  /*
  bool l;
  bool P1;
  bool P2;
  bool P3;
  bool P4;
  bool P5;
  bool Comp;
  bool Bass;
  */

  //float hearingTestResultR[18];
  //float hearingTestResultL[18];
  
} ;

// screen font size :
enum FontSize { smallFont, midFont, largFont };
enum ContrastValues {lowContrast , midContrast , highContrast};

#endif