#include "DataStore.h"
#include <Wire.h>
#include <Arduino.h>
User deviceUsers[USERS_COUNT];                       //creat array of users

uint8_t currentUserIndex = 0;

float hearingTestResult[HEARING_TEST_ROWS][HEARING_TEST_COLOMS];
float HearTest1[2][9] = {{-44.5, 20.3, 12.4, 24.6, 55.9, -54.23, -132.6, 99.34, -66.4} , 
                          {14.0, 13.0, 95.0, 110.0, 115.0, 15.0, 15.0, 10.0, 80.0}};
float HearTest2[2][9] = {{-114.5, 54.3, 88.4, 09.6, 97.9, -09.23, -55.67, 73.34, -66.4} ,
                          {14.0, 26.0, 95.0, 110.0, 11655.0, 1.04, 92.0, 24.0, 84.2}};
float HearTest3[2][9] = {{14.0, 13.0, 95.0, 110.0, 115.0, 15.0, 15.0, 10.0, 80.0} , 
                          {-44.5, 20.3, 12.4, 24.6, 55.9, -54.23, -132.6, 99.34, -66.4}};

bool user1DoneHearningTest = false;
bool user2DoneHearningTest = false;
bool user3DoneHearningTest = false;

void printSize()
{
  Serial.println(sizeof(deviceUsers));
  Serial.println(sizeof(deviceUsers[currentUserIndex]));
  Serial.println(sizeof(deviceUsers[currentUserIndex].userName));
  Serial.println(sizeof(deviceUsers[currentUserIndex].options));
  Serial.println(sizeof(deviceUsers[currentUserIndex].doneTestBefore));
  Serial.println(sizeof(deviceUsers[currentUserIndex].userHearingResult));
  // strcpy(deviceUsers[0].userName, "BSM1");
  // strcpy(deviceUsers[1].userName, "BSM2");
  // strcpy(deviceUsers[2].userName, "BSM3");
  // applyFirstRun();
  // bool firstTimeRun = isItFirstRun();
  // Serial.println(firstTimeRun);
  
  changeCurrentUser(0);
  printCurrentUserData();           //this fun will show what is the Data stored for current user in device memory!!
  setUserHearingResult(HearTest3);  //this fun will alter the stored Data for current user in device memory & eeprom!!
  setLinerUserOptions(true);
  setP1UserOptions(false);
  setP2UserOptions(false);
  setCompUserOptions(false);
  setBassUserOptions(false);
  printCurrentUserData();
  
  //getUserHearingResult(hearingTestResult); //bring the data from EEprom and store it in hearingTestResult[][]
  changeCurrentUser(1);
  printCurrentUserData();
  setUserHearingResult(HearTest1);
  getUserHearingResult(hearingTestResult); //bring the data from EEprom and store it in hearingTestResult[][]
  setLinerUserOptions(false);
  setP1UserOptions(true);
  setP2UserOptions(false);
  setCompUserOptions(false);
  setBassUserOptions(true);
  printCurrentUserData();

  // firstTimeRun = isItFirstRun();
  // Serial.println(firstTimeRun);
}

void eepromInit(void)
{
  strcpy(deviceUsers[0].userName, "andi");
  strcpy(deviceUsers[1].userName, "BSAM");
  strcpy(deviceUsers[2].userName, "mooz");
  
  pinMode(WrPin, OUTPUT);
  digitalWrite(WrPin, HIGH);
  Wire.begin();
}

void writeByteToEepromAddress(uint16_t stroingAddress, byte byteData)
{
  digitalWrite(WrPin, LOW);
  delay(200);
  Wire.beginTransmission(EEPROM_ADDRESS);
  Wire.write((int)(stroingAddress >> 8));   // MSB
  Wire.write((int)(stroingAddress & 0xFF)); // LSB
  Wire.write(byteData);
  Wire.endTransmission();
  digitalWrite(WrPin, HIGH);
  delay(5);
}

byte readByteFromEepromAddress(uint16_t stroingAddress)
{
  byte rdata = 0xFF;

  Wire.beginTransmission(EEPROM_ADDRESS);
  Wire.write((int)(stroingAddress >> 8));   // MSB
  Wire.write((int)(stroingAddress & 0xFF)); // LSB
  Wire.endTransmission();

  Wire.requestFrom(EEPROM_ADDRESS, 1);

  if (Wire.available())
    rdata = Wire.read();

  return rdata;
}

void writeTowDimArray(uint16_t eeaddress, float dataArray[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  digitalWrite(WrPin, LOW);
  delay(200);
  byte *myBytes = (byte *)(void *)dataArray;
  /*Serial.println("myBytes is :");
  for(size_t i = 0; i < HEARING_TEST_SIZE; i++)
    Serial.println(myBytes[i]); */
  _writeEEPROM(eeaddress, myBytes, HEARING_TEST_SIZE);
  digitalWrite(WrPin, HIGH);
  delay(5);
}

float *readTowDimArray(uint16_t eeaddress)
{
  float *temp;
  static float retVal[HEARING_TEST_ELEMENTS_COUNT];

  byte buff[HEARING_TEST_SIZE];
  Wire.beginTransmission(EEPROM_ADDRESS);
  Wire.write((int)(eeaddress >> 8));   //MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission();
  Wire.requestFrom(EEPROM_ADDRESS, HEARING_TEST_SIZE);
  //Serial.println("Data in Buff :");
  if (Wire.available())
    for (int i = 0; i < HEARING_TEST_SIZE; i++)
    {
      buff[i] = Wire.read();
      //Serial.println(buff[i]);
    }
  temp = (float *)(void *)buff;
  for (int i = 0; i < HEARING_TEST_ELEMENTS_COUNT; i++)
  {
    retVal[i] = *(temp + i);
    //Serial.println(retVal[i]);
  }
  delay(100);
  return retVal;
}

void _writeEEPROM(uint16_t eeaddress, byte *data, int dataSize)
{
  unsigned char i = 0, counter = 0;
  unsigned int address;
  unsigned int page_space;
  unsigned int page = 0;
  unsigned int num_writes;
  unsigned char first_write_size;
  unsigned char last_write_size;
  unsigned char write_size;
  // Calculate space available in first page
  page_space = int(((eeaddress / 64) + 1) * 64) - eeaddress;
  //Serial.print("page_space  is :"); Serial.println(page_space);
  // Calculate first write size
  if (page_space > 64)
  {
    first_write_size = page_space - ((page_space / 64) * 64);
    if (first_write_size == 0)
      first_write_size = 64;
  }
  else
    first_write_size = page_space;
  // Serial.print("first_write_size  is :");  Serial.println(first_write_size);
  // calculate size of last write
  if (dataSize > first_write_size)
    last_write_size = (dataSize - first_write_size) % 64;

  // Calculate how many writes we need
  if (dataSize > first_write_size)
    num_writes = ((dataSize - first_write_size) / 64) + 2;
  else
    num_writes = 1;

  //Serial.print("num_writes  is :");  Serial.println(num_writes);
  i = 0;
  address = eeaddress;
  for (page = 0; page < num_writes; page++)
  {
    if (page == 0)
      write_size = first_write_size;
    else if (page == (num_writes - 1))
      write_size = last_write_size;
    else
      write_size = 64;
    //Serial.print("write_size  is :");  Serial.println(write_size);
    Wire.beginTransmission(EEPROM_ADDRESS);
    Wire.write((int)((address) >> 8)); // MSB
    Wire.write((int)((address)&0xFF)); // LSB
    for (counter = 0; counter < write_size; counter++)
    {
      Wire.write(data[i]);
      //Serial.println(data[i]);
      i++;
    }
    Wire.endTransmission();
    address += write_size; // Increment address for next write
    delay(6);              // needs 5ms for page write
  }
}
void changeCurrentUser(uint8_t newUserID)
{
  currentUserIndex = newUserID;
  writeByteToEepromAddress(currentUserAddress, (byte)currentUserIndex);
}

uint8_t getCurrentUserIdx()
{
  return currentUserIndex;
}

void getUserFromEEPROM()
{
  getUserName();
  getUserOptions();
  getUserHearingTestFlag();
  getUserHearingResult(deviceUsers[currentUserIndex].userHearingResult);

}

void getAllUserFromEEPROM()
{
  for(size_t i = 0; i < 3; i++)
  {
    changeCurrentUser(i);
    getUserFromEEPROM();
  }
}

bool isItFirstRun()
{
  uint8_t usrIdx = getCurrentUserIdx();
  bool retVal = true;
  for(size_t i = 0 ; i < USERS_COUNT ; i++)
  {
    changeCurrentUser(i);
    Serial.print("User ");Serial.print(i);Serial.print(" : ");
    if(getUserHearingTestFlag())
    {
      Serial.println("Done hearing test");
      retVal = false;
    }
    else
    {
      Serial.println("never Done hearing test");
    }
  }
  changeCurrentUser(usrIdx);
  return retVal;
}
void applyFirstRun()
{
  uint8_t usrIdx = getCurrentUserIdx();
  bool retVal = true;
  for(size_t i = 0 ; i < USERS_COUNT ; i++)
  {
    changeCurrentUser(i);
    setUserHearingTestFlag(0);
  }
  changeCurrentUser(usrIdx);
}
//User staruct functions:
//1-Name:
void setUserName(String usrName)
{
  int str_len = usrName.length() + 1;
  usrName.toCharArray(deviceUsers[currentUserIndex].userName, str_len);
  //TODO: make him update data in eeprom also
}

String getUserName(void)
{
  //TODO: make him read data from eeprom also
  String retVal(deviceUsers[currentUserIndex].userName);
  return retVal;
}

//2-Options:
void setUserOptions(UserOptions opt)
{
  // char b[sizeof(struct_data)];
  // memcpy(b, &struct_data, sizeof(struct_data));
  deviceUsers[currentUserIndex].options = opt;
  byte optionData = *((byte*)(&deviceUsers[currentUserIndex].options));
  writeByteToEepromAddress(getOptionAddress(), optionData);
}

void setLinerUserOptions(bool lin)
{
  UserOptions temp = getUserOptions();
  temp.L = lin;
  setUserOptions(temp);
}
void setP1UserOptions(bool p1Option)
{
  UserOptions temp = getUserOptions();
  temp.P1 = p1Option;
  setUserOptions(temp);
}
void setP2UserOptions(bool p2Option)
{
  UserOptions temp = getUserOptions();
  temp.P2 = p2Option;
  setUserOptions(temp);
}
void setCompUserOptions(bool compOption)
{
  UserOptions temp = getUserOptions();
  temp.Comp = compOption;
  setUserOptions(temp);
}
void setBassUserOptions(bool bassOption)
{
  UserOptions temp = getUserOptions();
  temp.Pass = bassOption;
  setUserOptions(temp);
}

UserOptions getUserOptions()
{
  byte dat = readByteFromEepromAddress(getOptionAddress());
  UserOptions optionDat = *((UserOptions*)(&dat));
  deviceUsers[currentUserIndex].options = optionDat;
  return deviceUsers[currentUserIndex].options;
}
//3-Hear test flag:
void setUserHearingTestFlag(byte hearTestFlag)
{
  deviceUsers[currentUserIndex].doneTestBefore = hearTestFlag;
  writeByteToEepromAddress( getDoneTestAddress() , deviceUsers[currentUserIndex].doneTestBefore);
}

byte getUserHearingTestFlag(void)
{
  deviceUsers[currentUserIndex].doneTestBefore = readByteFromEepromAddress( getDoneTestAddress() );
  return deviceUsers[currentUserIndex].doneTestBefore;
}

//4-Hear test result:
void setUserHearingResult(float hearingRes[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  writeTowDimArray(getHearingResultAddress(), hearingRes);
  _cpyHearingTest(hearingRes,deviceUsers[currentUserIndex].userHearingResult);
  setUserHearingTestFlag(255);          //make him done test
}

void getUserHearingResult(float res[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  float *Mydata;
  int counter = 0;
  Mydata = readTowDimArray( getHearingResultAddress() );
  for (size_t i = 0; i < HEARING_TEST_ROWS; i++)
    for (size_t j = 0; j < HEARING_TEST_COLOMS; j++)
      res[i][j] = Mydata[counter++];
    
  _cpyHearingTest(res,deviceUsers[currentUserIndex].userHearingResult);
}

//TODO: utils function
void printCurrentUserData()
{
  UserOptions opt = deviceUsers[currentUserIndex].options;
  Serial.println(deviceUsers[currentUserIndex].userName);
  Serial.printf("userOptions: %u,%u,%u,%u,%u\n",opt.L,opt.P1,opt.P2,opt.Comp,opt.Pass);
  _printHearingResult(deviceUsers[currentUserIndex].userHearingResult);
  // float hearingRes[2][18] = {{0}} ;
  // for(size_t i = 0; i < 2; i++)
  //   for(size_t j = 0; j < 18; j++)
  //     hearingRes[i][j] = deviceUsers[currentUserIndex].userHearingResult[i][j];
}

void _printHearingResult(float res[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  for (size_t i = 0; i < HEARING_TEST_ROWS; i++)
  {
    for (size_t j = 0; j < HEARING_TEST_COLOMS; j++)
    {
      Serial.print(res[i][j]);
      Serial.print(" , ");
    }
    Serial.println();
  }
  Serial.println();
}


void _cpyHearingTest(float src[HEARING_TEST_ROWS][HEARING_TEST_COLOMS] ,
                     float dst[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  for(size_t i = 0; i < HEARING_TEST_ROWS; i++)
    for(size_t j = 0; j < HEARING_TEST_COLOMS; j++)
      dst[i][j] = src[i][j];
}
uint16_t getBassUserAddress()
{
  uint16_t startAddress = sizeof(User) * currentUserIndex;
  return startAddress;
}
uint16_t getUserNameAddress()
{
  uint16_t retVal = getBassUserAddress();
  return retVal;
}

uint16_t getOptionAddress()
{
  uint16_t retVal = getBassUserAddress() 
                  + sizeof(deviceUsers[currentUserIndex].userName);
  return retVal;
}

uint16_t getDoneTestAddress()
{
  uint16_t retVal = getBassUserAddress() 
                  + sizeof(deviceUsers[currentUserIndex].userName) 
                  + sizeof(deviceUsers[currentUserIndex].options);
  return retVal;
}

uint16_t getHearingResultAddress()
{
  uint16_t retVal = getBassUserAddress() 
                  + sizeof(deviceUsers[currentUserIndex].userName) 
                  + sizeof(deviceUsers[currentUserIndex].options);
                  +sizeof(deviceUsers[currentUserIndex].doneTestBefore);
  return retVal;
}

////////////////////////////////////////////////////////////////////////////////////
//////////////////////TODO: for review:
////////////////////////////////////////////////////////////////////////////////////

void writeHearingTestToEEprom(int userNummber, float hearingTestToWrite[HEARING_TEST_ROWS][HEARING_TEST_COLOMS])
{
  /*
  int address = _getUserHearingTestAddress(userNummber);
  writeTowDimArray(address, hearingTestToWrite);
  */
}

void writeUserStrToEeprom(uint16_t stroingAddress, User userdata)
{
  // digitalWrite(WrPin, LOW);
  // delay(200);
  // byte *myBytes = (byte *)(void *)&userdata;
  // uint8_t dataSize = sizeof(userdata);
  // /*
  // Serial.println(dataSize);
  // Serial.print("myBytes is :");
  // for(size_t i = 0; i < dataSize; i++)
  //   Serial.println(myBytes[i]);
  // */
  // _writeEEPROM(stroingAddress, myBytes, dataSize);
  // /*Wire.beginTransmission(EEPROM_ADDRESS);
  // Wire.write((int)(stroingAddress >> 8));   // MSB
  // Wire.write((int)(stroingAddress & 0xFF)); // LSB
  // for (uint8_t i = 0; i < dataSize; i++)
  //   Wire.write(*myBytes++);
  // Wire.endTransmission();*/
  // digitalWrite(WrPin, HIGH);
  // delay(5);
}

User readUserStrFromEeprom(uint16_t stroingAddress)
{
  // User *temp;
  // User retVal;

  // byte buff[USER_STRUCT_SIZE];
  // Wire.beginTransmission(EEPROM_ADDRESS);
  // Wire.write((int)(stroingAddress >> 8));   //MSB
  // Wire.write((int)(stroingAddress & 0xFF)); // LSB
  // Wire.endTransmission();
  // Wire.requestFrom(EEPROM_ADDRESS, USER_STRUCT_SIZE);

  // if (Wire.available())
  //   for (int i = 0; i < USER_STRUCT_SIZE; i++)
  //   {
  //     buff[i] = Wire.read();
  //     //Serial.println(buff[i]);
  //   }
  // temp = (User *)(void *)buff;
  // retVal.P1 = temp->P1;
  // retVal.P2 = temp->P2;
  // retVal.P3 = temp->P3;
  // return retVal;
}

void upateUserStrInEeprom(uint16_t stroingAddress, User newData)
{
  // User temp = readUserStrFromEeprom(stroingAddress);
  // uint8_t P1Size = sizeof(temp.P1);
  // uint8_t P2Size = sizeof(temp.P2);
  // uint8_t P3Size = sizeof(temp.P3);
  // if (temp.P1 != newData.P1)
  // {
  //   writeByteToEepromAddress(stroingAddress, newData.P1);
  //   Serial.println("Updated P1");
  // }
  // else
  //   Serial.println("DATA IN P1 is the Same");
  // if (temp.P2 != newData.P2)
  // {
  //   writeByteToEepromAddress(stroingAddress + P1Size, newData.P2);
  //   Serial.println("Updated P2");
  // }
  // else
  //   Serial.println("DATA IN P2 is the Same");
  // if (temp.P3 != newData.P3)
  // {
  //   writeByteToEepromAddress(stroingAddress + P1Size + P2Size, newData.P3);
  //   Serial.println("Updated P3");
  // }
  // else
  //   Serial.println("DATA IN P3 is the Same");
}

bool isUserDoneHearingTest(uint8_t usrId)
{
  // byte firstRunByte = readByteFromEepromAddress(firstRunFlagsAddress);

  // switch (usrId)
  // {
  // case 1:
  //   return firstRunByte & (1 << User1FlagBIT);
  //   break;
  // case 2:
  //   return firstRunByte & (1 << User2FlagBIT);
  //   break;
  // case 3:
  //   return firstRunByte & (1 << User3FlagBIT);
  //   break;
  // }
}
void setUserHearingTestStatus(uint8_t usrId, bool status)
{
  // byte firstRunByte = readByteFromEepromAddress(firstRunFlagsAddress);
  // switch (usrId)
  // {
  // case 1:
  //   firstRunByte ^= (-(unsigned long)status ^ firstRunByte) & (1UL << User1FlagBIT);
  //   break;
  // case 2:
  //   firstRunByte ^= (-(unsigned long)status ^ firstRunByte) & (1UL << User2FlagBIT);
  //   break;
  // case 3:
  //   firstRunByte ^= (-(unsigned long)status ^ firstRunByte) & (1UL << User3FlagBIT);
  //   break;
  // }
  // writeByteToEepromAddress(firstRunFlagsAddress,firstRunByte);
}

void setUserTest(bool bitVal)
{
  // user1DoneHearningTest = bitVal;
  // byte test = 11 << 0 && bitVal;
  // writeByteToEepromAddress(firstRunFlagsAddress, test);
}

void setFirstRunFlag(bool FRFlag)
{
  // byte firstRunFlag = (FRFlag) ? 1 : 0;
  // writeByteToEepromAddress(firstRunFlagsAddress, firstRunFlag);
}
