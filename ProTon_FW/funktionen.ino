/*
 * funktionen.ino
 * Version 0.3.0.2
 */

#include <Arduino.h>
#define FREQ 0   // Speicheradresse für Array
#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array

uint8_t testLINKS = true;   // Standard, es wird mit der linken Seite angefangen
uint8_t testRECHTS = false; // Standard, somit ist rechts false

extern float hearingTestSWAP[3][31];
extern float saveIteration1[3][31];
extern float saveIteration2[3][31];
//extern float saveIteration3[3][9];
extern float hearingTestErgebnis[3][31];
extern float hearingTestSAVE[5][31];
extern uint8_t IntensityChoice;

void debugModePin();
void Melodie(void);
void hearingTestSAVEinSWAP(void);
void hearingTestIntensity(void);


/******************************************************************************************************************************** 
 * Funktion:      VolumeMonoConvertANDsend()
 * DSP_ADDR:      Registeradresse des Mono Volumereglers des DSP
 * VolumeINdB:    erwartet eine Fließkommazahl der Lautstärke in dB
 * 
 * Konvertiert aus der Lautstärke in dB (-120.00dB..+10.00dB) eine 5.23 Bit Zahl und sendet an Registeradresse des Volumereglers
 *********************************************************************************************************************************/
void VolumeMonoConvertANDsend(int DSP_ADDR, float VolumeINdB) // Umrechnung von dB in 23Bit Wert und Senden an hearingTestSineVolume
{
  float VolumetoSend;
  VolumetoSend = pow(10, VolumeINdB / 20.0);                  // Volumeumrechnung -- volume2 = 10^(hearingTestvolumedB/20)
  MasterVolumeMono(DEVICE_ADDR_7bit, DSP_ADDR, VolumetoSend); // Set Volume2 to SineGen
  //return VolumetoSend;
}

/********************************************************************************************************************************
 * Funktion:      VolumeStereoConvertANDsend()
 * DSP_ADDR:      Registeradresse des Mono Volumereglers des DSP
 * VolumeINdB:    erwartet eine Fließkommazahl der Lautstärke in dB
 * 
 * Konvertiert aus der Lautstärke in dB (-120.00dB..+10.00dB) eine 5.23 Bit Zahl und sendet an Registeradresse des Volumereglers
 *********************************************************************************************************************************/
void VolumeStereoConvertANDsend(int DSP_ADDR, float VolumeINdB)
{
  float VolumetoSend;
  VolumetoSend = pow(10, VolumeINdB / 20.0);
  MasterVolumeStereo(DEVICE_ADDR_7bit, DSP_ADDR, VolumetoSend);
}

/*******************************************************************
 * int Seitenwahl()
 * 
 * Funktion, die entweder 1 für links oder 2 für Rechts zurückgibt
 * wird für die Seitenwahl des Hörtests benötigt. 
 * links Sinus, dann rechts Noise und umgekehrt
 *******************************************************************/
int Seitenwahl()
{
  int y;
  if ((testLINKS == true) && (testRECHTS == false))
  {
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für Sinus auf Links
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für NOISE auf Rechts
    y = LINKS;
    return y;
  }
  if ((testLINKS == false) && (testRECHTS == true))
  {
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für Sinus auf Rechts
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für NOISE auf Links
    y = RECHTS;
    return y;
  }
}

/*******************************************************************
 * int SeitenwahlOHNEnoise()
 * 
 * Funktion, die entweder 1 für links oder 2 für Rechts zurückgibt
 * wird für die Seitenwahl des Hörtests benötigt. 
 * Sinus wird auf verschiedene Seiten gegeben ohne Noise
 *******************************************************************/
int SeitenwahlOHNEnoise()
{
  if ((testLINKS == true) && (testRECHTS == false))
  {
    //muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für Sinus auf Links
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    Melodie();
    return LINKS;
  }
  if ((testLINKS == false) && (testRECHTS == true))
  {
    //muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für Sinus auf Rechts
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS, 2);
    Melodie();
    return RECHTS;
  }
}

/****************************************************************
 * void debugModePIN()
 * Wenn PIN_DEBUG (PIN 5) LOW ist, dann aktiviere den DEBUG MODE
 ****************************************************************/
void debugModePIN()
{
  if (digitalRead(PIN_DEBUG) == LOW)
  {
    PINdebugON = true;
  }
  else
  {
    PINdebugON = false;
  }
}

/************************************************
 * void VersionsnummerAbfrage(void);
 ************************************************/
void VersionsnummerAbfrage(void)
{
  Serial.print(F("Version: "));
  Serial.print(VersionsnummerFW);
  Serial.println(VersionsnummerDSP);
  Serial.print(F("Stand: "));
  Serial.println(DatumVersion);
}

void Melodie(void)
{
  int i;
  float VolDown = -40.00;
  float VolDownStep = 5.00;
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, VolDown);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 500.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  delay(100);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 1000.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  delay(250);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 750.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  for (i = 0; i <= 10; i++)
  {
    VolDown = VolDown - VolDownStep;
    VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, VolDown);
    delay(250);
  }
  delay(500);
}

void hearingTestSAVEinSWAP(void) // Zwischenspeichern des Hörtests in hearingTestSWAP[][];
{
  int ii, jj;
  for (ii = 0; ii <= 4; ii++) //Zeilenweise
  {
    for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
    {
      hearingTestSWAP[ii][jj] = hearingTestSAVE[ii][jj];
      hearingTestErgebnis[ii][jj] = hearingTestSAVE[ii][jj];
      //hearingTestSWAP[ii][jj] = testAndi[ii][jj];
      //saveIteration1[ii][jj] = testAndi[ii][jj];
      //saveIteration2[ii][jj] = testAndi[ii][jj];
      //saveIteration3[ii][jj] = testAndi[ii][jj];
    }
  }
}

void hearingTestIntensity(void) // Zwischenspeichern des Hörtests in hearingTestSWAP[][];
{
  int ii, jj;
  for (ii = 0; ii <= 4; ii++) //Zeilenweise
  {
    for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
    {
      // Auswahl der Intensität der Personalisierung
      switch (IntensityChoice)
      {
      case 0:
        // 20%
        hearingTestSWAP[ii][jj] = 0.2 * hearingTestSAVE[ii][jj];
        break;

      case 1:
        // 40%

        hearingTestSWAP[ii][jj] = 0.4 * hearingTestSAVE[ii][jj];
        break;

      case 2:
      // 60%
      default:

        hearingTestSWAP[ii][jj] = 0.6 * hearingTestSAVE[ii][jj];
        break;

      case 3:
        // 80%

        hearingTestSWAP[ii][jj] = 0.8 * hearingTestSAVE[ii][jj];
        break;

      case 4:
        // 100%

        hearingTestSWAP[ii][jj] = hearingTestSAVE[ii][jj];
        break;
      }
    }
  }
  //choose_algo();
}
