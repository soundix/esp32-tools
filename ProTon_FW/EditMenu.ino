
/*
 * EditMenu.ino
 * * Version 0.3.0.2
 */

#include <arduino.h>

//String screenPlatz[7] = {"", "", "", "", "", "", "BACK"};
//String currentUserOptions[4] = {"U1", "L", " ", " "};

String screenPlatz[5] = {"", "", "", "", "BACK"};
String currentUserOptions[5] = {"1 ", "   ", "   ", "   ", "2"};

int levelNumbers[5] = {20, 40, 60, 80, 100};

bool compressorFlag = false;
bool bypassFlag = false;

extern uint8_t user1FLAG;
extern uint8_t user2FLAG;
extern uint8_t user3FLAG;
extern uint8_t linFLAG;
extern uint8_t per1FLAG;
extern uint8_t per2FLAG;
extern uint8_t per3FLAG;
extern uint8_t compFLAG;
extern uint8_t bassFLAG;
extern bool int20FLAG ;
extern bool int40FLAG ;
extern bool int60FLAG ;
extern bool int80FLAG ;
extern bool int100FLAG ;


int editMenuScroler = 0;
int xx, yy;

void updatePlatz(void)
{
  if (user1FLAG == true)
  {
    currentUserOptions[0] = "1 ";
  }
  else if (user2FLAG == true)
  {
    currentUserOptions[0] = "2 ";
  }
  else if (user3FLAG == true)
  {
    currentUserOptions[0] = "3 ";
  }
  else
  {
    currentUserOptions[0] = "  ";
  }

  if (linFLAG == true)
  {
    currentUserOptions[1] = " L  ";
  }
  else if (per1FLAG == true)
  {
    currentUserOptions[1] = " P1 ";
  }
  else if (per2FLAG == true)
  {
    currentUserOptions[1] = " P2 ";
  }
  else
  {
    currentUserOptions[1] = "    ";
  }

  if (Ble_CONTROL_MODE_FLAG == true)
  {
    currentUserOptions[2] = " BLE";
  }
  else
  {
    currentUserOptions[2] = "     ";
  }

  if (MUTE_Flag == true)
  {
    currentUserOptions[3] = " M ";
  }
  else
  {
    currentUserOptions[3] = "    ";
  }

  if (int20FLAG == true)
  {
    currentUserOptions[4] = "20";
  }
  else if (int40FLAG == true)
  {
    currentUserOptions[4] = "40";
  }
  else if (int60FLAG == true)
  {
    currentUserOptions[4] = "60";
  }
  else if ( int80FLAG == true)
  {
    currentUserOptions[4] = "80";
  }
  else if (int100FLAG == true)
  {
    currentUserOptions[4] = "100";
  }
  else
  {
    currentUserOptions[4] = "   ";
  }
}

void LCDprintFLAG(void)
{
  //lcdDevice.setCursor(0, 0);
  //lcdDevice.print(Platz1 + Platz2 + Platz3 + Platz4);
}

int getEditMenuScroller()
{
  return editMenuScroler;
}

void changeUser(void)
{
  if (user1FLAG == true)
  {
    currentUserOptions[0] = "USR 1";
  }
  else if (user2FLAG == true)
  {
    currentUserOptions[0] = "USR 2";
  }
  else if (user3FLAG == true)
  {
    currentUserOptions[0] = "USR 3";
  }
  else
  {
    currentUserOptions[0] = "     ";
  }
}

void editMenuScrolTo(int selectedIndex)
{
  Serial.println("editMenuScrolTo");
  Serial.println(selectedIndex);
  editMenuScroler = selectedIndex;

  switch (selectedIndex)
  {
  case 0:
    if (linFLAG == true)
      screenPlatz[0] = "[L*] ";
    else
      screenPlatz[0] = "[L] ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";
    //  if (levelFLAG == true)screenPlatz[3] = "level ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 1:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = "[P1*] ";
    else
      screenPlatz[1] = "[P1] ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";

    //  if (levelFLAG == true)screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 2:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = "[P2*] ";
    else
      screenPlatz[2] = "[P2] ";
    screenPlatz[3] = " LEVEL";

    // if (levelFLAG == true) screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 3:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = "[LEVEL]";

    //if (levelFLAG == true) screenPlatz[3] = "[level*]";
    // else                  screenPlatz[3] = "[level] ";
    screenPlatz[4] = " BACK ";
    break;

  case 4:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";
    // if (levelFLAG == true) screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = "[BACK]";
    break;
  }
}

int MesseMenuExcute(void)
{
  Serial.print ("MesseMenuExcute :"); Serial.println(editMenuScroler);
  switch (editMenuScroler)
  {
  case 0:
    //L
    Serial.println("liner handler");
    //linear
    linFLAG = true;
    per1FLAG = false;
    per2FLAG = false;
    muteVoice(true);
    PersonalizedMultibandCompressorBypassCompOFF();
    MainDynamicBassBypassOFF();
    MainDirectMode();
    delay(10);
    muteVoice(false);
    return 0;
    break;

  case 1:
    //P1
    //personalisiert Kurve 1
    linFLAG = false;
    per1FLAG = true;
    per2FLAG = false;

    preset = 1;
    muteVoice(true);
    MainPersonalizedMode();
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    PersonalizedMultibandCompressorBypassCompOFF();
    delay(10);
    muteVoice(false);
    Serial.println("PLUS Agorithmus");
    return 1;
    break;

  case 2:
    //P2
    //personalisiert Kurve 2
    // EqConfigurations();
    linFLAG = false;
    per1FLAG = false;
    per2FLAG = true;

    preset = 2;
    muteVoice(true);
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    PersonalizedMultibandCompressorBypassCompOFF();
    MainPersonalizedMode();
    delay(10);
    muteVoice(false);
    Serial.println("LIFESTYLE Algorithmus");
    return 2;
    break;

  case 3:
    levelHandler();
    return 3;
    break;

  case 4:
    //BACK
    Serial.println("BACK handler");
    return 4;
    break;
  }
}

void levelHandler()
{
  activatUserInputMode(0, 5, selectNumber, showNumbers, 1);
  showNumbers(0); 
}
void selectNumber(int16_t *sel, uint8_t length)
{
  Serial.println(" change The Intinsity ");
  Serial.print("*sel :") ; Serial.println( *sel ) ;
  IntensityChoice = *sel; 
  setIntFlags( IntensityChoice ); 
  updatePlatz();
  muteVoice(true);
  MainPersonalizedMode();
  delay(10);
  muteVoice(false);
}


void showNumbers(int16_t sel)
{
  showSelectedLevel(" LEVEL ", String(levelNumbers[sel]));
  Serial.println(levelNumbers[sel]);
}

void setIntFlags(uint8_t i )
{
  switch (i)
   {
   case 0:
      int20FLAG = true;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = false;
     break;
   
   case 1:
      int20FLAG = false;
      int40FLAG = true;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = false;
     
     break;
   case 2:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = true;
      int80FLAG = false;
      int100FLAG = false;
     
     break;

   case 3:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = true;
      int100FLAG = false;
     
     break;

   case 4:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = true;
     
     break;
  }
}