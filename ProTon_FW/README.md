# ProTonPlus
ESP32 with ADAU1701

Arduino Board Configuration:

Selected Board: ESP32 Dev Module (esp32)
PSRAM: Disabled
Partition Scheme: Huge APP (3MB No OTA/1MB SPIFFS)
CPU Frequency: 240MHz (WiFi/BT)
Flash Mode: QIO
Flash Frequency: 40MHz
Flash Size: 4MB (32Mb)
Upload Speed: 921600
