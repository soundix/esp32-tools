/*
 * Define_adrress.h
 * * Version 0.3.0.2
 */

#define ADAU170x
#define DEVICE_ADDR 0x68
#define DEVICE_ADDR_7bit DEVICE_ADDR>>1
//#define DEVICE_ADDR_7bit 0x34
 
#define DC1   11

/*
// vERSION 1.0.0.0
#define hearingTestSinusgeneratorAddr 	0
#define hearingTestSineVolumeAddr 	3
#define hearingTestSineSwitchAddr 	4
#define personalizedMultibandCompressorAddr 	109
#define personalizedMultibandCompressorBypassAddr 	186
#define personalizedRMSCompAddr 	148
#define personalizedMidEQ1Addr 	19
#define personalizedMidEQ1_2Addr 	64
#define MasterSwitchAddr 	187
#define MasterVolumeAddr 	190
#define DynamicBassAddr 	6
#define DynamicBassBypassAddr 	18
#define SignalDetectionAddr 	15
*/



/*
// Version 1.2
#define hearingTestSinusgeneratorAddr 	0
#define hearingTestSineVolumeAddr 	3
#define hearingTestSineSwitchAddr 	4
#define personalizedMultibandCompressorAddr 	106
#define personalizedMultibandCompressorBypassAddr 	145
#define personalizedMidEQ1Addr 	16
#define personalizedMidEQ1_2Addr 	61
#define MasterSwitchAddr 	146
#define MasterVolumeAddr 	149
#define DynamicBassAddr 	6
#define DynamicBassBypassAddr 	15

*/
/*
// 31Filter Terz Test
#define hearingTestSinusgeneratorAddr 	0
#define hearingTestSineVolumeAddr 	3
#define hearingTestSineSwitchAddr 	4
#define personalizedMultibandCompressorAddr 	326
#define personalizedMultibandCompressorBypassAddr 	365
#define personalizedMidEQ1Addr 	16
#define personalizedMidEQ1_2Addr 	91
#define personalizedMidEQ1_3Addr 	166
#define personalizedMidEQ1_4Addr 	241
#define personalizedMidEQ1_5Addr 	316
#define personalizedMidEQ1_6Addr 	321
#define MasterSwitchAddr 	366
#define MasterVolumeAddr 	369
#define DynamicBassAddr 	6
#define DynamicBassBypassAddr 	15

*/

// Address 31Filter headphone hearing test with 20dB Gain
// no Hardwareamplifier, just Software
#define hearingTestSinusgeneratorAddr 	0
#define hearingTestSineVolumeAddr 	3
#define hearingTestSineSwitchAddr 	15
#define hearingTestSineGainAddr 	4
#define personalizedMultibandCompressorAddr 	327
#define personalizedMultibandCompressorBypassAddr 	366
#define personalizedMidEQ1Addr 	17
#define personalizedMidEQ1_2Addr 	92
#define personalizedMidEQ1_3Addr 	167
#define personalizedMidEQ1_4Addr 	242
#define personalizedMidEQ1_5Addr 	317
#define personalizedMidEQ1_6Addr 	322
#define MasterSwitchAddr 	367
#define MasterVolumeAddr 	370
#define DynamicBassAddr 	5
#define DynamicBassBypassAddr 	14
