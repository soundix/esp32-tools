/*
 * menuLib.cpp
 * Version 0.3.0.2
 */


#include <Arduino.h>
#include "menuLib.h"

//#define _debug //to activate debugging print function

//extern listItem* MenusStack[];

Menu::Menu(listItem *mainMenu, int mnSize, int maxDepth)
//Menu::Menu(listItem *mainMenu, int mnSize, int maxDepth,Print lcd)
{
    _printDebug("Menu::Menu");
    currentList = mainMenu;
    _currentListSize = mnSize;
    _maximumDepth = maxDepth;

    _selectedItemIndex = 0; //default marked is the first one
    _scrollAbility = 1;     //default scrolling is enable
    _createStacks();        //create arrays for stacking purpose
  _printStack();
};

//TODO: deprecated
void Menu::lcdInit(uint8_t lcdAdd,uint8_t lcdCols,uint8_t lcdRows)
{
  /*_lcdAdd = lcdAdd;
  _lcdCols = lcdCols;
  _lcdRows = lcdRows;
  _lcd =  LCD_I2C(_lcdAdd,_lcdCols,_lcdRows);
  //_lcd.setData(_lcdAdd,_lcdCols,_lcdRows);
  _lcd.begin();
  */
}

//_getFullListLength: this function compute the final stack size and store it in *_menuStackLength*
int Menu::_getFullListLength(listItem *testedList, int testedListSize)
{
  _printDebug("Menu::_getFullListLength start");
  int i=0;
  for( i = 0; i < testedListSize; i++)
  {
    _printDebug((testedList+i)->itemName);
    _menuStackLength++;
    _printDebug(String(_menuStackLength));
    if ((testedList+i)->subMenuItemsCount > 0) {
      _getFullListLength((testedList+i)->subMenu,(testedList+i)->subMenuItemsCount);
    }
  }
  
 /* for (int i = 0; i < testedListSize; ++i)
  {
    Serial.println(testedList[i].itemName);
    _menuStackLength++;
    if (testedList[i].subMenuItemsCount > 0)
    {
      _getFullListLength(testedList[i].subMenu,testedList[i].subMenuItemsCount);
    }
  }
*/
_printDebug("Menu::_getFullListLength end");
return _menuStackLength;
}

//print menu items and mark one of them debinding on his inedx
listItem* Menu::printStyledList(uint8_t style)
{
  if (style == 0) 
    return _printListItemStyleArrow();
  else 
    return _printListItemStyleBrackites();  

}

listItem* Menu::_printListItemStyleArrow()
{
  Serial.println("*******Menu number "+String(countStackPointer+1)+"*******\n");
  listItem* retVal;
  String msgToPrint = "";
  for (int i = 0; i < _currentListSize; ++i)
  {
    if (i == _selectedItemIndex)
    {
      msgToPrint+=">>";
      msgToPrint +=(currentList+i)->itemName;
    }
    else
    {
      msgToPrint+="..";
      msgToPrint +=(currentList+i)->itemName;
    }
    msgToPrint +=".\n";
    Serial.print(msgToPrint);
    
    msgToPrint ="";
    retVal = currentList;
    
  }
  Serial.println("\n***************************");
  return retVal;
}

listItem* Menu::_printListItemStyleBrackites()
{
  Serial.println("*******Menu number "+String(countStackPointer+1)+"*******\n");
  listItem* retVal;
  String msgToPrint="";
  for (int i = 0; i < _currentListSize; ++i)
  {
    
    if (i == _selectedItemIndex) 
    {
      msgToPrint +="[[";
    } 
    msgToPrint += String(i)+"-"+ (currentList+i)->itemName+".";
    if (i == _selectedItemIndex) 
    {
      msgToPrint +="]]";
    }
    Serial.println(msgToPrint);
    msgToPrint ="";
  retVal = currentList;
  }
  Serial.println("\n***************************");
  return retVal;
}

//TODO: deprecated function
void Menu::printLCDList()
{
  //to show lcd content
  /*String lcdContent[_currentListSize]; 
  
  for(int i = 0; i < _currentListSize; i++)
    lcdContent[i] = "";             //at begining all rows is empty

  int counter =0;
  bool first = true;
  for(int i = _selectedItemIndex; i <_currentListSize; i++)
  {
      lcdContent[counter++] = String(currentList[i].itemName);  
  }
  _lcd.clear(); 
  for(int i = 0; i < _lcdRows; i++)
  {
    _lcd.setCursor(1,i);
    _lcd.print(lcdContent[i]); 
  }
  //TODO: create select select item Indecator 
  //byte Arrow[8] = { 0x00, 0x04, 0x06, 0x1F, 0x06, 0x04, 0x00, 0x00};
  //_lcd.createChar(1, Arrow);
  _lcd.setCursor(0,0);
  _lcd.write(126);

  //TODO: create scrolBar
  uint8_t scrolIndecator[8]  = {0x7, 0x7, 0x7};
  _lcd.createChar(0, scrolIndecator);
  _lcd.setCursor(15,0);
  _lcd.write(0);
  */
  
}

int Menu::getMenuItemsCount()
{
  int retVal = _currentListSize;
  return retVal;
}
int Menu::getSelectedItem()
{
  return _selectedItemIndex;
}

int Menu::getScrollAbility()
{
  int retVal = _scrollAbility;
  return retVal;
}

void Menu::scrol(int newIndex)
{
  if (newIndex < 0 && newIndex >= MenuItemsCountStack[countStackPointer])
  {
    _printDebug("bad index!!");
    return;
  }

  if (_scrollAbility != 0)
  {
    _selectedItemIndex = newIndex;
    MenuItemsselectedStack[countStackPointer] = newIndex;
    _printDebug(String(newIndex));
  }
}

void Menu::puch()
{
  _printDebug("you chose to show sub-menu of :"+currentList[_selectedItemIndex].itemName);
  listItem motherList = currentList[_selectedItemIndex];
  listItem *doughterList = motherList.subMenu;
  _printDebug(String(motherList.subMenuItemsCount));
  currentList = new listItem[motherList.subMenuItemsCount];
  for (int i = 0; i < motherList.subMenuItemsCount; i++)
  {
    currentList[i] = doughterList[i];
  }
  _currentListSize = motherList.subMenuItemsCount;
  countStackPointer++;
  MenuItemsCountStack[countStackPointer] = _currentListSize;
  MenuItemsselectedStack[countStackPointer] = _selectedItemIndex;
  MenusStack[countStackPointer] = currentList;
  _selectedItemIndex = 0;
  _printStack();
}

int Menu::pop()
{
  _printDebug("Menu::pop");
  int retVal = 0;
  countStackPointer--;
  if (countStackPointer >= 0)
  {
    //menuStackPointer -=_currentListSize; 
    _currentListSize = MenuItemsCountStack[countStackPointer];
    currentList = MenusStack[countStackPointer];
    _selectedItemIndex = MenuItemsselectedStack[countStackPointer];
    retVal = 1; //stack poped successfully to previous list!
  }
  else
  {
    countStackPointer=0;
  }
  _printStack();
  return retVal;
}

//exexute: execute the handler of selected menu item if possiable or open new menu if not
void Menu::execute()
{
  _printDebug("Menu::execute");
  if (currentList[_selectedItemIndex].subMenuItemsCount == 0)
  {
    _setScrollState(0); //stop UI scrolling
    _printDebug("execute the handler of");
    _printDebug(currentList[_selectedItemIndex].itemName);
    currentList[_selectedItemIndex].itemHandler();
    _setScrollState(1); //re-enable UI scrolling
  }
  else //item has no handler *GO DEEP!!*
  {
    //_printStack();
    puch();
  }
}

int Menu::getMenuItemCount()
{
  return MenuItemsCountStack[countStackPointer];
}

//setScrollState: set menu scrolling ability 1:true menu is scrolable 0:false the menu is unscrollable
//this will used to prevent user from change UI state when item handler is executes
void Menu::_setScrollState(int state)
{

  if (state == 0)
  {
    _printDebug("scrolling disabled..");
  }
  else
  {
    _printDebug("scrolling enabled..");
  }

  _scrollAbility = state;
}

void Menu::_printDebug(String msg)
{
#ifdef _debug
  Serial.println(msg);
#endif
}

void Menu::_createStacks()
{
  if (MenuItemsCountStack != 0 || MenusStack != 0|| MenuItemsselectedStack !=0) {
    _printDebug("inside if statment..");
    delete [] MenuItemsCountStack;
    delete [] MenusStack;
    delete [] MenuItemsselectedStack;
  }
  MenuItemsCountStack = new int [_maximumDepth];
  MenuItemsselectedStack = new int [_maximumDepth];
  MenusStack = new listItem*[_maximumDepth];
  listItem* temp = new listItem[_currentListSize];
  
  for(int i = 0; i < _currentListSize; i++)
  {
    temp[i] = currentList[i];
  }
  
  countStackPointer++;
  MenuItemsCountStack[countStackPointer] = _currentListSize;
  MenuItemsselectedStack[countStackPointer] = 0;              //at first run the 0s item is selected 
  MenusStack[countStackPointer] = temp;
}

void Menu::_printStack()
{
  _printDebug("*stack: ");
  _printDebug("stack pointer value is : "+String(countStackPointer));
  for (int i = 0; i <= countStackPointer; i++)
  {
    _printDebug(MenusStack[i]->itemName);
    _printDebug(String(MenuItemsCountStack[i]));
    _printDebug(String(MenuItemsselectedStack[i]));
  }
  _printDebug("*list: ");
  for (int i = 0; i < _currentListSize; i++)
  {
    _printDebug(currentList[i].itemName);
  }
  _printDebug(String(_currentListSize));
  _printDebug("=====================");
}
