/*
 * File:           C:\Users\achmi\OneDrive\Dokumente\Git\ProTon_Plus\ProTon_Plus\ProTon_FW\Lehmann - Drachenfels\Terzfilter_drachenfels_IC_2_PARAM.h
 *
 * Created:        Sunday, October 27, 2019 8:22:58 PM
 * Description:    Terzfilter_drachenfels:IC 2 parameter RAM definitions.
 *
 * This software is distributed in the hope that it will be useful,
 * but is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software may only be used to program products purchased from
 * Analog Devices for incorporation by you into audio products that
 * are intended for resale to audio product end users. This software
 * may not be distributed whole or in any part to third parties.
 *
 * Copyright ©2019 Analog Devices, Inc. All rights reserved.
 */
#ifndef __TERZFILTER_DRACHENFELS_IC_2_PARAM_H__
#define __TERZFILTER_DRACHENFELS_IC_2_PARAM_H__


#endif
