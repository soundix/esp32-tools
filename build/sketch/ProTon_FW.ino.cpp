#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
/*
* ProTon_FW.ino
* Version 0.3.0.3
*/

//#define CS43131 //Hardware config.
//#define EEPROM
//#define Drachenfels
#include "AidaDSP.h"
#include "menuLib.h"
#include <Arduino.h>
#include <WiFi.h>
/*
* für ADAU1701 --> #include "AidaDSP.h"
* für ADAU1442 --> #include "AidaDSP2.h"
*/
#include "Define_adrress.h"
#include "parameter.h"
#include "OledUtility.h"
#include "DataStore.h"

//Config ADAU1701 over I2C
#ifndef EEPROM //define EEPROM to use EEPROM
#include "SoftI2C_mod.h"
#include "TxBuffer_IC_1_plus.h"
#define SDA_PIN 21
#define SCL_PIN 22
#endif

#ifdef CS43131
#include "CS43131_config.h"
#endif


// DEFINES USER INTERFACE
#define VOLMAX 0.00
#define VOLMIN -99.00
//#define VolumeINIT 80
//#define VOLMIN -120.00

#define max_number_of_pulses 96
#define encoderStep 0.5
#define EncoderPulsesPerClick 1

// DEFINES I/O

/*
* für ESP32:
ESP 32 WRROM - DEV Kit
#define encoderPin1 35
#define encoderPin2 32
#define encoderSwitchPin 33 //push button switch


* für MEGA2560:
#define encoderPin1 2
#define encoderPin2 3
#define encoderSwitchPin 4 //push button switch
*/

#define PIN_LED 13
#define PIN_LED1 7
#define PIN_DEBUG 14
#define PIN_AudioDetect 53
#define encoderPin1 35
#define encoderPin2 32
#define encoderSwitchPin 33 //push button switch
#define SELFBOOT 17
#define WP 18
#define ADAU_RST 5
#define LevelShift 12



//function prototyping:
void updateEncoder();
void initHardWare(void);
void shortPressHandler(void);
void longPressHandler(void);
void extremeLongPressHandler(void);
void muteVoice(bool muteVal);
void changeVoiceVolum(void);
double dbVlaueToEncoder(double dbVal);
double encoderToDBVoice(double minval, double maxval, double pulses);
bool ManualHearingTestFlag = false;
//mode activition functions:
void activitNormalMode(void);
void activitEditMode(void);
void activitMenuMode(void);

//extern void VolumeMonoConvertANDsend(int, int);
//extern void VolumeStereoConvertANDsend(int, int);

#ifndef EEPROM
bool i2c_write_block(byte ic_addr, word reg_addr, byte * data, int data_length);
void config_adau1701();
#endif

#ifdef CS43131
void config_cs43131(byte ic_addr, byte * data, int data_length);
#endif


void activatUserInputMode(int16_t minVal, int16_t maxVal, void (*InputCompleteHandler)(int16_t *, uint8_t), void (*InputScrolHandler)(int16_t), int8_t selectedValCount);
void saveLastMode(void);
void restoreLastMode(void);

//TODO: debug function remove it later;
void printCurrentMODE(void);
void LCDprintFLAG(void);
void printOnTerminal(String);

void initBluetooth(void);
void bluetoothREAD(void);

volatile bool vertuelSwitch = false;

//Encoder staff:
//1-Normal mode encoder vars:
volatile uint32_t nm_lastEncoded = 0b11;
volatile double nm_encoderValue = 0;
volatile double nm_lastencoderValue = 0;
double dBVoiceVolum;

//2-Edit mode encoder vars:
volatile uint32_t em_lastEncoded = 0b11;
volatile double em_encoderValue = 0;
volatile double em_lastencoderValue = 0;

//3-Menu mode encoder vars:
volatile uint32_t mm_lastEncoded = 0b11;
volatile double mm_encoderValue = 0;
volatile double mm_lastencoderValue = 0;

//4-User Input mode encoder vars:
volatile uint32_t ur_lastEncoded = 0b11;
volatile double ur_encoderValue = 0;
volatile double ur_lastencoderValue = 0;

//double dBVoiceVolum;

//Mode Flags:
volatile bool NORMAL_MODE_Flag = true; //normal mode: mute\unmute + change voice volume.
volatile bool EDIT_MODE_Flag = false;  //edit mode: simple menu to set basic function.
volatile bool MENU_MODE_Flag = false;  //menu mode: extanded menu to set all functions.
volatile bool USER_INPUT_Flag = false;
//volatile bool MUTE_Flag = false;      //moved to parameter.h
volatile bool updateContentFlag = false; //to update terminal & lcd contents.
volatile bool refreshEditMenuTrigger = false;
int lastMode = 0;
//lcd things
extern Menu mainMenu;
int editMenuSelectedIndex = 0;
extern String screenPlatz[5];
extern String currentUserOptions[5];
extern bool Octav;
//user input mode staff:
int16_t maxSelectedVal = 0;
int16_t minSelectedVal = 0;
int16_t tempSelectedVal = 0;
int16_t *selectedValPtr;
uint16_t selectedValIdx = 0;
uint16_t selectedValCnt = 0;
void (*userInputFunc)(int16_t *sel, uint8_t length);
void (*showInputFunc)(int16_t);

#ifndef EEPROM
SoftI2C * i2c;
#endif

#line 171 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void setup();
#line 214 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void loop();
#line 310 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void initHardWare();
#line 585 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void changeVoiceVolum();
#line 681 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void AudioDetect(void);
#line 692 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void printOnTerminal(String message);
#line 701 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void printCurrentMODE();
#line 743 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void activatUserInputMode(int16_t minVal, int16_t maxVal, void (*singleInputHandler)(int16_t *, uint8_t), void (*showInputHandler)(int16_t), int8_t selectedValCount);
#line 762 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void saveLastMode();
#line 773 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void restoreLastMode();
#line 324 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Iteration1(int Seite);
#line 334 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Iteration1_ohneHeadphone(int Seite);
#line 343 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Iteration2(int Seite);
#line 407 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Sound2(int Seite);
#line 426 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Sound3(int Seite);
#line 443 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Sound4(int Seite);
#line 461 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
void Sound5(int Seite);
#line 498 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
float MittelwertBACK(int Seite);
#line 517 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
int KurvekleinerGrenze(int Seite, float GrenzedB);
#line 538 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
int KurvegroesserGrenze(int Seite, float GrenzedB);
#line 559 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
int KurvegleichGrenze(int Seite, float GrenzedB);
#line 601 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
int choose_algo_seite(int Seite);
#line 50 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\AudioFunktionen.ino"
void setMainDynamicBassBypass(bool enable);
#line 128 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\AudioFunktionen.ino"
void MainPersonalizedMode( void );
#line 230 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\AudioFunktionen.ino"
void muteADAU();
#line 237 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\AudioFunktionen.ino"
void unmuteADAU();
#line 580 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\BleUtills.ino"
void BLEInit(void);
#line 663 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\BleUtills.ino"
void sinGeneratore(int freq);
#line 674 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\BleUtills.ino"
void executeAlgorithms(int A ,float OFF);
#line 715 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\BleUtills.ino"
void generatingTonWithFreq(int D ,float F ,float dB );
#line 33 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void EqConfigurations();
#line 68 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void printIteration(int m,int n ,float *Iteration );
#line 81 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void setEqParametersOktave();
#line 154 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void setEqParametersTerZ();
#line 226 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void saveIteration1InEqBoost();
#line 294 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void saveIteration2InEqBoost();
#line 362 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
void sendEqDataToBoard();
#line 39 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void updatePlatz(void);
#line 125 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
int getEditMenuScroller();
#line 130 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void changeUser(void);
#line 150 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void editMenuScrolTo(int selectedIndex);
#line 258 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
int MesseMenuExcute(void);
#line 329 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void levelHandler();
#line 334 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void selectNumber(int16_t *sel, uint8_t length);
#line 348 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void showNumbers(int16_t sel);
#line 354 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"
void setIntFlags(uint8_t i );
#line 11 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\SerialControl.ino"
void ReadSerialInput(void);
#line 260 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Sprachen.ino"
void LanguageSelect();
#line 274 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Sprachen.ino"
void peakDeutsch();
#line 367 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Sprachen.ino"
void peakEnglish();
#line 48 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\bluethooth.ino"
void Check_Protocol(char inStr[]);
#line 35 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
void VolumeMonoConvertANDsend(int DSP_ADDR, float VolumeINdB);
#line 50 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
void VolumeStereoConvertANDsend(int DSP_ADDR, float VolumeINdB);
#line 64 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
int Seitenwahl();
#line 90 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
int SeitenwahlOHNEnoise();
#line 112 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
void debugModePIN();
#line 127 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
void VersionsnummerAbfrage(void);
#line 71 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void Sinus_GeneratorMod(float dBVolumeVal, float freq);
#line 289 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void HeaingTestManuellOctav();
#line 297 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void HeaingTestManuellTerz();
#line 305 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void selectedMode(int S);
#line 336 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void applyOctav(int16_t *sel,uint8_t length);
#line 376 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void showOctav(int16_t sel);
#line 393 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void applyTerz(int16_t *sel,uint8_t length);
#line 438 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
void showTerz(int16_t sel);
#line 454 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
int switchDirection(int D);
#line 345 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\menuParameter.ino"
void selectName(int16_t *sel, uint8_t length);
#line 360 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\menuParameter.ino"
void showLetters(int16_t sel);
#line 389 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\menuParameter.ino"
void selectLevel(int16_t *sel, uint8_t length);
#line 434 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\menuParameter.ino"
void showLevel(int16_t sel);
#line 171 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\ProTon_FW.ino"
void setup()
{
  
  //turn off bt & wifi
  WiFi.mode(WIFI_OFF);
  btStop();
  //btStart();

  Serial.begin(SerialBAUD);
  while (!Serial)
    ;

  
  initHardWare();

  oledInit();
  // eepromInit();
  #ifndef Drachenfels
  OledPrintLogo();
  delay(2000);
  myPerfectAudio("my.");
  delay(1000);
  myPerfectAudio("perfect.");
  delay(1000);
  myPerfectAudio("audio.");
  delay(1000);
  
  //oledU8g2Test();
  oledStartUp();
  #endif
  printOnTerminal("starting setup..");
  BLEInit();
  changeVoiceVolum();
  //activatUserInputMode(0, 25, userSelectHandler, showOption, 10);
  //activatUserInputMode(0, 2, userSelectHandler, showOption, 1);

  printCurrentMODE();
  
  //initBluetooth();
  digitalWrite(LevelShift, HIGH);   //to enable TSX0108 LevelShift IC
}


void loop()
{
  //bluetoothREAD();

  int held = 0;
  while (digitalRead(encoderSwitchPin) == LOW && held < 40) // war 20
  {
    delay(100);
    held++;
  }
  if (held < MenuPressDelayMenu && held > 0)
  {
    shortPressHandler();
  }
  else if (held >= MenuPressDelayMenu && held < MenuPressDelayHOME) // MenuPressDelayHOME war 20
  {
    longPressHandler();
  }
  else if (held >= MenuPressDelayHOME && held < 40)
  {
    extremeLongPressHandler();
  }
  if (updateContentFlag)
  {
    //when normal is active
    
    updatePlatz();
    if (NORMAL_MODE_Flag)
      if (nm_lastencoderValue != nm_encoderValue)
      {
        //updateNotificationBar(currentUserOptions);
        changeVoiceVolum();
        nm_lastencoderValue = nm_encoderValue; 
        return;
      }
    //when edit is active
    if (EDIT_MODE_Flag)
    {
      if (em_lastencoderValue != em_encoderValue)
      {
        int index = abs(em_encoderValue / EncoderPulsesPerClick);
        //editMenuSelectedIndex = ; // from 0 -> 6
        editMenuScrolTo(index % 7);
        printEditModeMenu(screenPlatz, currentUserOptions);
        refreshEditMenuTrigger = true;
        em_lastencoderValue = em_encoderValue;
        return;
      }
    }
    //when menu is active
    if (MENU_MODE_Flag)
    {
      if (mm_lastencoderValue != mm_encoderValue)
        if (mainMenu.getScrollAbility() != 0)
        {
          int index = abs(mm_encoderValue / EncoderPulsesPerClick);
          int selectedMenuItemIndex = index % mainMenu.getMenuItemCount();
          //printing staff:
          /*
              Serial.print("encoderValue : ");                       Serial.println(mm_encoderValue);
              Serial.print("lastencoderValue : ");                   Serial.println(mm_lastencoderValue);
              Serial.print("encoderValue/EncoderPulsesPerClick :");  Serial.println(mm_encoderValue/EncoderPulsesPerClick );
              Serial.print("index : ");                              Serial.println(index);
              Serial.print("currentMenuItemCount : ");               Serial.println(mainMenu.getMenuItemCount());
              Serial.print("index%menuItemCnt : ");                  Serial.println(selectedMenuItemIndex);
            */
          mainMenu.scrol(selectedMenuItemIndex);
          listItem *list = mainMenu.printStyledList(printListStyle);
          oledPrintList(list, mainMenu.getMenuItemCount(), selectedMenuItemIndex);
          //mainMenu.printLCDList();
          mm_lastencoderValue = mm_encoderValue;
          return;
        }
    }
    //when user input is active
    if (USER_INPUT_Flag)
    {
      if (ur_lastencoderValue != ur_encoderValue)
      {
        int index = abs(ur_encoderValue / EncoderPulsesPerClick);
        tempSelectedVal = (index % maxSelectedVal) + minSelectedVal;
        showInputFunc(tempSelectedVal);
        ur_lastencoderValue = ur_encoderValue;
        return;
      }
    }
    updateContentFlag = false; //make updateContentFlag false agine
  }
  if (EDIT_MODE_Flag && refreshEditMenuTrigger)
  {
    editMenuScrolTo(getEditMenuScroller());
    printEditModeMenu(screenPlatz, currentUserOptions);
    refreshEditMenuTrigger = false;
  }
}

void initHardWare()
{
  //TODO: add restart here with pins to enter self boot mode!
  pinMode(encoderPin1, INPUT_PULLUP);
  pinMode(encoderPin2, INPUT_PULLUP);
  pinMode(encoderSwitchPin, INPUT_PULLUP);
  //pinMode(LED_BUILTIN, OUTPUT);
  pinMode(SELFBOOT, OUTPUT);
  pinMode(ADAU_RST, OUTPUT);
  pinMode(WP, OUTPUT);
  pinMode(LevelShift, OUTPUT);

  digitalWrite(encoderPin1, HIGH);      //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH);      //turn pullup resistor on
  digitalWrite(encoderSwitchPin, HIGH); //turn pullup resistor on
  //digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(ADAU_RST, LOW); //ADAU1701 processor turend off

  delay(500);

  digitalWrite(ADAU_RST, HIGH); //ADAU1701 processor turend on
  digitalWrite(WP, HIGH);

  #ifdef EEPROM
  digitalWrite(SELFBOOT, HIGH);
  #else
  digitalWrite(SELFBOOT, LOW);
  #endif

  delay(1000);

  #ifdef CS43131 
  config_cs43131(cs43131_i2c_addr, cs43131_dat, sizeof(cs43131_dat));
  #endif

  #ifndef EEPROM
  config_adau1701();
  #endif

  attachInterrupt(digitalPinToInterrupt(encoderPin1), updateEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoderPin2), updateEncoder, CHANGE);
  Wire.begin(SDA, SCL, 400000UL); // join i2c bus (address optional for master)
  delay(10);
}

void IRAM_ATTR updateEncoder()
{
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit
  int encoded = (MSB << 1) | LSB;     //converting the 2 pin value to single number

  if (NORMAL_MODE_Flag && !MUTE_Flag ) //here we just change voice volume:
  {
    int sum = (nm_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      nm_encoderValue = nm_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      nm_encoderValue = nm_encoderValue - encoderStep;
    nm_lastEncoded = encoded; //store this value for next time
    if ( nm_encoderValue <= 3 )     //Encoder Minimum Greenzen
    {
      nm_encoderValue = 3;
    }
    else if(nm_encoderValue >= 100 ) //Encoder Maximum Greenzen
    { 
      nm_encoderValue = 100;  
    }
     
  }

  if (EDIT_MODE_Flag) //here we scrol through edit menu
  {
    int sum = (em_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      em_encoderValue = em_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      em_encoderValue = em_encoderValue - encoderStep;
    em_lastEncoded = encoded; //store this value for next time
  }

  if (MENU_MODE_Flag) //here we scrol through main menu
  {
    int sum = (mm_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      mm_encoderValue = mm_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      mm_encoderValue = mm_encoderValue - encoderStep;
    mm_lastEncoded = encoded; //store this value for next time
  }

  if (USER_INPUT_Flag) //here we let the user to choose item
  {
    int sum = (ur_lastEncoded << 2) | encoded; //adding it to the previous encoded value
    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
      ur_encoderValue = ur_encoderValue + encoderStep;
    if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
      ur_encoderValue = ur_encoderValue - encoderStep;
    ur_lastEncoded = encoded; //store this value for next time
  }
  updateContentFlag = true;
}

void shortPressHandler(void)
{
  printOnTerminal("short press");
  if (NORMAL_MODE_Flag)
  {
    bool key = MUTE_Flag;
    (key) ? muteVoice(false) : muteVoice(true);
    updatePlatz();
    oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
    //printCurrentMODE();
    return;
  }

  if (EDIT_MODE_Flag)
  {
    int excutionVal = MesseMenuExcute();

    if (excutionVal == 4) //back handler excuted!
    {
      activitNormalMode();
      printCurrentMODE();
      refreshEditMenuTrigger = false;
      changeVoiceVolum();
      updateContentFlag = true;
      return;
    }
    updatePlatz();
    refreshEditMenuTrigger = true;
    return;
  }

  if (MENU_MODE_Flag)
  {
    Serial.println("now you try to excute some item handler");
    mainMenu.execute();
    //this step just to ensure that loop will update screen
    mm_encoderValue = 0;
    mm_lastencoderValue = 0.1;
    updateContentFlag = true;
    return;
  }
  if (USER_INPUT_Flag)
  {
    Serial.println("now you try to enter some value");
    //this step just to ensure that loop will update screen
    selectedValPtr[selectedValIdx] = tempSelectedVal;
    selectedValIdx++;
    float freq = 0.0;
    if(ManualHearingTestFlag)
    {
      if(Octav)
        freq = f_countOctav[selectedValIdx];
      else
        freq = f_countTerz[selectedValIdx];
      sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq);
      VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, hearingTestVolMINdB);         //Schreibe aktuelle Volume in DSP

    }
    Serial.print("current Inputs: ");
    Serial.println(selectedValIdx);
    Serial.print("max inputs: ");
    Serial.println(selectedValCnt);
    //Serial.println(showInputFunc(selectedValPtr[selectedValIdx]));
    if (selectedValIdx == selectedValCnt)
    {
      Serial.println("u did reach max input so go back to the last activated  Mode ");
      userInputFunc(selectedValPtr, selectedValIdx);
      restoreLastMode();
      updateContentFlag = true;
    }
    else
    {
      Serial.println("u did not reach max input so you can enter ");
      ur_encoderValue = 0;
      ur_lastencoderValue = 0.1;
      updateContentFlag = true;
    }

    return;
  }
}

void longPressHandler(void)
{
  printOnTerminal("long press");
  if (NORMAL_MODE_Flag)
  {
    activitEditMode();
    printCurrentMODE();
    editMenuScrolTo(0);
    printEditModeMenu(screenPlatz, currentUserOptions);
    updateContentFlag = true;
    return;
  }

  if (EDIT_MODE_Flag)
  {
    activitMenuMode();
    refreshEditMenuTrigger = false;
    mainMenu.scrol(0);
    listItem *list = mainMenu.printStyledList(printListStyle);
    oledPrintList(list, mainMenu.getMenuItemCount(), 0);
    updateContentFlag = true;
    return;
  }

  if (MENU_MODE_Flag)
  {
    int state = mainMenu.pop();
    if (state)
    {
      //this step just to ensure that loop will update screen
      mm_encoderValue = 0;
      mm_lastencoderValue = 0.1;
      updateContentFlag = true;
    }
    else
    {
      activitNormalMode();
      LCDprintFLAG();
      changeVoiceVolum();
      //updateContentFlag = true;
    }
  }
  if (USER_INPUT_Flag)
  {
    userInputFunc(selectedValPtr, selectedValIdx);
    restoreLastMode();
    //activitMenuMode();
    printCurrentMODE();
    updateContentFlag = true;
    return;
  }
}

void extremeLongPressHandler(void)
{
  Serial.println("extreme long press");
  if (NORMAL_MODE_Flag)
  {
    printCurrentMODE();
    return;
  }

  if (EDIT_MODE_Flag || MENU_MODE_Flag)
  {
    activitNormalMode();
    printCurrentMODE();
    changeVoiceVolum();
    updateContentFlag = true; // <-go back to loop and print voice Volume
    return;
  }
}

void muteVoice(bool muteVal)
{
  Serial.print("muteVoice(");
  Serial.print(muteVal);
  Serial.print(")");
  if (muteVal == true)
  {
    MUTE_Flag = true;
    VolumeStereoConvertANDsend(MasterVolumeAddr, VOLMIN);
    Serial.println("Mute");
  }
  else
  {
    MUTE_Flag = false;
    VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    Serial.println("Unmute");
  }
}

void changeVoiceVolum()
{
  //Serial.printf("nm_encoderValue %f before run\n",nm_encoderValue);
  dBVoiceVolum = encoderToDBVoice(VOLMIN, VOLMAX, (nm_encoderValue - VolumeINIT)); // dB
  printOnTerminal(" Master Vol. : \n" + String(dBVoiceVolum) + "dB");
  oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
  //updateNotificationBar(currentUserOptions);
  VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
  //Serial.printf("nm_encoderValue %f after run\n", VolumeINIT - dbVlaueToEncoder(dBVoiceVolum) );
  
  return;
}


double dbVlaueToEncoder(double dbVal)
{
  if (dbVal < 0)
  {
    return  (dbVal * max_number_of_pulses)/( VOLMIN);
  }
  else //(dbVal >= 0)
  {
    
    return 200 - (dbVal * max_number_of_pulses)/( VOLMAX) ;
  }
}  
/**
   This function transform pulses from encoder in user defined range values
   @param minval
   @param maxval
   @param pulses - the actual pulses count see getPulses()
   @return double - return a value between minval and maxval when user turn encoder knob
*/
double encoderToDBVoice(double minval, double maxval, double pulses)
{
  double val = 0.00;

  if (minval < 0 && maxval <= 0)
  {
    if (pulses >= 0)
      return maxval;
    else
    {
      val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval;
      //Serial.print("val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval:");
      //Serial.println(val = ((abs(pulses) * (minval - maxval)) / max_number_of_pulses) + maxval);
      
      if (val < minval)
        return minval;
      else
        return val;
    }
  }
  else if (minval >= 0 && maxval > 0)
  {
    if (pulses >= 0)
    {
      val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval;
      //Serial.print("val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval :");
      //Serial.println(val = (pulses * ((maxval - minval) / max_number_of_pulses)) + minval);
      
      if (val > maxval)
        return maxval;
      else
        return val;
    }
    else
      return minval;
  }
  else if (minval < 0 && maxval > 0)
  {
    if (pulses >= 0)
    {
      if (pulses == 0)
        return 0.00;
      else
      {
        val = pulses * (maxval / max_number_of_pulses);
        if (val > maxval)
          return maxval;
        else
          return val;
      }
    }
    else // pulses < 0
    {
      val = abs(pulses) * (minval / max_number_of_pulses);

      if (val < minval)
        return minval;
      else
        return val;
    }
  }
}

void AudioDetect(void)
{
  if (digitalRead(PIN_AudioDetect) == true)
  {
  }
  else
  {
    changeVoiceVolum();
  }
}

void printOnTerminal(String message)
{
  if (SerialTermON == true)
  {
    Serial.println(message);
  }
}

//TODO: Utilites
void printCurrentMODE()
{
  String msg = "";
  if (NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "NORMAL MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "EDIT MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & MENU_MODE_Flag & !USER_INPUT_Flag)
    msg = "MENU MODE IS ACTIVE NOW..";
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & USER_INPUT_Flag)
    msg = " USER INPUT MODE IS ACTIVE NOW..";

  Serial.println("/= = = = = = = = = = = = = = = = /");
  Serial.println(msg);
  Serial.println("/= = = = = = = = = = = = = = = = /");
  return;
}

void activitNormalMode(void)
{
  NORMAL_MODE_Flag = true;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = false;
}

void activitEditMode(void)
{
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = true;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = false;
}

void activitMenuMode(void)
{
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = true;
  USER_INPUT_Flag = false;
}

void activatUserInputMode(int16_t minVal, int16_t maxVal, void (*singleInputHandler)(int16_t *, uint8_t), void (*showInputHandler)(int16_t), int8_t selectedValCount)
{
  //save the last Mode

  saveLastMode();
  NORMAL_MODE_Flag = false;
  EDIT_MODE_Flag = false;
  MENU_MODE_Flag = false;
  USER_INPUT_Flag = true;

  maxSelectedVal = maxVal;
  minSelectedVal = minVal;
  selectedValCnt = selectedValCount;
  selectedValPtr = new int16_t[selectedValCnt];
  selectedValIdx = 0;
  userInputFunc = singleInputHandler;
  showInputFunc = showInputHandler;
}

void saveLastMode()
{

  if (NORMAL_MODE_Flag & !EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 0;
  else if (!NORMAL_MODE_Flag & EDIT_MODE_Flag & !MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 1;
  else if (!NORMAL_MODE_Flag & !EDIT_MODE_Flag & MENU_MODE_Flag & !USER_INPUT_Flag)
    lastMode = 2;
}

void restoreLastMode()
{
  switch (lastMode)
  {
  case 0:
    activitNormalMode();
    break;
  case 1:
    activitEditMode();
    break;
  case 2:
    activitMenuMode();
    break;
  }
}


#ifndef EEPROM
//This function writes a block of data to the I2C Bus
bool i2c_write_block(byte ic_addr, word reg_addr, byte * data, int data_length){

    /* This function needs  SoftI2C.h/I2CMaster.h included and a global SoftI2C object called i2c.*/

    i2c->startWrite(ic_addr);

    i2c->write(highByte(reg_addr));

    i2c->write(reg_addr);

    for( int i = 0; i < data_length; i++){

        i2c->write(data[i]);

    }

    return i2c->endWrite();

}
#endif

#ifndef EEPROM
void config_adau1701(){

  i2c = new SoftI2C(SDA_PIN, SCL_PIN);

      Serial.print("Init ADAU1701: ");
      Serial.println(i2c_write_block(DEVICE_ADDR_7bit, adau1701_core_reg, adau1701_core_dat1, sizeof(adau1701_core_dat1)));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_prog_reg, adau1701_prog_dat, sizeof(adau1701_prog_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_param_reg, adau1701_param_dat, sizeof(adau1701_param_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_hwconf_reg, adau1701_hwconf_dat, sizeof(adau1701_hwconf_dat));
      i2c_write_block(DEVICE_ADDR_7bit, adau1701_core_reg, adau1701_core_dat2, sizeof(adau1701_core_dat2));

  delete i2c;

};
#endif

/* #ifdef CS43131
void config_cs43131(byte ic_addr, byte * data, int data_length){

    /*  This function needs the CS43131_config.h included. The format must be:
        
        byte cs43131_i2c_addr = 0x30; //chip address 0x60 shifted right

        byte cs43131_reg[N] = { 0xRR, 0xRR, 0xRR, 0xDD, 0xDD, //3 register bytes, 2 data bytes
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD,
                                ...
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD};
                                
        0xRR = register byte
        0xDD = data byte*/
    
    /*

    i2c = new SoftI2C(SDA_PIN, SCL_PIN);

    for(int i = 0; i < data_length; i = i + 5){

      i2c->startWrite(ic_addr);
      
      i2c->write(data[i]);

      i2c->write(data[i+1]);
      
      i2c->write(data[i+2]);
      
      i2c->write(data[i+3]);
      
      i2c->write(data[i+4]);

      i2c->endWrite();

    }

    delete i2c;

}
#endif */
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Algorithmus1.ino"
/*
* Algorithmus1.ino
* Version 0.3.0.2
*/

#include "headphoneParameter.h"
#include <Arduino.h>
#define FREQ 0   // Speicheradresse für Array
#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array




extern uint8_t SoundSelect;
extern bool fineState;
int jj = 0;

float saveIteration1[3][31] = { // Profil 1
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

float saveIteration2[3][31] = { // Profil 2
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

/*
float saveIteration3[3][9] = { // Profil 3
    63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00};
*/
float hearingTestSWAP[3][31] = { // Zwischenspeicherung des Messeregnisses
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};
/*
float testAndi[3][9] = {
    {63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},
    {30.00, 15.00, 15.00, 5.00, 5.00, 25.00, 40.00, 45.00, 40.00},
    {30.00, 10.00, 15.00, 5.00, 15.00, 30.00, 50.00, 50.00, 45.00}};
*/
float hearingTestErgebnis[3][31]{// Gespeichertes Ergebnis des Hörtests
    20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00};

extern float headphoneMW[3][1];



/*
float saveIteration1[3][9];
float saveIteration2[3][9];
float saveIteration3[3][9];
*/
extern int f_countMAX;

float MWLINKS;
float MWRECHTS;
float MAXvalue;
float MINvalue;
float SchrittLinks = 0.00;
float SchrittRechts = 0.00;
uint8_t hearingLeftCASE = 0;
uint8_t hearingRightCASE = 0; // Fallunterscheidung
                              // 1. Hörkurve innerhalb Algo1NormalGrenze (25dB)
                              // 2. ein Teil der Hörkurve außerhalb der Algo1NormalGrenze
                              // 3. Hörkurve außerhalb der Algo1NormalGrenze
                              // 4. Hörkurve um Algo1NormalGrenze herum
float left20, left25, left31, left40, left50, left63, left80, left100, left125, left160, left200, left250, left315, left400, left500, left630, left800, left1000, left1250, left1600, left2000, left2500, left3150, left4000, left5000, left6300, left8000, left10000, left12500, left16000, left20000;
float right20, right25, right31, right40, right50, right63, right80, right100, right125, right160, right200, right250, right315, right400, right500, right630, right800, right1000, right1250, right1600, right2000, right2500, right3150, right4000, right5000, right6300, right8000, right10000, right12500, right16000, right20000;

void hearingTestSAVEinSWAP(void);
void MainDynamicBassBypassON(void);
void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void hearingTestIntensity(void);
//float MINcalculate(int Seite);
float MINcalculate(int Seite);
float MAXcalculate(int Seite);
void choose_algo();
int choose_algo_seite(int);
/*
 *  void Algo1(void); 
 *  einfacher Algorithmus zur Demonstration auf Messe
 *  es werden stupide in drei Iterationsschritten die Defizite angehoben und alle drei Profile zum Hören angeboten
 */

float MW[2], MIN[2], MAX[2];
int AnzahlkleinerGrenze[2], AnzahlgroesserGrenze[2], AnzahlgleichGrenze[2];

void Algo1(void)
{
    hearingTestSAVEinSWAP();                                                        // Zwischenspeichern des Hörtests

    /*
    MWLINKS = MittelwertBACK(LINKS);                                                 // Mittelwert linke Seite bilden
    MWRECHTS = MittelwertBACK(RECHTS);                                               // Mittelwert rechte Seite bilden
    int AnzahlkleinerGrenzeLINKS = KurvekleinerGrenze(LINKS, Algo1NormalGrenze);     // Anzahl der Messpunkte innerhalb der Grenze links
    int AnzahlkleinerGrenzeRECHTS = KurvekleinerGrenze(RECHTS, Algo1NormalGrenze);   // Anzahl der Messpunkte innerhalb der Grenze rechte
    int AnzahlgroesserGrenzeLINKS = KurvegroesserGrenze(LINKS, Algo1NormalGrenze);   // Anzahl der Messpunkte außerhalb der Grenze links
    int AnzahlgroesserGrenzeRECHTS = KurvegroesserGrenze(RECHTS, Algo1NormalGrenze); // Anzahl der Messpunkte außerhalb der Grenze rechts
    int AnzahlgleichGrenzeLINKS = KurvegleichGrenze(LINKS, Algo1NormalGrenze);       // Anzahl der Messpunkte auf der Grenze links (inkl. Toleranzband)
    int AnzahlgleichGrenzeRECHTS = KurvegleichGrenze(RECHTS, Algo1NormalGrenze);     // Anzahl der Messpunkte auf der Grenze rechts (inkl. Toleranzband)
    float MINlinks = MINcalculate(LINKS);
    float MINrechts = MINcalculate(RECHTS);
    float MAXlinks = MAXcalculate(LINKS);
    float MAXrechts = MAXcalculate(RECHTS);
    */


    float MWSWAP[2] = {0.00, 0.00};
    //float MWSWAPR = 0.00;
    //int AnzahlkleinerGrenze[2] = {0, 0};
    //int AnzahlkleinerGrenzeRECHTS = 0;
    //int AnzahlgroesserGrenze[2] = {0, 0};
    //int AnzahlgroesserGrenzeRECHTS = 0;
    //int AnzahlgleichGrenze[2] = {0, 0};
    //int AnzahlgleichGrenzeRECHTS = 0;

    if(fineState){

    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        MWSWAP[LINKS-1] = MWSWAP[LINKS-1] + hearingTestErgebnis[LINKS][kk];
        MWSWAP[RECHTS-1] = MWSWAP[RECHTS-1] + hearingTestErgebnis[RECHTS][kk];

        if (hearingTestErgebnis[LINKS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[RECHTS-1]++;
        }

    }

    MW[LINKS-1] = MWSWAP[LINKS-1] / (f_countMAX + 1);
    MW[RECHTS-1] = MWSWAP[RECHTS-1] / (f_countMAX + 1);
    
    MIN[LINKS-1] = hearingTestSAVE[LINKS][0];
    MIN[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[LINKS][kk] < MIN[LINKS-1])
        {
            MIN[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] < MIN[RECHTS-1])
        {
            MIN[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    MAX[LINKS-1] = hearingTestSAVE[LINKS][0];
    MAX[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[LINKS][kk] >= MAX[LINKS-1])
        {
            MAX[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] >= MAX[RECHTS-1])
        {
            MAX[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    } else {

    for (int kk = 5; kk <= f_countMAX; kk += 3)
    {
        MWSWAP[LINKS-1] = MWSWAP[LINKS-1] + hearingTestErgebnis[LINKS][kk];
        MWSWAP[RECHTS-1] = MWSWAP[RECHTS-1] + hearingTestErgebnis[RECHTS][kk];

        if (hearingTestErgebnis[LINKS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] <= Algo1NormalGrenze)
        {
            AnzahlkleinerGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] > Algo1NormalGrenze)
        {
            AnzahlgroesserGrenze[RECHTS-1]++;
        }

        if (hearingTestErgebnis[LINKS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[LINKS-1]++;
        }

        if (hearingTestErgebnis[RECHTS][kk] == Algo1NormalGrenze)
        {
            AnzahlgleichGrenze[RECHTS-1]++;
        }

    }

    MW[LINKS-1] = MWSWAP[LINKS-1] / (f_countMAX + 1);
    MW[RECHTS-1] = MWSWAP[RECHTS-1] / (f_countMAX + 1);
    
    MIN[LINKS-1] = hearingTestSAVE[LINKS][0];
    MIN[RECHTS-1] = hearingTestSAVE[RECHTS][0];

    for (int kk = 8; kk <= f_countMAX; kk += 3)
   {
        if (hearingTestSAVE[LINKS][kk] < MIN[LINKS-1])
        {
            MIN[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] < MIN[RECHTS-1])
        {
            MIN[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }
 
    MAX[LINKS-1] = hearingTestSAVE[LINKS][5];
    MAX[RECHTS-1] = hearingTestSAVE[RECHTS][5];

    for (int kk = 8; kk <= f_countMAX; kk += 3)
    {
        if (hearingTestSAVE[LINKS][kk] >= MAX[LINKS-1])
        {
            MAX[LINKS-1] = hearingTestSAVE[LINKS][kk];
        }

        if (hearingTestSAVE[RECHTS][kk] >= MAX[RECHTS-1])
        {
            MAX[RECHTS-1] = hearingTestSAVE[RECHTS][kk];
        }
    }

    }

    Serial.print(F("Anzahl kleiner Grenze Links: "));
    Serial.println(AnzahlkleinerGrenze[LINKS-1]);
    Serial.print(F("Anzahl kleiner Grenze Rechts: "));
    Serial.println(AnzahlkleinerGrenze[RECHTS-1]);
    Serial.print(F("Anzahl groesser Grenze Links: "));
    Serial.println(AnzahlgroesserGrenze[LINKS-1]);
    Serial.print(F("Anzahl groesser Grenze Rechts: "));
    Serial.println(AnzahlgroesserGrenze[RECHTS-1]);
    Serial.print(F("Anzahl gleich Grenze Links: "));
    Serial.println(AnzahlgleichGrenze[LINKS-1]);
    Serial.print(F("Anzahl gleich Grenze Rechts: "));
    Serial.println(AnzahlgleichGrenze[RECHTS-1]);
    Serial.print(F("Mittelwert links: "));
    Serial.println(MW[LINKS-1]);
    Serial.print(F("Mittelwert rechts: "));
    Serial.println(MW[RECHTS-1]);
    Serial.print(F("Minimum links: "));
    Serial.println(MIN[LINKS-1]);
    Serial.print(F("Minimum rechts: "));
    Serial.println(MIN[RECHTS-1]);
    Serial.print(F("Maximum links: "));
    Serial.println(MAX[LINKS-1]);
    Serial.print(F("Maximum rechts: "));
    Serial.println(MAX[RECHTS-1]);

    hearingTestIntensity();

    //choose_algo();

    Iteration1(LINKS);
    Iteration1(RECHTS);
    Iteration2(LINKS);
    Iteration2(RECHTS);

    
    if (headphone == false)
    {  
    
        
        Iteration1(LINKS);
        Iteration1(RECHTS);
        Iteration2(LINKS);
        Iteration2(RECHTS);
        // Iteration3(LINKS);
        //Iteration3(RECHTS);
    }

    if (headphone == true)
    {
        Iteration1_ohneHeadphone(LINKS);
        Iteration1_ohneHeadphone(RECHTS);
        //Iteration2_ohneHeadphone(LINKS);
        //Iteration2_ohneHeadphone(RECHTS);
        // Iteration3(LINKS);
        //Iteration3(RECHTS);
    }

    
}

// Algorithmus P1
void Iteration1(int Seite)
{

    for (jj = 0; jj <= f_countMAX; jj++)
    {
        saveIteration1[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
    }
}


void Iteration1_ohneHeadphone(int Seite)
{
    for (jj = 0; jj <= f_countMAX; jj++)
    {
        saveIteration1[Seite][jj] = (((HeadphoneFrequenzgang[Seite][jj] - headphoneMW[Seite][1]) + hearingTestSWAP[Seite][jj]) / 2);
    }
}


void Iteration2(int Seite)
{

    if(fineState){
        for (jj = 0; jj <= f_countMAX; jj++)
        {
            
            //switch (SoundSelect)
            switch (0)
            {
            case 0:
                Sound2(Seite);
                break;

            case 1:
                Sound3(Seite);
                break;

            case 2:
                Sound4(Seite);
                break;

            case 3:
            default:
                Sound5(Seite);
                break;
            }
            
        //Sound5(Seite);
        }
    } else {

        for (jj = 5; jj <= f_countMAX; jj += 3)
        {
            
            //switch (SoundSelect)
            switch (1)
            {
            case 0:
                Sound2(Seite);
                break;

            case 1:
                Sound3(Seite);
                break;

            case 2:
                Sound4(Seite);
                break;

            case 3:
            default:
                Sound5(Seite);
                break;
            }
            
        //Sound5(Seite);
        }

    }
    
}

//Algorithmus P2
void Sound2(int Seite)
{

    //saveIteration2[Seite][jj] = saveIteration1[Seite][jj] + (saveIteration1[Seite][jj] / 8);
    if (hearingTestSAVE[Seite][jj] <= MW[Seite-1])
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] - MW[Seite-1]) + (MW[Seite-1] / 3);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    if (hearingTestSAVE[Seite][jj] >MW[Seite-1])
    {
        //saveIteration2[Seite][jj] = MittelwertBACK(Seite) - hearingTestSWAP[Seite][jj];
        saveIteration2[Seite][jj] = (MW[Seite-1] - hearingTestSWAP[Seite][jj]) + (MW[Seite-1] / 3);
    }
}

//Algo P3
void Sound3(int Seite)
{

    if ((hearingTestSAVE[Seite][jj] < MAX[Seite-1]) && (MAX[Seite-1] - hearingTestSAVE[Seite][jj] <= 20.00))
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (MAX[Seite-1]) - hearingTestSWAP[Seite][jj] + (hearingTestSWAP[Seite][jj]/3);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    if ((hearingTestSAVE[Seite][jj] < MAX[Seite-1]) && (MAX[Seite-1] - hearingTestSAVE[Seite][jj] > 20.00))
    {
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
    }
}

//Algorithmus P4
void Sound4(int Seite)
{

    if (hearingTestSAVE[Seite][jj] <= Algo1NormalGrenze)
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
}

/*
void Iteration2(int Seite)
{
    */

//Algorithmus P5
void Sound5(int Seite)
{
    float neueGrenze;
    //if (MAXcalculate(Seite) <= 0.00)
    //{
    neueGrenze = (MAX[Seite-1] + MW[Seite-1]) / 2;
    //}
    //if (MAXcalculate(Seite) > 0.00)
    //{
    //  neueGrenze = MittelwertBACK(Seite) - MAXcalculate(Seite);
    //}
    Serial.print(F("neue Grenze: "));
    Serial.println(neueGrenze);

    if (hearingTestSAVE[Seite][jj] <= neueGrenze)
    {
        // die Kurve wird um (MittelwertBACK(Seite) / 3) -> also um ein Drittel des MW angehoben, da sonst zu leise ist
        saveIteration2[Seite][jj] = (hearingTestSWAP[Seite][jj] / 2);
        //folgende Codezeilen beschränken den Boost auf ein MAximum von 0dB (da sonst übersteuert und verzerrt wird)
        // todo
    }
    /*       
        if (hearingTestSAVE[Seite][jj] > MittelwertBACK(Seite))
        {
           
            saveIteration2[Seite][jj] = (MittelwertBACK(Seite) - hearingTestSWAP[Seite][jj]) + (MittelwertBACK(Seite) / 3);
        }
        */
}

/************************************************************
 * float MittelwertBACK(int Seite)
 * int Seite: 1 = links; 2 = RECHTS
 * 
 * bildet arithmetischen Mittelwert aller Messpunkte
 * und gibt ihn als float zurück
 ************************************************************/
float MittelwertBACK(int Seite)
{

    float MWSWAP = 0.00;
    float Mittelwert = 0.00;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        MWSWAP = MWSWAP + hearingTestErgebnis[Seite][kk];
    }
    return Mittelwert = MWSWAP / (f_countMAX + 1);
}

/************************************************************
 * int KurvekleinerGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * innerhalb der Grenze liegen
 ************************************************************/
int KurvekleinerGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] <= GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

/************************************************************
 * int KurvegroesserGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * außerhalb der Grenze liegen
 ************************************************************/
int KurvegroesserGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] > GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

/************************************************************
 * int KurvegleichGrenze(int Seite, float GrenzedB)
 * int Seite: 1 = links; 2 = RECHTS
 * float GrenzedB: übergibt die Grenze zur Hörschädigung
 * gibt als int die Anzahl der Punkte zurück, die 
 * auf der Grenze liegen (inkl. Toleranzband)
 ************************************************************/
int KurvegleichGrenze(int Seite, float GrenzedB)
{

    int AnzahlWerte = 0;
    for (int kk = 0; kk <= f_countMAX; kk++)
    {
        if (hearingTestErgebnis[Seite][kk] == GrenzedB)
        {
            AnzahlWerte++;
        }
    }
    return AnzahlWerte;
}

float MINcalculate(int Seite)
{
    float min = hearingTestSAVE[Seite][0];

    for (int kk = 1; kk <= f_countMAX; kk++)
    {
        if (hearingTestSAVE[Seite][kk] < min)
        {
            min = hearingTestSAVE[Seite][kk];
        }
    }
    return min;
}

float MAXcalculate(int Seite)
{
    float max = hearingTestSAVE[Seite][0];

    for (int kk = 1; kk <= f_countMAX;kk++)
    {
        if (hearingTestSAVE[Seite][kk] >= max)
        {
            max = hearingTestSAVE[Seite][kk];
        }
    }
    return max;
}

int choose_algo_seite(int Seite){ //0 = Hifi; 1 = Medical1; 2 = Medical2

    int klassifiziert[9] = {0}; //0: below 30db, 1: hysteresis, 2: above 25db
    int anzahlMesspunkte[3] = {0}; //0: below 30db, 1: hysteresis, 2: above 25db

    for( int i = 0; i < f_countMAX; i++){ //label the data

        if( hearingTestSAVE[Seite][i] > 40){ //below 40db

            return 2; //2 = Medical

        } else if( hearingTestSAVE[Seite][i] > 30){ //below 30db

            klassifiziert[i] = 0;
            anzahlMesspunkte[0]++; 

        } else if( hearingTestSAVE[Seite][i] > 25){ //between 30db and 25db

            klassifiziert[i] = 1;
            anzahlMesspunkte[1]++;

        } else { //above 25db

            klassifiziert[i] = 2;
            anzahlMesspunkte[2]++;

        }

    }
    
    //Hifi

    if( anzahlMesspunkte[2] == (f_countMAX || (f_countMAX - 1)) ) { //all above 25db || one below 35db, other above 25db

        return 0;

    } /* else

    if( (anzahlMesspunkte[2] == (f_countMAX - 2)) && (anzahlMesspunkte[1] == 2)){ //two in hysteresis, other above 25db

        bool tmp = false;

        for(int i = 0; i < (f_countMAX - 1) ; i++){

            if( (klassifiziert[i] == 1) && (klassifiziert[i+1] == 1) ){ //neighbors in hysteresis

                for(int j = 2; j <= (f_countMAX - i); j++){

                    if( klassifiziert[i + j] < 2){ //any other below 25db

                        tmp = true;
                        break;

                    }

                }

            }else if( (i == (f_countMAX - 1))){

                return 0; //0 = Hifi

            }else if(tmp)
            {
                break;
            }
            

        }

    } */
    else if( (anzahlMesspunkte[2] == (f_countMAX - 2)) && (anzahlMesspunkte[1] == 2)){ //two in hysteresis

        for (int i = 0; i < f_countMAX; i++)
        {
            if ( (klassifiziert[i] == 1) && (klassifiziert[i + 1] == 1) )
            {
                return 1;
            }
            
        }

        return 0;
        
    
    } else if((anzahlMesspunkte[2] = f_countMAX-1)){

                return 0; //0 = Hifi

            } else

    //Medical

    if (anzahlMesspunkte[1] >= 3) { // three or more in hysteresis
  
        return 1;

    } else

    if( (anzahlMesspunkte[1] >= 1) && (anzahlMesspunkte[0] = 1)){ // min. one in hyteresis and one below 35db

        return 1;

    } else { //everything else

        return 1;

    }
    

}


void choose_algo(){

    Serial.println("Algorithmus linke Seite: ");
    int temp = choose_algo_seite(LINKS);
        switch (temp)
        {
        case 0:
            Iteration2(LINKS);
            Serial.println("Hifi");
            break;
        
        case 1:
            Iteration1(LINKS);
            Serial.println("Medical1");
            break;

        case 2:
            Iteration1(LINKS);
            Serial.println("Medical2");
            break;

        default:
            Serial.println("something went wrong...");
            break;
        }
        
        Serial.println("Algorithmus rechte Seite: ");
        temp = choose_algo_seite(RECHTS);
        switch (temp)
        {
        case 0:
            Iteration2(RECHTS);
            Serial.println("Hifi");
            break;
        
        case 1:
            Iteration1(RECHTS);
            Serial.println("Medical1");
            break;

        case 2:
            Iteration1(RECHTS);
            Serial.println("Medical2");
            break;

        default:
            Serial.println("something went wrong...");
            break;
        }
}
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\AudioFunktionen.ino"
/*
 * AudioFunktionen.ino
 *  * * Version 0.3.0.2
 */
#include"arduino.h"
#include "AidaDSP.h"

extern uint8_t user1FLAG;
extern uint8_t user2FLAG;
extern uint8_t user3FLAG;
extern uint8_t linFLAG;
extern uint8_t compFLAG;
extern uint8_t per1FLAG;
extern uint8_t per2FLAG;
extern uint8_t per3FLAG;
extern uint8_t bassFLAG;

extern void AIDA_WRITE_REGISTER(uint8_t dspAddress, uint16_t address, uint8_t length, uint8_t *data);

void setMainDynamicBassBypass(bool );
void setPersonalizedCompressorBypassMode(int mode);

enum compressorMode 
{
CompressorMode_OFF,          //no compresor at all
CompressorMode_MultiBand,    //multiBand compresor
CompressorMode_RMS           //RMS compresor
};

compressorMode currentCompressorMode = CompressorMode_OFF;


void MainDynamicBassBypassON(void);
void MainDynamicBassBypassOFF(void);
void MainDirectMode(void);
void MainPersonalizedMode(void );
void MainHearingTestMode(void);
void MainEQBypassON(void);
void MainEQBypassOFF(void);
void PersonalizedMultibandCompressorBypassCompOFF(void);
void PersonalizedMultibandCompressorBypassMultibandCompressor(void);
void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void hearingSineSwitchLeft(void);
void hearingSineSwitchRight(void);
void muteADAU(void);
void unmuteADAU(void);



void setMainDynamicBassBypass(bool enable)
{
    if (enable) 
    {
        muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
        Serial.println(F("Bass On.."));
    }
    else 
    {
        muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
        Serial.println(F("Bass off.."));
    }
    bassFLAG = enable;
}

void setPersonalizedCompressorBypassMode(int mode)
{
    switch (mode)
    {
        case 0:                         //off
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 1);
             Serial.println(F("Comp off.."));
            compFLAG = false;
        break;
    
        case 1:                         //MultibandCompressor
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 2);
             Serial.println(F("Comp On .."));
        break;
/*
        case 2:                         //RMSCompressor
            muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 3);
            compFLAG = true;
        break;
        */
    }

}
/******************************
 * void MainDynamicBassBypassON(void)
 * 
 * schaltet Dynamic Bass AN
 *****************************/
void MainDynamicBassBypassON(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
    bassFLAG = true;
}

/******************************
 * void MainDynamicBassBypassOFF(void)
 * 
 * schaltet Dynamic Bass AUS
 *****************************/
void MainDynamicBassBypassOFF(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
    bassFLAG = false;
}

/******************************
 * void MainDirectMode(void)
 * 
 * schaltet in direct mode
 *****************************/
void MainDirectMode(void)
{
    muteADAU();
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 1, 3);
    unmuteADAU();
    Serial.println(F("1 - Direkt"));
}

/******************************
 * void MainPersonalizedMode( int ,int )
 * 
 * schaltet in personalized mode
 *****************************/
void MainPersonalizedMode( void )
{
    Serial.println(F("2 - personalized"));
    //MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, hearingTestvolumedB / 20.0));
    muteADAU();
    Algo1( );
    EqConfigurations( );
    delay(200);
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 2, 3);
    unmuteADAU();
    //PersonalizedMultibandCompressorBypassRMSCompressor();
   //MainDynamicBassBypassON();
}

/******************************
 * void MainHearingTestMode(void)
 * 
 * schaltet Hörtest an
 *****************************/
void MainHearingTestMode(void)
{
    //mux(DEVICE_ADDR_7bit, EQBypassAddr, 1, 2);
    muteADAU();
    mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
    unmuteADAU();
    Serial.println(F("123 - Hörtest"));
    //hearingTest();
}

/******************************
 * void MainEQBypassON(void)
 * 
 * schaltet EQ Aus
 *****************************/
void MainEQBypassON(void)
{
    //muxnoiseless(DEVICE_ADDR_7bit, EQBypassAddr, 1);
}

/******************************
 * void MainEQBypassOFF(void)
 * 
 * schaltet EQ An
 *****************************/
void MainEQBypassOFF(void)
{
    //muxnoiseless(DEVICE_ADDR_7bit, EQBypassAddr, 2);
}

/******************************
 * void PersonalizedMultibandCompressorBypassCompOFF(void)
 * 
 * schaltet Kompressor aus
 *****************************/
void PersonalizedMultibandCompressorBypassCompOFF(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 1);
    compFLAG = false;
}

/******************************
 * void PersonalizedMultibandCompressorBypassMultibandCompressor(void)
 * 
 * schaltet MultibandKompressor an
 *****************************/
void PersonalizedMultibandCompressorBypassMultibandCompressor(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 2);
    compFLAG = true;
}

/******************************
 * void PersonalizedMultibandCompressorBypassRMSCompressor(void)
 * 
 * schaltet RMSKompressor an
 *****************************/
void PersonalizedMultibandCompressorBypassRMSCompressor(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, personalizedMultibandCompressorBypassAddr, 3);
    compFLAG = true;
}

/******************************
 * void hearingSineSwitchLeft(void)
 * 
 * schaltet Sinusgenerator auf linken Kanal
 *****************************/
void hearingSineSwitchLeft(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1);
}

/******************************
 * void hearingSineSwitchRight(void)
 * 
 * schaltet Sinusgenerator auf rechten Kanal
 *****************************/
void hearingSineSwitchRight(void)
{
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2);
}

void muteADAU(){

    byte data[2] = {0x00, 0x18};
    AIDA_WRITE_REGISTER(DEVICE_ADDR_7bit, 0x081C, 2, data);

}

void unmuteADAU(){

    byte data[2] = {0x00, 0x1C};
    AIDA_WRITE_REGISTER(DEVICE_ADDR_7bit, 0x081C, 2, data);

}

/*
void VolumeUpSlewRate(double raisetime)
{
    for (int i = 0; i <= raisetime; i++)
    {
        VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    }
}

*/
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\BleUtills.ino"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <ArduinoJson.h>
#include <Arduino.h>
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"

extern User deviceUsers[USERS_COUNT];        //bring the array of users   
extern void intensityLevel20();
extern void intensityLevel40();
extern void intensityLevel60();
extern void intensityLevel80();

extern void intensityLevel100();

extern bool fineState ;
float tempArray[2][31] ;    
int lastDirection = 0;
String intinsityAsString = "2" ;
// https://www.uuidgenerator.net/


#define BLE_READ    BLECharacteristic::PROPERTY_READ
#define BLE_WRITE   BLECharacteristic::PROPERTY_WRITE


//BLE UUID's:

/*
 *
 *   TheMainService_UUID contains six sendUsersProfilesChar, linearP1P2Char, MuteVoiceChar, 
 *    BassSelectChar, CompSelectChar, IntensityChar, VoiceUpDownChar 
 * 
 */
#define TheMainService_UUID               "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define sendUsersProfilesChar_UUID        "43fdcd92-fa15-41d6-9be8-3708d0b91f0e"
#define linearP1P2Char_UUID               "8a42b311-f1f3-4389-bb22-9f47bbae00d2"
#define MuteVoiceChar_UUID                "beb5483e-36e1-4688-b7f5-ea07361b26a8"         // Mute
#define BassSelectChar_UUID               "66ea3d83-08b6-4e3f-b5b8-79237962442a" // Bass                   B1-B0
#define CompSelectChar_UUID               "ffcf4483-b621-4be4-8522-0592699a5532" // Compressor             C1-C0
#define IntensityChar_UUID                "c1960a3c-33c0-11e9-b210-d663bd873d93" // Intensity 20%, 40%, 60%, 80%, 100%
#define VoiceUpDownChar_UUID              "e59fa828-b46a-4d63-8fbc-3eb361985c47"

//*******************************************************************************************//

/*
 *
 * ManuelHearingService contains four characteristics: TonGeneratoreChar, iHeardTheToneChar, 
 * applyResultChar, BackToNormalChar
 * 
 */

#define ManuelHearingSrvs_UUID            "9dc68d22-364f-4fd5-94e2-dec2f3e66d2e"
#define TonGeneratoreChar_UUID            "5ed10ee4-c0ed-4c49-8eec-9fbe4fca64c5"
#define iHeardTheToneChar_UUID            "8647b1f1-cc5d-4a0c-bba7-4fa4d7e2ef8a"
#define applyResultChar_UUID              "91bb85b4-c800-4988-aad5-6985b0b6866a"  
#define BackToNormalChar_UUID             "03bba7fa-b915-499b-9fc7-ee8be47bbde8"  


//*******************************************************************************************//

// #define ChangedBLevelChar_UUID            "10d5294c-99d5-49bd-ad21-347a52a2c64c"
// #define ChangedBFineUPChar_UUID           "8b692ea8-af99-481e-8eb1-c0195640b950"
// //#define ChangedBFineDownChar_UUID         "4f09cda6-6980-405b-b471-8cb0965c1afb"
// #define FineChar_UUID                     "c2bc865b-a57e-438d-82e6-c4817be97128"   // ON-Off
// #define LinksRechtsChar_UUID             "0bf1a846-3304-4ebf-9c70-bb47e6e2735b"  // L-R

// #define ChangeVoiceVolumeChar_UUID        "bf8ecca6-eab5-48d3-b600-ca7d151c5a7d"         // Volume


// #define UserMangementSrvs_UUID            "d2ce3196-c0b2-4e90-aef1-8fd9ac9272b8"
// #define NewUserChar_UUID                  "67cd46aa-82e5-476e-8bde-1374e183ca25"
//*******************************************************************************************//
                  // "996f758e-33c0-11e9-b210-d663bd873d93"
                  // "764dd03c-4fe8-486b-8340-6ade505bae0f"
                  // "3cae3efc-d2bb-483a-b9c4-7713ab1c068a"
                  //"6cf79dc9-1393-429a-83d2-96800ac8f905"
                  //"3134fdec-117d-4efc-bbf1-1a1e96a4b436"

//*******************************************************************************************//

               
void changePersonlize(int);
//void muteVoice(bool );
void ledControl(bool);
void changeVoiceVolum(void);
void changeIntensity(int);
void MainPersonalizedMode(void);
void startHearingTest(void);
//void HearingButton(void);
void activateCurve_1(void);
void sinGeneratore(int);

bool muteFlag = false;
bool LEDFlag = false;

//*******************************************************************************************//
class MyServerCallBack : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    Serial.println("*********");
    //Serial.print("there is: ");
    Serial.print(pServer->getConnectedCount());
    Serial.println(" you are connected now");
    Serial.println("*********");
    Ble_CONTROL_MODE_FLAG = true ;
    oledBleConnectNotifier(true);
  }
  void onDisconnect(BLEServer *pServer)
  {
    Serial.println("*********");
    //Serial.print("there is: ");
    Serial.print(pServer->getConnectedCount());
    Serial.println(" you are disconnected now");
    Serial.println("*********");
    Ble_CONTROL_MODE_FLAG = false ;
    oledBleConnectNotifier(false);
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////                       ///////////////////////////////////////////////// 
///////////////////////////////////////////     JSON READ and WRITE        /////////////////////////////////////////////
////////////////////////////////////////////////                       /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class sendUsersProfilesCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_ARRAY_SIZE(62) + JSON_OBJECT_SIZE(11) + 350;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str() ;
    
    Serial.print("strVal :") ; Serial.println( strVal ) ; 
    
    deserializeJson(doc, strVal);
    const char* name = doc["n"]; 
    bool linear = doc["l"]; 
    bool p1 = doc["p1"]; 
    bool p2 = doc["p2"]; 
    String temp1 = doc["lo"]; 
    temp1.replace(",", ".");
    float linearOffset = String(temp1).toFloat() ;
    String temp2 = doc["p1o"]; 
    temp2.replace(",", ".");
    float p1Offset = String(temp2).toFloat() ;
    bool mute = doc["m"]; 
    bool pass = doc["b"]; 
    bool comp = doc["c"]; 
    int volume = doc["v"]; 
    JsonArray result = doc["r"];
    
    // Serial.print("linear :") ; Serial.println( linear) ; 
    // Serial.print("p1 :") ; Serial.println( p1 ) ; 
    // Serial.print("p2 :") ; Serial.println( p2 ) ; 
    // Serial.print("linearOffset :") ; Serial.println(linearOffset ) ; 
    // Serial.print("p1Offset :") ; Serial.println( p1Offset ) ; 
    // Serial.print("mute :") ; Serial.println( mute ) ; 
    // Serial.print("pass :") ; Serial.println( pass ) ; 
    // Serial.print("comp :") ; Serial.println( comp ) ; 
    // Serial.print("volume :") ; Serial.println( volume ) ; 

     int k = 0 ;
    Serial.println("The saved array :") ; 
   // int resTest[31] = result[31].as<int>() ;
    //TODO: how to know fine state??
    /*fine = true;
    if (result[0].as<int>() == 0 && result[1].as<int>() == 0 ) 
    {
      fine = false;
    }*/

  int fine = doc["hd"];
   if(fine == 31)
    {
    
      fineState = true;
    }
    else
    {
    
      fineState = false;
    }
  
    for(int  i = 1; i < 3; i++)
    {
      for( int f_counter = 0; f_counter < 31; f_counter++)
      {
      hearingTestvolumedB = result[k].as<int>() ;
      Serial.print(String( hearingTestvolumedB )+ "  ") ;

      hearingTestSPL[i][f_counter] = VolumedBFS + hearingTestvolumedB;
      hearingTestHL[i][f_counter] = hearingTestSPL[i][f_counter] - hearingTestSAVE[i + 2][f_counter];
      hearingTestSAVE[i][f_counter] = hearingTestHL[i][f_counter];
      k++ ;
      }   
      Serial.println() ;
    }


    if (mute)   muteVoice(true);
    if (linear) executeAlgorithms( 0 , linearOffset) ;
    if (p1)     executeAlgorithms( 1 , p1Offset) ;
    if (p2)     executeAlgorithms( 2 , 0) ;
    if (pass)   setMainDynamicBassBypass(1);
    if(comp)    setPersonalizedCompressorBypassMode(1);
    strcpy(deviceUsers[0].userName, name );
    deviceUsers[0].options.L = linear ;
    deviceUsers[0].options.P1 = p1 ;
    deviceUsers[0].options.P2 = p2 ;
    deviceUsers[0].options.Comp = comp ;
    deviceUsers[0].options.Pass = pass;

    String temp = doc["n"];
    oledBLegreetingUser(temp);
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};
//*******************************************************************************************//

class linearP1P2CallBack : public BLECharacteristicCallbacks
{
   void onWrite(BLECharacteristic *pCharacteristic)
   {
    std::string value = pCharacteristic->getValue();
   // const char* strVal = value.c_str();
     Serial.print("strVal :") ; Serial.println( value.c_str() ) ; 
    const size_t capacity = JSON_OBJECT_SIZE(2) + 30;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc, value );

    int Algo = doc["Algo"];
    preset = Algo ;   
    String temp = doc["offset"];    
    temp.replace(",", ".");
    float offset = String(temp).toFloat() ;
    Serial.print("Algo :") ; Serial.println( Algo ) ; 
    Serial.print("temp :") ; Serial.println( temp ) ; 
    Serial.print("offset :") ; Serial.println( offset ) ; 
    if(Algo == 2) Algo = 1;//only P1
    executeAlgorithms( Algo , offset); 
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic) {  }
};

//*******************************************************************************************//

class MuteCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    if (i == 0)
      muteVoice(false);
    else
      muteVoice(true);  
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);  
  }

  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string muteStatus = "";
    muteStatus = (muteFlag) ? "voice muted" : "voice unmuted";

    /*if (muteFlag)
      muteStatus = "voice muteed";
    else
      muteStatus = "voice unmuteed";
    */
    pCharacteristic->setValue(muteStatus);
  }
};
//*******************************************************************************************//
class BassSelectCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    bassFLAG = (bool)i;
    setMainDynamicBassBypass(bassFLAG);
  oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string bassStatus = "Bass mode is disabled";
    if (bassFLAG)
      bassStatus = "Bass mode is enabled";
    pCharacteristic->setValue(bassStatus);
  }
};
//*******************************************************************************************//
class CompSelectCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    Serial.println( i );
    setPersonalizedCompressorBypassMode(i);
    compFLAG = (bool)i;
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string CompressorStatus = "compressor is OFF";

    if (currentCompressorMode == CompressorMode_MultiBand)
      CompressorStatus = "Compressor in multiBand Mode.";
    else if (currentCompressorMode == CompressorMode_RMS)
      CompressorStatus = "Compressor in RMS Mode";

    pCharacteristic->setValue(CompressorStatus);
  }
};
//*******************************************************************************************//
class IntensityCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    
    //the new Intincity Protocol works only in the master and in the new branches after 23.09.2019.
    /*Serial.print(strVal);
    strVal.replace("0", "20");
    strVal.replace("1", "40");
    strVal.replace("2", "60");
    strVal.replace("3", "80");
    strVal.replace("4", "100");*/
    intinsityAsString = strVal;
    
    int i = strVal.toInt();
    Serial.print(" I :") ; Serial.println( i ) ; 
    //  if(!MUTE_Flag)
    // {
    //     muteVoice(true);
    //   if (i = 0)        intensityLevel20();
    //   else if ( i = 1)  intensityLevel40();
    //   else if ( i = 2)  intensityLevel60();
    //   else if ( i = 3)  intensityLevel80();
    //   else if ( i = 4)  intensityLevel100();
    //   muteVoice(false);
    // }
     if(!MUTE_Flag)
    {
      muteVoice(true);
      IntensityChoice = i;
      if (per1FLAG || per2FLAG)
        MainPersonalizedMode();
      muteVoice(false);
    }
      oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
  
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
    std::string currentPersonaliz = "";
    pCharacteristic->setValue(currentPersonaliz);
  }
};
//*******************************************************************************************//
class VoiceUpDownCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String volumeVal_str = value.c_str();
    dBVoiceVolum = volumeVal_str.toInt();
    Serial.print("the new Voice volume is: ");  Serial.println(dBVoiceVolum);
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
    VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    nm_encoderValue =VolumeINIT -  dbVlaueToEncoder(dBVoiceVolum);
    //Serial.print("nm_encoderValue in Ble is: ");  Serial.println(nm_encoderValue);
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};

//*******************************************************************************************//
int headphonePosition =0;
class TonGeneratoreCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    const char* strVal = value.c_str();
    Serial.print("strVal :") ; Serial.println( value.c_str() ) ; //{"D":1,"F":5,"V":"-83,9","P":0}
    
    const size_t capacity = JSON_OBJECT_SIZE(4) + 20;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc, strVal );

    int Direction = doc["D"];   // 1 : links - 2 : Rechts
    int frequencyIndex = doc["F"];   // sendIndex 0
    headphonePosition = doc["P"];//    1:headphoneVorne  ,0:headphoneHinten
    String temp = doc["V"]; // between -80,0 
    temp.replace("," ,".");
    float dBValue = String(temp).toFloat() ;
    Serial.print("Direction :") ; Serial.println( Direction ) ; 
    Serial.print("frequencyIndex :") ; Serial.println( frequencyIndex ) ;
    Serial.print("headphonePosition :") ; Serial.println( headphonePosition );
    Serial.print("dBValue :") ; Serial.println( dBValue ) ; 
    Serial.println("**********TonGeneratoreCallBack*************");
    float freq = f_count[frequencyIndex] ;
    Serial.print("change freq :");   Serial.println( freq );
    oledBleHearingTestNotifier(Direction,frequencyIndex,dBValue);
    
    if(headphonePosition){
      
      if(Direction == 2){

         Direction = 1;

      } else{

         Direction = 2;

      }
    }

      generatingTonWithFreq( Direction ,freq ,dBValue );

  }
  void onRead(BLECharacteristic *pCharacteristic){  }
};

//*******************************************************************************************//

class iHeardTheToneCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_OBJECT_SIZE(4) + 30;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    Serial.print("strVal :") ; Serial.println( value.c_str() ) ; 

    deserializeJson(doc, value );
    int Direction = doc["D"];      // 1 : links - 2 : Recht
    int frequencyIndex = doc["F"]; 
    //bool fineState = doc["fine"];
    String temp = doc["V"]; // between -80,0 
    temp.replace("," ,".");
    float dBValue = String(temp).toFloat() ;
    headphonePosition = doc["P"];//    1:headphoneVorne  ,0:headphoneHinten
    if(headphonePosition){
      
      if(Direction == 2){

         Direction = 1;

      } else{

         Direction = 2;

      }
    }

    Serial.print("Direction :") ; Serial.println( Direction ) ; 
    Serial.print("frequencyIndex :") ; Serial.println( frequencyIndex ) ; 
    Serial.print("dBValue :") ; Serial.println( dBValue ) ; 
    Serial.println("*********iHeardTheToneCallBack**************");
    
    if(Direction <= 2 && frequencyIndex < 31)
    {
      tempArray[Direction - 1][frequencyIndex] = dBValue ;
    }
    for(int j =0 ; j < 2 ; j++)
    {
      for(int k = 0 ; k < 31 ; k++)
        Serial.print(String( tempArray[j][k])+ "  ") ;
      Serial.println() ;
    }
    if(frequencyIndex == 30)
    {
      frequencyIndex = 0;
      if (Direction == 1){ Direction = 2 ;}
      else if (Direction == 2) { Direction = 1 ;}
    }
    dBValue = -120 ;
    float freq = f_count[frequencyIndex] ;
    oledBleHearingTestNotifier(Direction,frequencyIndex,dBValue);
    generatingTonWithFreq(Direction ,freq ,dBValue );
  }

  void onRead(BLECharacteristic *pCharacteristic) { }
};

//*******************************************************************************************//
class applyResultCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    const size_t capacity = JSON_ARRAY_SIZE(62) + JSON_OBJECT_SIZE(2) + 320;
    DynamicJsonDocument doc(capacity);
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str() ;
    deserializeJson(doc, strVal );
    Serial.println("===================================================="); 
    Serial.print("strVal :") ; Serial.println( strVal ) ; 
    Serial.println("===================================================="); 
    
    JsonArray result = doc["result"];
    int fine = doc["hd"];

    int k = 0 ;
   if(fine == 31)
    {
    
      fineState = true;
    }
    else
    {
    
      fineState = false;
    }
    Serial.println("======================= hearingTestvolumedB ============================="); 
    for(int  i = 1; i < 3; i++)
    {
      for( int f_counter = 0; f_counter < 31; f_counter++)
      {
        hearingTestvolumedB = result[(i-1) * 31 + f_counter].as<int>() ;
        delay(2);
        Serial.print(String( hearingTestvolumedB )+ "  ") ;
  
        hearingTestSPL[i][f_counter] = VolumedBFS + hearingTestvolumedB;
        hearingTestHL[i][f_counter] = hearingTestSPL[i][f_counter] - hearingTestSAVE[i + 2][f_counter];
        hearingTestSAVE[i][f_counter] = hearingTestHL[i][f_counter];
        k++ ;
      }   
      Serial.println() ;
    }
    Serial.println() ;


    Serial.println("======================hearingTestSAVEArray=============================="); 
    for(int  i = 0; i < 5; i++)
    {
      for( int j = 0; j < 31; j++)
      {
        Serial.print(String( hearingTestSAVE[i][j])+ "  ") ;
      }   
      Serial.println() ;
    }
    Serial.println() ;
    
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};


//*******************************************************************************************//
class BackToNormalCallBack : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string value = pCharacteristic->getValue();
    String strVal = value.c_str();
    int i = strVal.toInt();
    MainDirectMode();
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    oledBleChangeVoiceVolume(dBVoiceVolum,preset,intinsityAsString,MUTE_Flag,bassFLAG,compFLAG);
    PersonalizedMultibandCompressorBypassCompOFF();   
  }
  void onRead(BLECharacteristic *pCharacteristic){ }
};
//*******************************************************************************************//

void BLEInit(void)
{
  //1-create BLE device:
  #ifdef Drachenfels
  BLEDevice::init("ProTon/Drachenfels");
  #else
  BLEDevice::init("ProTon PLUS");
  #endif
  //BLEDevice::setMTU(512);
  esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);
  //2-create server:
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallBack);
  //3-create services:
  BLEService *TheMainService = pServer->createService(TheMainService_UUID);
  BLEService *ManuelHearingService = pServer->createService(ManuelHearingSrvs_UUID);

  //4-create characteristics:
  BLECharacteristic *sendUsersProfilesCharacteristic = TheMainService->createCharacteristic(sendUsersProfilesChar_UUID, BLE_WRITE);
 
  BLECharacteristic *linearP1P2Characteristic = TheMainService->createCharacteristic(linearP1P2Char_UUID, BLE_WRITE);

  BLECharacteristic *MuteVoiceCharacteristic = TheMainService->createCharacteristic(MuteVoiceChar_UUID, BLE_WRITE);

  BLECharacteristic *BassSelectCharacteristic = TheMainService->createCharacteristic(BassSelectChar_UUID, BLE_WRITE);

  BLECharacteristic *CompSelectCharacteristic = TheMainService->createCharacteristic(CompSelectChar_UUID, BLE_WRITE);

  BLECharacteristic *IntensityCharacteristic = TheMainService->createCharacteristic(IntensityChar_UUID, BLE_WRITE);

  BLECharacteristic *VoiceUpDownCharacteristic = TheMainService->createCharacteristic(VoiceUpDownChar_UUID, BLE_WRITE);

  //*******************************************************************************************// 

  BLECharacteristic *TonGeneratoreCharacteristic = ManuelHearingService->createCharacteristic(TonGeneratoreChar_UUID, BLE_WRITE);

  BLECharacteristic *iHeardTheToneCharacteristic = ManuelHearingService->createCharacteristic(iHeardTheToneChar_UUID, BLE_WRITE);

  BLECharacteristic *applyResultCharacteristic = ManuelHearingService->createCharacteristic(applyResultChar_UUID, BLE_WRITE);
  
  BLECharacteristic *BackToNormalCharacteristic = ManuelHearingService->createCharacteristic(BackToNormalChar_UUID, BLE_WRITE); 
   
 //*******************************************************************************************//

  sendUsersProfilesCharacteristic->setCallbacks(new sendUsersProfilesCallBack);
  linearP1P2Characteristic->setCallbacks(new linearP1P2CallBack);
  MuteVoiceCharacteristic->setCallbacks(new MuteCallBack);
  BassSelectCharacteristic->setCallbacks(new BassSelectCallBack);
  CompSelectCharacteristic->setCallbacks(new CompSelectCallBack);
  IntensityCharacteristic->setCallbacks(new IntensityCallBack);
  VoiceUpDownCharacteristic->setCallbacks(new VoiceUpDownCallBack);
  TonGeneratoreCharacteristic->setCallbacks(new TonGeneratoreCallBack);
  iHeardTheToneCharacteristic->setCallbacks(new iHeardTheToneCallBack);
  applyResultCharacteristic->setCallbacks(new applyResultCallBack);
  BackToNormalCharacteristic->setCallbacks(new BackToNormalCallBack);
  
  //*******************************************************************************************//
  //6-start all services:
  TheMainService->start();
  ManuelHearingService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();
  printOnTerminal("Start The services ..");
}

// void ledControl(bool LEDVal)
// {
//   if (LEDVal)
//   {
//     LEDFlag = true;
//     dc_source(DEVICE_ADDR_7bit, DC1, 1.00);
//     Serial.println(F("LED ON .."));
//   }
//   else
//   {
//     LEDFlag = false;
//     dc_source(DEVICE_ADDR_7bit, DC1, 0.00);
//     Serial.println(F("LED OFF .."));
//   }
// }


void sinGeneratore(int freq)
{
  Serial.print("change freq :");   Serial.println( freq );

  VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
  mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq); 
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, 0);       //hearingTestvolumedB 

}
void executeAlgorithms(int A ,float OFF){
  switch (A)
    {
      case 0 :
      linFLAG = true;
      per1FLAG = false;
      per2FLAG = false;
      Serial.println(" linearCallBack");
      preset = 0;
      MainDirectMode();
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum + OFF) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainDynamicBassBypassOFF();
      Serial.println("linear..");
      break;
  
      case 1 :
      linFLAG = false;
      per1FLAG = true;
      per2FLAG = false;
      preset = 1;
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum + OFF) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainPersonalizedMode();
      Serial.println("P1CallBack( PLUS Agorithmus )..");
      break;
    
      case 2 :
      linFLAG = false;
      per1FLAG = false;
      per2FLAG = true;
      preset = 2;
      muteVoice(true);
      MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
      PersonalizedMultibandCompressorBypassCompOFF();
      MainPersonalizedMode();
      muteVoice(false);
      Serial.println("P2CallBack( LIFESTYLE Algorithmus )");
      break;
  }
}
void generatingTonWithFreq(int D ,float F ,float dB )
{
  VolumeStereoConvertANDsend(MasterVolumeAddr, VOLMAX );     //set the voice to the maximum value
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 3);
  
  #ifndef Drachenfels 
  //temporal changes must be resored to the orginal functionality when the new Hardware arrive..
  if (D == 1)     
    {
      mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2, 2);
    } 
  else if (D == 2)   
   {
      mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1, 2);
    }
  #else
  if(lastDirection != D)
     mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, D, 2);
  
  #endif
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, F); 
  if (headphonePosition)
    {
      gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
      if (D == 1)
        mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 2, 2);
      else
        mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, 1, 2);
       Serial.println("headphonePosition ist Vrone");
    }
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, dB );       //hearingTestvolumedB 
  //MasterVolumeMono(DEVICE_ADDR_7bit, hearingTestSineVolumeAddr, dBValue);
  lastDirection = D ;
}
 

#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EQ.ino"
#include <Arduino.h>

#include "AidaDSP.h"
#include "Define_adrress.h"

#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array


//equalizer_t EQsleft[31] ; 
equalizer_t EQleft20, EQleft25, EQleft31, EQleft40, EQleft50, EQleft63, EQleft80, EQleft100, EQleft125, EQleft160, 
            EQleft200, EQleft250, EQleft315, EQleft400, EQleft500, EQleft630, EQleft800, EQleft1000, EQleft1250, 
            EQleft1600, EQleft2000, EQleft2500, EQleft3150, EQleft4000, EQleft5000, EQleft6300, EQleft8000, EQleft10000, 
            EQleft12500, EQleft16000, EQleft20000;

//equalizer_t EQsright[31] ;
equalizer_t EQright20, EQright25, EQright31, EQright40, EQright50, EQright63, EQright80, EQright100, EQright125, EQright160, 
            EQright200, EQright250, EQright315, EQright400, EQright500, EQright630, EQright800, EQright1000, EQright1250, 
            EQright1600, EQright2000, EQright2500, EQright3150, EQright4000, EQright5000, EQright6300, EQright8000, EQright10000, 
            EQright12500, EQright16000, EQright20000;


extern float hearingTestSWAP[3][31];
extern float saveIteration1[3][31];
extern float saveIteration2[3][31];
extern float f_count[31];
extern bool fineState;
//extern float saveIteration3[3][9];

void PersonalizedMultibandCompressorBypassRMSCompressor(void);
void MainDynamicBassBypassON(void);

void EqConfigurations()            //it's name was  ProfileAndi()
{
    if(!fineState )    //9 Filter
    {       
      setEqParametersOktave();
    }
    else              //31 Filter
    {
      setEqParametersTerZ();  
    }
    
    switch (preset)               //  preset to switch between the Algorithms..
    {
        case 1:                    //Algorithms 1
        Serial.println("erste Iteration!");
        
        saveIteration1InEqBoost();

        sendEqDataToBoard();

        printIteration(3,31,saveIteration1[0] );
        
        break;

    case 2:                     //Algorithms 2
        Serial.println("zweite Iteration!");
        //////////////////////////////thies Funktions has not yet been created///////////////////////////
        saveIteration2InEqBoost();

        sendEqDataToBoard();

        printIteration(3,31,saveIteration2[0] );
    }
}

    void printIteration(int m,int n ,float *Iteration )
    {
        for (int i = 0; i <= m; i++) //Zeilenweise
        {
            for (int j = 0; j <= n; j++) //Spaltenweise
            {
                Serial.print(Iteration[i * n + j]);
                Serial.print("\t");
            }
            Serial.println(" ");
        }
    }

    void setEqParametersOktave()
    {
          //in each line we configure one frequence..
          
        //Filters for the left side
        float Guete = 1.41;
        EQleft20.Q = Guete;     EQleft20.boost = 0.00;       EQleft20.f0 = f_count[0];       EQleft20.type = Parametric;       EQleft20.phase = true;       EQleft20.onoff = false;
        EQleft25.Q = Guete;     EQleft25.boost = 0.00;       EQleft25.f0 = f_count[1];       EQleft25.type = Parametric;       EQleft25.phase = true;       EQleft25.onoff = false;
        EQleft31.Q = Guete;     EQleft31.boost = 0.00;       EQleft31.f0 = f_count[2];       EQleft31.type = Parametric;       EQleft31.phase = true;       EQleft31.onoff = false;
        EQleft40.Q = Guete;     EQleft40.boost = 0.00;       EQleft40.f0 = f_count[3];       EQleft40.type = Parametric;       EQleft40.phase = true;       EQleft40.onoff = false;
        EQleft50.Q = Guete;     EQleft50.boost = 0.00;       EQleft50.f0 = f_count[4];       EQleft50.type = Parametric;       EQleft50.phase = true;       EQleft50.onoff = false;
        EQleft63.Q = Guete;     EQleft63.boost = 0.00;       EQleft63.f0 = f_count[5];       EQleft63.type = Parametric;       EQleft63.phase = true;       EQleft63.onoff = true;
        EQleft80.Q = Guete;     EQleft80.boost = 0.00;       EQleft80.f0 = f_count[6];       EQleft80.type = Parametric;       EQleft80.phase = true;       EQleft80.onoff = false;
        EQleft100.Q = Guete;    EQleft100.boost = 0.00;      EQleft100.f0 = f_count[7];      EQleft100.type = Parametric;      EQleft100.phase = true;      EQleft100.onoff = false;
        EQleft125.Q = Guete;    EQleft125.boost = 0.00;      EQleft125.f0 = f_count[8];      EQleft125.type = Parametric;      EQleft125.phase = true;      EQleft125.onoff = true;
        EQleft160.Q = Guete;    EQleft160.boost = 0.00;      EQleft160.f0 = f_count[9];      EQleft160.type = Parametric;      EQleft160.phase = true;      EQleft160.onoff = false;
        EQleft200.Q = Guete;    EQleft200.boost = 0.00;      EQleft200.f0 = f_count[10];     EQleft200.type = Parametric;      EQleft200.phase = true;      EQleft200.onoff = false;
        EQleft250.Q = Guete;    EQleft250.boost = 0.00;      EQleft250.f0 = f_count[11];     EQleft250.type = Parametric;      EQleft250.phase = true;      EQleft250.onoff = true;
        EQleft315.Q = Guete;    EQleft315.boost = 0.00;      EQleft315.f0 = f_count[12];     EQleft315.type = Parametric;      EQleft315.phase = true;      EQleft315.onoff = false;
        EQleft400.Q = Guete;    EQleft400.boost = 0.00;      EQleft400.f0 = f_count[13];     EQleft400.type = Parametric;      EQleft400.phase = true;      EQleft400.onoff = false;
        EQleft500.Q = Guete;    EQleft500.boost = 0.00;      EQleft500.f0 = f_count[14];     EQleft500.type = Parametric;      EQleft500.phase = true;      EQleft500.onoff = true;
        EQleft630.Q = Guete;    EQleft630.boost = 0.00;      EQleft630.f0 = f_count[15];     EQleft630.type = Parametric;      EQleft630.phase = true;      EQleft630.onoff = false;
        EQleft800.Q = Guete;    EQleft800.boost = 0.00;      EQleft800.f0 = f_count[16];     EQleft800.type = Parametric;      EQleft800.phase = true;      EQleft800.onoff = false;
        EQleft1000.Q = Guete;   EQleft1000.boost = 0.00;     EQleft1000.f0 = f_count[17];    EQleft1000.type = Parametric;     EQleft1000.phase = true;     EQleft1000.onoff = true;
        EQleft1250.Q = Guete;   EQleft1250.boost = 0.00;     EQleft1250.f0 = f_count[18];    EQleft1250.type = Parametric;     EQleft1250.phase = true;     EQleft1250.onoff = false;
        EQleft1600.Q = Guete;   EQleft1600.boost = 0.00;     EQleft1600.f0 = f_count[19];    EQleft1600.type = Parametric;     EQleft1600.phase = true;     EQleft1600.onoff = false;
        EQleft2000.Q = Guete;   EQleft2000.boost = 0.00;     EQleft2000.f0 = f_count[20];    EQleft2000.type = Parametric;     EQleft2000.phase = true;     EQleft2000.onoff = true;
        EQleft2500.Q = Guete;   EQleft2500.boost = 0.00;     EQleft2500.f0 = f_count[21];    EQleft2500.type = Parametric;     EQleft2500.phase = true;     EQleft2500.onoff = false;
        EQleft3150.Q = Guete;   EQleft3150.boost = 0.00;     EQleft3150.f0 = f_count[22];    EQleft3150.type = Parametric;     EQleft3150.phase = true;     EQleft3150.onoff = false;
        EQleft4000.Q = Guete;   EQleft4000.boost = 0.00;     EQleft4000.f0 = f_count[23];    EQleft4000.type = Parametric;     EQleft4000.phase = true;     EQleft4000.onoff = true;
        EQleft5000.Q = Guete;   EQleft5000.boost = 0.00;     EQleft5000.f0 = f_count[24];    EQleft5000.type = Parametric;     EQleft5000.phase = true;     EQleft5000.onoff = false;
        EQleft6300.Q = Guete;   EQleft6300.boost = 0.00;     EQleft6300.f0 = f_count[25];    EQleft6300.type = Parametric;     EQleft6300.phase = true;     EQleft6300.onoff = false;
        EQleft8000.Q = Guete;   EQleft8000.boost = 0.00;     EQleft8000.f0 = f_count[26];    EQleft8000.type = Parametric;     EQleft8000.phase = true;     EQleft8000.onoff = true;
        EQleft10000.Q = Guete;  EQleft10000.boost = 0.00;    EQleft10000.f0 = f_count[27];   EQleft10000.type = Parametric;    EQleft10000.phase = true;    EQleft10000.onoff = false;
        EQleft12500.Q = Guete;  EQleft12500.boost = 0.00;    EQleft12500.f0 = f_count[28];   EQleft12500.type = Parametric;    EQleft12500.phase = true;    EQleft12500.onoff = false;
        EQleft16000.Q = Guete;  EQleft16000.boost = 0.00;    EQleft16000.f0 = f_count[29];   EQleft16000.type = Parametric;    EQleft16000.phase = true;    EQleft16000.onoff = true;
        EQleft20000.Q = Guete;  EQleft20000.boost = 0.00;    EQleft20000.f0 = f_count[30];   EQleft20000.type = Parametric;    EQleft20000.phase = true;    EQleft20000.onoff = false;

        //Filters for the right side
        EQright20.Q = Guete;     EQright20.boost = 0.00;       EQright20.f0 = f_count[0];       EQright20.type = Parametric;       EQright20.phase = true;       EQright20.onoff = false;
        EQright25.Q = Guete;     EQright25.boost = 0.00;       EQright25.f0 = f_count[1];       EQright25.type = Parametric;       EQright25.phase = true;       EQright25.onoff = false;
        EQright31.Q = Guete;     EQright31.boost = 0.00;       EQright31.f0 = f_count[2];       EQright31.type = Parametric;       EQright31.phase = true;       EQright31.onoff = false;
        EQright40.Q = Guete;     EQright40.boost = 0.00;       EQright40.f0 = f_count[3];       EQright40.type = Parametric;       EQright40.phase = true;       EQright40.onoff = false;
        EQright50.Q = Guete;     EQright50.boost = 0.00;       EQright50.f0 = f_count[4];       EQright50.type = Parametric;       EQright50.phase = true;       EQright50.onoff = false;
        EQright63.Q = Guete;     EQright63.boost = 0.00;       EQright63.f0 = f_count[5];       EQright63.type = Parametric;       EQright63.phase = true;       EQright63.onoff = true;
        EQright80.Q = Guete;     EQright80.boost = 0.00;       EQright80.f0 = f_count[6];       EQright80.type = Parametric;       EQright80.phase = true;       EQright80.onoff = false;
        EQright100.Q = Guete;    EQright100.boost = 0.00;      EQright100.f0 = f_count[7];      EQright100.type = Parametric;      EQright100.phase = true;      EQright100.onoff = false;
        EQright125.Q = Guete;    EQright125.boost = 0.00;      EQright125.f0 = f_count[8];      EQright125.type = Parametric;      EQright125.phase = true;      EQright125.onoff = true;
        EQright160.Q = Guete;    EQright160.boost = 0.00;      EQright160.f0 = f_count[9];      EQright160.type = Parametric;      EQright160.phase = true;      EQright160.onoff = false;
        EQright200.Q = Guete;    EQright200.boost = 0.00;      EQright200.f0 = f_count[10];     EQright200.type = Parametric;      EQright200.phase = true;      EQright200.onoff = false;
        EQright250.Q = Guete;    EQright250.boost = 0.00;      EQright250.f0 = f_count[11];     EQright250.type = Parametric;      EQright250.phase = true;      EQright250.onoff = true;
        EQright315.Q = Guete;    EQright315.boost = 0.00;      EQright315.f0 = f_count[12];     EQright315.type = Parametric;      EQright315.phase = true;      EQright315.onoff = false;
        EQright400.Q = Guete;    EQright400.boost = 0.00;      EQright400.f0 = f_count[13];     EQright400.type = Parametric;      EQright400.phase = true;      EQright400.onoff = false;
        EQright500.Q = Guete;    EQright500.boost = 0.00;      EQright500.f0 = f_count[14];     EQright500.type = Parametric;      EQright500.phase = true;      EQright500.onoff = true;
        EQright630.Q = Guete;    EQright630.boost = 0.00;      EQright630.f0 = f_count[15];     EQright630.type = Parametric;      EQright630.phase = true;      EQright630.onoff = false;
        EQright800.Q = Guete;    EQright800.boost = 0.00;      EQright800.f0 = f_count[16];     EQright800.type = Parametric;      EQright800.phase = true;      EQright800.onoff = false;
        EQright1000.Q = Guete;   EQright1000.boost = 0.00;     EQright1000.f0 = f_count[17];    EQright1000.type = Parametric;     EQright1000.phase = true;     EQright1000.onoff = true;
        EQright1250.Q = Guete;   EQright1250.boost = 0.00;     EQright1250.f0 = f_count[18];    EQright1250.type = Parametric;     EQright1250.phase = true;     EQright1250.onoff = false;
        EQright1600.Q = Guete;   EQright1600.boost = 0.00;     EQright1600.f0 = f_count[19];    EQright1600.type = Parametric;     EQright1600.phase = true;     EQright1600.onoff = false;
        EQright2000.Q = Guete;   EQright2000.boost = 0.00;     EQright2000.f0 = f_count[20];    EQright2000.type = Parametric;     EQright2000.phase = true;     EQright2000.onoff = true;
        EQright2500.Q = Guete;   EQright2500.boost = 0.00;     EQright2500.f0 = f_count[21];    EQright2500.type = Parametric;     EQright2500.phase = true;     EQright2500.onoff = false;
        EQright3150.Q = Guete;   EQright3150.boost = 0.00;     EQright3150.f0 = f_count[22];    EQright3150.type = Parametric;     EQright3150.phase = true;     EQright3150.onoff = false;
        EQright4000.Q = Guete;   EQright4000.boost = 0.00;     EQright4000.f0 = f_count[23];    EQright4000.type = Parametric;     EQright4000.phase = true;     EQright4000.onoff = true;
        EQright5000.Q = Guete;   EQright5000.boost = 0.00;     EQright5000.f0 = f_count[24];    EQright5000.type = Parametric;     EQright5000.phase = true;     EQright5000.onoff = false;
        EQright6300.Q = Guete;   EQright6300.boost = 0.00;     EQright6300.f0 = f_count[25];    EQright6300.type = Parametric;     EQright6300.phase = true;     EQright6300.onoff = false;
        EQright8000.Q = Guete;   EQright8000.boost = 0.00;     EQright8000.f0 = f_count[26];    EQright8000.type = Parametric;     EQright8000.phase = true;     EQright8000.onoff = true;
        EQright10000.Q = Guete;  EQright10000.boost = 0.00;    EQright10000.f0 = f_count[27];   EQright10000.type = Parametric;    EQright10000.phase = true;    EQright10000.onoff = false;
        EQright12500.Q = Guete;  EQright12500.boost = 0.00;    EQright12500.f0 = f_count[28];   EQright12500.type = Parametric;    EQright12500.phase = true;    EQright12500.onoff = false;
        EQright16000.Q = Guete;  EQright16000.boost = 0.00;    EQright16000.f0 = f_count[29];   EQright16000.type = Parametric;    EQright16000.phase = true;    EQright16000.onoff = true;
        EQright20000.Q = Guete;  EQright20000.boost = 0.00;    EQright20000.f0 = f_count[30];   EQright20000.type = Parametric;    EQright20000.phase = true;    EQright20000.onoff = false;

    }

    void setEqParametersTerZ()
    {
        //in each line we configure one frequence..
        
        //Filters for the left side
        float Guete = 4.38;
        EQleft20.Q = Guete;     EQleft20.boost = 0.00;       EQleft20.f0 = f_count[0];       EQleft20.type = Parametric;       EQleft20.phase = true;       EQleft20.onoff = true;
        EQleft25.Q = Guete;     EQleft25.boost = 0.00;       EQleft25.f0 = f_count[1];       EQleft25.type = Parametric;       EQleft25.phase = true;       EQleft25.onoff = true;
        EQleft31.Q = Guete;     EQleft31.boost = 0.00;       EQleft31.f0 = f_count[2];       EQleft31.type = Parametric;       EQleft31.phase = true;       EQleft31.onoff = true;
        EQleft40.Q = Guete;     EQleft40.boost = 0.00;       EQleft40.f0 = f_count[3];       EQleft40.type = Parametric;       EQleft40.phase = true;       EQleft40.onoff = true;
        EQleft50.Q = Guete;     EQleft50.boost = 0.00;       EQleft50.f0 = f_count[4];       EQleft50.type = Parametric;       EQleft50.phase = true;       EQleft50.onoff = true;
        EQleft63.Q = Guete;     EQleft63.boost = 0.00;       EQleft63.f0 = f_count[5];       EQleft63.type = Parametric;       EQleft63.phase = true;       EQleft63.onoff = true;
        EQleft80.Q = Guete;     EQleft80.boost = 0.00;       EQleft80.f0 = f_count[6];       EQleft80.type = Parametric;       EQleft80.phase = true;       EQleft80.onoff = true;
        EQleft100.Q = Guete;    EQleft100.boost = 0.00;      EQleft100.f0 = f_count[7];      EQleft100.type = Parametric;      EQleft100.phase = true;      EQleft100.onoff = true;
        EQleft125.Q = Guete;    EQleft125.boost = 0.00;      EQleft125.f0 = f_count[8];      EQleft125.type = Parametric;      EQleft125.phase = true;      EQleft125.onoff = true;
        EQleft160.Q = Guete;    EQleft160.boost = 0.00;      EQleft160.f0 = f_count[9];      EQleft160.type = Parametric;      EQleft160.phase = true;      EQleft160.onoff = true;
        EQleft200.Q = Guete;    EQleft200.boost = 0.00;      EQleft200.f0 = f_count[10];     EQleft200.type = Parametric;      EQleft200.phase = true;      EQleft200.onoff = true;
        EQleft250.Q = Guete;    EQleft250.boost = 0.00;      EQleft250.f0 = f_count[11];     EQleft250.type = Parametric;      EQleft250.phase = true;      EQleft250.onoff = true;
        EQleft315.Q = Guete;    EQleft315.boost = 0.00;      EQleft315.f0 = f_count[12];     EQleft315.type = Parametric;      EQleft315.phase = true;      EQleft315.onoff = true;
        EQleft400.Q = Guete;    EQleft400.boost = 0.00;      EQleft400.f0 = f_count[13];     EQleft400.type = Parametric;      EQleft400.phase = true;      EQleft400.onoff = true;
        EQleft500.Q = Guete;    EQleft500.boost = 0.00;      EQleft500.f0 = f_count[14];     EQleft500.type = Parametric;      EQleft500.phase = true;      EQleft500.onoff = true;
        EQleft630.Q = Guete;    EQleft630.boost = 0.00;      EQleft630.f0 = f_count[15];     EQleft630.type = Parametric;      EQleft630.phase = true;      EQleft630.onoff = true;
        EQleft800.Q = Guete;    EQleft800.boost = 0.00;      EQleft800.f0 = f_count[16];     EQleft800.type = Parametric;      EQleft800.phase = true;      EQleft800.onoff = true;
        EQleft1000.Q = Guete;   EQleft1000.boost = 0.00;     EQleft1000.f0 = f_count[17];    EQleft1000.type = Parametric;     EQleft1000.phase = true;     EQleft1000.onoff = true;
        EQleft1250.Q = Guete;   EQleft1250.boost = 0.00;     EQleft1250.f0 = f_count[18];    EQleft1250.type = Parametric;     EQleft1250.phase = true;     EQleft1250.onoff = true;
        EQleft1600.Q = Guete;   EQleft1600.boost = 0.00;     EQleft1600.f0 = f_count[19];    EQleft1600.type = Parametric;     EQleft1600.phase = true;     EQleft1600.onoff = true;
        EQleft2000.Q = Guete;   EQleft2000.boost = 0.00;     EQleft2000.f0 = f_count[20];    EQleft2000.type = Parametric;     EQleft2000.phase = true;     EQleft2000.onoff = true;
        EQleft2500.Q = Guete;   EQleft2500.boost = 0.00;     EQleft2500.f0 = f_count[21];    EQleft2500.type = Parametric;     EQleft2500.phase = true;     EQleft2500.onoff = true;
        EQleft3150.Q = Guete;   EQleft3150.boost = 0.00;     EQleft3150.f0 = f_count[22];    EQleft3150.type = Parametric;     EQleft3150.phase = true;     EQleft3150.onoff = true;
        EQleft4000.Q = Guete;   EQleft4000.boost = 0.00;     EQleft4000.f0 = f_count[23];    EQleft4000.type = Parametric;     EQleft4000.phase = true;     EQleft4000.onoff = true;
        EQleft5000.Q = Guete;   EQleft5000.boost = 0.00;     EQleft5000.f0 = f_count[24];    EQleft5000.type = Parametric;     EQleft5000.phase = true;     EQleft5000.onoff = true;
        EQleft6300.Q = Guete;   EQleft6300.boost = 0.00;     EQleft6300.f0 = f_count[25];    EQleft6300.type = Parametric;     EQleft6300.phase = true;     EQleft6300.onoff = true;
        EQleft8000.Q = Guete;   EQleft8000.boost = 0.00;     EQleft8000.f0 = f_count[26];    EQleft8000.type = Parametric;     EQleft8000.phase = true;     EQleft8000.onoff = true;
        EQleft10000.Q = Guete;  EQleft10000.boost = 0.00;    EQleft10000.f0 = f_count[27];   EQleft10000.type = Parametric;    EQleft10000.phase = true;    EQleft10000.onoff = true;
        EQleft12500.Q = Guete;  EQleft12500.boost = 0.00;    EQleft12500.f0 = f_count[28];   EQleft12500.type = Parametric;    EQleft12500.phase = true;    EQleft12500.onoff = true;
        EQleft16000.Q = Guete;  EQleft16000.boost = 0.00;    EQleft16000.f0 = f_count[29];   EQleft16000.type = Parametric;    EQleft16000.phase = true;    EQleft16000.onoff = true;
        EQleft20000.Q = Guete;  EQleft20000.boost = 0.00;    EQleft20000.f0 = f_count[30];   EQleft20000.type = Parametric;    EQleft20000.phase = true;    EQleft20000.onoff = true;

        //Filters for the right side
        EQright20.Q = Guete;     EQright20.boost = 0.00;       EQright20.f0 = f_count[0];       EQright20.type = Parametric;       EQright20.phase = true;       EQright20.onoff = true;
        EQright25.Q = Guete;     EQright25.boost = 0.00;       EQright25.f0 = f_count[1];       EQright25.type = Parametric;       EQright25.phase = true;       EQright25.onoff = true;
        EQright31.Q = Guete;     EQright31.boost = 0.00;       EQright31.f0 = f_count[2];       EQright31.type = Parametric;       EQright31.phase = true;       EQright31.onoff = true;
        EQright40.Q = Guete;     EQright40.boost = 0.00;       EQright40.f0 = f_count[3];       EQright40.type = Parametric;       EQright40.phase = true;       EQright40.onoff = true;
        EQright50.Q = Guete;     EQright50.boost = 0.00;       EQright50.f0 = f_count[4];       EQright50.type = Parametric;       EQright50.phase = true;       EQright50.onoff = true;
        EQright63.Q = Guete;     EQright63.boost = 0.00;       EQright63.f0 = f_count[5];       EQright63.type = Parametric;       EQright63.phase = true;       EQright63.onoff = true;
        EQright80.Q = Guete;     EQright80.boost = 0.00;       EQright80.f0 = f_count[6];       EQright80.type = Parametric;       EQright80.phase = true;       EQright80.onoff = true;
        EQright100.Q = Guete;    EQright100.boost = 0.00;      EQright100.f0 = f_count[7];      EQright100.type = Parametric;      EQright100.phase = true;      EQright100.onoff = true;
        EQright125.Q = Guete;    EQright125.boost = 0.00;      EQright125.f0 = f_count[8];      EQright125.type = Parametric;      EQright125.phase = true;      EQright125.onoff = true;
        EQright160.Q = Guete;    EQright160.boost = 0.00;      EQright160.f0 = f_count[9];      EQright160.type = Parametric;      EQright160.phase = true;      EQright160.onoff = true;
        EQright200.Q = Guete;    EQright200.boost = 0.00;      EQright200.f0 = f_count[10];     EQright200.type = Parametric;      EQright200.phase = true;      EQright200.onoff = true;
        EQright250.Q = Guete;    EQright250.boost = 0.00;      EQright250.f0 = f_count[11];     EQright250.type = Parametric;      EQright250.phase = true;      EQright250.onoff = true;
        EQright315.Q = Guete;    EQright315.boost = 0.00;      EQright315.f0 = f_count[12];     EQright315.type = Parametric;      EQright315.phase = true;      EQright315.onoff = true;
        EQright400.Q = Guete;    EQright400.boost = 0.00;      EQright400.f0 = f_count[13];     EQright400.type = Parametric;      EQright400.phase = true;      EQright400.onoff = true;
        EQright500.Q = Guete;    EQright500.boost = 0.00;      EQright500.f0 = f_count[14];     EQright500.type = Parametric;      EQright500.phase = true;      EQright500.onoff = true;
        EQright630.Q = Guete;    EQright630.boost = 0.00;      EQright630.f0 = f_count[15];     EQright630.type = Parametric;      EQright630.phase = true;      EQright630.onoff = true;
        EQright800.Q = Guete;    EQright800.boost = 0.00;      EQright800.f0 = f_count[16];     EQright800.type = Parametric;      EQright800.phase = true;      EQright800.onoff = true;
        EQright1000.Q = Guete;   EQright1000.boost = 0.00;     EQright1000.f0 = f_count[17];    EQright1000.type = Parametric;     EQright1000.phase = true;     EQright1000.onoff = true;
        EQright1250.Q = Guete;   EQright1250.boost = 0.00;     EQright1250.f0 = f_count[18];    EQright1250.type = Parametric;     EQright1250.phase = true;     EQright1250.onoff = true;
        EQright1600.Q = Guete;   EQright1600.boost = 0.00;     EQright1600.f0 = f_count[19];    EQright1600.type = Parametric;     EQright1600.phase = true;     EQright1600.onoff = true;
        EQright2000.Q = Guete;   EQright2000.boost = 0.00;     EQright2000.f0 = f_count[20];    EQright2000.type = Parametric;     EQright2000.phase = true;     EQright2000.onoff = true;
        EQright2500.Q = Guete;   EQright2500.boost = 0.00;     EQright2500.f0 = f_count[21];    EQright2500.type = Parametric;     EQright2500.phase = true;     EQright2500.onoff = true;
        EQright3150.Q = Guete;   EQright3150.boost = 0.00;     EQright3150.f0 = f_count[22];    EQright3150.type = Parametric;     EQright3150.phase = true;     EQright3150.onoff = true;
        EQright4000.Q = Guete;   EQright4000.boost = 0.00;     EQright4000.f0 = f_count[23];    EQright4000.type = Parametric;     EQright4000.phase = true;     EQright4000.onoff = true;
        EQright5000.Q = Guete;   EQright5000.boost = 0.00;     EQright5000.f0 = f_count[24];    EQright5000.type = Parametric;     EQright5000.phase = true;     EQright5000.onoff = true;
        EQright6300.Q = Guete;   EQright6300.boost = 0.00;     EQright6300.f0 = f_count[25];    EQright6300.type = Parametric;     EQright6300.phase = true;     EQright6300.onoff = true;
        EQright8000.Q = Guete;   EQright8000.boost = 0.00;     EQright8000.f0 = f_count[26];    EQright8000.type = Parametric;     EQright8000.phase = true;     EQright8000.onoff = true;
        EQright10000.Q = Guete;  EQright10000.boost = 0.00;    EQright10000.f0 = f_count[27];   EQright10000.type = Parametric;    EQright10000.phase = true;    EQright10000.onoff = true;
        EQright12500.Q = Guete;  EQright12500.boost = 0.00;    EQright12500.f0 = f_count[28];   EQright12500.type = Parametric;    EQright12500.phase = true;    EQright12500.onoff = true;
        EQright16000.Q = Guete;  EQright16000.boost = 0.00;    EQright16000.f0 = f_count[29];   EQright16000.type = Parametric;    EQright16000.phase = true;    EQright16000.onoff = true;
        EQright20000.Q = Guete;  EQright20000.boost = 0.00;    EQright20000.f0 = f_count[30];   EQright20000.type = Parametric;    EQright20000.phase = true;    EQright20000.onoff = true;

    }
    void saveIteration1InEqBoost()
    {
        EQleft20.boost = saveIteration1[LINKS][0];
        EQleft25.boost = saveIteration1[LINKS][1];
        EQleft31.boost = saveIteration1[LINKS][2];
        EQleft40.boost = saveIteration1[LINKS][3];
        EQleft50.boost = saveIteration1[LINKS][4];
        EQleft63.boost = saveIteration1[LINKS][5];
        EQleft80.boost = saveIteration1[LINKS][6];
        EQleft100.boost = saveIteration1[LINKS][7];
        EQleft125.boost = saveIteration1[LINKS][8];
        EQleft160.boost = saveIteration1[LINKS][9];
        EQleft200.boost = saveIteration1[LINKS][10];
        EQleft250.boost = saveIteration1[LINKS][11];
        EQleft315.boost = saveIteration1[LINKS][12];
        EQleft400.boost = saveIteration1[LINKS][13];
        EQleft500.boost = saveIteration1[LINKS][14];
        EQleft630.boost = saveIteration1[LINKS][15];
        EQleft800.boost = saveIteration1[LINKS][16];
        EQleft1000.boost = saveIteration1[LINKS][17];
        EQleft1250.boost = saveIteration1[LINKS][18];
        EQleft1600.boost = saveIteration1[LINKS][19];
        EQleft2000.boost = saveIteration1[LINKS][20];
        EQleft2500.boost = saveIteration1[LINKS][21];
        EQleft3150.boost = saveIteration1[LINKS][22];
        EQleft4000.boost = saveIteration1[LINKS][23];
        EQleft5000.boost = saveIteration1[LINKS][24];
        EQleft6300.boost = saveIteration1[LINKS][25];
        EQleft8000.boost = saveIteration1[LINKS][26];
        EQleft10000.boost = saveIteration1[LINKS][27];
        EQleft12500.boost = saveIteration1[LINKS][28];
        EQleft16000.boost = saveIteration1[LINKS][29];
        EQleft20000.boost = saveIteration1[LINKS][30];

        EQright20.boost = saveIteration1[RECHTS][0];
        EQright25.boost = saveIteration1[RECHTS][1];
        EQright31.boost = saveIteration1[RECHTS][2];
        EQright40.boost = saveIteration1[RECHTS][3];
        EQright50.boost = saveIteration1[RECHTS][4];
        EQright63.boost = saveIteration1[RECHTS][5];
        EQright80.boost = saveIteration1[RECHTS][6];
        EQright100.boost = saveIteration1[RECHTS][7];
        EQright125.boost = saveIteration1[RECHTS][8];
        EQright160.boost = saveIteration1[RECHTS][9];
        EQright200.boost = saveIteration1[RECHTS][10];
        EQright250.boost = saveIteration1[RECHTS][11];
        EQright315.boost = saveIteration1[RECHTS][12];
        EQright400.boost = saveIteration1[RECHTS][13];
        EQright500.boost = saveIteration1[RECHTS][14];
        EQright630.boost = saveIteration1[RECHTS][15];
        EQright800.boost = saveIteration1[RECHTS][16];
        EQright1000.boost = saveIteration1[RECHTS][17];
        EQright1250.boost = saveIteration1[RECHTS][18];
        EQright1600.boost = saveIteration1[RECHTS][19];
        EQright2000.boost = saveIteration1[RECHTS][20];
        EQright2500.boost = saveIteration1[RECHTS][21];
        EQright3150.boost = saveIteration1[RECHTS][22];
        EQright4000.boost = saveIteration1[RECHTS][23];
        EQright5000.boost = saveIteration1[RECHTS][24];
        EQright6300.boost = saveIteration1[RECHTS][25];
        EQright8000.boost = saveIteration1[RECHTS][26];
        EQright10000.boost = saveIteration1[RECHTS][27];
        EQright12500.boost = saveIteration1[RECHTS][28];
        EQright16000.boost = saveIteration1[RECHTS][29];
        EQright20000.boost = saveIteration1[RECHTS][30];

    }

    void saveIteration2InEqBoost()
    {
        EQleft20.boost = saveIteration2[LINKS][0];
        EQleft25.boost = saveIteration2[LINKS][1];
        EQleft31.boost = saveIteration2[LINKS][2];
        EQleft40.boost = saveIteration2[LINKS][3];
        EQleft50.boost = saveIteration2[LINKS][4];
        EQleft63.boost = saveIteration2[LINKS][5];
        EQleft80.boost = saveIteration2[LINKS][6];
        EQleft100.boost = saveIteration2[LINKS][7];
        EQleft125.boost = saveIteration2[LINKS][8];
        EQleft160.boost = saveIteration2[LINKS][9];
        EQleft200.boost = saveIteration2[LINKS][10];
        EQleft250.boost = saveIteration2[LINKS][11];
        EQleft315.boost = saveIteration2[LINKS][12];
        EQleft400.boost = saveIteration2[LINKS][13];
        EQleft500.boost = saveIteration2[LINKS][14];
        EQleft630.boost = saveIteration2[LINKS][15];
        EQleft800.boost = saveIteration2[LINKS][16];
        EQleft1000.boost = saveIteration2[LINKS][17];
        EQleft1250.boost = saveIteration2[LINKS][18];
        EQleft1600.boost = saveIteration2[LINKS][19];
        EQleft2000.boost = saveIteration2[LINKS][20];
        EQleft2500.boost = saveIteration2[LINKS][21];
        EQleft3150.boost = saveIteration2[LINKS][22];
        EQleft4000.boost = saveIteration2[LINKS][23];
        EQleft5000.boost = saveIteration2[LINKS][24];
        EQleft6300.boost = saveIteration2[LINKS][25];
        EQleft8000.boost = saveIteration2[LINKS][26];
        EQleft10000.boost = saveIteration2[LINKS][27];
        EQleft12500.boost = saveIteration2[LINKS][28];
        EQleft16000.boost = saveIteration2[LINKS][29];
        EQleft20000.boost = saveIteration2[LINKS][30];

        EQright20.boost = saveIteration2[RECHTS][0];
        EQright25.boost = saveIteration2[RECHTS][1];
        EQright31.boost = saveIteration2[RECHTS][2];
        EQright40.boost = saveIteration2[RECHTS][3];
        EQright50.boost = saveIteration2[RECHTS][4];
        EQright63.boost = saveIteration2[RECHTS][5];
        EQright80.boost = saveIteration2[RECHTS][6];
        EQright100.boost = saveIteration2[RECHTS][7];
        EQright125.boost = saveIteration2[RECHTS][8];
        EQright160.boost = saveIteration2[RECHTS][9];
        EQright200.boost = saveIteration2[RECHTS][10];
        EQright250.boost = saveIteration2[RECHTS][11];
        EQright315.boost = saveIteration2[RECHTS][12];
        EQright400.boost = saveIteration2[RECHTS][13];
        EQright500.boost = saveIteration2[RECHTS][14];
        EQright630.boost = saveIteration2[RECHTS][15];
        EQright800.boost = saveIteration2[RECHTS][16];
        EQright1000.boost = saveIteration2[RECHTS][17];
        EQright1250.boost = saveIteration2[RECHTS][18];
        EQright1600.boost = saveIteration2[RECHTS][19];
        EQright2000.boost = saveIteration2[RECHTS][20];
        EQright2500.boost = saveIteration2[RECHTS][21];
        EQright3150.boost = saveIteration2[RECHTS][22];
        EQright4000.boost = saveIteration2[RECHTS][23];
        EQright5000.boost = saveIteration2[RECHTS][24];
        EQright6300.boost = saveIteration2[RECHTS][25];
        EQright8000.boost = saveIteration2[RECHTS][26];
        EQright10000.boost = saveIteration2[RECHTS][27];
        EQright12500.boost = saveIteration2[RECHTS][28];
        EQright16000.boost = saveIteration2[RECHTS][29];
        EQright20000.boost = saveIteration2[RECHTS][30];

    }

    void sendEqDataToBoard()
    {  
       int EQtmpAddressleft = personalizedMidEQ1Addr;
       int EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
       int EQtmpAddressleft2 = personalizedMidEQ1_5Addr;

       int EQtmpAddressright = personalizedMidEQ1_2Addr;
       int EQtmpAddressright1 = personalizedMidEQ1_4Addr;
       int EQtmpAddressright2 = personalizedMidEQ1_6Addr;
        
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft20);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft25);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft31);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft40);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft50);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft63);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft80);            EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft100);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft125);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft160);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft200);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft250);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft315);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft400);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft, &EQleft500);           EQtmpAddressleft += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft630);          EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft800);          EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1250);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft1600);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft2000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft2500);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft3150);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft4000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft5000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft6300);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft8000);         EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft10000);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft12500);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft1, &EQleft16000);        EQtmpAddressleft1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressleft2, &EQleft20000);        EQtmpAddressleft2 += 5;

       
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright20);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright25);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright31);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright40);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright50);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright63);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright80);            EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright100);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright125);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright160);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright200);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright250);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright315);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright400);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright, &EQright500);           EQtmpAddressright += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright630);          EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright800);          EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1250);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright1600);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright2000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright2500);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright3150);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright4000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright5000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright6300);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright8000);         EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright10000);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright12500);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright1, &EQright16000);        EQtmpAddressright1 += 5;
        EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddressright2, &EQright20000);        EQtmpAddressright2 += 5;

    }

        // void sendFilters()
    // {
    //     if (fineState)
    //     {
    //         send31FiltersToDSP();
    //     }
    //     else
    //     {
    //         send9FiltersToDSP();
    //     }    
    // }




    // void send9FiltersToDSP()
    // {
    //     EQtmpAddressleft = personalizedMidEQ1Addr;
    //     EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
        
    //     EQtmpAddressright = personalizedMidEQ1_2Addr;
    //     EQtmpAddressright1 = personalizedMidEQ1_4Addr;
    //     int EQtmpAddress = 0;
    //     for (size_t i = 0; i < 9; i++)
    //     {
    //         if (fineAndCourseFreqMapper[i] < 15)    EQtmpAddress = EQtmpAddressleft;
    //         else                                    EQtmpAddress = EQtmpAddressleft1;     
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsleft[fineAndCourseFreqMapper[i]]);          EQtmpAddress += 5;   
             
    //     }
    //     for (size_t i = 0; i < 9; i++)
    //     {
    //         if (fineAndCourseFreqMapper[i] < 15)    EQtmpAddress = EQtmpAddressright;
    //         else                                    EQtmpAddress = EQtmpAddressright1;     
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsright[fineAndCourseFreqMapper[i]]);          EQtmpAddress += 5;   
             
    //     }
        
    // }
    // void send31FiltersToDSP()
    // {
    //     EQtmpAddressleft = personalizedMidEQ1Addr;
    //     EQtmpAddressleft1 = personalizedMidEQ1_3Addr;
    //     EQtmpAddressleft2 = personalizedMidEQ1_5Addr;

    //     EQtmpAddressright = personalizedMidEQ1_2Addr;
    //     EQtmpAddressright1 = personalizedMidEQ1_4Addr;
    //     EQtmpAddressright2 = personalizedMidEQ1_6Addr;
    //     int EQtmpAddress = 0;
    //     for (size_t i = 0; i < 31; i++)
    //     {
    //         if ( i < 15 )                          EQtmpAddress = EQtmpAddressleft;
    //         else if( i > 15 && i < 30)             EQtmpAddress = EQtmpAddressleft1;
    //         else                                   EQtmpAddress = EQtmpAddressleft2;
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsleft[i]);          EQtmpAddress += 5;    
    //     }
    //     for (size_t i = 0; i < 31; i++)
    //     {
    //         if ( i < 15 )                          EQtmpAddress = EQtmpAddressright;
    //         else if( i > 15 && i < 30)             EQtmpAddress = EQtmpAddressright1;
    //         else                                   EQtmpAddress = EQtmpAddressright2;
    //         EQ2ndOrd(DEVICE_ADDR_7bit, EQtmpAddress, &EQsright[i]);          EQtmpAddress += 5;    
    //     }
    // }
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\EditMenu.ino"

/*
 * EditMenu.ino
 * * Version 0.3.0.2
 */

#include <arduino.h>

//String screenPlatz[7] = {"", "", "", "", "", "", "BACK"};
//String currentUserOptions[4] = {"U1", "L", " ", " "};

String screenPlatz[5] = {"", "", "", "", "BACK"};
String currentUserOptions[5] = {"1 ", "   ", "   ", "   ", "2"};

int levelNumbers[5] = {20, 40, 60, 80, 100};

bool compressorFlag = false;
bool bypassFlag = false;

extern uint8_t user1FLAG;
extern uint8_t user2FLAG;
extern uint8_t user3FLAG;
extern uint8_t linFLAG;
extern uint8_t per1FLAG;
extern uint8_t per2FLAG;
extern uint8_t per3FLAG;
extern uint8_t compFLAG;
extern uint8_t bassFLAG;
extern bool int20FLAG ;
extern bool int40FLAG ;
extern bool int60FLAG ;
extern bool int80FLAG ;
extern bool int100FLAG ;


int editMenuScroler = 0;
int xx, yy;

void updatePlatz(void)
{
  if (user1FLAG == true)
  {
    currentUserOptions[0] = "1 ";
  }
  else if (user2FLAG == true)
  {
    currentUserOptions[0] = "2 ";
  }
  else if (user3FLAG == true)
  {
    currentUserOptions[0] = "3 ";
  }
  else
  {
    currentUserOptions[0] = "  ";
  }

  if (linFLAG == true)
  {
    currentUserOptions[1] = " L  ";
  }
  else if (per1FLAG == true)
  {
    currentUserOptions[1] = " P1 ";
  }
  else if (per2FLAG == true)
  {
    currentUserOptions[1] = " P2 ";
  }
  else
  {
    currentUserOptions[1] = "    ";
  }

  if (Ble_CONTROL_MODE_FLAG == true)
  {
    currentUserOptions[2] = " BLE";
  }
  else
  {
    currentUserOptions[2] = "     ";
  }

  if (MUTE_Flag == true)
  {
    currentUserOptions[3] = " M ";
  }
  else
  {
    currentUserOptions[3] = "    ";
  }

  if (int20FLAG == true)
  {
    currentUserOptions[4] = "20";
  }
  else if (int40FLAG == true)
  {
    currentUserOptions[4] = "40";
  }
  else if (int60FLAG == true)
  {
    currentUserOptions[4] = "60";
  }
  else if ( int80FLAG == true)
  {
    currentUserOptions[4] = "80";
  }
  else if (int100FLAG == true)
  {
    currentUserOptions[4] = "100";
  }
  else
  {
    currentUserOptions[4] = "   ";
  }
}

void LCDprintFLAG(void)
{
  //lcdDevice.setCursor(0, 0);
  //lcdDevice.print(Platz1 + Platz2 + Platz3 + Platz4);
}

int getEditMenuScroller()
{
  return editMenuScroler;
}

void changeUser(void)
{
  if (user1FLAG == true)
  {
    currentUserOptions[0] = "USR 1";
  }
  else if (user2FLAG == true)
  {
    currentUserOptions[0] = "USR 2";
  }
  else if (user3FLAG == true)
  {
    currentUserOptions[0] = "USR 3";
  }
  else
  {
    currentUserOptions[0] = "     ";
  }
}

void editMenuScrolTo(int selectedIndex)
{
  Serial.println("editMenuScrolTo");
  Serial.println(selectedIndex);
  editMenuScroler = selectedIndex;

  switch (selectedIndex)
  {
  case 0:
    if (linFLAG == true)
      screenPlatz[0] = "[L*] ";
    else
      screenPlatz[0] = "[L] ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";
    //  if (levelFLAG == true)screenPlatz[3] = "level ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 1:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = "[P1*] ";
    else
      screenPlatz[1] = "[P1] ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";

    //  if (levelFLAG == true)screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 2:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = "[P2*] ";
    else
      screenPlatz[2] = "[P2] ";
    screenPlatz[3] = " LEVEL";

    // if (levelFLAG == true) screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = " BACK ";
    break;

  case 3:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = "[LEVEL]";

    //if (levelFLAG == true) screenPlatz[3] = "[level*]";
    // else                  screenPlatz[3] = "[level] ";
    screenPlatz[4] = " BACK ";
    break;

  case 4:
    if (linFLAG == true)
      screenPlatz[0] = " L* ";
    else
      screenPlatz[0] = " L ";
    if (per1FLAG == true)
      screenPlatz[1] = " P1* ";
    else
      screenPlatz[1] = " P1 ";
    if (per2FLAG == true)
      screenPlatz[2] = " P2* ";
    else
      screenPlatz[2] = " P2 ";
    screenPlatz[3] = " LEVEL";
    // if (levelFLAG == true) screenPlatz[3] = "level* ";
    // else                  screenPlatz[3] = "level ";
    screenPlatz[4] = "[BACK]";
    break;
  }
}

int MesseMenuExcute(void)
{
  Serial.print ("MesseMenuExcute :"); Serial.println(editMenuScroler);
  switch (editMenuScroler)
  {
  case 0:
    //L
    Serial.println("liner handler");
    //linear
    linFLAG = true;
    per1FLAG = false;
    per2FLAG = false;
    muteVoice(true);
    PersonalizedMultibandCompressorBypassCompOFF();
    MainDynamicBassBypassOFF();
    MainDirectMode();
    delay(10);
    muteVoice(false);
    return 0;
    break;

  case 1:
    //P1
    //personalisiert Kurve 1
    linFLAG = false;
    per1FLAG = true;
    per2FLAG = false;

    preset = 1;
    muteVoice(true);
    MainPersonalizedMode();
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    PersonalizedMultibandCompressorBypassCompOFF();
    delay(10);
    muteVoice(false);
    Serial.println("PLUS Agorithmus");
    return 1;
    break;

  case 2:
    //P2
    //personalisiert Kurve 2
    // EqConfigurations();
    linFLAG = false;
    per1FLAG = false;
    per2FLAG = true;

    preset = 2;
    muteVoice(true);
    MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum) / 20.0));
    PersonalizedMultibandCompressorBypassCompOFF();
    MainPersonalizedMode();
    delay(10);
    muteVoice(false);
    Serial.println("LIFESTYLE Algorithmus");
    return 2;
    break;

  case 3:
    levelHandler();
    return 3;
    break;

  case 4:
    //BACK
    Serial.println("BACK handler");
    return 4;
    break;
  }
}

void levelHandler()
{
  activatUserInputMode(0, 5, selectNumber, showNumbers, 1);
  showNumbers(0); 
}
void selectNumber(int16_t *sel, uint8_t length)
{
  Serial.println(" change The Intinsity ");
  Serial.print("*sel :") ; Serial.println( *sel ) ;
  IntensityChoice = *sel; 
  setIntFlags( IntensityChoice ); 
  updatePlatz();
  muteVoice(true);
  MainPersonalizedMode();
  delay(10);
  muteVoice(false);
}


void showNumbers(int16_t sel)
{
  showSelectedLevel(" LEVEL ", String(levelNumbers[sel]));
  Serial.println(levelNumbers[sel]);
}

void setIntFlags(uint8_t i )
{
  switch (i)
   {
   case 0:
      int20FLAG = true;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = false;
     break;
   
   case 1:
      int20FLAG = false;
      int40FLAG = true;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = false;
     
     break;
   case 2:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = true;
      int80FLAG = false;
      int100FLAG = false;
     
     break;

   case 3:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = true;
      int100FLAG = false;
     
     break;

   case 4:
      int20FLAG = false;
      int40FLAG = false;
      int60FLAG = false;
      int80FLAG = false;
      int100FLAG = true;
     
     break;
  }
}
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\SerialControl.ino"
/*
*   Eingabe von Steuerbefehlen zur Kontrolle des ProTon plus über seriellen Monitor
*
*/

#include "Wire.h"

//int incomingByte;
extern double dBVoiceVolum;

void ReadSerialInput(void)
{
  int i;
  int eingabe[7];
  while (Serial.available() > 0)
  {
    i++;
  }
  if (eingabe[0] == '*' && eingabe[3] == '+' && eingabe[6] == '#')
  {
    if (eingabe[0] == '0' && eingabe[1] == '1')
    {
      dBVoiceVolum = -(eingabe[4]);
      printOnTerminal(" Master Vol. : \n" + String(dBVoiceVolum) + "dB");
      oledPrintNormalMode(dBVoiceVolum, currentUserOptions);
      //updateNotificationBar(currentUserOptions);
      VolumeStereoConvertANDsend(MasterVolumeAddr, dBVoiceVolum);
    }
  }
}
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\Sprachen.ino"
/*
 * Sprachen.ino
 * Version 0.3.0.2
 */
#include <Arduino.h>

char En_menuItem_1  []= "user profile";
char En_menuItem_1_1  []= "load profile";
char En_menuItem_1_1_1  []= "memory 1";
char En_menuItem_1_1_2  []= "memory 2";
char En_menuItem_1_1_3  []= "memory 3";
char En_menuItem_1_2  []= "new profile";

char En_menuItem_1_3  []= "delete profile";
char En_menuItem_1_3_1  []= "memory 1";
char En_menuItem_1_3_2  []= "memory 2";
char En_menuItem_1_3_3  []= "memory 3";

char En_menuItem_2  []= "operating mode";
char En_menuItem_2_1  []= "linear";
char En_menuItem_2_2  []= "personalized";
char En_menuItem_2_2_1  []= "hearing profile 1";
char En_menuItem_2_2_2  []= "hearing profile 2";
char En_menuItem_2_2_3  []= "hearing profile 3";

char En_menuItem_3  []= "sound settings";
char En_menuItem_3_1  []= "audio presets";
char En_menuItem_3_1_1  []= "EQ";
char En_menuItem_3_1_1_1  []= "Jazz";
char En_menuItem_3_1_1_2  []= "Rock";
char En_menuItem_3_1_1_3  []= "Flat";
char En_menuItem_3_1_2  []= "dynamic bass";
char En_menuItem_3_1_2_1  []= "on";
char En_menuItem_3_1_2_2  []= "off";
char En_menuItem_3_2  []= "compressor";

char En_menuItem_4  []= "hearing test";
char En_menuItem_4_1  []= "start hearing test";
char En_menuItem_4_2  []= "noise generator";
char En_menuItem_4_2_1  []= "on";
char En_menuItem_4_2_2  []= "off";
char En_menuItem_4_2_3  []= "automatic";
char En_menuItem_4_3  []= "start Octav test";
char En_menuItem_4_4  []= "start Terz test";

char En_menuItem_5  []= "settings";
char En_menuItem_5_1  []= "general";
char En_menuItem_5_1_1  []= "hearing test";
char En_menuItem_5_1_1_1  []= "fast";
char En_menuItem_5_1_1_2  []= "according to standard";
char En_menuItem_5_1_1_3  []= "exactly";
char En_menuItem_5_1_2  []= "max. volume";
char En_menuItem_5_1_2_1  []= "volume max. in dB";
char En_menuItem_5_1_3  []= "initial volume";
char En_menuItem_5_1_3_1  []= "minimal";
char En_menuItem_5_1_3_2  []= "default";
char En_menuItem_5_1_3_3  []= "since last start";
char En_menuItem_5_2  []= "expert mode";
char En_menuItem_5_2_1  []= "step size";
char En_menuItem_5_2_1_1  []= "step in dB";
char En_menuItem_5_2_2  []= "dynamic bass";
char En_menuItem_5_2_2_1  []= "on";
char En_menuItem_5_2_2_2  []= "off";
char En_menuItem_5_2_3  []= "noise generator";
char En_menuItem_5_2_3_1  []= "on";
char En_menuItem_5_2_3_2  []= "off";
char En_menuItem_5_2_4  []= "language";
char En_menuItem_5_2_4_1  []= "german";
char En_menuItem_5_2_4_2  []= "english";
char En_menuItem_5_2_5  []= "information";
char En_menuItem_5_2_5_1  []= "version";
char En_menuItem_5_2_5_2  []= "serial number";
char En_menuItem_5_2_5_3  []= "support";
char En_menuItem_6 []= "HearingTest start";
char En_menuItem_7 [] = "MultiUser Input";
char En_menuItem_8 [] = "SingleUser Input";
char En_menuItem_9 [] = "Intensity";
char En_menuItem_9_1[] = "20%";
char En_menuItem_9_2[] = "40%";
char En_menuItem_9_3[] = "60%";
char En_menuItem_9_4[] = "80%";
char En_menuItem_9_5[] = "100%";
char En_menuItem_10 []= "Soundselect";
char En_menuItem_10_1 [] = "Sound 1";
char En_menuItem_10_2 [] = "Sound 2";
char En_menuItem_10_3 [] = "Sound 3";
char En_menuItem_10_4 [] = "Sound 4";

///////////////////////////////////////////////////////
char De_menuItem_1 []=  "Benutzerprofil";
char De_menuItem_1_1 []=  "Profil laden";
char De_menuItem_1_1_1 []=  "Speicher 1";
char De_menuItem_1_1_2 []=  "Speicher 2";
char De_menuItem_1_1_3 []=  "Speicher 3";
char De_menuItem_1_2 []=  "Neues Profil";

char De_menuItem_1_3 []=  "Profil loeschen";
char De_menuItem_1_3_1 []=  "Speicher 1";
char De_menuItem_1_3_2 []=  "Speicher 2";
char De_menuItem_1_3_3 []=  "Speicher 3";

char De_menuItem_2 []=  "Betriebsmodi";
char De_menuItem_2_1 []=  "direkt";
char De_menuItem_2_2 []=  "Personalisiert";
char De_menuItem_2_2_1 []=  "Profil 1";
char De_menuItem_2_2_2 []=  "Profil 2";
char De_menuItem_2_2_3 []=  "Profil 3";

char De_menuItem_3 []=  "Audio";
char De_menuItem_3_1 []=  "Audio Presets";
char De_menuItem_3_1_1 []=  "EQ";
char De_menuItem_3_1_1_1 []=  "Jazz";
char De_menuItem_3_1_1_2 []=  "Rock";
char De_menuItem_3_1_1_3 []=  "Flat";
char De_menuItem_3_1_2 []=  "Dyn. Bass Boost";
char De_menuItem_3_1_2_1 []=  "An";
char De_menuItem_3_1_2_2 []=  "Aus";
char De_menuItem_3_2 []=  "Kompressor";

char De_menuItem_4 []=  "Hoertest";
char De_menuItem_4_1 []=  "Auto Messung st";
char De_menuItem_4_2 []=  "Rauschgenerator";
char De_menuItem_4_2_1 []=  "An";
char De_menuItem_4_2_2 []=  "Aus";
char De_menuItem_4_2_3 []=  "Automatik";
char De_menuItem_4_3 []=  "Manu Octav st";
char De_menuItem_4_4 []=  "Manu Terz st";

char De_menuItem_5 []=  "Einstellungen";
char De_menuItem_5_1 []=  "Generell";
char De_menuItem_5_1_1 []=  "Hoertest";
char De_menuItem_5_1_1_1 []=  "schnell";
char De_menuItem_5_1_1_2 []=  "nach DIN Norm";
char De_menuItem_5_1_1_3 []=  "genau";
char De_menuItem_5_1_2 []=  "max. Volume";
char De_menuItem_5_1_2_1 []=  "Volume max. in dB";
char De_menuItem_5_1_3 []=  "Init Volume";
char De_menuItem_5_1_3_1 []=  "minimal";
char De_menuItem_5_1_3_2 []=  "Standard";
char De_menuItem_5_1_3_3 []=  "nach letztem Start";
char De_menuItem_5_2 []=  "Expertenmodus";
char De_menuItem_5_2_1 []=  "Schrittweite";
char De_menuItem_5_2_1_1 []=  "Schrittweite in dB";
char De_menuItem_5_2_2 []=  "Dyn. Bass Boost";
char De_menuItem_5_2_2_1 []=  "An";
char De_menuItem_5_2_2_2 []=  "Aus";
char De_menuItem_5_2_3 []=  "Rauschgenerator";
char De_menuItem_5_2_3_1 []=  "An";
char De_menuItem_5_2_3_2 []=  "Aus";
char De_menuItem_5_2_4 []=  "Sprache";
char De_menuItem_5_2_4_1 []=  "Deutsch";
char De_menuItem_5_2_4_2 []=  "Englisch";
char De_menuItem_5_2_5 []=  "Information";
char De_menuItem_5_2_5_1 []=  "Versionsnummer";
char De_menuItem_5_2_5_2 []=  "Seriennummer";
char De_menuItem_5_2_5_3 []=  "Support";
char De_menuItem_6 [] = "Messung starten";
char De_menuItem_7 [] = "MultiUser Input";
char De_menuItem_8 [] = "SingleUser Input";
char De_menuItem_9 [] = "Intensitaet";
char De_menuItem_9_1[] = "20%";
char De_menuItem_9_2[] = "40%";
char De_menuItem_9_3[] = "60%";
char De_menuItem_9_4[] = "80%";
char De_menuItem_9_5[] = "100%";
char De_menuItem_10 []= "Soundwahl";
char De_menuItem_10_1 [] = "Sound 1";
char De_menuItem_10_2 [] = "Sound 2";
char De_menuItem_10_3 [] = "Sound 3";
char De_menuItem_10_4 [] = "Sound 4";


char menuItem_1[] = "Benutzer";
char menuItem_1_1 [] = "Profil laden";
char menuItem_1_1_1 [] = "Speicher 1";
char menuItem_1_1_2 [] = "Speicher 2";
char menuItem_1_1_3 [] = "Speicher 3";
char menuItem_1_2 [] = "Neues Profil";

char menuItem_1_3 [] = "Profil loeschen";
char menuItem_1_3_1 [] = "Speicher 1";
char menuItem_1_3_2 [] = "Speicher 2";
char menuItem_1_3_3 [] = "Speicher 3";

char menuItem_2 [] = "Sound";
char menuItem_2_1 [] = "direkt";
char menuItem_2_2 [] = "Personalisiert";
char menuItem_2_2_1 [] = "Profil 1";
char menuItem_2_2_2 [] = "Profil 2";
char menuItem_2_2_3 [] = "Profil 3";

char menuItem_3 [] = "Audio";
char menuItem_3_1 [] = "Audio Presets";
char menuItem_3_1_1 [] = "EQ";
char menuItem_3_1_1_1 [] = "Jazz";
char menuItem_3_1_1_2 [] = "Rock";
char menuItem_3_1_1_3 [] = "Flat";
char menuItem_3_1_2 [] = "Dyn. Bass Boost";
char menuItem_3_1_2_1 [] = "An";
char menuItem_3_1_2_2 [] = "Aus";
char menuItem_3_2 [] = "Kompressor";
char menuItem_3_2_1 [] = "An";
char menuItem_3_2_2 [] = "Aus";

char menuItem_4 [] = "Hoertest";
char menuItem_4_1 [] = "Auto Mess st";
char menuItem_4_2 [] = "Rauschgen.";
char menuItem_4_2_1 [] = "An";
char menuItem_4_2_2 [] = "Aus";
char menuItem_4_2_3 [] = "Automatik";
char menuItem_4_3 [] = "Manu Octav st";
char menuItem_4_4 [] = "Manu Terz st";

char menuItem_5 [] = "Einstellungen";
char menuItem_5_1 [] = "Generell";
char menuItem_5_1_1 [] = "Hoertest";
char menuItem_5_1_1_1 [] = "schnell";
char menuItem_5_1_1_2 [] = "nach DIN Norm";
char menuItem_5_1_1_3 [] = "genau";
char menuItem_5_1_2 [] = "max. Vol.";
char menuItem_5_1_2_1 [] = "Vol. max. dB";
char menuItem_5_1_3 [] = "Init Vol.";
char menuItem_5_1_3_1 [] = "minimal";
char menuItem_5_1_3_2 [] = "Standard";
char menuItem_5_1_3_3 [] = "NLS";  // nach letztem Start
char menuItem_5_2 [] = "Expertenmodus";
char menuItem_5_2_1 [] = "Step Messung";
char menuItem_5_2_1_1 [] = "Step in dB";
char menuItem_5_2_2 [] = "Dyn. Bass Boost";
char menuItem_5_2_2_1 [] = "An";
char menuItem_5_2_2_2 [] = "Aus";
char menuItem_5_2_3 [] = "Rauschgen.";
char menuItem_5_2_3_1 [] = "An";
char menuItem_5_2_3_2 [] = "Aus";
char menuItem_5_2_4 [] = "Sprache";
char menuItem_5_2_4_1 [] = "Deutsch";
char menuItem_5_2_4_2 [] = "English";
char menuItem_5_2_5 [] = "Information";
char menuItem_5_2_5_1 [] = "Versionsnummer";
char menuItem_5_2_5_2 [] = "Seriennummer";
char menuItem_5_2_5_3 [] = "Support";
char menuItem_6 [] = "Hoertest start";
char menuItem_7 [] = "MultiUser Input";
char menuItem_8 [] = "SingleUser Input";
char menuItem_9 [] = "Intensitaet";
char menuItem_9_1[] = "20%";
char menuItem_9_2[] = "40%";
char menuItem_9_3[] = "60%";
char menuItem_9_4[] = "80%";
char menuItem_9_5[] = "100%";
char menuItem_10 []= "SoundSelect";
char menuItem_10_1 [] = "Sound 1";
char menuItem_10_2 [] = "Sound 2";
char menuItem_10_3 [] = "Sound 3";
char menuItem_10_4 [] = "Sound 4";




void LanguageSelect()
{
    switch (SprachAuswahl)
    {
        case 0: 
        default:
            peakDeutsch();
        break;
        case 1:
            peakEnglish();
        break;
    }
}

void peakDeutsch()
{
    Serial.println("peakDeutsch start..");
    //TODO: 
    strcpy(menuItem_1 , De_menuItem_1) ;
    strcpy(menuItem_1_1 , De_menuItem_1_1 );
    strcpy(menuItem_1_1_1 , De_menuItem_1_1_1 );
    strcpy(menuItem_1_1_2 , De_menuItem_1_1_2 );
    strcpy(menuItem_1_1_3 , De_menuItem_1_1_3 );
    strcpy(menuItem_1_2 , De_menuItem_1_2 );

    strcpy(menuItem_1_3 , De_menuItem_1_3 );
    strcpy(menuItem_1_3_1 , De_menuItem_1_3_1 );
    strcpy(menuItem_1_3_2 , De_menuItem_1_3_2 );
    strcpy(menuItem_1_3_3 , De_menuItem_1_3_3 );

    strcpy(menuItem_2 , De_menuItem_2 );
    strcpy(menuItem_2_1 , De_menuItem_2_1 );
    strcpy(menuItem_2_2 , De_menuItem_2_2 );
    strcpy(menuItem_2_2_1 , De_menuItem_2_2_1 );
    strcpy(menuItem_2_2_2 , De_menuItem_2_2_2 );
    strcpy(menuItem_2_2_3 , De_menuItem_2_2_3 );

    strcpy(menuItem_3 , De_menuItem_3 );
    strcpy(menuItem_3_1 , De_menuItem_3_1 );
    strcpy(menuItem_3_1_1 , De_menuItem_3_1_1 );
    strcpy(menuItem_3_1_1_1 , De_menuItem_3_1_1_1 );
    strcpy(menuItem_3_1_1_2 , De_menuItem_3_1_1_2 );
    strcpy(menuItem_3_1_1_3 , De_menuItem_3_1_1_3 );
    strcpy(menuItem_3_1_2 , De_menuItem_3_1_2 );
    strcpy(menuItem_3_1_2_1 , De_menuItem_3_1_2_1 );
    strcpy(menuItem_3_1_2_2 , De_menuItem_3_1_2_2 );
    strcpy(menuItem_3_2 , De_menuItem_3_2 );
    strcpy(menuItem_3_2_1 , " " );
    strcpy(menuItem_3_2_2 , " " );

    strcpy(menuItem_4 , De_menuItem_4 );
    strcpy(menuItem_4_1 , De_menuItem_4_1 );
    strcpy(menuItem_4_2 , De_menuItem_4_2 );
    strcpy(menuItem_4_2_1 , De_menuItem_4_2_1 );
    strcpy(menuItem_4_2_2 , De_menuItem_4_2_2 );
    strcpy(menuItem_4_2_3 , De_menuItem_4_2_3 );
    strcpy(menuItem_4_3 , De_menuItem_4_3 );
    strcpy(menuItem_4_4 , De_menuItem_4_4 );
    
    strcpy(menuItem_5 , De_menuItem_5 );
    strcpy(menuItem_5_1 , De_menuItem_5_1 );
    strcpy(menuItem_5_1_1 , De_menuItem_5_1_1 );
    strcpy(menuItem_5_1_1_1 , De_menuItem_5_1_1_1 );
    strcpy(menuItem_5_1_1_2 , De_menuItem_5_1_1_2 );
    strcpy(menuItem_5_1_1_3 , De_menuItem_5_1_1_3 );
    strcpy(menuItem_5_1_2 , De_menuItem_5_1_2 );
    strcpy(menuItem_5_1_2_1 , De_menuItem_5_1_2_1 );
    strcpy(menuItem_5_1_3 , De_menuItem_5_1_3 );
    strcpy(menuItem_5_1_3_1 , De_menuItem_5_1_3_1 );
    strcpy(menuItem_5_1_3_2 , De_menuItem_5_1_3_2 );
    strcpy(menuItem_5_1_3_3 , De_menuItem_5_1_3_3 );
    strcpy(menuItem_5_2 , De_menuItem_5_2 );
    strcpy(menuItem_5_2_1 , De_menuItem_5_2_1 );
    strcpy(menuItem_5_2_1_1 , De_menuItem_5_2_2_1);
    strcpy(menuItem_5_2_2 , De_menuItem_5_2_2 );
    strcpy(menuItem_5_2_2_1 , De_menuItem_5_2_2_1 );
    strcpy(menuItem_5_2_2_2 , De_menuItem_5_2_2_2 );
    strcpy(menuItem_5_2_3 , De_menuItem_5_2_3 );
    strcpy(menuItem_5_2_3_1 , De_menuItem_5_2_3_1 );
    strcpy(menuItem_5_2_3_2 , De_menuItem_5_2_3_2 );
    strcpy(menuItem_5_2_4 , De_menuItem_5_2_4 );
    strcpy(menuItem_5_2_4_1 , De_menuItem_5_2_4_1 );
    strcpy(menuItem_5_2_4_2 , De_menuItem_5_2_4_2 );
    strcpy(menuItem_5_2_5 , De_menuItem_5_2_5_1 );
    strcpy(menuItem_5_2_5_1 , De_menuItem_5_2_5_1 );
    strcpy(menuItem_5_2_5_2 , De_menuItem_5_2_5_2 );
    strcpy(menuItem_5_2_5_3 , De_menuItem_5_2_5_3 );
    strcpy(menuItem_6 , De_menuItem_6);
    strcpy(menuItem_7 , De_menuItem_7);
    strcpy(menuItem_8 , De_menuItem_8);
    strcpy(menuItem_9 , De_menuItem_9);
    strcpy(menuItem_9_1 , De_menuItem_9_1);
    strcpy(menuItem_9_2 , De_menuItem_9_2);
    strcpy(menuItem_9_3 , De_menuItem_9_3);
    strcpy(menuItem_9_4 , De_menuItem_9_4);
    strcpy(menuItem_9_5 , De_menuItem_9_5);
    strcpy(menuItem_10 , De_menuItem_10);
    strcpy(menuItem_10_1 , De_menuItem_10_1);
    strcpy(menuItem_10_2 , De_menuItem_10_2);
    strcpy(menuItem_10_3 , De_menuItem_10_3);
    strcpy(menuItem_10_4 , De_menuItem_10_4);

    
    Serial.println("peakDeutsch end..");
}


void peakEnglish()
{
    Serial.println("peakEnglish start..");
    //TODO:

strcpy(menuItem_1 , En_menuItem_1) ;
strcpy(menuItem_1_1 , En_menuItem_1_1 );
strcpy(menuItem_1_1_1 , En_menuItem_1_1_1 );
strcpy(menuItem_1_1_2 , En_menuItem_1_1_2 );
strcpy(menuItem_1_1_3 , En_menuItem_1_1_3 );
strcpy(menuItem_1_2 , En_menuItem_1_2 );

strcpy(menuItem_1_3 , En_menuItem_1_3 );
strcpy(menuItem_1_3_1 , En_menuItem_1_3_1 );
strcpy(menuItem_1_3_2 , En_menuItem_1_3_2 );
strcpy(menuItem_1_3_3 , En_menuItem_1_3_3 );

strcpy(menuItem_2 , En_menuItem_2 );
strcpy(menuItem_2_1 , En_menuItem_2_1 );
strcpy(menuItem_2_2 , En_menuItem_2_2 );
strcpy(menuItem_2_2_1 , En_menuItem_2_2_1 );
strcpy(menuItem_2_2_2 , En_menuItem_2_2_2 );
strcpy(menuItem_2_2_3 , En_menuItem_2_2_3 );

strcpy(menuItem_3 , En_menuItem_3 );
strcpy(menuItem_3_1 , En_menuItem_3_1 );
strcpy(menuItem_3_1_1 , En_menuItem_3_1_1 );
strcpy(menuItem_3_1_1_1 , En_menuItem_3_1_1_1 );
strcpy(menuItem_3_1_1_2 , En_menuItem_3_1_1_2 );
strcpy(menuItem_3_1_1_3 , En_menuItem_3_1_1_3 );
strcpy(menuItem_3_1_2 , En_menuItem_3_1_2 );
strcpy(menuItem_3_1_2_1 , En_menuItem_3_1_2_1 );
strcpy(menuItem_3_1_2_2 , En_menuItem_3_1_2_2 );
strcpy(menuItem_3_2 , En_menuItem_3_2 );
strcpy(menuItem_3_2_1 , " " );
strcpy(menuItem_3_2_2 , " " );

strcpy(menuItem_4 , En_menuItem_4 );
strcpy(menuItem_4_1 , En_menuItem_4_1 );
strcpy(menuItem_4_2 , En_menuItem_4_2 );
strcpy(menuItem_4_2_1 , En_menuItem_4_2_1 );
strcpy(menuItem_4_2_2 , En_menuItem_4_2_2 );
strcpy(menuItem_4_2_3 , En_menuItem_4_2_3 );
strcpy(menuItem_4_3 , En_menuItem_4_3 );
strcpy(menuItem_4_4 , En_menuItem_4_4 );

strcpy(menuItem_5 , En_menuItem_5 );
strcpy(menuItem_5_1 , En_menuItem_5_1 );
strcpy(menuItem_5_1_1 , En_menuItem_5_1_1 );
strcpy(menuItem_5_1_1_1 , En_menuItem_5_1_1_1 );
strcpy(menuItem_5_1_1_2 , En_menuItem_5_1_1_2 );
strcpy(menuItem_5_1_1_3 , En_menuItem_5_1_1_3 );
strcpy(menuItem_5_1_2 , En_menuItem_5_1_2 );
strcpy(menuItem_5_1_2_1 , En_menuItem_5_1_2_1 );
strcpy(menuItem_5_1_3 , En_menuItem_5_1_3 );
strcpy(menuItem_5_1_3_1 , En_menuItem_5_1_3_1 );
strcpy(menuItem_5_1_3_2 , En_menuItem_5_1_3_2 );
strcpy(menuItem_5_1_3_3 , En_menuItem_5_1_3_3 );
strcpy(menuItem_5_2 , En_menuItem_5_2 );
strcpy(menuItem_5_2_1 , En_menuItem_5_2_1 );
strcpy(menuItem_5_2_1_1 , En_menuItem_5_2_2_1);
strcpy(menuItem_5_2_2 , En_menuItem_5_2_2 );
strcpy(menuItem_5_2_2_1 , En_menuItem_5_2_2_1 );
strcpy(menuItem_5_2_2_2 , En_menuItem_5_2_2_2 );
strcpy(menuItem_5_2_3 , En_menuItem_5_2_3 );
strcpy(menuItem_5_2_3_1 , En_menuItem_5_2_3_1 );
strcpy(menuItem_5_2_3_2 , En_menuItem_5_2_3_2 );
strcpy(menuItem_5_2_4 , En_menuItem_5_2_4 );
strcpy(menuItem_5_2_4_1 , En_menuItem_5_2_4_1 );
strcpy(menuItem_5_2_4_2 , En_menuItem_5_2_4_2 );
strcpy(menuItem_5_2_5 , En_menuItem_5_2_5_1 );
strcpy(menuItem_5_2_5_1 , En_menuItem_5_2_5_1 );
strcpy(menuItem_5_2_5_2 , En_menuItem_5_2_5_2 );
strcpy(menuItem_5_2_5_3 , En_menuItem_5_2_5_3 );
strcpy(menuItem_6 , En_menuItem_6);
strcpy(menuItem_7 , En_menuItem_7);
strcpy(menuItem_8 , En_menuItem_8);
 strcpy(menuItem_9 , De_menuItem_9);
    strcpy(menuItem_9_1 , En_menuItem_9_1);
    strcpy(menuItem_9_2 , En_menuItem_9_2);
    strcpy(menuItem_9_3 , En_menuItem_9_3);
    strcpy(menuItem_9_4 , En_menuItem_9_4);
    strcpy(menuItem_9_5 , En_menuItem_9_5);
        strcpy(menuItem_10 , En_menuItem_10);
    strcpy(menuItem_10_1 , En_menuItem_10_1);
    strcpy(menuItem_10_2 , En_menuItem_10_2);
    strcpy(menuItem_10_3 , En_menuItem_10_3);
    strcpy(menuItem_10_4 , En_menuItem_10_4);

Serial.println("peakEnglish end..");
}

#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\bluethooth.ino"
/*
 *    bluetooth.ino
 */

int tx = 1;        // Pin für transmit serial
int rx = 0;        // Pin für receive serial
char inSerial[15]; // Charakterarray für Keywords

uint8_t TackBool = false;



void linear(void);                //menuItem_2_1
void activateCurve_1(void);       //menuItem_2_2_1
void activateCurve_2(void);       //menuItem_2_2_2
void activateCurve_3(void);       //menuItem_2_2_3
void Menu_3_1_2_1_Handler(void);      //BASS ON
void Menu_3_1_2_2_Handler(void);      //BASS OFF
void compressorOn(void);  //menuItem_3_2_1
void compressorOff(void); //menuItem_3_2_2
//void hearingTest();                // Hörtest starten
void startHearingTest(void);
//void Menu_4_1_Handler(void);

void initBluetooth(void)
{
    pinMode(tx, OUTPUT);
    pinMode(rx, OUTPUT); // eigentlich ist das ein INPUT, aber es wird zunächst nur unidirektional gesendet.
}

void bluetoothREAD(void)
{
    int i = 0;
    int m = 0;
    delay(500);
    if (Serial.available() > 0)
    {
        while (Serial.available() > 0)
        {
            inSerial[i] = Serial.read();
            i++;
        }
        inSerial[i] = '\0';
        Check_Protocol(inSerial);
    }
}

void Check_Protocol(char inStr[])
{
  int i = 0;
  int m = 0;
  Serial.println(inStr);

  if(!strcmp(inStr,"linear"))   //Linear Mode
  {
    //allpinslow();
    // ToDo
    linear();

    Serial.println("Linear Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }
  
  if(!strcmp(inStr,"P1"))   //Personalisiert 1 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_1();
    Serial.println("P1 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"P2"))   //Personalisiert 2 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_2();
    Serial.println("P2 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"P3"))   //Personalisiert 3 Mode
  {
    //allpinslow();
    // ToDo
    activateCurve_3();
    Serial.println("P3 Mode");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"BassON"))   //Bass Boost ON
  {
    //allpinslow();
    // ToDo
    Menu_3_1_2_1_Handler();
    Serial.println("Bass Boost ON");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"BassOFF"))   //Bass Boost OFF
  {
    //allpinslow();
    // ToDo
    Menu_3_1_2_2_Handler();
    Serial.println("Bass Boost OFF");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"CompON"))   //Compressor ON
  {
    //allpinslow();
    // ToDo
    compressorOn();
    Serial.println("Compressor ON");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

if(!strcmp(inStr,"CompOFF"))   //Compressor OFF
  {
    //allpinslow();
    // ToDo
    compressorOff();
    Serial.println("Compressor OFF");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"TestStart"))   //Hörtest starten
  {
    //allpinslow();
    // ToDo
    //hearingTest();
    startHearingTest();
    Serial.println("Hoertest start");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  if(!strcmp(inStr,"Tack"))   //Test Acknowledge
  {
    TackBool = true;
    
    Serial.println("bestaetigt");
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
  }

  
  
  else
  {
    for(m = 0; m < 11; m++)
    {
      inStr[m] = 0;
    }
    i = 0;
    TackBool = false;
  }
}
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\funktionen.ino"
/*
 * funktionen.ino
 * Version 0.3.0.2
 */

#include <Arduino.h>
#define FREQ 0   // Speicheradresse für Array
#define LINKS 1  // Speicheradresse für Array
#define RECHTS 2 // Speicheradresse für Array

uint8_t testLINKS = true;   // Standard, es wird mit der linken Seite angefangen
uint8_t testRECHTS = false; // Standard, somit ist rechts false

extern float hearingTestSWAP[3][31];
extern float saveIteration1[3][31];
extern float saveIteration2[3][31];
//extern float saveIteration3[3][9];
extern float hearingTestErgebnis[3][31];
extern float hearingTestSAVE[5][31];
extern uint8_t IntensityChoice;

void debugModePin();
void Melodie(void);
void hearingTestSAVEinSWAP(void);
void hearingTestIntensity(void);


/******************************************************************************************************************************** 
 * Funktion:      VolumeMonoConvertANDsend()
 * DSP_ADDR:      Registeradresse des Mono Volumereglers des DSP
 * VolumeINdB:    erwartet eine Fließkommazahl der Lautstärke in dB
 * 
 * Konvertiert aus der Lautstärke in dB (-120.00dB..+10.00dB) eine 5.23 Bit Zahl und sendet an Registeradresse des Volumereglers
 *********************************************************************************************************************************/
void VolumeMonoConvertANDsend(int DSP_ADDR, float VolumeINdB) // Umrechnung von dB in 23Bit Wert und Senden an hearingTestSineVolume
{
  float VolumetoSend;
  VolumetoSend = pow(10, VolumeINdB / 20.0);                  // Volumeumrechnung -- volume2 = 10^(hearingTestvolumedB/20)
  MasterVolumeMono(DEVICE_ADDR_7bit, DSP_ADDR, VolumetoSend); // Set Volume2 to SineGen
  //return VolumetoSend;
}

/********************************************************************************************************************************
 * Funktion:      VolumeStereoConvertANDsend()
 * DSP_ADDR:      Registeradresse des Mono Volumereglers des DSP
 * VolumeINdB:    erwartet eine Fließkommazahl der Lautstärke in dB
 * 
 * Konvertiert aus der Lautstärke in dB (-120.00dB..+10.00dB) eine 5.23 Bit Zahl und sendet an Registeradresse des Volumereglers
 *********************************************************************************************************************************/
void VolumeStereoConvertANDsend(int DSP_ADDR, float VolumeINdB)
{
  float VolumetoSend;
  VolumetoSend = pow(10, VolumeINdB / 20.0);
  MasterVolumeStereo(DEVICE_ADDR_7bit, DSP_ADDR, VolumetoSend);
}

/*******************************************************************
 * int Seitenwahl()
 * 
 * Funktion, die entweder 1 für links oder 2 für Rechts zurückgibt
 * wird für die Seitenwahl des Hörtests benötigt. 
 * links Sinus, dann rechts Noise und umgekehrt
 *******************************************************************/
int Seitenwahl()
{
  int y;
  if ((testLINKS == true) && (testRECHTS == false))
  {
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für Sinus auf Links
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für NOISE auf Rechts
    y = LINKS;
    return y;
  }
  if ((testLINKS == false) && (testRECHTS == true))
  {
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für Sinus auf Rechts
    muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für NOISE auf Links
    y = RECHTS;
    return y;
  }
}

/*******************************************************************
 * int SeitenwahlOHNEnoise()
 * 
 * Funktion, die entweder 1 für links oder 2 für Rechts zurückgibt
 * wird für die Seitenwahl des Hörtests benötigt. 
 * Sinus wird auf verschiedene Seiten gegeben ohne Noise
 *******************************************************************/
int SeitenwahlOHNEnoise()
{
  if ((testLINKS == true) && (testRECHTS == false))
  {
    //muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS); // Setze MUX für Sinus auf Links
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    Melodie();
    return LINKS;
  }
  if ((testLINKS == false) && (testRECHTS == true))
  {
    //muxnoiseless(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS); // Setze MUX für Sinus auf Rechts
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS, 2);
    Melodie();
    return RECHTS;
  }
}

/****************************************************************
 * void debugModePIN()
 * Wenn PIN_DEBUG (PIN 5) LOW ist, dann aktiviere den DEBUG MODE
 ****************************************************************/
void debugModePIN()
{
  if (digitalRead(PIN_DEBUG) == LOW)
  {
    PINdebugON = true;
  }
  else
  {
    PINdebugON = false;
  }
}

/************************************************
 * void VersionsnummerAbfrage(void);
 ************************************************/
void VersionsnummerAbfrage(void)
{
  Serial.print(F("Version: "));
  Serial.print(VersionsnummerFW);
  Serial.println(VersionsnummerDSP);
  Serial.print(F("Stand: "));
  Serial.println(DatumVersion);
}

void Melodie(void)
{
  int i;
  float VolDown = -40.00;
  float VolDownStep = 5.00;
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, VolDown);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 500.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  delay(100);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 1000.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  delay(250);
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, 750.0); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  for (i = 0; i <= 10; i++)
  {
    VolDown = VolDown - VolDownStep;
    VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, VolDown);
    delay(250);
  }
  delay(500);
}

void hearingTestSAVEinSWAP(void) // Zwischenspeichern des Hörtests in hearingTestSWAP[][];
{
  int ii, jj;
  for (ii = 0; ii <= 4; ii++) //Zeilenweise
  {
    for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
    {
      hearingTestSWAP[ii][jj] = hearingTestSAVE[ii][jj];
      hearingTestErgebnis[ii][jj] = hearingTestSAVE[ii][jj];
      //hearingTestSWAP[ii][jj] = testAndi[ii][jj];
      //saveIteration1[ii][jj] = testAndi[ii][jj];
      //saveIteration2[ii][jj] = testAndi[ii][jj];
      //saveIteration3[ii][jj] = testAndi[ii][jj];
    }
  }
}

void hearingTestIntensity(void) // Zwischenspeichern des Hörtests in hearingTestSWAP[][];
{
  int ii, jj;
  for (ii = 0; ii <= 4; ii++) //Zeilenweise
  {
    for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
    {
      // Auswahl der Intensität der Personalisierung
      switch (IntensityChoice)
      {
      case 0:
        // 20%
        hearingTestSWAP[ii][jj] = 0.2 * hearingTestSAVE[ii][jj];
        break;

      case 1:
        // 40%

        hearingTestSWAP[ii][jj] = 0.4 * hearingTestSAVE[ii][jj];
        break;

      case 2:
      // 60%
      default:

        hearingTestSWAP[ii][jj] = 0.6 * hearingTestSAVE[ii][jj];
        break;

      case 3:
        // 80%

        hearingTestSWAP[ii][jj] = 0.8 * hearingTestSAVE[ii][jj];
        break;

      case 4:
        // 100%

        hearingTestSWAP[ii][jj] = hearingTestSAVE[ii][jj];
        break;
      }
    }
  }
  //choose_algo();
}

#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\headphone.ino"
/*
* headphone.ino
*
* Kopfhörerberechnung
*/


/*
#include "headphoneParameter.h"

float MittelwertBACK_HeadphoneFrequenzgang(int Seite);







float MittelwertBACK_HeadphoneFrequenzgang(int Seite)
{ 
    float MWSWAP = 0.00;
    float Mittelwert = 0.00;
    for (jj = 0; jj <= f_countMAX; jj++)
    {
        MWSWAP = MWSWAP + HeadphoneFrequenzgang[Seite][jj];
    }
    return Mittelwert = MWSWAP / (f_countMAX + 1);
}


*/
#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\hearingtest.ino"
/*
 *    hearingtest.ino
 */
#include <Arduino.h>
#include "AidaDSP.h"
extern bool fineState;
extern float headphoneGain;
uint8_t countHearingTest = 0; // Hilfsvariable für Fallunterscheidung beim Drücken des Encoders beim Hörtest
uint8_t countNoise = 0;       //Zählvariable für Fallunterscheidung beim Rauschgenerator

float sendMinVol = pow(10, hearingTestVolMINdB / 20.0);

int hearingTestfrequencySwitch;

int TestDelay = 1500; // Delay für Testfunktion
int SeiteTEST;        //
int counter = 0;
uint8_t hearingTest_state = false;

//HearingTestFunktions :
void hearingTest();
void hearingTestINIT();
void Sinus_Generator();
void hearingSinusMod();
void hearingTestPRINT();
void FrequenzAusgabe();
void hearingBubbleSort();
void HearingButton(void);
void Sinus_GeneratorMod(float, float);
void Algo1(void);

void hearingTest()
{
  fineState = true;
  Serial.println(F("hearing test 1..."));
  VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);

  Serial.println(F("Linke Seite!"));
  testLINKS = true;
  testRECHTS = false;
  hearingSinusMod();

  Serial.println(F("Rechte Seite!"));
  testLINKS = false;
  testRECHTS = true;
  hearingSinusMod();

  f_counter = 0;
  hearingBubbleSort();
  hearingTestPRINT();
}

/*******************************************************************
   void hearingTestINIT()

   Initialisierung des Hörtests - Alle Werte auf Startwerte setzen
 *******************************************************************/
void hearingTestINIT() // Hörtest initialisieren
{
  f_counter = 0;                             //Zählvariable für Frequenzwahl auf Startwert
  hearingTestvolumedB = hearingTestVolMINdB; // Startlautstärke ist Minimum
  sine_count = 0;                            // Sinuszähler auf Minimum
  volume_state = false;                      // Es wird keine Lautstärkeänderung über Encoder zugelassen
}

/*******************************************************************
   void Sinus_GeneratorMod(float dBVolumeVal, float freq)

   generate sin wave with spacific frequency at spacific volume value
 *******************************************************************/
void Sinus_GeneratorMod(float dBVolumeVal, float freq)
{
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, freq); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, dBVolumeVal);         //Schreibe aktuelle Volume in DSP
}

/*******************************************************************
   void Sinus_Generator()

   Sinusgenerator ein- und nach Pause wieder ausschalten
 *******************************************************************/
void Sinus_Generator()
{
  sine_source(DEVICE_ADDR_7bit, hearingTestSinusgeneratorAddr, f_count[f_counter]); //Frequenz aus 'f_count[]' in Sinusgenerator schreiben
  gainCell(DEVICE_ADDR_7bit, hearingTestSineGainAddr, pow(10, headphoneGain / 20.0) );      // Gain Faktor 10 linear
  VolumeMonoConvertANDsend(hearingTestSineVolumeAddr, hearingTestvolumedB);         //Schreibe aktuelle Volume in DSP
}

/*******************************************************************
   void hearingSinus()

   der eigentliche Hörtest
 *******************************************************************/

void hearingSinusMod()
{
  SeiteTEST = SeitenwahlOHNEnoise(); // Variable für Seitenwahl (1 -> links, 2 -> rechts)
  hearingTestINIT();                     // Initialisierung
  while ((f_counter <= f_countMAX))      // solange Lautstärke oder Frequenz nicht MAX. erreicht hat
  {
    FrequenzAusgabe();                 // Zuweisung des Laufindizes der richtigen Frequenz (aus Array)
    Serial.print(hearingTestvolumedB); // formatierte Ausgabe der Frequenz
    Serial.println(F(" dB"));

    String hearSide = (SeiteTEST == 1) ? "Links:" : "Rechts:";
    oledShowFreq(hearSide, f_count[f_counter], hearingTestvolumedB);

    Sinus_Generator();                                               // mache Ton an und nach einer Pause wieder aus
    while ((digitalRead(encoderSwitchPin) == HIGH && counter < 600)) // = 400ms
    {
      delay(1);
      counter++;
      //Serial.print(".");
    }
    Serial.print("\n");
    counter = 0;
    hearingTestvolumedB = hearingTestvolumedB + hearingTestStepdB;                            // inkrementiere Lautstärke um Schrittweite (Default: 5dB)
    if ((digitalRead(encoderSwitchPin) == LOW) || hearingTestvolumedB == hearingTestVolMAXdB) // Wenn mit Button bestätigt oder max. Lautstärke erreicht wird, dann speichern
    {
      // HearingButton();
      hearingTestSPL[SeiteTEST][f_counter] = VolumedBFS + hearingTestvolumedB - hearingTestStepdB;
      hearingTestHL[SeiteTEST][f_counter] = hearingTestSPL[SeiteTEST][f_counter] - hearingTestSAVE[SeiteTEST + 2][f_counter];
      hearingTestSAVE[SeiteTEST][f_counter] = hearingTestHL[SeiteTEST][f_counter];

      //hearingTestSAVE[SeiteTEST][f_counter] = (hearingTestSAVE[SeiteTEST + 2][f_counter] + hearingTestvolumedB - hearingTestStepdB); // Speichern der aktuellen Lautstärke in Array; 100 addiert, um dB[HL] zu erhalten
      Serial.println(F("gespeichert"));
      delay(50);
      f_counter++;                               // nächste Frequenz
      hearingTestvolumedB = hearingTestVolMINdB; // fange mit Minimallautstärke an
      //hearingTestvolumedB = (hearingTestSAVE[SeiteTEST + 2][f_counter] * (-1));
    }
    delay(350);
  }
  Serial.println(F("Ende!")); // Wenn Lautstärke oder Frequenz max ist, oder mit Button bestätigt wurde
}


/*
//alt hearingSinusMod()
void hearingSinusMod()
{
  SeiteTEST = SeitenwahlOHNEnoise(); // Variable für Seitenwahl (1 -> links, 2 -> rechts)
  hearingTestINIT();                     // Initialisierung
  while ((f_counter <= f_countMAX))      // solange Lautstärke oder Frequenz nicht MAX. erreicht hat
  {
    FrequenzAusgabe();                 // Zuweisung des Laufindizes der richtigen Frequenz (aus Array)
    Serial.print(hearingTestvolumedB); // formatierte Ausgabe der Frequenz
    Serial.println(F(" dB"));

    String hearSide = (SeiteTEST == 1) ? "Links:" : "Rechts:";
    oledShowFreq(hearSide, f_count[f_counter], hearingTestvolumedB);

    Sinus_Generator();                                               // mache Ton an und nach einer Pause wieder aus
    while ((digitalRead(encoderSwitchPin) == HIGH && counter < 800)) // = 400ms
    {
      delay(2);
      counter++;
      //Serial.print(".");
    }
    Serial.print("\n");
    counter = 0;
    hearingTestvolumedB = hearingTestvolumedB + hearingTestStepdB;                            // inkrementiere Lautstärke um Schrittweite (Default: 5dB)
    if ((digitalRead(encoderSwitchPin) == LOW) || hearingTestvolumedB == hearingTestVolMAXdB) // Wenn mit Button bestätigt oder max. Lautstärke erreicht wird, dann speichern
    {
     // HearingButton();
       hearingTestSAVE[SeiteTEST][f_counter] = (hearingTestSAVE[SeiteTEST + 2][f_counter] + hearingTestvolumedB - hearingTestStepdB); // Speichern der aktuellen Lautstärke in Array; 100 addiert, um dB[HL] zu erhalten
      Serial.println(F("gespeichert"));
      delay(50);
      f_counter++;                               // nächste Frequenz
      hearingTestvolumedB = hearingTestVolMINdB; // fange mit Minimallautstärke an
      //hearingTestvolumedB = (hearingTestSAVE[SeiteTEST + 2][f_counter] * (-1));
    }
    delay(350);
  }
  Serial.println(F("Ende!")); // Wenn Lautstärke oder Frequenz max ist, oder mit Button bestätigt wurde
}
*/

void hearingTestPRINT() // Ausgabe des gespeicherten Arrays
{
  if (SerialTermON == true)
  {
    int ii, jj;
    for (ii = 0; ii <= 2; ii++) //Zeilenweise
    {
      for (jj = 0; jj <= f_countMAX; jj++) //Spaltenweise
      {
        Serial.print(hearingTestSAVE[ii][jj]);
        Serial.print("\t");
      }
      Serial.println(" ");
    }
  }
}

/*******************************************************************
   void FrequenzAusgabe()

   for printing on lcdDevice
 *******************************************************************/
void FrequenzAusgabe()
{

  //TODO: conver switch to indexed printing using arrau it self
  if (SerialTermON == true)
  {
    int x = f_counter;
    
    switch (x)
    {
    case 0:
      Serial.print(F("f:    63Hz\t"));
      break;
    case 1:
      Serial.print(F("f:    125Hz\t"));
      break;
    case 2:
      Serial.print(F("f:    250Hz\t"));
      break;
    case 3:
      Serial.print(F("f:    500Hz\t"));
      break;
    case 4:
      Serial.print(F("f:    1kHz\t"));
      break;
    case 5:
      Serial.print(F("f:    2kHz\t"));
      break;
    case 6:
      Serial.print(F("f:    4kHz\t"));
      break;
    case 7:
      Serial.print(F("f:    8kHz\t"));
      break;
    case 8:
      Serial.print(F("f:   16kHz\t"));
      break;
    }
  }
}

void hearingBubbleSort()
{
  if ((SerialTermON == true) && ((debugON == true) || (PINdebugON == true)))
  {
    Serial.println(F("Vorher!"));
    hearingTestPRINT();
  }
  for (int valueLEFT = 0; valueLEFT <= (f_countMAX - 1); valueLEFT++)
  {
    for (int valueRIGHT = 0; valueRIGHT <= (f_countMAX - (valueLEFT + 1)); valueRIGHT++)
    {
      if (hearingTestSAVE[FREQ][valueRIGHT] > hearingTestSAVE[FREQ][valueRIGHT + 1])
      {
        for (int ii = 0; ii <= 4; ii++)
        {
          float SWAP1 = hearingTestSAVE[ii][valueRIGHT];
          hearingTestSAVE[ii][valueRIGHT] = hearingTestSAVE[ii][valueRIGHT + 1];
          hearingTestSAVE[ii][valueRIGHT + 1] = SWAP1;
        }
      }
    }
  }
  if ((SerialTermON == true) && ((debugON == true) || (PINdebugON == true)))
  {
    Serial.print("Sorted Array: ");
    for (int i = 0; i < 19; i++)
    {
      Serial.print(hearingTestSAVE[FREQ][i]);
      Serial.print(",");
    }
    Serial.println("");
    Serial.print("Max Number: ");
    Serial.print(hearingTestSAVE[FREQ][8]);
    Serial.println("");
    Serial.print("Min Number: ");
    Serial.print(hearingTestSAVE[FREQ][0]);
    Serial.println("");
    Serial.println(F("Nachher!"));
    hearingTestPRINT();
    delay(1000); //Make sure we have enough time to see the output before starting the demo again.
  }
}


bool Octav = false;
bool Terz = false;
void HeaingTestManuellOctav()
{
  ManualHearingTestFlag = true;
  Octav = true;
  Terz = false;
  selectedMode(0);
}

void HeaingTestManuellTerz()
{
  ManualHearingTestFlag = true;
  Terz = true;
  Octav = false;
  selectedMode(1);
}

void selectedMode(int S)
{
  if(!S)
  {
    Serial.println("Octav Mode..");
    float volumeVal = -100.00 ;
    VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);  //set the voice to the maximum
    SeiteTEST = switchDirection(1);                      //set Left
    String hearSide = (SeiteTEST == 1 ) ? "Links" : "Rechts" ;
    oledShowFreq(hearSide, f_countOctav[f_counter], -120); //shows in the oled
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    //turn on frequency 63 at -100 db
    Sinus_GeneratorMod(volumeVal,f_countOctav[0]);           //start generating the sine wave
    
    activatUserInputMode(0,101,applyOctav,showOctav,18);     //start the input mode
  }
  else
  {
    Serial.println("Terz Mode..");
    float volumeVal = -100.00 ;
    VolumeStereoConvertANDsend(MasterVolumeAddr, 0.00);  //set the voice to the maximum
    SeiteTEST = switchDirection(1);                      //set Left
    String hearSide = (SeiteTEST == 1 ) ? "Links" : "Rechts" ;
    
    oledShowFreq(hearSide, f_countTerz[f_counter],  -120);
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    Sinus_GeneratorMod(volumeVal,f_countTerz[0]);
    activatUserInputMode(0,101,applyTerz,showTerz,62);
  }
}

void applyOctav(int16_t *sel,uint8_t length)
{ 
   //mapper from 9 to 31 
  int mapper[]={5, 8, 11, 14, 17, 20, 23, 26, 29 };

  /*TODO:
  1-split long array into 2 small arrays 
  2-convert each on to propreate dB values
  3-Store each array in it's location */
  float temp[2][31];
  int k = 0 ;
  for (int j = 1; j < 3; j++)
  {
    for (int i = 0; i < 9; i++)
    {
      TODO: //Store data in hearingTestSAVE according to Index 
      //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
      temp[j][i] = (float) (*(sel + k) - 120);
      if( k < 9)
        Serial.printf("%i-freq : %.1f , templinks vol: %.1f  \n",mapper[i],f_countOctav[i], temp[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , temprechts vol: %.1f \n",mapper[i],f_countOctav[i], temp[j][i]) ;
      hearingTestSPL[j][mapper[i]] = (float) (*(sel + k) ) ;
      hearingTestHL[j][mapper[i]] = hearingTestSPL[j][mapper[i]] - hearingTestSAVE[j + 2][mapper[i]];
      hearingTestSAVE[j][mapper[i]] = hearingTestHL[j][mapper[i]];
      if( k < 9)
        Serial.printf("%i-freq : %.1f , links vol: %.1f  \n",mapper[i],f_countOctav[i], hearingTestSAVE[j][mapper[i]]) ; 
      else
        Serial.printf("%i-freq : %.1f , rechts vol: %.1f \n",mapper[i],f_countOctav[i], hearingTestSAVE[j][mapper[i]]) ;
      Serial.println("---------------------------------------------------");
      k++;
    }
  }
  k = 0;
  //nothings to hear
  MainDirectMode( );
  //or MainPersonalizedMode();
}

int seite = 1; // Strat Left
void showOctav(int16_t sel)
{
  Serial.printf("showOctav saved value: %i at index: %i \n",sel,selectedValIdx);
  if(selectedValIdx == 9)  //check right 
    seite = switchDirection(2);
  //1- scale selected value index to volume in dB
  float volumeVal = (float) sel - 120 ; 
  //2_Print on the OLED
  String hearSide = (seite == 1) ? "Links" : "Rechts" ;
  oledShowFreq(hearSide, f_countOctav[selectedValIdx], volumeVal);
  //3_send dB Value to Hardware
  
  Sinus_GeneratorMod(volumeVal,f_countOctav[selectedValIdx]);  
  
}


void applyTerz(int16_t *sel,uint8_t length)
{ 
  
  /*TODO:
  1-split long array into 2 small arrays 
  2-convert each on to propreate dB values
  3-Store each array in it's location */
  float temp[2][31];
  int k = 0 ;
  for (int j = 1; j < 3; j++)
  {
    for (int i = 0; i < 31; i++)
    {
      TODO: //Store data in hearingTestSAVE according to Index 
      //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
      temp[j][i] = (float) (*(sel + k) - 120);
      if( k < 31 )
        Serial.printf("%i-freq : %.1f , templinks vol: %.1f  \n",i,f_countOctav[i], temp[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , temprechts vol: %.1f \n",i,f_countOctav[i], temp[j][i]) ;
      hearingTestSPL[j][i] = (float) (*(sel + k) ) ;
      hearingTestHL[j][i] = hearingTestSPL[j][i] - hearingTestSAVE[j + 2][i];
      hearingTestSAVE[j][i] = hearingTestHL[j][i];
      if( k < 31 )
        Serial.printf("%i-freq : %.1f , links vol: %.1f  \n",i,f_countOctav[i], hearingTestSAVE[j][i]) ; 
      else
        Serial.printf("%i-freq : %.1f , rechts vol: %.1f \n",i,f_countOctav[i], hearingTestSAVE[j][i]) ;
      Serial.println("---------------------------------------------------");
      k++;
    }
  }
  k = 0;
  MainDirectMode( );

  // for (int i = 0; i < 31; i++)
  // {
  //   TODO: //Store data in hearingTestSAVE according to Index 
  //   //hearingTestSAVE[0][mapper[i]] = f_countOctav[i];
  //   hearingTestSAVE[1][i] = (float) (*(sel + i) -100.0);
  //   hearingTestSAVE[2][i] = (float) (*(sel + i + 30) - 100.0);
  //   Serial.printf("%i-freq : %.1f , links vol: %.1f , rechts vol: %.1f \n",i,hearingTestSAVE[0][i], hearingTestSAVE[1][i], hearingTestSAVE[2][i]) ; 
     
  // }
}

void showTerz(int16_t sel)
{
  Serial.printf("showTerz saved value: %i at index: %i \n",sel,selectedValIdx);
  if(selectedValIdx == 31)  //check right 
      seite = switchDirection(2);
  //1- scale selected value index to volume in dB
  float volumeVal = (float) sel - 120 ; 
  //2_Print on the OLED
  String hearSide = (seite == 1) ? "Links" : "Rechts" ;
  oledShowFreq(hearSide, f_countTerz[selectedValIdx], volumeVal);
  //3_send dB Value to Hardware
  
  Sinus_GeneratorMod(volumeVal,f_countTerz[selectedValIdx]); 
  
}

int switchDirection(int D)
{
  if (D == 1)
  {
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, LINKS, 2);
    testLINKS = true;
    testRECHTS = false;
    return 1;
  }
  else if(D == 2)
  {
    mux(DEVICE_ADDR_7bit, hearingTestSineSwitchAddr, RECHTS, 2);
    testLINKS = false;
    testRECHTS = true;
    return 2;
  }
}

#line 1 "c:\\Users\\Lukas\\Documents\\Git\\ProTon_Plus\\ProTon_FW\\menuParameter.ino"
/*
 *    menuParameter.ino
 * Version 0.3.0.2
 */

#include "menuLib.h"
#include <Arduino.h>

#define LIST_SIZE 7

uint8_t user1FLAG = true;
uint8_t user2FLAG = false;
uint8_t user3FLAG = false;
uint8_t linFLAG = true;
uint8_t per1FLAG = false;
uint8_t per2FLAG = false;
uint8_t per3FLAG = false;
uint8_t compFLAG = false;
uint8_t bassFLAG = false;
bool int20FLAG = false;
bool int40FLAG = false;
bool int60FLAG = true;
bool int80FLAG = false;
bool int100FLAG = false;

extern char menuItem_1[];
extern char menuItem_1_1[];
extern char menuItem_1_1_1[];
extern char menuItem_1_1_2[];
extern char menuItem_1_1_3[];
extern char menuItem_1_2[];
extern char menuItem_1_3[];
extern char menuItem_1_3_1[];
extern char menuItem_1_3_2[];
extern char menuItem_1_3_3[];

extern char menuItem_2[];
extern char menuItem_2_1[];
extern char menuItem_2_2[];
extern char menuItem_2_2_1[];
extern char menuItem_2_2_2[];
extern char menuItem_2_2_3[];
extern char menuItem_2_2_4[];
extern char menuItem_2_2_5[];

extern char menuItem_3[];
extern char menuItem_3_1[];
extern char menuItem_3_1_1[];
extern char menuItem_3_1_1_1[];
extern char menuItem_3_1_1_2[];
extern char menuItem_3_1_1_3[];
extern char menuItem_3_1_2[];
extern char menuItem_3_1_2_1[];
extern char menuItem_3_1_2_2[];
extern char menuItem_3_2[];
extern char menuItem_3_2_1[];
extern char menuItem_3_2_2[];

extern char menuItem_4[];
extern char menuItem_4_1[];
extern char menuItem_4_2[];
extern char menuItem_4_2_1[];
extern char menuItem_4_2_2[];
extern char menuItem_4_2_3[];
extern char menuItem_4_3[];
extern char menuItem_4_4[];

extern char menuItem_5[];
extern char menuItem_5_1[];
extern char menuItem_5_1_[];
extern char menuItem_5_1_1[];
extern char menuItem_5_1_1_1[];
extern char menuItem_5_1_1_2[];
extern char menuItem_5_1_1_3[];
extern char menuItem_5_1_2[];
extern char menuItem_5_1_2_1[];
extern char menuItem_5_1_3[];
extern char menuItem_5_1_3_1[];
extern char menuItem_5_1_3_2[];
extern char menuItem_5_1_3_3[];
extern char menuItem_5_2[];
extern char menuItem_5_2_1[];
extern char menuItem_5_2_1_1[];
extern char menuItem_5_2_2[];
extern char menuItem_5_2_2_1[];
extern char menuItem_5_2_2_2[];
extern char menuItem_5_2_3[];
extern char menuItem_5_2_3_1[];
extern char menuItem_5_2_3_2[];
extern char menuItem_5_2_4[];
extern char menuItem_5_2_4_1[];
extern char menuItem_5_2_4_2[];
extern char menuItem_5_2_5[];
extern char menuItem_5_2_5_1[];
extern char menuItem_5_2_5_2[];
extern char menuItem_5_2_5_3[];
extern char menuItem_6[];
extern char menuItem_7[];
extern char menuItem_7_1[];
extern char menuItem_7_2[];
extern char menuItem_7_3[];
extern char menuItem_7_4[];
extern char menuItem_7_5[];

extern uint8_t SprachAuswahl;
extern double dBVoiceVolum;
extern uint8_t IntensityChoice;
extern uint8_t headphone;

extern uint8_t SoundSelect;
//extern uint8_t restoreflagHearingTest;

//items handler singtures:
void storeProfileMemory_1(void);  //menuItem_1_1_1
void storeProfileMemory_2(void);  //menuItem_1_1_2
void storeProfileMemory_3(void);  //menuItem_1_1_3
void newProfile(void);            //menuItem_1_2
void deleteProfileMemory_1(void); //menuItem_1_3_1
void deleteProfileMemory_2(void); //menuItem_1_3_2
void deleteProfileMemory_3(void); //menuItem_1_3_3
void linear(void);                //menuItem_2_1
void activateCurve_1(void);       //menuItem_2_2_1
void activateCurve_2(void);       //menuItem_2_2_2
void activateCurve_3(void);       //menuItem_2_2_3
void Menu_3_1_1_1_Handler(void);
void Menu_3_1_1_2_Handler(void);
void Menu_3_1_1_3_Handler(void);
void Menu_3_1_2_1_Handler(void);
void Menu_3_1_2_2_Handler(void);
void compressorOn(void);  //menuItem_3_2_1
void compressorOff(void); //menuItem_3_2_2
void startHearingTest(void);  //hearingTest Automatik
void startHearingTestManulOctav(void);   //HeaingTest ManuellOctav
void startHearingTestManulTerz(void);    //HeaingTest ManuellTerz   
void Menu_4_2_1_Handler(void); //to be deleted
void Menu_4_2_2_Handler(void); //to be deleted
void Menu_4_2_3_Handler(void); //to be deleted
void Menu_5_1_2_Handler(void);
void changeLcdContrastMinimum(void); //menuItem_5_1_1     30%
void changeLcdContrastMidium(void);  //menuItem_5_1_2
void changeLcdContrastHigh(void);    //menuItem_5_1_3
void Menu_5_1_3_1_Handler(void);
void Menu_5_1_3_2_Handler(void);
void Menu_5_1_3_3_Handler(void);
void Menu_5_2_1_Handler(void);
void Menu_5_2_2_1_Handler(void);
void Menu_5_2_2_2_Handler(void);
void Menu_5_2_3_1_Handler(void);
void Menu_5_2_3_2_Handler(void);
void Menu_5_2_4_1_Handler(void);
void Menu_5_2_4_2_Handler(void);
void Menu_5_2_5_1_Handler(void);
void Menu_5_2_5_2_Handler(void);
void Menu_5_2_5_3_Handler(void);
void intensityLevel20(void);
void intensityLevel40(void);
void intensityLevel60(void);
void intensityLevel80(void);
void intensityLevel100(void);
void Soundwahl1(void);
void Soundwahl2(void);
void Soundwahl3(void);
void Soundwahl4(void);

void MultiUserInputHandler(void);
void SingleUserInputHandler(void);

/*****Laden*****/
listItem Menu_1_1[3] = {
    {menuItem_1_1_1, NULL, 0, &storeProfileMemory_1}, //Speicherplatz 1
    {menuItem_1_1_2, NULL, 0, &storeProfileMemory_2}, //Speicherplatz 2
    {menuItem_1_1_3, NULL, 0, &storeProfileMemory_3}, //Speicherplatz 3
};

/*****Löschen*****/
listItem Menu_1_3[3] = {
    {menuItem_1_3_1, NULL, 0, &deleteProfileMemory_1}, //Speicherplatz 1
    {menuItem_1_3_2, NULL, 0, &deleteProfileMemory_2}, //Speicherplatz 2
    {menuItem_1_3_3, NULL, 0, &deleteProfileMemory_3}, //Speicherplatz 3
};

/*****Benutzerprofil*****/
listItem Menu_1[3] = {
    {menuItem_1_1, Menu_1_1, 3, NULL},    //Laden
    {menuItem_1_2, NULL, 0, &newProfile}, //neues Profil
    {menuItem_1_3, Menu_1_3, 3, NULL},    //löschen
};

/*****personalisiert*****/
listItem Menu_2_2[3] = {
    {menuItem_2_2_1, NULL, 0, &activateCurve_1}, //Kurve 1
    {menuItem_2_2_2, NULL, 0, &activateCurve_2}, //Kurve 2
    {menuItem_2_2_3, NULL, 0, &activateCurve_3}, //Kurve 3
};

/****Betriebsmodi*****/
listItem Menu_2[2] = {
    {menuItem_2_1, NULL, 0, &linear},  //linear
    {menuItem_2_2, Menu_2_2, 3, NULL}, //personalisiert
};

/*****EQ******/
listItem Menu_3_1_1[3] = {
    {menuItem_3_1_1_1, NULL, 0, &Menu_3_1_1_1_Handler}, //Jazz
    {menuItem_3_1_1_2, NULL, 0, &Menu_3_1_1_2_Handler}, //Rock
    {menuItem_3_1_1_3, NULL, 0, &Menu_3_1_1_3_Handler}, //Flat
};

/*****Dynamic Bass*****/
listItem Menu_3_1_2[2] = {
    {menuItem_3_1_2_1, NULL, 0, &Menu_3_1_2_1_Handler}, //An
    {menuItem_3_1_2_2, NULL, 0, &Menu_3_1_2_2_Handler}, //Aus
};
/*****Audio Presets****/
listItem Menu_3_1[2] = {
    {menuItem_3_1_1, Menu_3_1_1, 3, NULL}, //EQ
    {menuItem_3_1_2, Menu_3_1_2, 2, NULL}, //Dynamic DynamicBassBypassAddr
};

/****Kompressor****/
listItem Menu_3_2[2] = {
    {menuItem_3_2_1, NULL, 0, &compressorOn},  //An
    {menuItem_3_2_2, NULL, 0, &compressorOff}, //Aus
};

/*****Soundeinstellungen*****/
listItem Menu_3[2] = {
    {menuItem_3_1, Menu_3_1, 2, NULL}, //Audio Presets
    {menuItem_3_2, Menu_3_2, 2, NULL}, //Kompressor
};

/****Rauschgenerator*****/
listItem Menu_4_2[3] = {
    {menuItem_4_2_1, NULL, 0, &Menu_4_2_1_Handler}, //An
    {menuItem_4_2_2, NULL, 0, &Menu_4_2_2_Handler}, //Aus
    {menuItem_4_2_3, NULL, 0, &Menu_4_2_3_Handler}, //Automatik
};
/****Hörtest*****/
listItem Menu_4[4] = {
    {menuItem_4_1, NULL, 0, &startHearingTest}, //starten
    {menuItem_4_3, NULL, 0, &startHearingTestManulOctav}, //Octav starten
    {menuItem_4_4, NULL, 0, &startHearingTestManulTerz}, //Terz starten
    {menuItem_4_2, Menu_4_2, 3, NULL},          //Rauschgenerator
};

/*****changeLcdContrast********/
listItem Menu_5_1_1[3] = {
    {menuItem_5_1_1_1, NULL, 0, &changeLcdContrastMinimum}, //schnell
    {menuItem_5_1_1_2, NULL, 0, &changeLcdContrastMidium},  //nach DirectHandler
    {menuItem_5_1_1_3, NULL, 0, &changeLcdContrastHigh},    //genau
};

/******Startlautstärke*****/
listItem Menu_5_1_3[3] = {
    {menuItem_5_1_3_1, NULL, 0, &Menu_5_1_3_1_Handler}, //Minimal
    {menuItem_5_1_3_2, NULL, 0, &Menu_5_1_3_2_Handler}, //Standard
    {menuItem_5_1_3_3, NULL, 0, &Menu_5_1_3_3_Handler}, //nach letztem Start
};

/****Dynamic Bass An AUS*****/
listItem Menu_5_2_2[2] = {
    {menuItem_5_2_2_1, NULL, 0, &Menu_5_2_2_1_Handler}, //Dynamic Bass An
    {menuItem_5_2_2_2, NULL, 0, &Menu_5_2_2_2_Handler}, //Dynamic Bass Aus
};

/*****Rauschgenerator AN AUS******/
listItem Menu_5_2_3[2] = {
    {menuItem_5_2_3_1, NULL, 0, &Menu_5_2_3_1_Handler}, //Rauschgenerator AN
    {menuItem_5_2_3_2, NULL, 0, &Menu_5_2_3_2_Handler}, //Rauschgenerator AUS
};

/*****Sprache******/
listItem Menu_5_2_4[2] = {
    {menuItem_5_2_4_1, NULL, 0, &Menu_5_2_4_1_Handler}, //Deutsch
    {menuItem_5_2_4_2, NULL, 0, &Menu_5_2_4_2_Handler}, //Englisch
};

/*****Information******/
listItem Menu_5_2_5[3] = {
    {menuItem_5_2_5_1, NULL, 0, &Menu_5_2_5_1_Handler}, //Versionsnummer
    {menuItem_5_2_5_2, NULL, 0, &Menu_5_2_5_2_Handler}, //Seriennummer
    {menuItem_5_2_5_3, NULL, 0, &Menu_5_2_5_3_Handler}, //Support
};
/****Generell******/
listItem Menu_5_1[3] = {
    {menuItem_5_1_1, Menu_5_1_1, 3, NULL},          //Hörtest
    {menuItem_5_1_2, NULL, 0, &Menu_5_1_2_Handler}, //max. Lautstärke
    {menuItem_5_1_3, Menu_5_1_3, 3, NULL},          //Startlautstärke
};
/******Expertenmodus*******/
listItem Menu_5_2[5] = {
    {menuItem_5_2_1, NULL, 0, &Menu_5_2_1_Handler}, //Schrittweite
    {menuItem_5_2_2, Menu_5_2_2, 2, NULL},          //Dynamic Bass
    {menuItem_5_2_3, Menu_5_2_3, 2, NULL},          //Rauschgenerator
    {menuItem_5_2_4, Menu_5_2_4, 2, NULL},          //Sprache
    {menuItem_5_2_5, Menu_5_2_5, 3, NULL},          //Information
};

/*****Einstellungen*******/
listItem Menu_5[2] = {
    {menuItem_5_1, Menu_5_1, 3, NULL}, //Generell
    {menuItem_5_2, Menu_5_2, 5, NULL}, //Expertenmodus
};

listItem Menu_9[5] = {
    {menuItem_9_1, NULL, 0, &intensityLevel20},  //Intensität 20%
    {menuItem_9_2, NULL, 0, &intensityLevel40},  //Intensität 40%
    {menuItem_9_3, NULL, 0, &intensityLevel60},  //Intensität 60%
    {menuItem_9_4, NULL, 0, &intensityLevel80},  //Intensität 80%
    {menuItem_9_5, NULL, 0, &intensityLevel100}, //Intensität 100%
};

listItem Menu_10[] = {
    {menuItem_10_1, NULL, 0, &Soundwahl1}, //Intensität 20%
    {menuItem_10_2, NULL, 0, &Soundwahl2}, //Intensität 40%
    {menuItem_10_3, NULL, 0, &Soundwahl3}, //Intensität 60%
    {menuItem_10_4, NULL, 0, &Soundwahl4}, //Intensität 80%
};

/*****Hauptmenue*****/
listItem mainList[LIST_SIZE] = {
    // {menuItem_8, NULL, NULL, &SingleUserInputHandler},    // Test Single UserInput
    // {menuItem_7, NULL, NULL, &MultiUserInputHandler},    // Test Multi UserInput
    //{menuItem_6, NULL, NULL, &startHearingTest}, // Hörtest starten
    {menuItem_3, Menu_3, 2, NULL},   //Soundeinstellungen
    {menuItem_9, Menu_9, 5, NULL},   // Intensität (20%, 40%, 60%, 80%, 100%)
    {menuItem_10, Menu_10, 4, NULL}, // Soundauswahl
    {menuItem_1, Menu_1, 3, NULL},   //Benutzerprofil
    {menuItem_2, Menu_2, 2, NULL},   //Betriebsmodi
    {menuItem_4, Menu_4, 4, NULL}, //Hörtest
    {menuItem_5, Menu_5, 2, NULL}, //Einstellungen
};

Menu mainMenu = Menu(mainList, LIST_SIZE, mxDeapth);

String englishLetters[26] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                             "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

void MultiUserInputHandler(void)
{
  activatUserInputMode(0, 25, selectName, showLetters, 5);
  showLetters(0);
}

void selectName(int16_t *sel, uint8_t length)
{
  char arr[length];
  Serial.println(" ============= ");
  for (uint16_t i = 0; i < length; i++)
  {
    Serial.print(englishLetters[sel[i]]);
    arr[i] = englishLetters[sel[i]].charAt(0);
  }
  Serial.println("");
  Serial.println(" ============= ");
  showSelectedItem("entered input:", String(arr));
  delay(1000);
}

void showLetters(int16_t sel)
{
  String showMsg = "";
  if (sel == 0)
  {
    showMsg = englishLetters[25] + "." + englishLetters[sel] + "." + englishLetters[sel + 1];
  }
  else if (sel == 25)
  {
    showMsg = englishLetters[sel - 1] + "." + englishLetters[sel] + "." + englishLetters[0];
  }
  else
  {
    showMsg = englishLetters[sel - 1] + "." + englishLetters[sel] + "." + englishLetters[sel + 1];
  }
  showSelectedItem("UserInput", showMsg);
  //return String(showMsg);
}

uint16_t level[5] = {20, 40, 60, 80, 100};

void SingleUserInputHandler(void)
{
  /*
   activatUserInputMode(0, 4, selectLevel, showLevel, 1);
  showLevel(0);
  */
}

void selectLevel(int16_t *sel, uint8_t length)
{
  /*
  uint16_t i = 0;
  uint16_t arr[length];
  Serial.println(" ============= ");
  for (i = 0; i < length; i++)
  {
    Serial.print(level[sel[i]]);
    arr[i] = level[sel[i]];
  }

if (i == 0)
{
   intensityLevel20();
}

else if (i == 1)
{
   intensityLevel40();
}

else if (i == 2)
{
   intensityLevel60();
}

else if (i == 3)
{
   intensityLevel80();
}

else if (i == 4)
{
   intensityLevel100();
}

  Serial.println("");
  Serial.println(" ============= ");
  showSelectedItem("entered input:", String(arr[0]));
  delay(1000);

  */
}

void showLevel(int16_t sel)
{
  /*
  String showMsg = "";
  if (sel == 0)
    showMsg = String(level[4]) + "." + String(level[sel]) + "." + String(level[sel + 1]);
  else if (sel == 4)
    showMsg = String(level[sel - 1]) + "." + String(level[sel]) + "." + String(level[0]);
  else
    showMsg = String(level[sel - 1]) + "." + String(level[sel]) + "." + String(level[sel+1]);
    
  showSelectedItem("UserInput", showMsg);
  //return String(showMsg);
  */
}

void newProfile(void) //menuItem_1_2
{
  // neues Profil
}

void storeProfileMemory_1(void) //menuItem_1_1_1
{
  //Profil laden Speicherplatz 1
  //user1FLAG = true;

  VersionsnummerAbfrage();
}

void storeProfileMemory_2(void) //menuItem_1_1_2
{
  //Profil laden Speicherplatz 2

  //user2FLAG = true;
}

void storeProfileMemory_3(void) //menuItem_1_1_3
{
  //Profil laden Speicherplatz 3
  //user3FLAG = true;
}

void deleteProfileMemory_1(void) //menuItem_1_3_1
{
  //Profil löschen Speicherplatz 1
}

void deleteProfileMemory_2(void) //menuItem_1_3_2
{
  //Profil löschen Speicherplatz 2
}

void deleteProfileMemory_3(void) //menuItem_1_3_3
{
  //Profil löschen Speicherplatz 3
}

void linear(void) //menuItem_2_1
{
  //linear
  linFLAG = true;
  per1FLAG = false;
  per2FLAG = false;
  per3FLAG = false;

  MainDirectMode();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainDynamicBassBypassOFF();
}

void activateCurve_1(void) //menuItem_2_2_1
{
  //personalisiert Kurve 1
  linFLAG = false;
  per1FLAG = true;
  per2FLAG = false;
  per3FLAG = false;

  preset = 1;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void activateCurve_2(void) //menuItem_2_2_2
{
  //personalisiert Kurve 2
  // EqConfigurations();
  linFLAG = false;
  per1FLAG = false;
  per2FLAG = true;
  per3FLAG = false;

  preset = 2;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void activateCurve_3(void) //menuItem_2_2_3
{
  //personalisiert Kurve 3
  linFLAG = false;
  per1FLAG = false;
  per2FLAG = false;
  per3FLAG = true;
  preset = 3;
  MasterVolumeStereo(DEVICE_ADDR_7bit, MasterVolumeAddr, pow(10, (dBVoiceVolum - 15.00) / 20.0));
  //MainDynamicBassBypassON();
  PersonalizedMultibandCompressorBypassCompOFF();
  MainPersonalizedMode();
}

void Menu_3_1_1_1_Handler(void)
{
  //EQ Jazz
}

void Menu_3_1_1_2_Handler(void)
{
  // EQ Rock
}

void Menu_3_1_1_3_Handler(void)
{
  //EQ Flat
}

void Menu_3_1_2_1_Handler(void)
{
  //Dynamic Bass An

  MainDynamicBassBypassON();
}

void Menu_3_1_2_2_Handler(void)
{
  //Dynamic Bass Aus

  MainDynamicBassBypassOFF();
}

void compressorOn(void)
{
  //Kompressor An

  PersonalizedMultibandCompressorBypassMultibandCompressor();
  //PersonalizedMultibandCompressorBypassRMSCompressor();
}

void compressorOff(void)
{
  // Kompressor Aus

  PersonalizedMultibandCompressorBypassCompOFF();
}

void startHearingTest(void)
{
  //Hörtest starten
  //hearingSinus();
  //hearingSinusMod();               //Modifiziert
  //restoreflagHearingTest = true;
  MainHearingTestMode();
  hearingTest();
}
void startHearingTestManulOctav(void)
{
  MainHearingTestMode();
  HeaingTestManuellOctav();
  
}
void startHearingTestManulTerz(void)
{
  MainHearingTestMode();
  HeaingTestManuellTerz();
}

void Menu_4_2_1_Handler(void)
{
  //Rauschgenerator An
}

void Menu_4_2_2_Handler(void)
{
  //Rauschgenerator Aus
}

void Menu_4_2_3_Handler(void)
{
  //Rauschgenerator Automatik
}

void Menu_5_1_2_Handler(void)
{
  //max. Lautstärke
}

void changeLcdContrastMinimum(void)
{
  //Hörtest schnell
}

void changeLcdContrastMidium(void)
{
  //Hörtest nach DIN Norm
}

void changeLcdContrastHigh(void)
{
  //Hörtest genau
}

void Menu_5_1_3_1_Handler(void)
{
  //Startlautstärke Minimal
}

void Menu_5_1_3_2_Handler(void)
{
  //Startlautstärke Standard
}

void Menu_5_1_3_3_Handler(void)
{
  //Startlautstärke nach letztem Start
}

void Menu_5_2_1_Handler(void)
{
  //Schrittweite
}

void Menu_5_2_2_1_Handler(void)
{
  //Dynamic Bass An
  MainDynamicBassBypassOFF();
}

void Menu_5_2_2_2_Handler(void)
{
  //Dynamic Bass Aus

  MainDynamicBassBypassON();
}

void Menu_5_2_3_1_Handler(void)
{
  headphone = true;
  //Rauschgenerator An
}

void Menu_5_2_3_2_Handler(void)
{
  headphone = false;
  //Rauschgenerator Aus
}

void Menu_5_2_4_1_Handler(void)
{
  //Deutsch
  SprachAuswahl = 0;
  LanguageSelect();
}

void Menu_5_2_4_2_Handler(void)
{
  //Englisch
  SprachAuswahl = 1;
  LanguageSelect();
}

void Menu_5_2_5_1_Handler(void)
{
  //Versionsnummer
  VersionsnummerAbfrage();
}

void Menu_5_2_5_2_Handler(void)
{
  //Seriennummer
}

void Menu_5_2_5_3_Handler(void)
{
  //Support
}

void intensityLevel20(void)
{
  int20FLAG = true;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 0;
  MainPersonalizedMode();
  Serial.println("Intensität 20%");
}

void intensityLevel40(void)
{
  int20FLAG = false;
  int40FLAG = true;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 1;
  MainPersonalizedMode();
  Serial.println("Intensität 40%");
}

void intensityLevel60(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = true;
  int80FLAG = false;
  int100FLAG = false;

  IntensityChoice = 2;
  MainPersonalizedMode();
  Serial.println("Intensität 60%");
}

void intensityLevel80(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = true;
  int100FLAG = false;

  IntensityChoice = 3;
  MainPersonalizedMode();
  Serial.println("Intensität 80%");
}

void intensityLevel100(void)
{
  int20FLAG = false;
  int40FLAG = false;
  int60FLAG = false;
  int80FLAG = false;
  int100FLAG = true;

  IntensityChoice = 4;
  MainPersonalizedMode();
  Serial.println("Intensität 80%");
}

void Soundwahl1(void)
{
  SoundSelect = 0;
  Serial.println("Sound1 gewählt");
}

void Soundwahl2(void)
{
  SoundSelect = 1;
  Serial.println("Sound2 gewählt");
}

void Soundwahl3(void)
{
  SoundSelect = 2;
  Serial.println("Sound3 gewählt");
}

void Soundwahl4(void)
{
  SoundSelect = 3;
  Serial.println("Sound4 gewählt");
}

/*
void DirectHandler(void)
{
  Serial.println(F("Direct"));
  muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 1, 4);
}

void ParaHandler(void)
{
  Serial.println(F("Para"));
  muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 2);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 2, 4);
}

void HearingtestHandler(void)
{
  Serial.print(" handler of mainMenu2");
  Serial.println(F("Hearing Test"));
  //muxnoiseless(DEVICE_ADDR_7bit, DynamicBassBypassAddr, 1);
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 4, 4);
  restoreflagHearingTest = true;
  hearingTest();
}
void NoisegeratorHandler(void)
{
  //Serial.print(" handler of mainMenu3");
  Serial.println(F("Noise Test"));
  mux(DEVICE_ADDR_7bit, MasterSwitchAddr, 3, 4);
  restoreflagNoise = true;
  //NoiseGenerator();
}

void subMenuItem1(void)
{
  Serial.print(" handler of subMenuItem1");
}
void subMenuItem3(void)
{
  Serial.print(" handler of subMenuItem3");
  LanguageSelect();
  Serial.println(" ");
  Serial.print(menuItem_1);
}
void subMenuItem4(void)
{
  Serial.print(" handler of subMenuItem4");
}

void sub_subMenuItem1(void)
{
  Serial.print(" handler of sub-subMenuItem1");
}
*/

