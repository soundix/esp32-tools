#ifndef parameter
#define parameter
/*
 *    parameter.ino
 * Version 0.3.0.5.3
 */

/********************************************************************************************************
 * Hier können Parameter verändert werden! Nur HIER!!!!!!!! 
 * Zusätzlich gibts hier wichtige Infos zum Projekt! 
 * Defaultwerte sind, sofern vorhanden, am Ende der Kommentare in Klammern (DEFAULT) vermerkt  
 ********************************************************************************************************/
#define VersionsnummerFW  "0.3.1.1"                           // Versionsnummer der Firmware
#define VersionsnummerDSP "1.0.0.0"                             // Versionsnummer DSP
#define DatumVersion      "22/04/19"                            // Versionsstand
#define Seriennummer      "X-001"                               // Seriennummer der Version

float hearingTestvolumedB;                                      // aktuelle Lautstärke
float headphoneGain                     = 20;            // Faktor 10.0 -> 20dB                        (10.0)
                                                                // Faktor 1.0 -> 0dB
                                                                // Faktor 15.999264 -> 24dB

float VolumedBFS                        = 120.00;               // FS - Full scale Auflösung, aus dem Datasheet des DSP entnommen
float hearingTestVolMINdB               = -120.00;              // minimale Lautstärke für den Hörtest            (-100.0)
float hearingTestVolMAXdB               = -15.00;               // maximale Lautstärke für den Hörtest            (-10.0)
int VolumeINIT                          = 100;                  // Startlautstärke nach Programmstart             (50)
float floatVolumeINIT                   = -30.00;               // Startlautstärke nach Programmstart             (-50.00)
float hearingTestStepdB                 = 5.0;                  // Schrittweite / Auflösung für den Hörtest       (5)
float StartVolumedB                     = -10.00;               // (-60.00)
int hearingTestPause1                   = 450;                  // Zeit, wie lange Ton an ist                     (250)
int hearingTestPause2                   = 550;                  // Zeit, bis nächster Ton ertönt                  (550)
int MenuPressDelayMenu                  = 4;                    // Zeit, wie lange mindestens Button gedrückt werden muss, um ins Menu zu kommen (4)
int MenuPressDelayHOME                  = 20;                   // Zeit, wie lange gedrückt werden muss, um das Menu zu verlassen (20)

uint8_t f_state;                                                // Abfrage ob Frequenz max erreicht hat
uint8_t volume_state;                                           // Abfrage ob Volume max erreicht hat
uint8_t button_state;                                           // Abfrage ob Button gedrückt wurde
int sine_count;                                                 // Zählvariable Wdh. der gleichen Lautstärke
int f_counter;                                                  // Zählvariable um Sinusgeneratoren hochzuzählen
int f_countMAX                          = 30;                    // max. Anzahl an Sinusgeneratoren                                (8)
int sine_countMAX                       = 31;                    // max. Anzahl an Sinusgeneratoren plus 1 (Arrayzählweise)        (9)
bool fineState                          = false ;
bool Ble_CONTROL_MODE_FLAG              = false;
bool MUTE_Flag                          = false;
uint8_t debugON                         = true;                 // Debugmode per Software: Debugausgabe                           (false)
uint8_t PINdebugON                      = false;                // Debugmode per Hardware: Debugausgabe                           (false)
uint8_t SerialTermON                    = true;                 // Serielle Schnittstelle -> Interface via Terminal               (true)
uint8_t lcdON                           = true;                 // Ausgabe auf LCD                                                (false)
uint8_t SprachAuswahl                   = 0;                    // 0 deutsch, 1 englisch                                          (0)
uint8_t printListStyle                  = 0;                    // 0 oder 1
int32_t SerialBAUD                      = 115200;               //Übertragungsgeschwindigkeit serielle Schnittstelle              (115200)
uint8_t lcdAddress                      = 0x21;                 // für 16x2 via PCF8574
uint8_t lcdRowsCnt                      = 2;
uint8_t lcdColsCnt                      = 16;
uint8_t IntensityChoice                 = 2;                    // Intensität der Personalisierung: 0 -> 20%, 1 -> 40%, 2 -> 60%, 3 -> 80%, 4 -> 100%   (3)
uint8_t preset                          = 0;

uint8_t headphone                       = 0;                    // Headphone rausrechnen: 1 -> ja, 0 -> nein            (1)
uint8_t SoundSelect                     = 3;                    // verschiedene Audio-Algorithmen 0, 1, 2, 3  (3)
float Algo1Toleranz                     = 5.00;                 // Tolranzband für die Vierfallunterscheidung
float Algo1NormalGrenze                 = 25.00;                // Grenze zur Schwerhörigkeit



// MesseDemonstrator
float f_count[31] = {
   20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00,
   125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 
   630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 
   3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00
    };

float f_countOctav[18] = {
   63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00,
   63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00
   };

float f_countTerz[62] = {
   20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00,
   125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 
   630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 
   3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00,
   20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00,
   125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 
   630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 
   3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00
   };

/*
float hearingTestSAVE[5][31] = {
  {20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00},   // FREQ
   {},                                                                              // LINKS
   {},                                                                              // RECHTS
   {70.00, 63.00, 50.00, 45.00, 40.00, 37.00, 30.00, 25.00, 22.00, 18.00, 18.00, 17.00, 15.00, 10.00, 7.00, 5.00, 2.00, 1.00, 1.00, 0.00, 0.00, -3.00, -3.00, -2.00, 0.00, 1.00, 3.00, 5.00, 10.00, 35.00, 80.00},            // Hoerschwelle links
   {70.00, 63.00, 50.00, 45.00, 40.00, 37.00, 30.00, 25.00, 22.00, 18.00, 18.00, 17.00, 15.00, 10.00, 7.00, 5.00, 2.00, 1.00, 1.00, 0.00, 0.00, -3.00, -3.00, -2.00, 0.00, 1.00, 3.00, 5.00, 10.00, 35.00, 80.00}             // Hoerschwelle rechts
   };
*/
/*
// Andis Profil
float hearingTestSAVE[5][31] = {
   {20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00},   // FREQ
   {15.00, 27.00, 40.00, 25.00, 35.00, 33.00, 40.00,  35.00,  33.00,  37.00,  37.00,  33.00,  35.00,  40.00,  33.00,  35.00,  43.00,   39.00,   44.00,   65.00,   65.00,   68.00,   73.00,   82.00,   75.00,   84.00,   77.00,    85.00,    90.00,    50.00,    20.00},                                                                              // LINKS
   { 5.00, 17.00, 30.00, 15.00, 30.00, 33.00, 35.00,  35.00,  28.00,  37.00,  37.00,  38.00,  30.00,  35.00,  33.00,  35.00,  43.00,   44.00,   44.00,   60.00,   65.00,   55.00,   88.00,   87.00,   80.00,   94.00,   87.00,    85.00,    90.00,    50.00,    20.00},                                                                              // RECHTS
   {70.00, 63.00, 50.00, 65.00, 40.00, 37.00, 30.00,  25.00,  22.00,  18.00,  18.00,  17.00,  15.00,  10.00,   7.00,   5.00,   2.00,    1.00,    1.00,    0.00,    0.00,   -3.00,   -3.00,   -2.00,    0.00,    1.00,    3.00,     5.00,    10.00,    35.00,    80.00},            // Hoerschwelle links
   {70.00, 63.00, 50.00, 65.00, 40.00, 37.00, 30.00,  25.00,  22.00,  18.00,  18.00,  17.00,  15.00,  10.00,   7.00,   5.00,   2.00,    1.00,    1.00,    0.00,    0.00,   -3.00,   -3.00,   -2.00,    0.00,    1.00,    3.00,     5.00,    10.00,    35.00,    80.00}             // Hoerschwelle rechts
   };
   */
   float hearingTestSAVE[5][31] = {
   {20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00},   // FREQ
   {15.00, 27.00, 40.00, 25.00, 35.00, 33.00, 40.00,  35.00,  33.00,  37.00,  37.00,  33.00,  35.00,  40.00,  33.00,  35.00,  43.00,   39.00,   44.00,   65.00,   65.00,   68.00,   73.00,   82.00,   75.00,   84.00,   77.00,    85.00,    90.00,    50.00,    20.00},                                                                              // LINKS
   { 5.00, 17.00, 30.00, 15.00, 30.00, 33.00, 35.00,  35.00,  28.00,  37.00,  37.00,  38.00,  30.00,  35.00,  33.00,  35.00,  43.00,   44.00,   44.00,   60.00,   65.00,   55.00,   88.00,   87.00,   80.00,   94.00,   87.00,    85.00,    90.00,    50.00,    20.00},                                                                              // RECHTS
   {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},            // Hoerschwelle links
   {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},             // Hoerschwelle rechts
   };


float hearingTestSPL[3][31] = {
  {20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00},   // FREQ
   {},                                                                              // LINKS
   {} 
};

float hearingTestHL[3][31] = {
  {20.00, 25.00, 31.50, 40.00, 50.00, 63.00, 80.00, 100.00, 125.00, 160.00, 200.00, 250.00, 315.00, 400.00, 500.00, 630.00, 800.00, 1000.00, 1250.00, 1600.00, 2000.00, 2500.00, 3150.00, 4000.00, 5000.00, 6300.00, 8000.00, 10000.00, 12500.00, 16000.00, 20000.00},   // FREQ
   {},                                                                              // LINKS
   {} 
};



/*
// MesseDemonstrator
float f_count[] = {
   63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00
    };


float hearingTestSAVE[5][9] = {
  {63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},   // FREQ
   {},                                                                              // LINKS
   {},                                                                              // RECHTS
   {33.00, 20.00,  10.00,   5.00,   3.00,   0.00,  -3.00,  18.00,  40.00},            // Hoerschwelle links
   {33.00, 20.00,  10.00,   5.00,   3.00,   0.00,  -3.00,  18.00,  40.00}             // Hoerschwelle rechts
   };



float hearingTestSPL[3][9] = {
  {63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},   // FREQ
   {},                                                                              // LINKS
   {} 
};

float hearingTestHL[3][9] = {
  {63.00, 125.00, 250.00, 500.00, 1000.00, 2000.00, 4000.00, 8000.00, 16000.00},   // FREQ
   {},                                                                              // LINKS
   {} 
};
*/




/*
   Pinbelegung Arduino Uno:

   ESP32 PIN      DSP-Board (ADAU1442) PIN    LCD PIN   Encoder PIN
   --------------+-----------------------------+---------+-------------
   21 (SDA)      |     SDA  45                 |   SDA   |                  // 5V ok -> Levelconverter auf dem DSP Board
   22 (SCL)      |     SCL  43                 |   SCL   |                  // 5V ok -> Levelconverter auf dem DSP Board
   GND           |     GND                     |   GND   |   GND
   xx            |     RST                     |         |                  // 5V ok -> Levelconverter auf dem DSP Board
   13 (LED Anode)|                             |         |                  // LED ist nicht wichtig.. ist nur eine Debuggingfunktion
   26            |                             |         |  CCW (links)
   25            |                             |         |  CW (rechts)
   27            |                             |         |  Button
   5             |                             |         |  5V / GND        // DEBUG Pin
   GND           |                             |         |  GND
*/

#endif