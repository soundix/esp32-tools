#include <Arduino.h>

#include "CS43131_config.h"

byte cs43131_i2c_addr = 0x30; //chip address 0x60 shifted right

byte cs43131_dat[50] = { // config for CS43131, I2S, 96kHz, 24 Bit, 32 Bit Frame; 3 Byte Register address; 2 Byte Data
                              0x0F, 0x00, 0x00, 0x00, 0x10,
                              0x0F, 0x00, 0x10, 0x00, 0x86,
                              0x02, 0x00, 0x00, 0x00, 0xA6,
                              0x01, 0x00, 0x06, 0x00, 0x00,
                              0x01, 0x00, 0x0B, 0x00, 0x02,
                              0x01, 0x00, 0x0C, 0x00, 0x04,
                              0x07, 0x00, 0x04, 0x00, 0x02,
                              0x08, 0x00, 0x00, 0x00, 0x10, //Headphone Full Scale Voltage: 0x00-> 0.5V; 0x10->1V; 0x20-> 1.41V; 0x30-> 1.732V
                              0x09, 0x00, 0x02, 0x00, 0x00,
                              0x09, 0x00, 0x03, 0x00, 0xE8
                              };

/*byte cs43131_reg[N] = {       0xRR, 0xRR, 0xRR, 0xDD, 0xDD, //3 register bytes, 2 data bytes
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD,
                                ...
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD};
                                
        0xRR = register byte
        0xDD = data byte*/

void config_cs43131(byte ic_addr, byte * data, int data_length){

    /*  This function needs the CS43131_config.h included. The format must be:
        
        byte cs43131_i2c_addr = 0x30; //chip address 0x60 shifted right

        byte cs43131_reg[N] = { 0xRR, 0xRR, 0xRR, 0xDD, 0xDD, //3 register bytes, 2 data bytes
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD,
                                ...
                                0xRR, 0xRR, 0xRR, 0xDD, 0xDD};
                                
        0xRR = register byte
        0xDD = data byte*/
    
    SoftI2C * i2c;

    i2c = new SoftI2C(SDA_PIN, SCL_PIN);

    for(int i = 0; i < data_length; i = i + 5){

      i2c->startWrite(ic_addr);
      
      i2c->write(data[i]);

      i2c->write(data[i+1]);
      
      i2c->write(data[i+2]);
      
      i2c->write(data[i+3]);
      
      i2c->write(data[i+4]);

      i2c->endWrite();

    }

    delete i2c;

}