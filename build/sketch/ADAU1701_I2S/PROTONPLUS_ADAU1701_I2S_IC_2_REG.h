/*
 * File:           C:\Users\Lukas\Documents\Git\ProTon_Plus\ProTon_FW\ADAU1701_I2S\PROTONPLUS_ADAU1701_I2S_IC_2_REG.h
 *
 * Created:        Tuesday, August 20, 2019 12:24:56 PM
 * Description:    PROTONPLUS_ADAU1701_I2S:IC 2 control register definitions.
 *
 * This software is distributed in the hope that it will be useful,
 * but is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software may only be used to program products purchased from
 * Analog Devices for incorporation by you into audio products that
 * are intended for resale to audio product end users. This software
 * may not be distributed whole or in any part to third parties.
 *
 * Copyright ©2019 Analog Devices, Inc. All rights reserved.
 */
#ifndef __PROTONPLUS_ADAU1701_I2S_IC_2_REG_H__
#define __PROTONPLUS_ADAU1701_I2S_IC_2_REG_H__


#endif
