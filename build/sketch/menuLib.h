/*
 * menuLib.h
 * Version 0.3.0.2
 */

#ifndef _menu_lib_h
#define _menu_lib_h

#include <Arduino.h>

#include "Model.h"      //here we can find all structures 

#define mxDeapth 5 



class Menu {
  public:
    Menu(listItem* mainMenu, int mnSize, int maxDepth);
    void lcdInit(uint8_t lcdAdd,uint8_t lcdCols,uint8_t lcdRows);
    listItem* printStyledList(uint8_t );
    void printLCDList(void );
    void printAllListItem(void );
    int getMenuItemsCount(void);
    int getScrollAbility(void);
    void scrol(int newIndex);
    void puch(void );
    int pop(void);
    void execute(void);
    int getMenuItemCount(void);
    int getSelectedItem();
  private:
  listItem * currentList;

  int _maximumDepth ;
  //stacks vars:
  int countStackPointer = -1;
  int* MenuItemsCountStack =0;
  int* MenuItemsselectedStack =0;
  listItem**  MenusStack =0; 
  
  int _menuStackLength = 0;
  int _currentListSize;
  int _selectedItemIndex;
  int _scrollAbility; 
  
  void _setScrollState(int state);
  listItem* _printListItemStyleArrow();
  listItem* _printListItemStyleBrackites();
  void _printDebug(String msg);
  void _createStacks();
  void _printStack();
  int _getFullListLength(listItem* testedList, int testedListSize);

};
#endif
